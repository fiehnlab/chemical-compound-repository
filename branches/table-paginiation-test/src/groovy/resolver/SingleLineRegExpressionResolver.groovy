package resolver

import pattern.PatternHelper
import types.Hit

/**
 * very simple resolver which splits the string into strings and than matches the found strings
 * User: wohlgemuth
 * Date: Feb 1, 2010
 * Time: 11:43:22 PM
 */
class SingleLineRegExpressionResolver extends SplitResolver {

  /**
   * matches the string against several regular expressions and trys to discover the compound by this method
   */
  Set<Hit> discoverCompound(String v) {
    Hit hit = new Hit()
    hit.confidence = 1.0
    hit.origin = getClass()
    hit.value = v

    Set<Hit> result = new HashSet<Hit>()

    if (v.matches(PatternHelper.STD_INCHI_PATTERN)) {

      hit.type = Hit.INCHI

      cache hit, v
      result.add(hit)
    }
    else if (v.matches(PatternHelper.STD_INCHI_KEY_PATTERN)) {
      hit.type = Hit.INCHI_KEY

      cache hit, v
      result.add(hit)
    }
    else if (v.matches(PatternHelper.CAS_PATTERN)) {
        hit.type = Hit.CAS

        cache hit, v
        result.add(hit)
      }
      else if (v.matches(PatternHelper.KEGG_PATTERN)) {
          hit.type = Hit.KEGG

          cache hit, v
          result.add(hit)
        }



    return result;
  }
}

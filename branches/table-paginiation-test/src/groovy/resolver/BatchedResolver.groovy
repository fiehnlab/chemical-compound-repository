package resolver

import types.Hit

/**
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 7:25:39 PM
 */
interface BatchedResolver extends Resolveable {

  /**
   * add a batch to the list
   */
  void addBatch (String toResolve)

  /**
   * resolves all the content
   */
  Set<Hit> resolve()


   /**
    * do we enabled batching by default
    */
  void setBatchEnabledByDefault(boolean value)

}

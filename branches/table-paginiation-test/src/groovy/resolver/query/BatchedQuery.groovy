package resolver.query

import types.Hit

/**
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 8:05:53 PM
 */
public interface BatchedQuery extends Query{

  /**
   * adds a query to the batch of queries
   */

  public void addQuery(String query)


   /**
    * executes a query 
    */
  public Set<Hit> executeQuery()

}
package resolver.query

import types.Hit

/**
 * simple query interface
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 7:05:55 PM
 */
public interface Query {

  /**
   * execute a simple query against the database for the given implementation
   */
  public Set<Hit> executeQuery(String value);
  
}
import pattern.PatternHelper

class SearchService {

  //association to the lookup service
  LookupService lookupService

  boolean transactional = true

  /**
   * executes a generic search against the datbase dependend of regular expressions
   */
  def searchGeneric(String value) {

    if (value.matches(PatternHelper.STD_INCHI_KEY_PATTERN))
      return lookupService.lookupByInchiKey(value)
    else if (value.matches(PatternHelper.STD_INCHI_PATTERN))
      return lookupService.lookupByInchi(value)
    else if (value.matches(PatternHelper.CAS_PATTERN))
        return lookupService.lookupByCas(value)
      else if (value.matches(PatternHelper.KEGG_PATTERN))
          return lookupService.lookupByKegg(value)
        else if (value.matches(PatternHelper.LIPID_MAPS_PATTERN))
            return lookupService.lookupByLipidMapId(value)
          else if (value.matches(PatternHelper.HMDB_PATTERN))
              return lookupService.lookupByHMDB(value)
            else if (value.matches(PatternHelper.SID_PATTERN))
                return lookupService.lookupBySID(Integer.parseInt(value.split(":")[1]))
              else if (value.matches(PatternHelper.CID_PATTERN))
                  return lookupService.lookupByCID(Integer.parseInt(value.split(":")[1]))


                
                else
                  return lookupService.lookupByName(value)
  }

}

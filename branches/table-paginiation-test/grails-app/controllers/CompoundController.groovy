

class CompoundController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ compoundInstanceList: Compound.list( params ), compoundInstanceTotal: Compound.count() ]
    }

    def show = {
        def compoundInstance = Compound.get( params.id )

      System.out.println("=============================");

      System.out.println("From : " + params.from);
      System.out.println("to : " + params.to);

      System.out.println("=============================");

        if(!compoundInstance) {
            flash.message = "Compound not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ compoundInstance : compoundInstance ] }
    }
}

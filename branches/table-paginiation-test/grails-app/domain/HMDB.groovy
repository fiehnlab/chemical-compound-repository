class HMDB extends DBLinks {

  static constraints = {
    description(maxSize: 15000, nullable: true)
  }

  String hmdbId

  String description

  String toString() {
    return "HMDB: ${hmdbId}";
  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
}

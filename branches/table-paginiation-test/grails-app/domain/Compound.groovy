/**
 * a simple compound
 */
class Compound {


  Date dateCreated

  Date lastUpdated

  static hasMany = [
          synonyms: Synonym,
          dbLinks: DBLinks,
          smiles: Smiles,
          iupac: IUPAC
  ]

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
    synonyms: batchSize:30
    smiles: batchSize:2
    iupac: batchSize:5
    dbLinks: batchSize:20
  }
  static constraints = {
    inchi(maxSize: 15000, nullable: false, unique: true)
    formula(maxSize: 5000, nullable: true)
    molareMass(nullable: true)
    exactMolareMass(nullable: true)
  }

  static indexes = {
    inchiIndex('inchi')
    formulaIndex('formula')
  }

  static belongsTo = InchiHashKey

  InchiHashKey inchiHashKey

  String inchi

  String formula

  double molareMass

  double exactMolareMass

  String toString() {
    return "Compound: ${id} - ${inchi}"
  }
}

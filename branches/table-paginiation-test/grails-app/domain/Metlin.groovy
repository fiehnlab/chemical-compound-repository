class Metlin extends DBLinks {

  static constraints = {
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String metlinId

  String toString() {
    return "Metlin: ${metlinId}"
  }
}

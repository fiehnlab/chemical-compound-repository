<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 12:01:05 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><title>Home Page</title></head>

<style type="text/css">@import url("/compound-repository/css/WebGUIStandards.css");</style>


  <body topMargin=0 background="/compound-repository/images/FormBG.gif" >
 <br>
<table id='tabTitle' border='0' width="60%" align='center' cellpadding="0" cellspacing="0">
	<tr align='left' valign='bottom'>
		<td class="AppsLabel" align="center">
        <h2 style="COLOR: #0B3861; FONT-FAMILY: 'Verdana';TEXT-TRANSFORM: capitalize">
          Chemical Compound Repository
        <h2>
        </td>
	</tr>

<tr align='left' valign='bottom'>
		<td class="AppsLabel" align="center"> &nbsp; </td>
	</tr>


<tr align='left' valign='bottom'>
		<td align="center" >

<table border="0" id="tblSample" width="100%" align='center' cellpadding="0" cellspacing="0" class="TableBG">

  <tr><td height="1%"> &nbsp; </td></tr>
  
  <tr>
    	<td width='1%'> &nbsp; </td>
        <td width="10%">
			<g:link controller="search">
				      <img border="#ffffff" src="/compound-repository/images/Search.png" width="50" height="50" title='Search'>
			</g:link>
        </td>
        <td width='1%'> &nbsp; </td>
        <td valign="middle"> <g:link controller="search"> <h1 title="Search">Search</h1> </g:link> </td>
        <td width='1%'> &nbsp; </td>
	</tr>
          <tr><td height="1%"> &nbsp; </td></tr>
	<tr>
    	<td width='1%'> &nbsp; </td>
        <td width="10%">
			<g:link controller="discovery">
				      <img border="#ffffff" src="/compound-repository/images/advanceSearch.png" width="50" height="50" title='Discovery'>
			</g:link>
        </td>
        <td width='1%'> &nbsp; </td>
        <td valign="middle"> <g:link controller="discovery"> <h1 title="Discovery">Discovery</h1> </g:link> </td>
        <td width='1%'> &nbsp; </td>
	</tr>
          <tr><td height="1%"> &nbsp; </td></tr>
	<tr>
    	<td width='1%'> &nbsp; </td>
        <td width="10%">
			<g:link controller="transform">
				      <img border="#ffffff" src="/compound-repository/images/refresh.png" width="30" height="30" title='Transform'>
			</g:link>
        </td>
        <td width='1%'> &nbsp; </td>
        <td valign="middle"> <g:link controller="transform"> <h1 title="Transform">Transform<h1> </g:link> </td>
        <td width='1%'> &nbsp; </td>
	</tr>
       <tr><td height="1%"> &nbsp; </td></tr>
</table>
</td>
</tr>
</table>
  </body>
</html>
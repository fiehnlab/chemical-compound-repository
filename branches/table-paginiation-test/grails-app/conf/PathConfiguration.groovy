/**
 * User: wohlgemuth
 * Date: Jan 12, 2010
 * Time: 3:13:49 AM
 *
 * general configuraiton where the data are stored
 * and how to access them
 */

//to make sure we can overwrite it from the command line
String tmpDir = System.getProperty("java.io.tmpdir")

if(System.getProperty("compound.tmp") != null){
  tmpDir = System.getProperty("compound.tmp")
}


//where are our files located
files {
  pubchem {
    compound = "entries/pubchem/"
    substance = "entries/pubchem-substance/"
  }

  kegg {
    rawdata = "entries/kegg"
    mols = "entries/kegg-mol-files"
  }

  ncidb {
    rawdata = "entries/ncidb"
  }

  lipidmaps {
    rawdata = "entries/lipidmaps"
  }

  hmdb {
    rawdata = "entries/hmdb"
    sdf = "entries/hmdb-sdf"
  }

  chebi {
    rawdata = "entries/chebi"
  }
}

//how can we access binbase
binbase {
  server = "eros.fiehnlab.ucdavis.edu"
  datbases = ["rtx5", "volatile"]
}

//our temp directory for file operations
outputDirectory = "${tmpDir }/output"

//do we want to rename imported files
renameImportedFiles = false

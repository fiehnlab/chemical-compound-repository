dataSource {
  pooled = true
  driverClassName = "org.hsqldb.jdbcDriver"
//    driverClassName = "com.p6spy.engine.spy.P6SpyDriver" // use this driver to enable p6spy logging
  username = "sa"
  password = ""
}
hibernate {
  cache.use_second_level_cache = true
  cache.use_query_cache = true
  cache.provider_class = 'com.opensymphony.oscache.hibernate.OSCacheProvider'
}
// environment specific settings
environments {
  development {
    dataSource {
      dbCreate = "update"
      url = "jdbc:postgresql://bbtest.fiehnlab.ucdavis.edu:5432/compound-repository-devel"
//      driverClassName = "org.postgresql.Driver"
      driverClassName = "com.p6spy.engine.spy.P6SpyDriver" // use this driver to enable p6spy logging
      username = "compound"
      password = "asdf"
    }
  }
  test {

    dataSource {
      dbCreate = "update"
      url = "jdbc:postgresql://bbtest.fiehnlab.ucdavis.edu:5432/compound-repository-devel"
      driverClassName = "org.postgresql.Driver"
      username = "compound"
      password = "asdf"
      loggingSql = true
    }
  }
  production {
    dataSource {
      dbCreate = "update"
      url = "jdbc:postgresql://uranus.fiehnlab.ucdavis.edu:5432/compound-repository"
      driverClassName = "org.postgresql.Driver"
      username = "compound"
      password = "asdf"
    }
  }
}

includeTargets << grailsScript("Init")
includeTargets << new File("scripts/RunScript.groovy")

target(main: "The description of the script goes here!") {
  args = "builder/importChebi.groovy"

  //execute the script
  depends(runScript)
}

setDefaultTarget(main)

import grails.test.GrailsUnitTestCase
import types.Hit

class LookupServiceTests extends GrailsUnitTestCase {
  LookupService service = null


  protected void setUp() {
    service = new LookupService()
    super.setUp()
  }

  protected void tearDown() {
    super.tearDown()
    service = null
  }

  void testSomething() {

  }


  void testLookupByPartialInchiKey() {

    String key = "AAAFZMYJJHWUPN"
    def result = service.lookupByPartialInchiKey(key)

    assertTrue(result.size() == 2)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookupByInchiKey() {

    String key = "AAAFZMYJJHWUPN-TXICZTDVSA-N"
    def result = service.lookupByInchiKey(key)

    assertTrue(result instanceof Compound)

    assertTrue(result.inchiHashKey.completeKey == key)

    assertTrue(result.inchi == "InChI=1S/C5H12O11P2/c6-3-2(1-14-17(8,9)10)15-5(4(3)7)16-18(11,12)13/h2-7H,1H2,(H2,8,9,10)(H2,11,12,13)/t2-,3-,4-,5-/m1/s1")

  }


  void testLookupByInchi() {

    String key = "InChI=1S/C5H12O11P2/c6-3-2(1-14-17(8,9)10)15-5(4(3)7)16-18(11,12)13/h2-7H,1H2,(H2,8,9,10)(H2,11,12,13)/t2-,3-,4-,5-/m1/s1"
    def result = service.lookupByInchi(key)

    assertTrue(result instanceof Compound)

    assertTrue(result.inchi == "InChI=1S/C5H12O11P2/c6-3-2(1-14-17(8,9)10)15-5(4(3)7)16-18(11,12)13/h2-7H,1H2,(H2,8,9,10)(H2,11,12,13)/t2-,3-,4-,5-/m1/s1")

  }


  void testLookupByCompoundId() {
    LookupService service = new LookupService()

    long key = 281705
    def result = service.lookupByCompoundId(key)

    assertTrue(result instanceof Compound)

    assertTrue(result.inchi == "InChI=1S/C5H12O11P2/c6-3-2(1-14-17(8,9)10)15-5(4(3)7)16-18(11,12)13/h2-7H,1H2,(H2,8,9,10)(H2,11,12,13)/t2-,3-,4-,5-/m1/s1")

  }


  void testLookupByKegg() {

    String key = "C01151"
    def result = service.lookupByKegg(key)

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 281705)
    }
  }

  void testLookupByCas() {

    String key = "dadasdasdas"
    def result = service.lookupByCas(key)

    assertTrue(result.size() == 0)

    key = "553-97-9"
    result = service.lookupByCas(key)

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 308738)
    }

  }

  void testLookupByCID() {

    int key = 1840
    def result = service.lookupByCID(key)


    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 307010)
    }

  }

  void testLookupBySID() {

    int key = 85291312
    def result = service.lookupBySID(key)


    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 275456)
    }
  }


  void testLookupBySmile() {

    String key = "CC(CN)O"
    def result = service.lookupBySmile(key)


    assertTrue(result.size() == 2)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 271693)
    }
  }

  void testLookupByExactMass() {

    double key = 626.491
    def result = service.lookupByExactMass(key)


    assertTrue(result.size() == 3)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }



  void testLookupByFormula() {

    String key = "C40H66O5"
    def result = service.lookupByFormula(key)


    assertTrue(result.size() == 3)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookUpByName() {

    def result = service.lookupByName("benzene")

    assertTrue(result.size() == 1)

  }


  void testLookUpByHMDB() {

    def result = service.lookupByHMDB("HMDB06571")

    assertTrue(result.size() == 1)

  }



  void testLookUpByLipidMap() {

    def result = service.lookupByLipidMapId("LMFA01050367")

    assertTrue(result.size() == 1)

  }



  void testLookUpByChebi() {

    def result = service.lookupByChebiId("50599")

    assertTrue(result.size() == 1)

  }

  void testLookUpByCas() {

    def result = service.lookupByCas("121-66-4")

    assertTrue(result.size() == 1)

  }

  void testLookUpBySid() {

    def result = service.lookupBySID("14711308")

    assertTrue(result.size() == 1)

  }

  void testLookUpByCid() {

    def result = service.lookupByCID("5142")

    assertTrue(result.size() == 1)
  }

  void testLookUpByHitCas()
    {

      Hit hit = new Hit()
      hit.type = Hit.CAS
      hit.value = "492-62-6"
      
      def result = service.lookupByHit(hit)

      assertTrue(result.size() == 1)

    }


}

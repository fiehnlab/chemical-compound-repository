<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title><g.message code="TRANSFORM.RESULTS"/></title>
<export:resource />
</head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="contents">

    <div id="main">

      <!--
<table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9" class="AppsLabel" align="center"> <g.message code="TRANSFORM.RESULTS" /> </td>
	</tr>

    <tr>
		<td colspan="9" align='right'>
                  [ <g:link controller="transform" action="index" params="[from:from,to:to]"><g:message code="TRANSFORM.BACK" /></g:link> ]
				  [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
	    </td>
	</tr>

  <tr height='2'> <td> &nbsp; </td> </tr>
</table>
-->

      <table  border='0' width="100%" align='center'>
  <tr>
  <td>
      <div id="pagePath">
        <h1><g:link controller="homePage" action="index">Home</g:link> <span>&raquo;</span> <g:link controller="transform" params="[idValue:idValue,from:from,to:to]" action="index"><g:message code="TRANSFORM" /></g:link> <span>&raquo;</span> <g:message code="TRANSFORM.RESULTS" /></h1>
      </div>
      <div class="line"></div>
    <div >&nbsp;</div>

<table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >

  <tr>
    <td colspan="9">
      <table class="TableBG" width="100%">
      <tr>
          <td width="1%"> &nbsp; </td>
          <td width="2%" valign='top'> Id : </td>
          <td width="30%" class="Label3" align="left" valign='top'>${idValue}</td>
          <td width="1%"> &nbsp; </td>
          <td width="3%" valign='top' >From : </td>
          <td width="15%" class="Label3" align='left' valign='top'><g:message code="${from}"/></td>
          <td width="1%"> &nbsp; </td>
          <td width="2%" valign='top'> To :</td>
          <td width="10%" class="Label3" align='left' valign='top'><g:message code="${to}"/></td>
          <td width="30%"> &nbsp; </td>
      </tr>
      </table>
    </td>
  </tr>



  <tr>
    <td colspan="9">
  <table border="0" id="tblSample1" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >
  <tr align='left' valign='bottom' class="TableHeaderBG">
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="30%"> <g:message code="LIST.COMPOUND_NAME" /> </td>
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="20%"> <g:message code="LIST.INCHI_HASH_KEY" /> </td>
		<td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="40%"> <g:message code="LIST.TRANSLATIONS" /> </td>
        <td class="TableHeader" width="1%">&nbsp; </td>
		<td class="TableHeader" width="5%"> <g:message code="LIST.STRUCTURE" /> </td>
		<td class="TableHeader" width="1%">&nbsp;</td>
	</tr>


  <%
  if( transform!=null && transform.size() > 0 )
  {
    int liCnt=0;
    String lStrClass = "";
    String lColorFormulaBG = "";
    DataExportService dataExportService = new DataExportService();
    for(Map map : transform)
    {
      Compound compound = map.compound

      if( compound==null)
         continue;

      if( liCnt++ % 2 == 0 )
      {
        lStrClass="TableRowBG2";
        lColorFormulaBG = "#FFFFFD";
      }
    else
      {
         lStrClass = "TableRowBG1";
         lColorFormulaBG = "#DBE8F6";
      }

    def lResult = map.result
    String str = dataExportService.splitPreeFix(to,lResult)
    
      %>
  <tr class='<%=lStrClass%>'>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left">${compound.getDefaultName()}</td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left"><g:link action="show" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left">${str?.encodeAsHTML()}</td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left"><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}" /></td>
  <td class="Label3" width="1%">&nbsp;</td>
     </tr>
      <%
    }
 }else
   {
     %>
    <tr>
        <td class="Label3" width="1%">&nbsp;</td>
        <td class="Label3" colspan="7">

          <table border=0 align ="center" width="100%" BGCOLOR="WHITE" >
         <tr> <td height='20' > </td> </tr>

			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr>
			   <td align='center' height='70' valign="middle" > <font color='red' > No Compound Found... </font> </td>
			</tr>
			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr> <td height='20' > </td> </tr>
        </table>

        </td>
        <td class="Label3" width="1%">&nbsp;</td>
     </tr>
 <%   }
  %>
      </table>
    </td>
  </tr>

 <%
  if( transform!=null && transform.size() > 0 )
  {
    %>
  <tr>
       <td colspan="9">
               <g:customPaginate action="transform" controller="transform" params="[idValue:idValue,from:from,to:to,total:total]" total="${total}" prev="&lt; previous" next="next &gt;"/>
       </td>
  </tr>
  <tr>
       <td colspan="9"height="30px">
         <export:formats action="transform" controller="transform" formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']" params="[idValue:idValue,from:params.from,to:params.to]" />
        </td>
  </tr>
  <% } %>

</table>
      </tr>
</table>
</div>

<div id="footer">
  <g:render template="/footer" />
</div>

</div>
</body>
</html>
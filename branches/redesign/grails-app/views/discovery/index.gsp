<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 2, 2010
  Time: 2:40:38 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Discovery</title>
</head>
<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>
<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="shadow">
<div id="contents">


<g:form name="discoveryForm" controller="discovery">

  <div id="contents">

  <div id="main">

<table width="1000px"  border="0" align="center" height="100%">
  <tr>
    <td>
      <table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
       <tr align='center' valign='bottom'>
          <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-SIZE: 21px;FONT-FAMILY: 'Verdana';TEXT-TRANSFORM: capitalize">

              <g:message code="PROJECT_TITLE" />

          </td>
        </tr>
        <tr>

          <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-FAMILY: 'Verdana';">
              <g:message code="PROTOTYPE_MSG" args='["${session.getAttribute("PrototypeSize")}"]' />
          </td>
        </tr>
      <!--  <tr>
            <td  align='right' width="100%">
                      [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
            </td>
        </tr> -->
     </table>

    </td>
  </tr>

   <tr>
     <td>
       
       <div id="pagePath">
			<h1><g:link controller="homePage" action="index">Home</g:link> <span>&raquo;</span> <g:message code="DISCOVER" /></h1>
		</div>
          <div class="line"></div>

          <table id='tabTitle' border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
            <tr height='2'>
             <td width="1%" colspan="4"> &nbsp;</td>
             <td align='left'> &nbsp;
              <g:if test="${flash.message}">
               <div class="AlertLabel">
                  ${flash.message}
               </div>
              </g:if>
             </td> </tr>
             <td width="1%"> &nbsp;</td>
           <tr>
              <td width="1%"> &nbsp;</td>
              <td width="10%" valign="top"> Query </td>
              <td width="5%"  valign="top"> : </td>
              <td width="1%"> &nbsp;</td>
              <td width="60%">
                <g:textArea tabindex='1' name="query" value="${query?query:''}" rows="12" cols="110"></g:textArea>
              </td>
              <td width="1%"> &nbsp;</td>
            </tr>

          <tr height='2'> <td> &nbsp; </td> </tr>

            <tr>
              <td width="1%"> &nbsp;</td>
              <td colspan="4" align="center">
                <g:actionSubmit tabindex='2' value="Submit"  action="process"/>
              </td>
              <td width="1%"> &nbsp;</td>
            </tr>


            <tr>
            <td width="1%"> &nbsp; </td>
            <td colspan="4" align="left" >
              <table>
                <tr>
                  <td width="5%">
                       Tips:
                  </td>
                  <td>
                    &nbsp;
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td class="LableTips" align="left">
                       Enter text content like<br>
                       Hello benzene world, you are full of vitamin a
                  </td>
                </tr>
              </table>
            </td>
            <td width="1%"> &nbsp; </td>
          </tr>
         </table>
     </td>
   </tr>
</table>
</div>
<div id="footer">
  <g:render template="/footer" />
</div>
  </div>
</g:form>
</body>
</html>
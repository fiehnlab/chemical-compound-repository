<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Mar 18, 2010
  Time: 11:53:12 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><title><g:message code="MASS_LOOKUP" /></title></head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >
<g:form name="masslookup" controller="massLookup">

<div id="contents">

  <div id="main">

<table width="1000px"  border="0" align="center" height="100%">
  <tr>
    <td>
      <table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
       <tr align='center' valign='bottom'>
          <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-SIZE: 21px;FONT-FAMILY: 'Verdana';TEXT-TRANSFORM: capitalize">

              <g:message code="PROJECT_TITLE" />

          </td>
        </tr>
        <tr>

          <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-FAMILY: 'Verdana';">
              <g:message code="PROTOTYPE_MSG" args='["${session.getAttribute("PrototypeSize")}"]' />
          </td>
        </tr>
      <!--  <tr>
            <td  align='right' width="100%">
                      [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
            </td>
        </tr>      -->
     </table>

    </td>
  </tr>

   <tr>
     <td>
      <div id="pagePath">
			<h1><g:link controller="homePage" action="index">Home</g:link> <span>&raquo;</span> <g:message code="UTIL_APPS" /></h1>
		</div>

<div class="line"></div>
      <table width="100%" >
         <tr>
              <td >
               <table border=0 align ="center" width="98%" class="TableBG">
               <tr> <td height='20'  > </td> </tr>

                  <tr bgcolor="Gray">
                     <td height='2' align='center' valign="middle" ></td>
                  </tr>
                  <tr>
                     <td align='center' height='70' valign="middle" > <font color='blue' > Under Construction </font> </td>
                  </tr>
                  <tr bgcolor="Gray">
                     <td height='2' align='center' valign="middle" ></td>
                  </tr>
                  <tr> <td height='20' > </td> </tr>
              </table>
              </td>
           </tr>
     </table>
</td>
   </tr>
</table>
</div>
<div id="footer">
  <g:render template="/footer" />
</div>
  </div>

 </g:form>
  </body>

</html>
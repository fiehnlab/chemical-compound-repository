<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 22, 2010
  Time: 12:22:19 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="org.springframework.util.ClassUtils" contentType="text/html;charset=UTF-8" %>
<html>
<head><title><g:message code="MASS_LOOKUP.RESULTS"/></title>
<export:resource />
</head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="contents">


<div id="main">

  <!--
<table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">

    <tr>
		<td colspan="9" class="AppsLabel" align="center"> <g:message code="MASS_LOOKUP.RESULTS"/> </td>
	</tr>

    <tr>
		<td colspan="9" align='right'>
                  [ <g:link controller="massLookup" action="index" params="[from:from,to:to]"><g:message code="MASS_LOOKUP.BACK" /></g:link> ]
				  [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
	    </td>
	</tr>

  <tr height='2'> <td> &nbsp; </td> </tr>
</table>
 -->

<table  border='0' width="100%" align='center'>
  <tr>
  <td>
      <div id="pagePath">
        <h1><g:link controller="homePage" action="index">Home</g:link> <span>&raquo;</span> <g:link controller="massLookup" params="[from:from,to:to]" action="index"><g:message code="MASS_LOOKUP" /></g:link> <span>&raquo;</span> <g:message code="MASS_LOOKUP.RESULTS" /></h1>
      </div>
      <div class="line"></div>
    <div >&nbsp;</div>
<table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG" >

  <tr>
    <td colspan="9">
      <table class="TableBG" width="100%">
      <tr>
          <td width="1%"> &nbsp; </td>
          <td width="3%" valign="top"> Ids  </td>
          <td width="1%" valign="top"> : </td>
          <td width="50%" colspan="6" class="Label3" align="left" valign="top">

          <% if( idsList!=null && idsList.size() > 0 )
              {
                for(int liCnt=0; liCnt < 10 && liCnt < idsList.size(); liCnt++)
                  {

                     if( liCnt < idsList.size()-1 )
                     {
                %>
                        ${idsList.get(liCnt)+",&nbsp;"}
                <%   } else {%>
                        ${idsList.get(liCnt)}
              <%    }
                  }

                if( idsList.size() > 10 )
                    { %>
                    ${"..."}
                  <%
                    }
                  }%>

          </td>
          <td width="1%"> &nbsp; </td>
      </tr>
        <tr>
          <td width="1%"> &nbsp; </td>
          <td width="3%" valign="top">From </td>
          <td width="1%" valign="top"> : </td>
          <td width="15%" class="Label3" valign="top"> <g:message code="${from}"/></td>
          <td width="1%"> &nbsp; </td>
          <td width="3%" valign="top"> To</td>
          <td width="1%" valign="top"> :</td>
          <td width="70%" class="Label3" align="left" valign="top">
              <% if( to!=null && to.size() > 0 )
                  {
                    for(int liCnt=0; liCnt < to.size(); liCnt++)
                      {

                         if( liCnt < to.size()-1 )
                         {
                    %>
                            <g:message code="${to.get(liCnt)}" /> ${",&nbsp;"}
                    <%   } else {%>
                            <g:message code="${to.get(liCnt)}" />
                  <%    }
                      }
                    }%>
          </td>
          <td width="15%"> &nbsp; </td>
      </tr>
      </table>
    </td>
  </tr>

  <tr>
  <td colspan="9">
  <table border="0" id="tblSample1" width="100%" align='center' cellpadding="3" cellspacing="1" class="TableBG" >
   <tr align='left' valign='bottom' class="TableHeaderBG">

        <td class="TableHeader" width="10%" rowspan="2"> <g:message code="LIST.COMPOUND_NAME" /> </td>

        <td class="TableHeader" width="10%" rowspan="2"> <g:message code="LIST.INCHI_HASH_KEY" /> </td>

		<td class="TableHeader" width="70%" colspan="${to.size}" align="center"> <g:message code="LIST.TRANSLATIONS" /> </td>

        <td class="TableHeader" width="5%" rowspan="2"> <g:message code="LIST.STRUCTURE" /> </td>
	</tr>

    <tr align='left' valign='bottom' class="TableHeaderBG">
    <% if( to!=null && to.size() > 0 )
      {
        for(int liCnt=0; liCnt < to.size(); liCnt++)
        {
	%>
            <td class="TableHeader" width="10%"> <g:message code="${to.get(liCnt)}" /> </td>
	<%    }
       } %>
    </tr>


  <%
  if( transform!=null && transform.size() > 0 )
  {
    DataExportService dataExportService = new DataExportService();
    %>
       <g:each var="map" in="${transform}" status="index">

                          <%
                            Compound compound = map.compound

                            String lStrClass = "";
                            String lColorFormulaBG = "";

                            if (index % 2 == 0) {
                              lStrClass = "TableRowBG2"
                              lColorFormulaBG = "#FFFFFD"
                            }else {
                              lStrClass = "TableRowBG1"
                              lColorFormulaBG = "#DBE8F6"
                            }

                          %>

  <tr class='<%=lStrClass%>' onMouseOver="this.bgColor='yellow';">

    <td class="Label3" valign="top" align="left" width="10%">${compound.getDefaultName()}</td>
    <td class="Label3" valign="top" align="left" width="10%"><g:link action="show" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>

    <%
     if( to!=null && to.size() > 0 )
     {
        HashMap lMap = (map.result);
        
        for(int liCnt1=0; liCnt1 < to.size(); liCnt1++)
        {
            String keyname = to.get(liCnt1)
            def lResult =  lMap.get(keyname.toString().trim())
            String str = dataExportService.splitPreeFix(keyname,lResult)
	%>
    <td class="Label3" valign="top" align="left" width="10%">${str?.encodeAsHTML()}</td>
	<%   }
       }
    %>

      <td class="Label3" valign="top" align="left" width="10%"><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}" /></td>

     </tr>

</g:each>

    <%
 }else
   {
     %>
    <tr>
        <td class="Label3" colspan="${3+to.size}">

         <table border=0 align ="center" width="98%" class="TableBG">
         <tr> <td height='20'  > </td> </tr>

			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr>
			   <td align='center' height='70' valign="middle" > <font color='red' > No Compound Found... </font> </td>
			</tr>
			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr> <td height='20' > </td> </tr>
        </table>

        </td>

     </tr>
 <%   }
  %>
      </table>
    </td>
  </tr>

  <%
  if( transform!=null && transform.size() > 0 )
  {
    %>
  <tr>
       <td colspan="9">
             <g:customPaginate controller="massLookup" action="masstransform" params="[from:from,to:to,total:total]" total="${total}" prev="&lt; previous" next="next &gt;"/>
       </td>
  </tr>


  <tr>
       <td colspan="9"height="30px">
          <export:formats controller="massLookup" action="masstransform" formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']" params="[from:params.from,to:params.to]" />
        </td>
  </tr>

  <% } %>

</table>
 </td>
</tr>
</table>
</div>

<div id="footer">
  <g:render template="/footer" />
</div>

</div>

</body>
</html>
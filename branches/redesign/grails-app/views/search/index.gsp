<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 4:00:39 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><title>Search</title></head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>
 <style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'layout.css')}");</style>
<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="shadow">
  <div id="contents">

    <div id="location" >
			<table align='center' width="100%">
              <tr>
                  <td class="AppsLabel" align="center"> <g:message code="SEARCH" /> </td>
              </tr>
              <tr>
                    <td class="TableHeaderDescription" align="center"> <g:message code="SEARCH.DESCRIPTION" /> </td>
                </tr>
              <tr>
                  <td align='right'>
                            <!-- [  <g:link controller="search" action="advancedSearch">Go to advanced search</g:link> ] -->
                            [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
                  </td>
              </tr>
			</table>
	 </div>



    <div id="main">
<g:form name="searchForm" controller="search">

  <table id='tabTitle' border='0' width="100%" align='center' cellpadding="0" cellspacing="0" class="TableBG">

    <tr height='2'> <td colspan="3" align='center'> &nbsp;

     <g:if test="${flash.message}">
       <div class="AlertLabel">
          ${flash.message}
       </div>
     </g:if>

   </td> </tr>

   <tr>
      <td width="1%"> &nbsp;</td>
      <td valign="top" align='center'> 
        <g:textField name="query" value="${params.query?params.query:''}" style="width:300px;" tabindex='1'></g:textField>
        <g:actionSubmit value="search..."  action="textSearch" tabindex='2'/>
      </td>
      <td width="1%"> &nbsp;</td>
    </tr>

    <tr>
    <td width="1%"> &nbsp; </td>
    <td align="center" >
      <table>
        <tr>
          <td width="5%">
               Tips:
          </td>
          <td>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="LableTips" align="left">
               Enter Query Like<br>chebi:*&nbsp;,glucose&nbsp;,benzene&nbsp;,C00001&nbsp;,XLYOFNOQVPJJNP-UHFFFAOYSA-N etc. 
          </td>
        </tr>
      </table>
    </td>
    <td width="1%"> &nbsp; </td>
  </tr>


  </table>
</g:form>
   </div>
  <g:render template="/footer" />
</div>
 </div>
  </body>
</html>
import grails.converters.JSON
import grails.converters.deep.XML
import org.codehaus.groovy.grails.commons.ConfigurationHolder

/**
 * provides us with transformations
 */
class TransformController {

  /**
   * does the actual converting of data
   */
  ConvertService convertService
  DataExportService dataExportService = new DataExportService();

  def index = {

    String from = "-1"
    String to = "-1"
    String idValue = ""

    if(params.from != null)
    {
      from = params.from
    }

    if(params.to != null)
    {
      to = params.to
    }

    if(params.idValue != null)
    {
      idValue = params.idValue
    }


    return [fromFields:convertService.getFromConversions(),toFields:convertService.toConversions,idValue:idValue,from:from,to:to]
  }

  /**
   * exspects 3 params and convert them to the given result
   * using the converter service
   */
  def transform = {

      String from = params.from
      String to = params.to
      String idValue = params.idValue

      if( idValue == null || idValue.trim().length() == 0 )
      {
        flash.message = "Error! Id parameter is mandatory, Please Enter id."
        redirect(action:"index")
      }else {
        if( from == null || from.toString().equalsIgnoreCase("NULL") )
        {
          flash.message = "Error! From parameter is mandatory."
          redirect(action:"index")
        } else {
          if( to == null || to.toString().equalsIgnoreCase("NULL") )
          {
            flash.message = "Error! To parameter is mandatory."
            redirect(action:"index")
          }
        }
      }


     if(params?.format && params.format != "html") //Export
       {

         println "======== In Upload =============";
         //call the service to conver the data
         Collection<Map<Compound,?>> transform =  convertService.convert(from, to, idValue)

         dataExportService.exportTransform(transform,response,params) 
         
       }else
       {
           if( params.offset == null || params.offset.toString().equals("0"))
            {
                params.put("offset",0);
            }else
            {
               params.put("offset",Long.parseLong("0"+ params.offset.toString()) );
            }

            if( params.max == null || params.max.toString().equals("0"))
            {
                params.put("max",10);
            }else
            {
              params.put("max",Long.parseLong("0"+ params.max.toString()));
            }

            //call the service to to get Count of the data
            Long total = 0;
            if(params.total == null || params.total.toString().equals("0"))
            {
              total =  convertService.getCount(from, to, idValue)
            }else
            {
              total = Long.parseLong(params.total.toString())
            }

            //call the service to conver the data
            Collection<Map<Compound,?>> result =  convertService.convert(from, to, idValue, params)
            return [transform:result,idValue:idValue,from:from,to:to,max:params.max,offset:params.offset,total:total]
       }

  }

  /**
   * returns the provide formats
   */
  static List getFormats() {
    return ['xml', 'json']
  }


}

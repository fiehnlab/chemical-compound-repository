includeTargets << grailsScript("Init")
includeTargets << new File("scripts/RunScript.groovy")

/**
 * executes all the scripts in the builder directory and so imports all avaliable databases, which can take
 * a long long time
 */
target(main: "imports all data into the database in a completely automated way") {

  //where is our directory with the scripts
  def dir = "builder"

  new File(dir).listFiles().each {File file ->

    if (file.getName().endsWith(".groovy")) {
      args = "${args} ${dir}/${file.getName()}"
    }
  }

  depends(runScript)

}

setDefaultTarget(main)

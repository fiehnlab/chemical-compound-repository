package types

/**
 * a simple hit for a search
 * User: wohlgemuth
 * Date: Jan 30, 2010
 * Time: 2:16:35 AM
 */
class Hit implements Serializable {

  /**
   * what kind of type this hit is
   */
  enum HitType {
    INCHI,
    INCHI_KEY,
    OSCAR,
    CAS,
    KEGG,
    COMPOUND_ID
  }

  public final static HitType INCHI = HitType.INCHI
  public final static HitType INCHI_KEY = HitType.INCHI_KEY
  public final static HitType OSCAR = HitType.OSCAR
  public final static HitType CAS = HitType.CAS
  public final static HitType KEGG = HitType.KEGG
  public final static HitType COMPOUND_ID = HitType.COMPOUND_ID

  def int hashCode() {
    return value.toLowerCase().hashCode()
  }

  /**
   * confidence rating
   */
  double confidence = 1.0

  /**
   * value of the hit
   */
  String value

  /**
   * were is this hit comming from
   */
  Class origin = getClass()

  HitType type

  /**
   * needed when we use database based resolvers and manage to get the id directly
   */
  long compoundId

  public boolean equals(Object o) {
    if (o instanceof Hit) {
      return (o.value.toLowerCase().equals(value.toLowerCase()))
    }
    return false
  }

  public String toString() {
    return "Hit: ${value} - ${confidence} - ${type}"
  }
}


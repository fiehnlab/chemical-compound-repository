package resolver.interceptor

/**
 * intercept calls to the resolver and fitlers results or other stuff
 *
 */
public interface ResolveableInterceptor {

  /**
   * does the actual interception
   */
  public Set<String> intercept(Set<String> queries)
}
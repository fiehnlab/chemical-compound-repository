package resolver.query

import org.apache.log4j.Logger
import org.codehaus.groovy.grails.commons.ApplicationHolder
import types.Hit

/**
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 7:10:12 PM
 */
class SynonymQuery extends AbstractQuery {

  protected String getClassOfInterrest() {
    return "Synonym";
  }

  protected String getQuerySymbol() {
    return "name";
  }
}

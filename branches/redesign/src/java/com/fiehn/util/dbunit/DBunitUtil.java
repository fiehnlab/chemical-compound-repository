package com.fiehn.util.dbunit;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;

import javax.sql.DataSource;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 3, 2010
 * Time: 4:22:48 PM
 */
public class DBunitUtil {


    /**
     * initialize a dbunit dataset for testing, works only with HSQLDB!
     * @param source
     * @param data
     * @return
     * @throws Exception
     */
    public static DatabaseConnection intialize(DataSource source, IDataSet data) throws Exception{
        DatabaseConnection dbunit = new DatabaseConnection(source.getConnection());

        DatabaseConfig config = dbunit.getConfig();

        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,new HSQLDataTypeFactory());
        
        DatabaseOperation.CLEAN_INSERT.execute(dbunit,data);
        return dbunit;
    }
}

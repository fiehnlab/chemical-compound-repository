import grails.test.*

class NCITests extends GrailsUnitTestCase {
  NCI nci

  protected void setUp() {
    super.setUp()
    nci = new NCI()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testToString() {

    nci.nciId = "ddas"

    assertTrue(nci.toString() == "NCI: ddas")

  }

  void testSetExperimentalLogP() {
    nci.experimentalLogP = 343.3

    assertTrue(nci.experimentalLogP == 343.3)
  }

  void testSetKowLogP() {
    nci.kowLogP = 343.3

    assertTrue(nci.kowLogP == 343.3)

  }
}

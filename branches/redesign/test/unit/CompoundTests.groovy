import grails.test.*

class CompoundTests extends GrailsUnitTestCase {

  Compound compound

  protected void setUp() {
    super.setUp()

    compound = new Compound()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testSetIchi() {
    compound.inchi = "dadsa"

    assertTrue(compound.inchi == "dadsa")

  }

  void testSetInchHashKey() {
    compound.inchiHashKey = new InchiHashKey()
    assertTrue(compound.inchiHashKey != null)

    compound.inchiHashKey.completeKey = "Dasda"

    assertTrue(compound.inchiHashKey.completeKey == "Dasda")

  }


  void testSetExactMolareMass() {
    compound.exactMolareMass = 24334.3

    assertTrue(compound.exactMolareMass == 24334.3)

  }

  void testToString() {
    compound.id = 4
    compound.inchi = "sdsd"

    assertTrue(compound.toString() == "Compound: 4 - sdsd")
  }


  void testGetDeaultName() {
    compound.id = 4
    assertTrue(compound.getDefaultName() == "Unknown")
  }


}

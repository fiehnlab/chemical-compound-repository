package com.fiehn.filter;

import java.io.ByteArrayInputStream;

import junit.framework.TestCase;

public class FilterFactoryTest extends TestCase {

	public void testCreateStopListFilter() {
		Filter filter = FilterFactory.createStopListFilter(new ByteArrayInputStream(new byte[]{}));
		
		assertTrue(filter instanceof StopListFilter);
	}


	public void testCreateCaseSensitiveStopListFilter() {
		Filter filter = FilterFactory.createCaseSensitiveStopListFilter(new ByteArrayInputStream(new byte[]{}));
		
		assertTrue(filter instanceof StopListFilter);
		assertTrue(filter.isCaseSensitive());
	}

	public void testCreateSimpleStopListFilter() {
		Filter filter = FilterFactory.createSimpleStopListFilter();
		
		assertTrue(filter instanceof SimpleStopListFilter);
		assertTrue(filter.isCaseSensitive() == false);
	}

}

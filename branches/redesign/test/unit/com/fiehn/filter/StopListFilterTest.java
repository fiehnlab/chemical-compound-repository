package com.fiehn.filter;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import static org.junit.Assert.*;

public class StopListFilterTest extends TestCase {

	String testSet = "i\nAlpaha\nBeta\nHouse\nrubber\nfancy";
	
	@Test
	public void testFilterCaseSensitive() throws FileNotFoundException{
		
		Filter filter = getCaseSensitiveFilter();
		
		Set<String> content = new HashSet<String>();
		
		content.add("Glucose"); //should be in result
		content.add("i");
		content.add("Alpaha");
		content.add("Beta");
		content.add("House");
		content.add("rubber");
		content.add("fancy");
		content.add("benzene"); //should be in result
		
		Set<String> result = filter.filter(content);

		System.out.println(result);
		//some random tests
		assertTrue(result != null);
		assertTrue(result.size() == 2);
		assertTrue(result.contains("benzene"));
		assertTrue(result.contains("Glucose"));
	}
	

	@Test
	public void testCaseInsentiveFilter() throws FileNotFoundException{
		
		Filter filter = getCaseInSensitiveFilter();
		
		Set<String> content = new HashSet<String>();
		
		content.add("Glucose"); //should be in result
		content.add("i");
		content.add("Alpaha");
		content.add("Beta");
		content.add("House");
		content.add("Benzene"); //should be in result
		
		Set<String> result = filter.filter(content);
		System.out.println(result);
		//some random tests
		assertTrue(result != null);
		assertTrue(result.size() == 2);
		assertTrue(result.contains("Benzene"));
		assertTrue(result.contains("Glucose"));
	}

	protected Filter getCaseSensitiveFilter(){
		return FilterFactory.createStopListFilter(new ByteArrayInputStream(testSet.getBytes()));	
	}

	protected Filter getCaseInSensitiveFilter(){
		return FilterFactory.createCaseSensitiveStopListFilter(new ByteArrayInputStream(testSet.getBytes()));	
	}
	
}

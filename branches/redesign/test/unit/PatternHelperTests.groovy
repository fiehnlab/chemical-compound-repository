import pattern.PatternHelper

/**
 * User: wohlgemuth
 * Date: Feb 1, 2010
 * Time: 11:54:21 PM
 */
class PatternHelperTests extends GroovyTestCase {

  /**
   * tests our inchi pattern against a test file
   */
  void testInchiPattern() {

    def failed = 0
    double length = 0
    double success = 0
    def failedInchis = []

    new File("./test/unit/resources/inchis.txt").eachLine {String line ->

      def inchi = line.matches(PatternHelper.STD_INCHI_PATTERN)

      if (inchi) {
        success++
      }
      else {
        failedInchis[failed] = line
        failed++
      }
      length++


    }

    println "success rate: ${success / length * 100}"

    //test condition
    assertTrue(success == length)
    assertTrue(failedInchis.size() == 0)
  }


  void testStdInchiKey() {
    def failed = 0
    double length = 0
    double success = 0
    def failedInchis = []

    new File("./test/unit/resources/inchiKeys.txt").eachLine {String line ->

      def inchi = line.trim().matches(PatternHelper.STD_INCHI_KEY_PATTERN)

      if (inchi) {
        success++
      }
      else {
        failedInchis[failed] = line
        failed++
      }
      length++


    }


    println "success rate: ${success / length * 100}"

    //test condition
    assertTrue(success == length)
    assertTrue(failedInchis.size() == 0)
  }


  void testKegg() {
    def failed = 0
    double length = 0
    double success = 0

    new File("./test/unit/resources/kegg.txt").eachLine {String line ->

      def inchi = line.trim().matches(PatternHelper.KEGG_PATTERN)

      if (inchi) {
        success++
      }
      else {
        failed++
      }
      length++


    }

    println "success rate: ${success / length * 100}"

    //test condition
    assertTrue(success == length)
  }

  void testCas() {
    def failed = 0
    double length = 0
    double success = 0

    new File("./test/unit/resources/cas.txt").eachLine {String line ->

      def inchi = line.trim().matches(PatternHelper.CAS_PATTERN)

      if (inchi) {
        success++
      }
      else {

        failed++
      }
      length++


    }

    println "success rate: ${success / length * 100}"

    //test condition
    assertTrue(success == length)
  }

  void testHMDB() {
    def failed = 0
    double length = 0
    double success = 0

    new File("./test/unit/resources/hmdb.txt").eachLine {String line ->

      def inchi = line.trim().matches(PatternHelper.HMDB_PATTERN)

      if (inchi) {
        success++
      }
      else {
        failed++
      }
      length++


    }

    println "success rate: ${success / length * 100}"

    //test condition
    assertTrue(success == length)
  }


  void testLipidMaps() {
    def failed = 0
    double length = 0
    double success = 0

    new File("./test/unit/resources/lm.txt").eachLine {String line ->

      def inchi = line.trim().matches(PatternHelper.LIPID_MAPS_PATTERN)

      if (inchi) {
        success++
      }
      else {
        failed++
      }
      length++


    }

    println "success rate: ${success / length * 100}"

    //test condition
    assertTrue(success == length)
  }

  void testCID() {
    assertTrue("CID:324324".matches(PatternHelper.CID_PATTERN))
    assertTrue("cid:324324".matches(PatternHelper.CID_PATTERN))
  }

  void testSID() {
    assertTrue("SID:324324".matches(PatternHelper.SID_PATTERN))
    assertTrue("sid:324324".matches(PatternHelper.SID_PATTERN))
  }

}

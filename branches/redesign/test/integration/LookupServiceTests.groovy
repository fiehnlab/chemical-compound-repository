import test.GrailsDBunitIntegrationTest
import types.Hit

//import com.redpointtech.dbunit.*


class LookupServiceTests extends GrailsDBunitIntegrationTest
{
  LookupService lookupService = null
  public LookupServiceTests() {

  }

  protected void setUp() {
    super.setUp()

  }

  protected void tearDown() {
    super.tearDown()
  }


  void testLookupByPartialInchiKey() {

    String key = "LNRYWMBIOOXPID"
    def result = lookupService.lookupByPartialInchiKey(key)
    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookupByInchiKey() {

    String key = "ATQXXRJEZULCTG-DLCFQVRISA-N"
    def result = lookupService.lookupByInchiKey(key)

    println "result : ${result}"
    println "result.inchiHashKey.completeKey : ${result.inchiHashKey.completeKey}"

    assertTrue(result instanceof Compound)

    assertTrue(result.inchiHashKey.completeKey == key)

  }


  void testLookupByInchi() {

    String key = "InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1"
    def result = lookupService.lookupByInchi(key)

    assertTrue(result instanceof Compound)
    
    assertTrue(result.inchi == "InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1")

  }




  void testLookupByKegg() {

    String key = "C10001"
    def result = lookupService.lookupByKegg(key)

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }

  void testLookupByKeggList() {

    def key = ["C10001", "C10002"]
    def result = lookupService.lookupByKegg(key)

    assertTrue(result.size() == 2)
  }
  

  void testLookupByCas() {

    String key = "109345-26-8"
    def result = lookupService.lookupByCas(key)

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }

  }

  void testLookupByCID() {

    int key = 1
    def result = lookupService.lookupByCID(key)


    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }

  }


  void testLookupBySID() {

    int key = 74380251
    def result = lookupService.lookupBySID(key)


    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookupBySmile() {

    String key = "CC(CN)O"
    def result = lookupService.lookupBySmile(key)


    assertTrue(result.size() == 2)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }

  void testLookupByExactMass() {

    double key = 203.115758
    def result = lookupService.lookupByExactMass(key)


    println("Mass : Result : size : ${result.size()}")

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }



  void testLookupByFormula() {

    String key = "C9H17NO4"
    def result = lookupService.lookupByFormula(key)

    println("Formula : Result : size : ${result.size()}")

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookUpByName() {

    def result = lookupService.lookupByName("Deoxycytidine")

    assertTrue(result.size() == 1)

  }

  void testLookUpByMultipleName() {

    def result = lookupService.lookupByName(["Deoxycytidine","Cortexolone","Deoxycorticosterone"])
    assertTrue(result.size() == 3)

  }

  void testLookUpByHMDB() {

    def result = lookupService.lookupByHMDB("HMDB00011")

    assertTrue(result.size() == 1)

  }



  void testLookUpByLipidMap() {

    def result = lookupService.lookupByLipidMapId("LMFA00000001")

    assertTrue(result.size() == 1)

  }



  void testLookUpByChebi() {

    def result = lookupService.lookupByChebiId("7")

    assertTrue(result.size() == 1)

  }

  void testLookUpByCas() {

    def result = lookupService.lookupByCas("22365-13-5")

    assertTrue(result.size() == 1)

  }


  void testLookUpBySid() {

    def result = lookupService.lookupBySID("74380251")

    assertTrue(result.size() == 1)

  }

  void testLookUpByCid() {

    def result = lookupService.lookupByCID(1)

    assertTrue(result.size() == 1)
  }

  void testLookUpByHitCas() {

    Hit hit = new Hit()
    hit.type = Hit.KEGG
    hit.value = "C10094"

    def result = lookupService.lookupByHit(hit)

    println "result for test lookup by hit: ${result}"
    assertTrue(result.size() == 1)

  }



void testCountLookupByPartialInchiKey() {
  String key = "LNRYWMBIOOXPID"
  Long result = lookupService.countlookupByPartialInchiKey(key)
  assertTrue(result == 1)
}

void testCountLookupByInchiKey() {
    String key = "ATQXXRJEZULCTG-DLCFQVRISA-N"
    Long result = lookupService.countlookupByInchiKey(key)
    assertTrue(result == 1)
  }

void testCountLookupByInchi() {
   String key = "InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1"
    Long result = lookupService.countlookupByInchi(key)
 
   assertTrue(result ==1)
  }

void testCountLookupByKegg() {
    def key = ["C10001", "C10002"]
    Long result = lookupService.countlookupByKegg(key)
   assertTrue(result == 2)
  }

void testCountLookupByCas() {
    String key = "109345-26-8"
    Long result = lookupService.countlookupByCas(key)
    assertTrue(result == 1)
 }

void testCountLookupByCID() {
    int key = 1
    Long result = lookupService.countlookupByCID(key)
    assertTrue(result == 1)
  }

void testCountLookupBySID() {
    int key = 74380251
    Long result = lookupService.countlookupBySID(key)
    assertTrue(result == 1)
  }

void testCountLookupBySmile() {
    String key = "CC(CN)O"
    Long result = lookupService.countlookupBySmile(key)
    assertTrue(result == 2)
  }

void testCountLookupByExactMass() {
    double key = 203.115758
    Long result = lookupService.countlookupByExactMass(key)
  println result
    assertTrue(result == 1)
  }

void testCountLookupByFormula() {
    String key = "C9H17NO4"
    Long result = lookupService.countlookupByFormula(key)
    assertTrue(result == 1)
  }

void testCountLookUpByHMDB() {
    Long result = lookupService.countlookupByHMDB("HMDB00011")
    assertTrue(result == 1)
  }

void testCountLookUpByLipidMap() {
    Long result = lookupService.countlookupByLipidMapId("LMFA00000001")
    assertTrue(result == 1)
  }

void testCountLookUpByChebi() {
    Long result = lookupService.countlookupByChebiId("7")
    assertTrue(result == 1)
  }

  void testCountLookUpByMultipleName() {

    Long result = lookupService.countlookupByName(["Deoxycytidine","Cortexolone","Deoxycorticosterone"])
    
    assertTrue(result == 3)

  }

  void testCountLookUpByName() {

    Long result = lookupService.countlookupByName("Deoxycytidine")

    assertTrue(result == 1)

  }

}

import test.GrailsDBunitIntegrationTest

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Feb 24, 2010
 * Time: 11:54:56 AM
 * To change this template use File | Settings | File Templates.
 */
class DiscoveryServiceTests extends GrailsDBunitIntegrationTest {
    DiscoveryService discoveryService = null

    protected void setUp() {
      //service = new DiscoveryService()
      super.setUp()
    }

    protected void tearDown() {
      super.tearDown()
    }

    void testProcess()
    {
      String query = "Hello 1,2-campholide world, you are full of 2-Hexaprenylphenol"
      def result = discoveryService.process(query,0.5)

      println result
      assertTrue(result.size() == 2)

    }
}

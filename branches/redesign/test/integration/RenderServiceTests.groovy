import grails.test.GrailsUnitTestCase


import edu.ucdavis.genomics.metabolomics.binbase.connector.references.sdf.SdfToInchi
import test.GrailsDBunitIntegrationTest;


class RenderServiceTests extends GrailsDBunitIntegrationTest {

  RenderService renderService = null

  protected void setUp() {
    super.setUp()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testRenderToMolString() {

    String from = "InChI=1S/C6H12O6/c7-1-3(9)5(11)6(12)4(10)2-8/h1,3-6,8-12H,2H2"
    String mol = renderService.renderToMol(from)

    String to = SdfToInchi.sdfToInchi(new ByteArrayInputStream(mol.getBytes()))


    assert(from == to)

  }

  void testRenderToMolCompound() {

    assertTrue(renderService.renderToMol(Compound.get(895880)) != null)
  }

}

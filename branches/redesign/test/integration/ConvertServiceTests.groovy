import grails.test.GrailsUnitTestCase

class ConvertServiceTests extends GrailsUnitTestCase {
  ConvertService service = new ConvertService()

  protected void setUp() {
    super.setUp()
    service = new ConvertService()

    //assign the lookup service
    service.lookupService = new LookupService()

  }

  protected void tearDown() {
    super.tearDown()
  }

  void testConvert() {
    service.convert("kegg", "cas", ["C10001","C20002"])
  }


  Compound getCompound(long id) {
    return service.lookupService.lookupByCompoundId(id);
  }

  void testConvertToCOMPOUND() {
    assertTrue(service.convertToCOMPOUND(getCompound(895880)).inchiHashKey.completeKey == "BRMWTNUJHUMWMS-LURJTMIESA-N")
  }

  void testConvertToINCHI() {
    assertTrue(service.convertToINCHI(getCompound(895880)) == "InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1")

  }

  void testConvertToINCH895880IKEY() {
    assertTrue(service.convertToINCHIKEY(getCompound(895880)).completeKey == "BRMWTNUJHUMWMS-LURJTMIESA-N")
  }

  void testConvertToLINKS() {
    assertTrue(service.convertToLINKS(getCompound(895880)).size() == 1)
  }

  void testConvertToSMILES() {
    assertTrue(service.convertToSMILES(getCompound(931715)).size() == 2)

  }

  void testConvertToIUPAC() {
    assertTrue(service.convertToIUPAC(getCompound(931396)).size() == 1)
  }

  void testConvertToNAMES() {
    assertTrue(service.convertToNAMES(getCompound(931396)).size() == 1)
  }

  void testConvertToMASS() {
    assertTrue(service.convertToMASS(getCompound(931396)) == 626.491)
  }

  void testConvertToFORMULA() {
    assertTrue(service.convertToFORMULA(getCompound(931396)) == "C40H66O5")
  }

  void testConvertToKegg() {
    assertTrue(service.convertToKegg(getCompound(931418))[0].keggId == "C10000")

  }

  void testConvertToLipidMap() {
    assertTrue(service.convertToLipidMap(getCompound(931396))[0].lipidMapId == "LMFA00000001")

  }

  void testConvertToCID() {
    assertTrue(service.convertToCID(getCompound(931715))[0].cid == 1)

  }

  void testConvertToSID() {
    assertTrue(service.convertToSID(getCompound(931402))[0].sid == 74380252)

  }

  void testConvertToHMDB() {
    assertTrue(service.convertToHMDB(getCompound(895880))[0].hmdbId == "HMDB00001")

  }

  void testconvertToChebi() {
    assertTrue(service.convertToChebi(getCompound(931784))[0].chebiId== "7")

  }
}

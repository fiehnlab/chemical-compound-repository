import resolver.Resolveable
import resolver.OscarResolver
import types.Hit

/**
 * User: wohlgemuth
 * Date: Feb 1, 2010
 * Time: 11:08:25 PM
 */
class OscarResolverTests extends GroovyTestCase {

  //our test document
  String document = """"

    Using these reactions we can now follow Fischer's train of logic in assigning the configuration of D-glucose.

1.   Ribose and arabinose (two well known pentoses) both gave erythrose on Ruff degradation.
    As expected, Kiliani-Fischer synthesis applied to erythrose gave a mixture of ribose and arabinose.
2.   Oxidation of erythrose gave an achiral (optically inactive) aldaric acid. This defines the configuration of erythrose.
3.   Oxidation of ribose gave an achiral (optically inactive) aldaric acid. This defines the configuration of both ribose and arabinose.
4.   Ruff shortening of glucose gave arabinose, and Kiliani-Fischer synthesis applied to arabinose gave a mixture of glucose and mannose.
5.   Glucose and mannose are therefore epimers at C-2, a fact confirmed by the common product from their osazone reactions.
6.   A pair of structures for these epimers can be written, but which is glucose and which is mannose?


  """

  //resolver based on oscar
  Resolveable resolver = new OscarResolver()

  void testResolverConfidenceLevel05() {
    resolver.confidenceLevel = 0.5
    def result = resolver.resolve(document)

    println "result ${result}"

    //make sure we don't have a null result
    assertTrue result != null

    //we exspect 6 results
    assertTrue result.size() == 6
  }

  void testResolverConfidenceLevel09() {
    resolver.confidenceLevel = 0.9
    def result = resolver.resolve(document)

    println "result ${result}"
    assertTrue result != null

    //we exspect 2 results
    assertTrue result.size() == 2
  }

  void testResolverConfidenceLevel10() {
    resolver.confidenceLevel = 1.0
    def result = resolver.resolve(document)

    println "result ${result}"
    
    //make sure we don't have a null result
    assertTrue result != null

    assertTrue result.size() == 0
  }


      void testBenzene() {
        def result = resolver.resolve("hello you beautiful benzene world")

        println "result ${result}"

        //make sure we don't have a null result
        assertTrue result != null

        assertTrue result.size() == 1
      }


      void testABunch() {
        resolver.confidenceLevel = 0.5
        def result = resolver.resolve("""
16-methyl-octadecanoic acid
18-methyl-nonadecanoic acid
(+)-18-methyl-eicosanoic acid
20-methyl-heneicosanoic acid
(+)-20-methyl-docosanoic acid
22-methyl-tricosanoic acid
3,13,19-trimethyl-tricosanoic acid
23-methyl-tetracosanoic acid
24-methyl-pentacosanoic acid
(+)-24-methyl-hexacosanoic acid
26-methyl-heptacosanoic acid
2,4,6-trimethyl-octacosanoic acid
(+)-28-methyl-triacontanoic acid
2-methyl-2Z-butenoic acid
2-methyl-2E-butenoic acid
4-methyl-3-pentenoic acid
2,4,6-trimethyl-2Z-tetracosenoic acid
2,6-dimethyl-nonadecanoic acid
2,6-dimethyl-undecanoic acid
2,6-dimethyl-dodecanoic acid
4,8-dimethyl-dodecanoic acid
4,12-dimethyl-tridecanoic acid
2,6-dimethyl-tetradecanoic acid
2,8-dimethyl-tetradecanoic acid
2,6-dimethyl-pentadecanoic acid
4,8-dimethyl-pentadecanoic acid
2,6-dimethyl-hexadecanoic acid
4,8-dimethyl-hexadecanoic acid
2,14-dimethyl-hexadecanoic acid
4,14-dimethyl-hexadecanoic acid
6,14-dimethyl-hexadecanoic acid
4,14-dimethyl-heptadecanoic acid
2,14-dimethyl-octadecanoic acid
4,14-dimethyl-octadecanoic acid
6,14-dimethyl-octadecanoic acid
4,16-dimethyl-octadecanoic acid        
2-methoxy-12-methyloctadec-17-en-5-ynoyl anhydride
N-(3S-hydroxydecanoyl)-L-serine
N-(3-(hexadecanoyloxy)-heptadecanoyl)-L-ornithine
N-(9Z,12Z,15Z-octadecatrienoyl)-glutamine
N-(3-(15-methyl-hexadecanoyloxy)-13-methyl-tetradecanoyl)-L-serine
2-((2S)-6-amino-2-(3-hydroxy-14-methylpentadecanamido)hexanoyloxy)ethyl 2-hydroxy-13-methyltetradecanoate
N-hydroxydecanamide
(9S,10S)-10-hydroxy-9-(phosphonooxy)octadecanoic acid
6-(6-aminohexanamido)hexanoic acid
hexadecanoic acid
ethanoic acid
propanoic acid
butanoic acid
pentanoic acid
hexanoic acid
heptanoic acid
octanoic acid
nonanoic acid
decanoic acid
undecanoic acid
dodecanoic acid
tridecanoic acid
tetradecanoic acid
pentadecanoic acid
heptadecanoic acid
octadecanoic acid
nonadecanoic acid
eicosanoic acid
heneicosanoic acid
docosanoic acid
tricosanoic acid
tetracosanoic acid
pentacosanoic acid
hexacosanoic acid
heptacosanoic acid
octacosanoic acid
nonacosanoic acid
triacontanoic acid
hentriacontanoic acid
dotriacontanoic acid
tritriacontanoic acid
tetratriacontanoic acid
pentatriacontanoic acid
hexatriacontanoic acid
heptatriacontanoic acid
octatriacontanoic acid
hexatetracontanoic acid
methanoic acid
2-hydroxy-2-methyl-propanoic acid
17-methyl-6Z-octadecenoic acid
6-methyl-octanoic acid
7-methyl-octanoic acid
10-methyl-undecanoic acid
10-methyl-dodecanoic acid
11-methyl-dodecanoic acid
12-methyl-tridecanoic acid
12-methyl-tetradecanoic acid
13-methyl-tetradecanoic acid
14-methy-pentadecanoic acid
14-methyl-hexadecanoic acid
15-methyl-hexadecanoic acid
10-methyl-heptadecanoic acid
16-methyl-heptadecanoic acid
10-methyl-octadecanoic acid
16-methyl-octadecanoic acid
18-methyl-nonadecanoic acid
(+)-18-methyl-eicosanoic acid
20-methyl-heneicosanoic acid
(+)-20-methyl-docosanoic acid
22-methyl-tricosanoic acid
3,13,19-trimethyl-tricosanoic acid
23-methyl-tetracosanoic acid
24-methyl-pentacosanoic acid
(+)-24-methyl-hexacosanoic acid
26-methyl-heptacosanoic acid
2,4,6-trimethyl-octacosanoic acid
(+)-28-methyl-triacontanoic acid
2-methyl-2Z-butenoic acid
2-methyl-2E-butenoic acid
4-methyl-3-pentenoic acid
2,4,6-trimethyl-2Z-tetracosenoic acid
2,6-dimethyl-nonadecanoic acid
2,6-dimethyl-undecanoic acid
2,6-dimethyl-dodecanoic acid
4,8-dimethyl-dodecanoic acid
4,12-dimethyl-tridecanoic acid
2,6-dimethyl-tetradecanoic acid
2,8-dimethyl-tetradecanoic acid
2,6-dimethyl-pentadecanoic acid
4,8-dimethyl-pentadecanoic acid
2,6-dimethyl-hexadecanoic acid
4,8-dimethyl-hexadecanoic acid
2,14-dimethyl-hexadecanoic acid
4,14-dimethyl-hexadecanoic acid
6,14-dimethyl-hexadecanoic acid
4,14-dimethyl-heptadecanoic acid
2,14-dimethyl-octadecanoic acid
4,14-dimethyl-octadecanoic acid
6,14-dimethyl-octadecanoic acid
4,16-dimethyl-octadecanoic acid

        """)

        println ""
        println "result ${result.size()}"
        println ""

        for(Hit h : result){
          println h.getValue()
        }


        //make sure we don't have a null result
        assertTrue result != null

        assertTrue result.size() == 27
      }


}

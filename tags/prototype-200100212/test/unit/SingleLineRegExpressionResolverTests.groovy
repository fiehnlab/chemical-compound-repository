import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory
import resolver.Resolveable
import resolver.SingleLineRegExpressionResolver

/**
 * User: wohlgemuth
 * Date: Feb 2, 2010
 * Time: 1:32:02 AM
 */
class SingleLineRegExpressionResolverTests extends GroovyTestCase {

  String document = """
   czxcxzcxzc
   xcxzcxzc
SYKWLIJQEHRDNH-KRPIADGTSA-N  sdcvxvx vcxvcx vcx
DQJCDTNMLBYVAY-FEWQIWPRSA-N                    vccxvcxv
QIGBRXMKCJKVMJ-UHFFFAOYSA-N                            cvcxvcxv
NFVGYLGSSJPRKW-XBTRWLRFSA-N                                    vcvxvvxxc
dsdfadfckvls
zcxxzcv
xv                   InChI=1S/Cr/q+3
InChI=1S/Cu dsfcxcxzcz
InChI=1S/Fe           InChI=1S/Cr/q+3
      InChI=1S/Cu
    InChI=1S/Fe
cx
czx
c
xzc
xzc
xzc



  """


  protected void setUp() {
    super.setUp()

    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }

  protected void tearDown() {
    super.tearDown()
    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }



  void testResolveInchiInText() {

    Resolveable resolver = new SingleLineRegExpressionResolver()

    def result = resolver.resolve(document)

    println "result ${result}"

    //make sure we don't have a null result
    assertTrue result != null

    //we exspect 7 results
    assertTrue result.size() == 7
  }
}

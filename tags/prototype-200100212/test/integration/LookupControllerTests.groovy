import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory
import grails.test.ControllerUnitTestCase
import types.Hit

class LookupControllerTests extends ControllerUnitTestCase {

  protected void setUp() {
    super.setUp()

    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }

  protected void tearDown() {
    super.tearDown()
    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }

  void testValidationOfParams() {

    LookupController.metaClass.getParams = {-> [hits: []] }

    LookupController controller = new LookupController()

    //should throw no excetption
    controller.lookup()
  }


  void testValidationOfParamsFailsNoParam() {

    LookupController.metaClass.getParams = {-> []}

    LookupController controller = new LookupController()

    try {

      //should throw no excetption
      controller.lookup()

      assertTrue(false)
    }
    catch (Error e) {
      println "success!"
    }
  }

  void testValidationOfParamsFailsWrongType() {

    LookupController.metaClass.getParams = {-> [hits: new Hit(type: Hit.KEGG, value: "C00001")] }

    LookupController controller = new LookupController()

    try {

      //should throw no excetption
      controller.lookup()

      assertTrue(false)
    }
    catch (Error e) {
      println "success!"
    }
  }

  void testLookupByPartialInchiKey() {
    LookupController.metaClass.getParams = {-> [inchiKey: "AAAFZMYJJHWUPN"] }

    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()

    def result = controller.lookupByPartialInchiKey().lookup


    assertTrue(result.size() == 2)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookupByInchiKey() {
    LookupController.metaClass.getParams = {-> [inchiKey: "AAAFZMYJJHWUPN-TXICZTDVSA-N"] }

    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()

    def result = controller.lookupByInchiKey().lookup

    assertTrue(result instanceof Compound)

    assertTrue(result.inchi == "InChI=1S/C5H12O11P2/c6-3-2(1-14-17(8,9)10)15-5(4(3)7)16-18(11,12)13/h2-7H,1H2,(H2,8,9,10)(H2,11,12,13)/t2-,3-,4-,5-/m1/s1")

  }


  void testLookupByInchi() {
    LookupController.metaClass.getParams = {-> [inchi: "InChI=1S/C5H12O11P2/c6-3-2(1-14-17(8,9)10)15-5(4(3)7)16-18(11,12)13/h2-7H,1H2,(H2,8,9,10)(H2,11,12,13)/t2-,3-,4-,5-/m1/s1"] }

    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()

    def result = controller.lookupByInchi().lookup

    assertTrue(result instanceof Compound)

    assertTrue(result.inchi == "InChI=1S/C5H12O11P2/c6-3-2(1-14-17(8,9)10)15-5(4(3)7)16-18(11,12)13/h2-7H,1H2,(H2,8,9,10)(H2,11,12,13)/t2-,3-,4-,5-/m1/s1")

  }


  void testLookupByCompoundId() {
    LookupController.metaClass.getParams = {-> [compoundId: "281705"] }

    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()

    def result = controller.lookupByCompoundId().lookup

    assertTrue(result instanceof Compound)

    assertTrue(result.inchi == "InChI=1S/C5H12O11P2/c6-3-2(1-14-17(8,9)10)15-5(4(3)7)16-18(11,12)13/h2-7H,1H2,(H2,8,9,10)(H2,11,12,13)/t2-,3-,4-,5-/m1/s1")

  }


  void testLookupByKegg() {
    LookupController.metaClass.getParams = {-> [kegg: "C01151"] }

    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()


    def result = controller.lookupByKegg().lookup

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 281705)
    }
  }

  void testLookupByCas() {
    LookupController.metaClass.getParams = {-> [cas: "553-97-9"] }

    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()


    def result = controller.lookupByCas().lookup

    println result
    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 308738)
    }

  }

  void testLookupByCID() {
    LookupController.metaClass.getParams = {-> [cid: "1840"] }

    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()

    def result = controller.lookupByCID().lookup


    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 307010)
    }

  }

  void testLookupBySID() {
    LookupController.metaClass.getParams = {-> [sid: "85291312"] }
    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()

    def result = controller.lookupBySID().lookup


    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 275456)
    }
  }


  void testLookupBySmile() {
    LookupController.metaClass.getParams = {-> [smile: "CC(CN)O"] }

    LookupController controller = new LookupController()
    controller.lookupService = new LookupService()

    def result = controller.lookupBySmile()


    assertTrue(result.lookup.size() == 2)

    for (def res: result.lookup) {
      assertTrue(res instanceof Compound)
      assertTrue(res.id == 271693)
    }
  }



}

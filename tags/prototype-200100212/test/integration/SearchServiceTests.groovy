import grails.test.GrailsUnitTestCase

/**
 * tests the search service
 */
class SearchServiceTests extends GrailsUnitTestCase {
  SearchService service = null


  protected void setUp() {
    super.setUp()
    service = new SearchService()
    service.lookupService = new LookupService()
  }

  protected void tearDown() {
    super.tearDown()
  }


  public void testSearchGenericSTDInchi() {
    assertTrue(service.searchGeneric("InChI=1S/C10H16N5O13P3/c11-8-5-9(13-2-12-8)15(3-14-5)10-7(17)6(16)4(26-10)1-25-30(21,22)28-31(23,24)27-29(18,19)20/h2-4,6-7,10,16-17H,1H2,(H,21,22)(H,23,24)(H2,11,12,13)(H2,18,19,20)/t4-,6-,7-,10-/m1/s1").id == 271574)
  }


  public void testSearchGenericSTDInchiKey() {
    assertTrue(service.searchGeneric("GZGOCBNPEYNLEV-QGZVFWFLSA-N").id == 851379)
  }

  public void testSearchGenericCas() {
    assertTrue(service.searchGeneric("553-97-9")[0].id == 308738)
    assertTrue(service.searchGeneric("120-78-5")[0].id == 308793)
    assertTrue(service.searchGeneric("946-31-6")[0].id == 308858)
    assertTrue(service.searchGeneric("121-66-4")[0].id == 308889)

  }

  public void testSearchGenericKegg() {
    assertTrue(service.searchGeneric("C04700")[0].id == 307009)
    assertTrue(service.searchGeneric("C04702")[0].id == 307025)
    assertTrue(service.searchGeneric("C04703")[0].id == 307041)
    assertTrue(service.searchGeneric("C04706")[0].id == 307055)
  }

  public void testSearchLipidMaps() {
    assertTrue(service.searchGeneric("LMFA01050367")[0].id == 307822)
    assertTrue(service.searchGeneric("LMFA01050368")[0].id == 307848)
    assertTrue(service.searchGeneric("LMFA01050369")[0].id == 307875)
    assertTrue(service.searchGeneric("LMFA01050370")[0].id == 307903)
  }


  public void testSearchHMDB() {
    assertTrue(service.searchGeneric("HMDB06571")[0].id == 309351)
    assertTrue(service.searchGeneric("HMDB06732")[0].id == 311284)
    assertTrue(service.searchGeneric("HMDB06835")[0].id == 281511)
    assertTrue(service.searchGeneric("HMDB11232")[0].id == 397012)
  }


  public void testSearchSID() {
    assertTrue(service.searchGeneric("SID:14711308")[0].id == 402043)
    assertTrue(service.searchGeneric("SID:14711309")[0].id == 402079)
    assertTrue(service.searchGeneric("SID:14711310")[0].id == 402118)
    assertTrue(service.searchGeneric("SID:14711311")[0].id == 402150)

    assertTrue(service.searchGeneric("sid:14711308")[0].id == 402043)
    assertTrue(service.searchGeneric("sid:14711309")[0].id == 402079)
    assertTrue(service.searchGeneric("sid:14711310")[0].id == 402118)
    assertTrue(service.searchGeneric("sid:14711311")[0].id == 402150)
  }



  public void testSearchCID() {
    assertTrue(service.searchGeneric("cid:5142")[0].id == 400967)
    assertTrue(service.searchGeneric("cid:5143")[0].id == 400992)
    assertTrue(service.searchGeneric("cid:5144")[0].id == 400126)
    assertTrue(service.searchGeneric("cid:5145")[0].id == 401041)

    assertTrue(service.searchGeneric("CID:5142")[0].id == 400967)
    assertTrue(service.searchGeneric("CID:5143")[0].id == 400992)
    assertTrue(service.searchGeneric("CID:5144")[0].id == 400126)
    assertTrue(service.searchGeneric("CID:5145")[0].id == 401041)
  }

  public void testSearchExactName() {
    assertTrue(service.searchGeneric("benzene")[0].id == 276440)
    assertTrue(service.searchGeneric("water")[0].id == 271571)
  }

  public void testSearchLikelyName() {
    assertTrue(service.searchGeneric("banana%")[0].id == 358104)
    assertTrue(service.searchGeneric("banana%")[1].id == 429021)

  }

}

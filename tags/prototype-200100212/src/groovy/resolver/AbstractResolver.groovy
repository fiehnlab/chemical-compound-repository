package resolver

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.Cache
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory
import org.apache.log4j.Logger
import types.Hit

/**
 *
 * basic class to make it cleaner to implememnt resolvers
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 11:22:05 AM
 *
 */
abstract class AbstractResolver implements Resolveable {

  protected Logger logger = Logger.getLogger("resolver")

  double confidenceLevel

  /**
   * to improve the query time
   */

  Cache cache = SimpleCacheFactory.getInstance().createCache()

  /**
   * contains a list of registered second level caches
   */
  Set<Cache> associatedCaches = new HashSet<Cache>()



   /**
    * constructor 
    */
  public AbstractResolver() {
    this.addCache cache

  }

  def void activateResolver() {

  }

  def void disactivateResolver() {

  }

  /**
   * adds a cache to the associate caches
   */
  void addCache(Cache cache) {
    logger.debug "registering a cache: ${cache}"
    this.associatedCaches.add(cache)
  }
  /**
   * checks if this is already cached
   */
  boolean isCached(String value) {

    //check all registered caches for the value
    for (Cache cache: associatedCaches) {
      if (cache.contains(value.toLowerCase())) {
        return true
      }
    }
    return false
  }

  /**
   * returns the cached object
   */
  Hit getCached(String value) {
    return cache.get(value.toLowerCase())
  }

  /**
   * cache an object
   */
  void cache(Hit hit, String key) {
    if (hit != null && key != null) {
      cache.put(key.toLowerCase(), hit)
    }
  }
}

package annotations

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
import java.lang.annotation.ElementType
import java.lang.annotation.Inherited
/**
 * User: wohlgemuth
 * Date: Feb 10, 2010
 * Time: 7:43:53 PM
 *
 * used to determine if this method is queryable by the
 * transform server
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface Queryable {

  /**
   * the name of the field
   */
  String name()
}
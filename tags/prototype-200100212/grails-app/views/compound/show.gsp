
<html>
    <head>
        <title>Compound Details</title>
    </head>
<style type="text/css">@import url("/compound-repository/css/WebGUIStandards.css");</style>

<body topMargin=0 background="/compound-repository/images/FormBG.gif" >
<br>

<table  border='0' width="70%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td  class="AppsLabel" align="center"> Compound Details </td>
	</tr>

    <tr>
		<td  align='right'>
                  [ <g:link controller="transform" action="transform">Back to result</g:link> ]
				  [ <g:link controller="homePage" action="index">Back to home</g:link> ]
	    </td>
	</tr>

  <tr height='2'> <td> &nbsp; </td> </tr>

  <tr>
  <td>


<table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" >Id</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'id')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" >Inchi</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'inchi')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" >Formula</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'formula')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Molare Mass</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'molareMass')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" >Exact Molare Mass</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'exactMolareMass')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Date Created</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'dateCreated')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Db Links</td>
        <td valign="top" >:</td>
        <td  valign="top" style="text-align:left;" class="Label3">
            <ul>
            <g:each var="d" in="${compoundInstance.dbLinks}">
                <li>${d?.encodeAsHTML()}</li>
            </g:each>
            </ul>
        </td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" >Inchi Hash Key</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${compoundInstance?.inchiHashKey?.encodeAsHTML()}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Iupac</td>
        <td valign="top" >:</td>
        <td  valign="top" style="text-align:left;" class="Label3">
            <ul>
            <g:each var="i" in="${compoundInstance.iupac}">
                <li>${i?.encodeAsHTML()}</li>
            </g:each>
            </ul>
        </td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Last Updated</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'lastUpdated')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Smiles</td>
        <td valign="top" >:</td>
        <td  valign="top" style="text-align:left;" class="Label3">
            <ul>
            <g:each var="s" in="${compoundInstance.smiles}">
                <li>${s?.encodeAsHTML()}</li>
            </g:each>
            </ul>
        </td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Synonyms</td>
        <td valign="top" >:</td>
        <td  valign="top" style="text-align:left;" class="Label3">
            <ul>
            <g:each var="s" in="${compoundInstance.synonyms}">
                <li>${s?.encodeAsHTML()}</li>
            </g:each>
            </ul>
        </td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

</table>
  </td>
  </tr>
  </table>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 4:00:39 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><title>Transform</title></head>

<style type="text/css">@import url("/compound-repository/css/WebGUIStandards.css");</style>


  <body topMargin=0 background="/compound-repository/images/FormBG.gif" >
<g:form name="transForm" controller="transform">
<br>
<table  border='0' width="50%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9" class="AppsLabel" align="center"> Transform </td>
	</tr>

    <tr>
		<td colspan="9" align='right'>
				  [ <g:link controller="homePage" action="index">Back to home</g:link> ]
	    </td>
	</tr>

  <tr height='2'> <td> &nbsp; </td> </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td width="35%"> Id : <g:textField name="id" style="width:200px"/> </td>
    <td width="1%"> &nbsp; </td>
    <td width="20%">From : <g:select name="from" from="${fromFields}"/> </td>
    <td width="1%"> &nbsp; </td>
    <td width="20"> To : <g:select name="to" from="${toFields}" /></td>
    <td width="1%"> &nbsp; </td>
    <td width="20"> &nbsp; <g:actionSubmit value="Submit" action="transform"/> </td>
    <td width="1%"> &nbsp; </td>
  </tr>

</table>
</g:form>

  </body>

</html>
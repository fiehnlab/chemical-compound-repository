<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 4:00:39 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><title>Search</title></head>

<style type="text/css">@import url("/compound-repository/css/WebGUIStandards.css");</style>


<body topMargin=0 background="/compound-repository/images/FormBG.gif" >
<g:form name="searchForm" controller="search">
  <br>
  <table id='tabTitle' border='0' width="60%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3" class="AppsLabel" align="center"> Search </td>
	</tr>

    <tr>
		<td colspan="3" align='right'>
                  <!-- [  <g:link controller="search" action="advancedSearch">Go to advanced search</g:link> ] -->
				  [ <g:link controller="homePage" action="index">Back to home</g:link> ]
	    </td>
	</tr>

  <tr height='5'> <td> &nbsp; </td> </tr>

    <tr>
      <td width="1%"> &nbsp;</td>
      <td valign="top" align='center'> 
        <g:textField name="query" value="" style="width:300px;"></g:textField>
        <g:actionSubmit value="search..."  action="search"/>
      </td>
      <td width="1%"> &nbsp;</td>
    </tr>
  </table>





</g:form>
  </body>
</html>
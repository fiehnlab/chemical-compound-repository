class SearchController {

  SearchService searchService = null

  def index = { }

  /**
   * search against the controller
   */
  def search = {
    assert params.query != null, "you need to provide some a query in the params object"


    def result = searchService.searchGeneric(params.query)

    //forward to the controller
    return[result:result]
  }

  /**
   * advanced search against the controller
   */
  def advancedSearch = {}
}

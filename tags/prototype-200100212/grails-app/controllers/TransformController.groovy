import grails.converters.JSON
import grails.converters.deep.XML

/**
 * provides us with transformations
 */
class TransformController {

  /**
   * does the actual converting of data
   */
  ConvertService convertService

  def index = {
    return [fromFields:convertService.getFromConversions(),toFields:convertService.toConversions]
  }

  /**
   * exspects 3 params and convert them to the given result
   * using the converter service
   */
  def transform = {

    String from = params.from
    String to = params.to
    String id = params.id

    assert from != null, "please provide a from parameter"
    assert to != null, "please provide a to parameter"
    assert id != null, "please provide an id parameter"

    //call the service to conver the data
    Collection<Map<Compound,?>> result =  convertService.convert(from, to, id)

    //decide which modus we use for deployment
    if (params.format != null) {
      if (params.format == 'xml') {
        render result as XML
      }
      else if (params.format == 'json') {
        render result as JSON
      }
      else {
         return [transform:result,id:id,from:from,to:to]
      }
    }
    else {
      return [transform:result,id:id,from:from,to:to]
    }

  }

  /**
   * returns the provide formats
   */
  static List getFormats() {
    return ['xml', 'json']
  }


}

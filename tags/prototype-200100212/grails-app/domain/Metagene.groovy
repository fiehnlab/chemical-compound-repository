class Metagene extends DBLinks {

  static constraints = {
    metageneId(maxSize: 25000, nullable: false)

  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String metageneId;

  String toString() {
    return "Metagene: ${metageneId}"
  }
}

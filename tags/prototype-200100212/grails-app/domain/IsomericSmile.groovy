class IsomericSmile extends Smiles {
  static constraints = {
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }

  String toString() {
    return "Smiles: ${code}"
  }
}

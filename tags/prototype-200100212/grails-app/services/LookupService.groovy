import annotations.Queryable
import org.springframework.beans.factory.InitializingBean
import types.Hit

/**
 * simple service to lookup data from the database
 */
class LookupService implements InitializingBean {

  boolean transactional = true

  def grailsApplication
  def setting

  void afterPropertiesSet() { this.setting = grailsApplication.config.setting }

/**
 * can return 0 - n
 */
  @Queryable(name = "smiles")
  Collection<Compound> lookupBySmile(String value) {
    return Compound.executeQuery("Select a from Compound a, Smiles b where a.id = b.compound.id and b.code = ?", [value])
  }

/**
 * can return 0 - n
 */
  @Queryable(name = "kegg")
  Collection<Compound> lookupByKegg(String value) {
    return Compound.executeQuery("Select a from Compound a, Kegg b where a.id = b.compound.id and b.keggId = ?", [value])
  }

/**
 * can return 0 - n
 */
  @Queryable(name = "cid")
  Collection<Compound> lookupByCID(def value) {

    Integer v = null

    if (value instanceof Integer == false) {
      v = Integer.parseInt(value.toString())
    }
    else {
      v = value
    }
    return Compound.executeQuery("Select a from Compound a, PubchemCompound b where a.id = b.compound.id and b.cid = ?", [v])
  }

/**
 * can return 0-n
 */
  @Queryable(name = "sid")
  Collection<Compound> lookupBySID(def value) {
    Integer v = null

    if (value instanceof Integer == false) {
      v = Integer.parseInt(value.toString())
    }
    else {
      v = value
    }
    return Compound.executeQuery("Select a from Compound a, PubchemSubstance b where a.id = b.compound.id and b.sid = ?", [v])
  }

/**
 * can return 0-n
 */
  @Queryable(name = "cas")
  Collection<Compound> lookupByCas(String value) {
    return Compound.executeQuery("Select a from Compound a, Cas b where a.id = b.compound.id and b.casNumber = ?", [value])
  }

/**
 * can return 0 - 1
 */
  @Queryable(name = "compound")
  Compound lookupByCompoundId(def value) {
    Long v = null
    if (value instanceof Long == false) {
      v = Long.parseLong(value.toString())
    }
    else {
      v = value
    }
    return Compound.get(v)
  }

/**
 * can return 0 - 1
 */
  @Queryable(name = "inchi")
  Compound lookupByInchi(String value) {
    return Compound.find("from Compound a where a.inchi = ?", [value])
  }

  /**
   * can return 0 - 1
   */
  @Queryable(name = "formula")
  Collection<Compound> lookupByFormula(String value) {
    return Compound.findAll("from Compound a where a.formula = ?", [value])
  }

  /**
   * can return 0 - 1
   */
  @Queryable(name = "mass")
  Collection<Compound> lookupByExactMass(def value) {
    Double v = null
    if (value instanceof Double == false) {
      v = Double.parseDouble(value.toString())
    }
    else {
      v = value
    }

    return Compound.findAll("from Compound a where a.exactMolareMass = ?", [v])
  }

/**
 * can return 0 - 1
 */
  @Queryable(name = "inchikey")
  Compound lookupByInchiKey(String value) {
    return Compound.find("from Compound a where a.inchiHashKey.completeKey = ?", [value])
  }

/**
 * can return 0 - n
 */
  @Queryable(name = "skeleton")
  Collection<Compound> lookupByPartialInchiKey(String value) {
    return Compound.findAll("from Compound a where a.inchiHashKey.firstBlock = ?", [value])
  }

  @Queryable(name = "name")
  Collection<Compound> lookupByName(String value) {
    return Compound.executeQuery("Select distinct a from Compound a, Synonym b where a.id = b.compound.id and LOWER(b.name) like ?", [value.toLowerCase()])
  }

  @Queryable(name = "hmdb")
  Collection<Compound> lookupByHMDB(String value) {
    return Compound.executeQuery("Select a from Compound a, HMDB b where a.id = b.compound.id and b.hmdbId = ?", [value])
  }


  @Queryable(name = "lipidmap")
  Collection<Compound> lookupByLipidMapId(String value) {
    return Compound.executeQuery("Select a from Compound a, LipidMap b where a.id = b.compound.id and b.lipidMapId = ?", [value])
  }


  @Queryable(name = "chebi")
  Collection<Compound> lookupByChebiId(String value) {
    return Compound.executeQuery("Select a from Compound a, Chebi b where a.id = b.compound.id and b.chebiId = ?", [value])
  }

/**
 * looks up the compound for a single hit
 * can return 0 - n
 */
  Collection<Compound> lookupByHit(Hit hit) {
    //contains our results
    Collection<Compound> result = new HashSet<Compound>()

    //find out which type our hit is
    switch (hit.type) {
      case hit.INCHI:
        log.debug("detected inchi...")
        result.add(lookupByInchi(hit.value))
        break
      case hit.INCHI_KEY:
        log.debug("detected inchi key...")

        result.add(lookupByInchiKey(hit.value))
        break
      case hit.CAS:
        log.debug("detected cas...")

        result = lookupByCas(hit.value)
        break
      case hit.KEGG:
        log.debug("detected kegg...")

        result = lookupByKegg(hit.value)
        break
      case hit.COMPOUND_ID:
              log.debug("detected compound id...")

              result.add(lookupByCompoundId(hit.compoundId))
              break
      case hit.OSCAR:
              log.debug("detected oscar hit...")

              result = lookupByName(hit.value)
              break

      default:
        break
    }

    //print our model
    log.debug("model: ${result}")

    //return the result
    return result
  }

}

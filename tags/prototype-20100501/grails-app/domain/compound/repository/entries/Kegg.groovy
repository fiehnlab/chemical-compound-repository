package compound.repository.entries
/**
 * kegg informations
 */

class Kegg extends DBLinks {


  static searchable = {

    keggId index: 'not_analyzed', name : "kegg"
  }
  
  static constraints = {
    keggId(maxSize: 6)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String keggId


  String toString() {
    return "KEGG: ${keggId}";
  }
}

package compound.repository.entries
class Metagene extends DBLinks {


  static searchable = {

    metageneId index: 'not_analyzed', name : "metagene"
  }

  static constraints = {
    metageneId(maxSize: 25000, nullable: false)

  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String metageneId;

  String toString() {
    return "Metagene: ${metageneId}"
  }
}

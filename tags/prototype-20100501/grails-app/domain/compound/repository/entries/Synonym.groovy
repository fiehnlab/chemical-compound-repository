package compound.repository.entries
/**
 * a simple synonym for a compound
 */

class Synonym {

  /**
   * makes this synonym searchable
   */
  static searchable = {
    spellCheck "include"
    rating index: 'not_analyzed'
  }
  
  static belongsTo = [compound: Compound]

  static constraints = {
    name(maxSize: 15000)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
    rating(unique: false, nullable: true)

  }

  //the name for the synonym
  String name

  //rating for the synonym how common it is
  int rating = 0

  public String toString() {
    return "Synonym: ${name}"
  }

  Date dateCreated
  Date lastUpdated

}



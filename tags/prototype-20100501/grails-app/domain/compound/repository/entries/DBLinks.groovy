package compound.repository.entries
/**
 * main class for database links
 */

class DBLinks {
  static belongsTo = [compound: Compound]

  static searchable = true

  static constraints = {
  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  Date dateCreated
  Date lastUpdated

}

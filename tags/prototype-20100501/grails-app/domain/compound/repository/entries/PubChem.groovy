package compound.repository.entries
class PubChem extends DBLinks {

  static searchable = true
  
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  static constraints = {
  }
}

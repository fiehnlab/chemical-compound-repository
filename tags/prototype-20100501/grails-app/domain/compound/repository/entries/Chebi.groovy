package compound.repository.entries

/**
 * defines a chebi identifier
 */

class Chebi extends DBLinks {


  static searchable = {

    chebiId index: 'not_analyzed', name : "chebi"
  }
  
  static constraints = {
    chebiId(unique: false, nullable: false)

  }

  String chebiId

  String toString() {
    return "Chebi: ${chebiId}"
  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
}

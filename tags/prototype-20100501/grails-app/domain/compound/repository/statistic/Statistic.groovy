package compound.repository.statistic

abstract class Statistic {

  static constraints = {
  }

  /**
   * calculates the actual amount
   * @return
   */
  //public abstract long calculateAmmount()

  /**
   * calculates the date properties
   * @return
   */
  //public abstract long calculateDateProperties()

  /**
   * how many entries do we have
   */
  long ammount

  /**
   * hours of the date
   */
  int hour

  /**
   * day of the date
   */
  int day

  /**
   * month of the date
   */
  int month

  /**
   * year of the date
   */
  int year

}

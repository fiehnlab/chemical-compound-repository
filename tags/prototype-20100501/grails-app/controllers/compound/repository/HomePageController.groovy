package compound.repository

import compound.repository.entries.Compound

class HomePageController {

    def index = {

      // Remove session attribute
      if( session.getAttribute("ids") != null )
      {
        session.removeAttribute "ids"
      }
      if( session.getAttribute("query") != null )
      {
        session.removeAttribute "query"
      }

      int size = Compound.count()
      session.setAttribute("PrototypeSize", String.valueOf(size));

      def strQuery = "";
      if (params.query != null) {
        strQuery = params.query
      }

      return [compounds:size,query: strQuery]
    }
}

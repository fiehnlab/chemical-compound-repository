import grails.util.GrailsUtil
import javax.sql.DataSource
import test.GrailsDBunitIntegrationTest
import org.dbunit.dataset.xml.XmlDataSet
import com.fiehn.util.dbunit.DBunitUtil
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.commons.ApplicationHolder
import org.apache.lucene.search.BooleanQuery
import compound.repository.entries.Compound

class BootStrap {

  DataSource dataSource

  SearchableService searchableService

  def init = { servletContext ->
    if (GrailsUtil.getEnvironment() == GrailsApplication.ENV_TEST) {


      log.info("initializing test dataset...")
      DBunitUtil.intialize(dataSource, new XmlDataSet(new FileReader(GrailsDBunitIntegrationTest.DEFAULT_DATASET
      )))
      log.info("database size: ${Compound.count()}")

      log.info("force reindex of database...")

      searchableService.reindex()

      log.info("you should now be good to go")
    }
    //we want to have a max of like 20k values 
    BooleanQuery.setMaxClauseCount 20000
  }
  def destroy = {
  }
} 

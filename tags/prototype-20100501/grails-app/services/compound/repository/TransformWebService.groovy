package compound.repository

import grails.converters.JSON
import grails.converters.XML

/**
 * provides a webservice to transform compounds from any given name
 */
class TransformWebService {

  static expose = ['xfire']

  boolean transactional = true

  ConvertService convertService

  /**
   * does the actual transformation
   * @param from
   * @param to
   * @param value
   * @return a collection of strings for the transformed values
   */
  String transformToXmlString(String from, String to, String value) {
    return convertService.convert(from, to, value) as XML

  }

  /**
   * does the actual transformation
   * @param from
   * @param to
   * @param value
   * @return a collection of strings for the transformed values
   */
  String transformToDeepXmlString(String from, String to, String value) {
    return convertService.convert(from, to, value) as grails.converters.deep.XML

  }

  /**
   * does the actual transformation
   * @param from
   * @param to
   * @param value
   * @return a collection of strings for the transformed values
   */
  String transformToJSONString(String from, String to, String value) {
    return convertService.convert(from, to, value) as JSON

  }

  /**
   * does the actual transformation
   * @param from
   * @param to
   * @param value
   * @return a collection of strings for the transformed values
   */
  String transformToDeepJSONString(String from, String to, String value) {
    return convertService.convert(from, to, value) as grails.converters.deep.JSON

  }


}

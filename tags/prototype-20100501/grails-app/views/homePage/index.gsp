<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 12:01:05 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head></head>


<body class="BodyBackground"  >

<div id="contents">

  <div id="main">

<table width="1000px"  border="0" align="center" height="100%">
<tr>
  <td>
        <table align='center'>
				<tr align='center' valign='bottom'>
                  <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-SIZE: 21px;FONT-FAMILY: 'Verdana';TEXT-TRANSFORM: capitalize">

                      <g:message code="PROJECT_TITLE" />

                  </td>
                </tr>
                <tr>

                  <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-FAMILY: 'Verdana';">
                      <g:message code="PROTOTYPE_MSG" args='["${compounds}"]' />
                  </td>
                </tr>
		</table>
  </td>
</tr>

  <tr>
    <td>
        <div id="Left">
        <g:form name="searchForm" controller="search" method="post">

          <h3> Welcome </h3>
          <div class="line"></div>
           <p class="Label3">
              &nbsp;&nbsp;&nbsp;Publicly available chemical information including structures, chemical names, chemical synonyms, database identifiers, molecular masses, XlogP and proton-donor/acceptor data were downloaded from different databases and combined into a single internal repository for compound-specific, structure-based cross references.
              Molfile (SDF) to InChI code converters (vs. 1.0.2) and InChI code to InChI key converters were integrated into the tool set to allow any type of query access, e.g. by chemical names, structures, database identifiers.
              For data storage and retrieval we are using a PostgresSQL database.
           </p>
          <p class="Label3">
              &nbsp;&nbsp;&nbsp;This database allows executing queries at far higher speed compared to web-based applications that query external repositories in real time.
              For accessibility we use a web GUI and a soap-based application-programming interface was implemented for automated access.
          </p>

          <h3> Search </h3>
          <div class="line"></div>

          <table id='tabTitle' border='0' width="100%" align='center' cellpadding="0" cellspacing="0">

            <tr height='2'> <td colspan="3" align='center'> &nbsp;

             <g:if test="${flash.message}">
               <div class="AlertLabel">
                  ${flash.message}
               </div>
             </g:if>

           </td> </tr>

           <tr>
              <td width="1%"> &nbsp;</td>
              <td valign="top" align='center'>
                <g:textField name="query" value="${params.query?params.query:''}" style="width:300px;" tabindex='1'></g:textField>
                <g:actionSubmit value="Search..."  action="textSearch"/>
               <!--
                <g:link controller="search" action="textSearch" >
                <img border="#ffffff" src="${createLinkTo(dir: 'images',file : 'info16X16.png')}" title="Click to get info">
                </g:link>
                -->
              </td>
              <td width="1%"> &nbsp;</td>
            </tr>

            <tr>
            <td width="1%"> &nbsp; </td>
            <td align="center" >
              <table>
                <tr>
                  <td width="5%">
                       Tips:
                  </td>
                  <td>
                    &nbsp;
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td class="LableTips" align="left">
                       Enter Query Like<br>chebi:*&nbsp;,glucose&nbsp;,benzene&nbsp;,C00001&nbsp;,XLYOFNOQVPJJNP-UHFFFAOYSA-N etc.
                  </td>
                </tr>
              </table>
            </td>
            <td width="1%"> &nbsp; </td>
          </tr>


          </table>
</g:form>

      </div>

      <div id="Right">
       <h3> More Links... </h3>
          <div class="line"></div>
      <table border="0" id="tblSample" width="100%" align='center' cellpadding="0" cellspacing="0" >
        <tr><td width="1%" height="1px">&nbsp;</td></tr>
        <tr>
          <td width='1%'>&nbsp;</td>
          <td title="Discover">
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td><g:link controller="discovery"><img border="#ffffff" src="${createLinkTo(dir: 'images',file : 'discover16X16.png')}"></g:link> </td>
                  <td class="AppsLabel" aligm="left"><g:link controller="discovery"><span class="AppsLabel"><g:message code="DISCOVER" /></span></g:link> </td>
                </tr>
                <tr>
                  <td></td>
                  <td aligm="left"><g:link controller="discovery"><span class="Label3"><g:message code="DISCOVER.DESCRIPTION" /></span></g:link> </td>
                </tr>
              </table>
          </td>

          <td width='1px'>&nbsp;</td>
        </tr>
        <tr><td width="1%" height="1px">&nbsp;</td></tr>
        <tr>
          <td width='1%'>&nbsp;</td>
          <td title="Convert">
              <table width="100%" cellpadding="0" cellspacing="0" >
                <tr>
                  <td><g:link controller="transform"><img border="#ffffff" src="${createLinkTo(dir: 'images',file : 'convert16x16.png')}"></g:link> </td>
                  <td  aligm="left"><g:link controller="transform"><span class="AppsLabel"><g:message code="TRANSFORM" /></span></g:link></td>
                </tr>
                <tr>
                  <td></td>
                  <td class="Label3" aligm="left"><g:link controller="transform"><span class="Label3"><g:message code="TRANSFORM.DESCRIPTION" /></span></g:link> </td>
                </tr>
              </table>
          </td>

          <td width='1%'>&nbsp;</td>
        </tr>

        <tr><td width="1%" height="1px">&nbsp;</td></tr>
        <tr>
          <td width='1%'>&nbsp;</td>
          <td title="Batch Convert">
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td><g:link controller="massLookup"><img border="#ffffff" src="${createLinkTo(dir: 'images',file : 'masConvert16x16.png')}"></g:link> </td>
                  <td class="AppsLabel" aligm="left"><g:link controller="massLookup"><span class="AppsLabel"><g:message code="MASS_LOOKUP" /></span></g:link> </td>
                </tr>
                <tr>
                  <td></td>
                  <td class="Label3" aligm="left"><g:link controller="massLookup"><span class="Label3"><g:message code="MASS_LOOKUP.DESCRIPTION" /></span></g:link> </td>
                </tr>
              </table>
          </td>

          <td width='1%'>&nbsp;</td>
        </tr>
  <!--
        <tr><td width="1%" height="1px">&nbsp;</td></tr>
         <tr>
          <td width='1%'>&nbsp;</td>
          <td title="Utility Applications">
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td><g:link controller="utilApps" action="index"><img border="#ffffff" src="${createLinkTo(dir: 'images',file : 'UtilApps.png')}"></g:link> </td>
                  <td class="AppsLabel" aligm="left"><g:link controller="utilApps" action="index"><span class="AppsLabel"><g:message code="UTIL_APPS" /></span></g:link></td>
                </tr>
                <tr>
                  <td></td>
                  <td class="Label3" aligm="left"><g:link controller="utilApps" action="index"><span class="Label3"><g:message code="UTIL_APPS.DESCRIPTION" /></span></g:link> </td>
                </tr>
              </table>
          </td>

          <td width='1%'>&nbsp;</td>
        </tr>
  -->
        <tr><td height="1%">&nbsp;</td></tr>
        <tr><td width="1%" height="1px">&nbsp;</td></tr>
       </table>
        </div>

    </td>
  </tr>



</table>


</div>


<div id="footer">
  <g:render template="/footer" />
</div>
  </div>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 22, 2010
  Time: 11:06:39 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><title><g:message code="MASS_LOOKUP" /></title></head>


<body class="BodyBackground"  >
<g:form name="masslookup" controller="massLookup">
  
<div id="contents">

  <div id="main">

<table width="1000px"  border="0" align="center" height="100%">
  <tr>
    <td>
      <table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
       <tr align='center' valign='bottom'>
          <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-SIZE: 21px;FONT-FAMILY: 'Verdana';TEXT-TRANSFORM: capitalize">

              <g:message code="PROJECT_TITLE" />

          </td>
        </tr>
        <tr>

          <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-FAMILY: 'Verdana';">
              <g:message code="PROTOTYPE_MSG" args='["${session.getAttribute("PrototypeSize")}"]' />
          </td>
        </tr>
     <!--   <tr>
            <td  align='right' width="100%">
                      [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
            </td>
        </tr>      -->
     </table>

    </td>
  </tr>

   <tr>
     <td>
       <div id="pagePath">
			<h1><g:link controller="homePage" action="index">Home</g:link> <span>&raquo;</span> <g:message code="MASS_LOOKUP" /></h1>
		</div>
<div class="line"></div>
<table  border='0' width="100%" align='center' cellpadding="0" cellspacing="3" >
	
   <tr height='2'> <td colspan="5"> &nbsp;

   <g:if test="${flash.message}">
     <div class="AlertLabel">
        ${flash.message}
     </div>
   </g:if>

   </td> </tr>
  
  <tr>
      <td width="1%"> &nbsp; </td>
      <td width="5%" valign="top"> Ids </td>
      <td width="1%" valign="top"> : </td>
      <td width= "95%"> <g:textArea name="ids" value="${ids}" rows="10" cols="80" tabindex='1' ></g:textArea> </td>
      <td width="1%"> &nbsp; </td>
  </tr>
  <tr>
    <td width="1%"> &nbsp; </td>
    <td width="5%" valign="top"> From </td>
    <td width="1%" valign="top"> : </td>
    <td width="10%"><g:select name="from" from="${fromFields}" optionValue="VALUE" optionKey="ID" value="${from}" noSelection="${['null': '--- Select ---']}" tabindex='2' />



    </td>
    <td width="1%"> &nbsp; </td>
  </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td width="5%" valign="top"> To </td>
    <td width="1%" valign="top"> : </td>
    <td width= "60%">
      <table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
      <%
        int liCnt = 0;

        for(int i=0 ; i < toFields.size() ; i++)
          {
       %>
          <tr>
       <%
            ArrayList list = to
            for(int j=0 ; j < 4 && i < toFields.size() ; j++)
            {
              String id = ((Map)toFields.get(i)).get("ID");
              String value = ((Map)toFields.get(i)).get("VALUE");

         if( list!=null && list.contains(id) )
         {
            %>
          <td class="Label3" > <g:checkBox name="to" value="${id}" checked="true" tabindex='${2+i+1}' /> ${value} </td>
            <%}else{
              %>
                 <td class="Label3"> <g:checkBox name="to" value="${id}" checked="false"  tabindex='${2+i+1}' /> ${value} </td>
            <%  }
                i++;  
              }
              i--;
            %>
          </tr>
        <%
          }
        %>
       </table>

    </td>
    <td width="1%"> &nbsp; </td>
  </tr>
  
  <tr height='1'> <td> &nbsp; </td> </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td colspan="3" align="center"> &nbsp; <g:actionSubmit value="Submit" action="masstransform" controler="massLookup" tabindex='${2+toFields.size()+1}' /></td>
    <td width="1%"> &nbsp; </td>
  </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td colspan="3" align="left" >
      <table>
        <tr>
          <td width="5%">
               Tips:
          </td>
          <td>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="LableTips" align="left">
               <g:message code="MASS_LOOKUP.NOTES" />
          </td>
        </tr>
      </table>
    </td>
    <td width="1%"> &nbsp; </td>
  </tr>
</table>
  </td>
</tr>
  </table>
</div>
<div id="footer">
  <g:render template="/footer" />
</div>
  </div>

 </g:form>
  </body>

</html>
package compound.repository

import resolver.query.SynonymQuery
import test.GrailsDBunitIntegrationTest
import types.Hit

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Mar 9, 2010
 * Time: 11:54:22 AM
 * To change this template use File | Settings | File Templates.
 */
class SynonymQueryTests extends GrailsDBunitIntegrationTest {

  public void testQuery() {
    SynonymQuery query = new SynonymQuery()

    Set<Hit> set = query.executeQuery("2-hexaprenyl-6-methoxyphenol")

    assertTrue(set.size() == 1)
  }

  public void testQueryBatch() {
    SynonymQuery query = new SynonymQuery()
    query.addQuery "2-hexaprenyl-6-methoxyphenol"

    Set<Hit> set = query.executeQuery()

    assertTrue(set.size() == 1)
  }


}

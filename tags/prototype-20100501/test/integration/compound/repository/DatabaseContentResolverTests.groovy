package compound.repository

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory
import grails.test.GrailsUnitTestCase
import resolver.DatabaseContentResolver
import resolver.Resolveable
import types.Hit
import test.GrailsDBunitIntegrationTest

/**
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 2:14:01 PM
 *
 */
class DatabaseContentResolverTests extends GrailsDBunitIntegrationTest
{
  protected void setUp() {
    super.setUp()
  }

  protected void tearDown() {
    super.tearDown()
  }

  public void testResolve()

  {
    SimpleCacheFactory.newInstance().createCache().getCache().clear()

    Resolveable resolve = DatabaseContentResolver.getInstance()

    resolve.activateResolver()
    def result = resolve.resolve("1,3,4,5-tetracaffeoylquinic acid")

    logger.info "result: ${result}"
    
    resolve.disactivateResolver()

    assertTrue(result.size() == 1)

    Hit hit = result.asList().get(0)

    assertTrue(hit.type == Hit.COMPOUND_ID)
    assertTrue(hit.compoundId == 932024)


  }


  public void testResolveBatchMode() {

    SimpleCacheFactory.newInstance().createCache().getCache().clear()

    Resolveable resolve = DatabaseContentResolver.getInstance()

    resolve.activateResolver()
    resolve.addBatch "1,3,4,5-tetracaffeoylquinic acid"
    resolve.addBatch "sabrina"
    resolve.addBatch "cindy"
    
    def result = resolve.resolve()

    println result

    resolve.disactivateResolver()

    assertTrue(result.size() == 1)

    Hit hit = result.asList().get(0)

    assertTrue(hit.type == Hit.COMPOUND_ID)
    assertTrue(hit.compoundId == 932024)


  }


}

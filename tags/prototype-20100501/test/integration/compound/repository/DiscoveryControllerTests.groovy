package compound.repository

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory
import test.GrailsDBunitIntegrationTest
import compound.repository.LookupService
import compound.repository.DiscoveryService
import compound.repository.DiscoveryController

class DiscoveryControllerTests  extends GrailsDBunitIntegrationTest {

  DiscoveryController controller
  //our test document
  String document = """"


    In this discovery test we would like to discover (S)-linalool and (4S,7R)-4-isopropenyl-7-methyloxepan-2-one. It would also
    be nice to discover things like 1-Methylhistidine or 2-Hydroxybutyric acid.

    To round it all up some (+)-Iridodial and 1,3,4,5-Tetracaffeoylquinic acid are always nice to have
    """

  protected void setUp() {
    super.setUp()
    //overwrite the getPArams methode, basically a simple mocking
    DiscoveryController.metaClass.getParams = {-> [query: document] }

    //there has to be a better way...
    controller = new DiscoveryController()
    controller.discoveryService = new DiscoveryService()
    controller.discoveryService.lookupService = new LookupService()

  
    SimpleCacheFactory.newInstance().createCache().getCache().clear()
    
  }

  protected void tearDown() {
    super.tearDown()
    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }

  void testProcessing() {

  def result = controller.process()

    //make sure we don't have a null result
    assertTrue result != null

    //we exspect 6 results

    println result.result
    println "hits: ${result.result}"
    println result.size()
    assertTrue result.result.size() == 6

    println "success"
  }


}

package compound.repository

import test.GrailsDBunitIntegrationTest
import compound.repository.LookupService
import compound.repository.SearchService

/**
 * tests the search service
 */
class SearchServiceTests extends GrailsDBunitIntegrationTest {
  SearchService service = null

  def searchableService

  protected void setUp() {
    super.setUp()
    service = new SearchService()
    service.lookupService = new LookupService()
  }

  protected void tearDown() {
    super.tearDown()
  }

  protected void preSetup() {
    super.preSetup()
    searchableService.reindex()
  }

  public void testSearchGenericSTDInchi() {
    assertTrue(service.searchGeneric("InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1").results[0].id == 895880)
  }


  public void testSearchGenericSTDInchiKey() {
    assertTrue(service.searchGeneric("PRZXKPDANWDCNC-IBQHSZKZSA-N").results[0].id == 932070)
  }


  public void testSearchGenericKegg() {
    assertTrue(service.searchGeneric("C10020").results[0].id == 931478)
    assertTrue(service.searchGeneric("C10021").results[0].id == 931481)
  }

  public void testSearchLipidMaps() {
    assertTrue(service.searchGeneric("LMFA00000001").results[0].id == 931396)
  }


  public void testSearchHMDB() {
    assertTrue(service.searchGeneric("HMDB00001").results[0].id == 895880)
    assertTrue(service.searchGeneric("HMDB00002").results[0].id == 895883)
  }


  public void testSearchSID() {
    assertTrue(service.searchGeneric("SID:74380251").results[0].id == 931396)
    assertTrue(service.searchGeneric("SID:74380252").results[0].id == 931402)

    assertTrue(service.searchGeneric("sid:74380251").results[0].id == 931396)
    assertTrue(service.searchGeneric("sid:74380252").results[0].id == 931402)

  }



  public void testSearchCID() {
    assertTrue(service.searchGeneric("cid:1").results[0].id == 931715)
    assertTrue(service.searchGeneric("cid:2").results[0].id == 931723)

    assertTrue(service.searchGeneric("CID:1").results[0].id == 931715)
    assertTrue(service.searchGeneric("CID:2").results[0].id == 931723)

  }

  public void testSearchExactName() {
    assertTrue(service.searchGeneric("2,6-dichlorobenzonitrile").results[0].id == 932141)
    assertTrue(service.searchGeneric("2-methyl-1,3-dinitrobenzene").results[0].id == 932149)
  }

  public void testSearchLikelyName() {
    assertTrue(service.searchGeneric("b%").results.size() == 2)
  }


  public void testSearchGenericSTDInchiLucene() {
    assertTrue(service.textSearch("InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1").results[0].id == 895880)
  }


  public void testSearchGenericSTDInchiKeyLucene() {
    assertTrue(service.textSearch("PRZXKPDANWDCNC-IBQHSZKZSA-N").results.size() == 1)
  }


  public void testSearchGenericKeggLucene() {
    assertTrue(service.textSearch("kegg:C10020").results[0].id == 931478)
    assertTrue(service.textSearch("kegg:C10021").results[0].id == 931481)
  }

  public void testSearchLipidMapsLucene() {
    assertTrue(service.textSearch("lipidmap:LMFA00000001").results[0].id == 931396)
  }


  public void testSearchHMDBLucene() {
    assertTrue(service.textSearch("hmdb:HMDB00001").results[0].id == 895880)
    assertTrue(service.textSearch("hmdb:HMDB00002").results[0].id == 895883)
  }


  public void testSearchSIDLucene() {
    assertTrue(service.textSearch("sid:74380251").results[0].id == 931396)
    assertTrue(service.textSearch("sid:74380252").results[0].id == 931402)

  }



  public void testSearchCIDLucene() {
    assertTrue(service.textSearch("cid:1").results[0].id == 931715)
    assertTrue(service.textSearch("cid:2").results[0].id == 931723)

  }

  public void testSearchExactNameLucene() {
    assertTrue(service.textSearch("2,6-dichlorobenzonitrile").results[0].id == 932141)
    assertTrue(service.textSearch("2-methyl-1,3-dinitrobenzene").results[0].id == 932149)
  }

  public void testSearchLikelyNameLucene() {
    assertTrue(service.textSearch("name:b*").results.size() == 10)
  }


}

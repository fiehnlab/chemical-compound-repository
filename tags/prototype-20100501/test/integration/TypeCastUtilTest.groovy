import grails.test.ControllerUnitTestCase
import util.TypeCastUtil
/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 15, 2010
 * Time: 4:03:49 PM
 * To change this template use File | Settings | File Templates.
 */
class TypeCastUtilTest extends GroovyTestCase {

    TypeCastUtil lObjTypeCastUtil = null

    void testCastIntoInteger()
    {
         lObjTypeCastUtil = TypeCastUtil.getInstance()
         def input = "10" // Check with String
         def result = lObjTypeCastUtil.castIntoInteger(input)
         assertTrue result == 10

         input = 10 // Check with number
         result = lObjTypeCastUtil.castIntoInteger(input)
         assertTrue result == 10

         input = 1010101010 // Check with number
         result = lObjTypeCastUtil.castIntoInteger(input)
         assertTrue result == 1010101010

         input = ["10","20","30"] // Check with String array
         result = lObjTypeCastUtil.castIntoInteger(input)
         assertTrue result.size() == 3

         input = [10,20,30] // Check with Integer array
         result = lObjTypeCastUtil.castIntoInteger(input)
         assertTrue result.size() == 3
    }


    void testCastIntoLong()
    {
         lObjTypeCastUtil = TypeCastUtil.getInstance()
         def input = "10" // Check with String
         def result = lObjTypeCastUtil.castIntoLong(input)
         assertTrue result == 10

         input = 10 // Check with number
         result = lObjTypeCastUtil.castIntoLong(input)
         assertTrue result == 10

         input = 101010101010 // Check with number
         result = lObjTypeCastUtil.castIntoLong(input)
         assertTrue result == 101010101010





         input = ["10","20","30"] // Check with String array
         result = lObjTypeCastUtil.castIntoLong(input)
         assertTrue result.size() == 3

         input = [10,20,30] // Check with Integer array
         result = lObjTypeCastUtil.castIntoLong(input)
         assertTrue result.size() == 3

//         input = [10.0,20.5,30.27] // Check with Integer array
//         result = lObjTypeCastUtil.castIntoLong(input)
//         assertTrue result.size() == 3 
    }

    void testCastIntoDouble()
    {
         lObjTypeCastUtil = TypeCastUtil.getInstance()
         def input = "10" // Check with String
         def result = lObjTypeCastUtil.castIntoDouble(input)
         assertTrue result == 10

         input = 10 // Check with number
         result = lObjTypeCastUtil.castIntoDouble(input)
         assertTrue result == 10

         input = ["10","20","30"] // Check with String array
         result = lObjTypeCastUtil.castIntoDouble(input)
         assertTrue result.size() == 3

         input = [10,20,30] // Check with Integer array
         result = lObjTypeCastUtil.castIntoDouble(input)
         assertTrue result.size() == 3

         input = [10.1,20.2,30.3] // Check with Integer array
         result = lObjTypeCastUtil.castIntoDouble(input)
         assertTrue result.size() == 3
    }






}

import grails.test.*
import compound.repository.entries.Cas

class CasTests extends GrailsUnitTestCase {

  Cas cas

  protected void setUp() {
    super.setUp()
    cas = new Cas()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testToString() {

    cas.casNumber = "444-44-0"

    assertTrue(cas.toString() == "CAS: 444-44-0")
  }

  void testSetCasNumber() {
    cas.casNumber = "3434-43-3"

    assertTrue(cas.casNumber == "3434-43-3")
  }
}

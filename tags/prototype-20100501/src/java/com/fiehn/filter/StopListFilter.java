package com.fiehn.filter;

import com.fiehn.filter.exception.FilterException;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.*;

/**
 * filter based on a stop list
 *
 * @author pradeep
 */
public class StopListFilter implements Filter {

    private Logger logger = Logger.getLogger("filter");
    /**
     * contains the actual data
     */
    private Collection<String> list = null;

    /**
     * are we going to be case sensitive
     */
    private boolean casesensitive = true;

    public void configure(Object config, boolean caseSensitive) {

        this.casesensitive = caseSensitive;

        if (config == null) {
            throw new FilterException("sorry the provided config object was null!");
        }

        if (config instanceof InputStream) {

            logger.debug("intialize stoplist...");
            list = new HashSet<String>();

            Scanner scanner = new Scanner((InputStream) config);

            while (scanner.hasNextLine()) {
                if (casesensitive) {
                    list.add(scanner.nextLine().trim());
                } else {
                    list.add(scanner.nextLine().trim().toLowerCase());

                }
            }
        }
    }

    /**
     * filters against the internal stop list
     */
    public Set<String> filter(Set<String> content) {

        Set<String> result = new HashSet<String>();

        for (String s : content) {
            String temp = s;

            if (this.casesensitive == false) {
                temp = s.toLowerCase();
            }

            if (!this.list.contains(temp)) {
                result.add(s);
            }

        }
        // should filter the list
        return result;
    }

    public boolean isCaseSensitive() {
        return this.casesensitive;
    }
}

package util

import annotations.QueryObject
import compound.repository.ConvertService
import compound.repository.LookupService
import annotations.AnnotationHelper

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Apr 27, 2010
 * Time: 8:11:42 PM
 * To change this template use File | Settings | File Templates.
 */
class ConvertHelper {

  def g = new org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib()
  
  /**
   * index over the required keys
   */
  private Map<String, String> methodFromNames = new HashMap<String, String>()

  /**
   * index over the required keys
   */
  private Map<String, String> methodToNames = new HashMap<String, String>()

  /**
   * internal instance
   */
  private static ConvertHelper instance = null

  private ConvertHelper() {

    //contains all possible froms
    Collection<QueryObject> fromSet = AnnotationHelper.getQueryableAnnotations(LookupService.class)

    //contains all possible tos
    Collection<QueryObject> toSet = AnnotationHelper.getQueryableAnnotations(ConvertService.class)

    //generate our lookup association
    for (QueryObject q: fromSet) {
      methodFromNames.put(q.annotation.name().toLowerCase(), q.getMethod().getName())
    }

    //generate our lookup association
    for (QueryObject q: toSet) {
      methodToNames.put(q.annotation.name().toLowerCase(), q.getMethod().getName())
    }
  }

  /**
   * returns the instance
   * @return
   */
  public static ConvertHelper getInstance() {
    if (instance == null) {
      instance = new ConvertHelper()
    }

    return instance
  }
  /**
   * from what can we convert
   */
  Collection getFromConversions() {
    return getOptionList(methodFromNames.keySet().sort())
  }

  /**
   * get to conversions
   */
  Collection getToConversions() {
    return getOptionList(methodToNames.keySet().sort())
  }

  /**
   * gets the from method name
   * @param from
   * @return
   */
  String getFromMethodName(String from) {
    return methodFromNames.get(from)
  }

  /**
   * gets the to method name
   * @param to
   * @return
   */
  String getToMethodName(String to) {
    return methodToNames.get(to)
  }

  /**
   * Generate the option list
   */
  private Collection getOptionList(def data) {
    def list = new ArrayList();
    def map = [:]
    data.each {String s ->
      map = new HashMap()
      map.put("ID", s)
      map.put("VALUE", g.message(code: s, default: s, encodeAs: "HTML"))
      list.add(map)
    }
    //println list
    return list
  }
}

package resolver.query

/**
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 7:10:12 PM
 */
class SynonymQuery extends AbstractQuery {

  protected String getClassOfInterrest() {
    return "compound.repository.entries.Synonym";
  }

  protected String getQuerySymbol() {
    return "name";
  }
}

package resolver.analyzer

import pattern.PatternHelper

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Mar 12, 2010
 * Time: 3:06:45 PM
 * simple way to generate annalyerz for us
 */
class AnalyzerFactory {

  /**
   * returns an analyzer specialized on compounds
   * @return
   */
  public static Analyzer getCompoundAnalyzer() {
    return new SimpleTextAnalyzer()
  }

/**
 * returns an analyzer specialiced on cas numbers
 * @return
 */
  public static Analyzer getCasAnalyzer() {
    return new RegularExpressionAnalyzer(PatternHelper.CAS_PATTERN)
  }

/**
 * returns an analyzer specialiced on kegg numbers
 * @return
 */
  public static Analyzer getKeggAnalyzer() {
    return new RegularExpressionAnalyzer(PatternHelper.KEGG_PATTERN)
  }

  /**
   * analyzer specialiced for inchi codes
   * @return
   */
  public static Analyzer getInchiCodeAnalyzer() {
    return new RegularExpressionAnalyzer(PatternHelper.STD_INCHI_PATTERN)
  }

  /**
   * analyzer specialiced on inchi hash keys
   * @return
   */
  public static Analyzer getInchiHashKeyAnalyzer() {
    return new RegularExpressionAnalyzer(PatternHelper.STD_INCHI_KEY_PATTERN)
  }

  /**
   * analyzer specialiced on inchi hash keys
   * @return
   */
  public static Analyzer getHMDBAnalyzer() {
    return new RegularExpressionAnalyzer(PatternHelper.HMDB_PATTERN)
  }
  

}

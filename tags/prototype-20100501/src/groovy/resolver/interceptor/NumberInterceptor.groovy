package resolver.interceptor

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Mar 11, 2010
 * Time: 9:50:05 PM
 * filters all numbers
 */
class NumberInterceptor implements ResolveableInterceptor{
  def Set<String> intercept(Set<String> queries) {

    Set<String> result = new HashSet<String>()
    queries.each {
      try{
        Double.parseDouble(it)

        //if this is reached we wont add it
      }
      catch (Exception e){
        result.add it
      }
    }

    return result
  }
}

import edu.ucdavis.genomics.metabolomics.binbase.connector.references.pubchem.PubchemSubstanceSDFResolver
import org.apache.log4j.Logger
import edu.ucdavis.genomics.metabolomics.binbase.connector.references.sdf.*
import edu.ucdavis.genomics.metabolomics.binbase.connector.references.chebi.ChebiContentResolver

//configuration file
def config = new ConfigSlurper().parse(new File('grails-app/conf/PathConfiguration.groovy').toURL())

Logger logger = Logger.getLogger("ImportGLAbbrev")

logger.info "import gl abbrev files"
String tempDir = config.files.gl.rawdata

FileHelper.workOnDir(tempDir, {File file ->
  GLFinder sub = new GLFinder()
  SdfToInchi.sdfToInchi(new FileInputStream(file), sub)

}, logger, ".sdf")

class GLFinder implements InchFinder {
  private int counter = 0

  String last = ""

  void foundInchi(String s, Map<Object, Object> objectObjectMap) {
    Logger logger = Logger.getLogger("ImportGLAbbrev")

    if (!s.equals(last)) {

      GLAbbrevSDFContentResolver resolver = new GLAbbrevSDFContentResolver()
      resolver.prepare objectObjectMap

      Compound compound = CompoundHelper.getCompound(resolver.getInchi(), resolver.getInchiKey(), logger)

      List<String> synonyms = new ArrayList<String>();
      List<String> iupacs = new ArrayList<String>();

      synonyms.add(resolver.getAbreviation())
      iupacs.add(resolver.getName())
      
      CompoundHelper.addSynonym synonyms, logger, compound

      CompoundHelper.updateIUPACNames(iupacs, compound, logger)

      CompoundHelper.saveCompound compound, logger

      CompoundHelper.aquireStatistic()

      counter++

      logger.debug "import count: " + counter

      last = s

    }
  }
}
import util.ConvertInchi

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Mar 10, 2010
 * Time: 4:12:02 PM
 * To change this template use File | Settings | File Templates.
 */
class ConvertInchiTests  extends GroovyTestCase {

  public void testConvertInchiToMolareMass(){

    def value = ConvertInchi.convertInchiToMolareMass("InChI=1S/C7H8O4/c8-5-3-1-2-4(6(5)9)7(10)11/h1-3,5-6,8-9H,(H,10,11)")

    assertTrue(value != null)
    assertTrue(value instanceof Double)
    assertTrue(Math.abs(value - 156.042259) < 0.1 )

  }


  public void testConvertInchiToMolecularFormula(){
    def value = ConvertInchi.convertInchiToMolecularFormula("InChI=1S/C7H8O4/c8-5-3-1-2-4(6(5)9)7(10)11/h1-3,5-6,8-9H,(H,10,11)")

    assertTrue(value != null)
    assertTrue(value instanceof String)
    assertTrue(value.equals("C7H8O4"))
  }

}

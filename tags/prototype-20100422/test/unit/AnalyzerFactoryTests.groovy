import resolver.analyzer.AnalyzerFactory
/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Mar 12, 2010
 * Time: 4:17:13 PM
 */
class AnalyzerFactoryTests extends GroovyTestCase {

  /**
   * returns an analyzer specialized on compounds
   * @return
   */
  public void testGetCompoundAnalyzer() {
    String text = """

      this is a simple test and we should get glucose
    """

    Set<String> result = AnalyzerFactory.getCompoundAnalyzer().analyze(text)

    assertTrue(result.contains("glucose"))


  }

/**
 * returns an analyzer specialiced on cas numbers
 * @return
 */
  public void testGetCasAnalyzer() {

    String text = """

      this is a simple test and we should get only 492-62-6
    """

    Set<String> result = AnalyzerFactory.getCasAnalyzer().analyze(text)

    assertTrue(result.size() == 1)
    assertTrue(result.contains("492-62-6"))

  }

/**
 * returns an analyzer specialiced on kegg numbers
 * @return
 */
  public void testGetKeggAnalyzer() {
    String text = """

          this is a simple test and we should get only C00001 or C00002
        """

    Set<String> result = AnalyzerFactory.getKeggAnalyzer().analyze(text)

    assertTrue(result.size() == 2)
    assertTrue(result.contains("C00001"))
    assertTrue(result.contains("C00002"))


  }

  /**
   * analyzer specialiced for inchi codes
   * @return
   */
  public void testGetInchiCodeAnalyzer() {
    String text = """

          this is a simple test and we should get only InChI=1S/C9H17NO4/c1-7(11)14-8(5-9(12)13)6-10(2,3)4/h8H,5-6H2,1-4H3/p+1 but
          not InChI=1/C9H17NO4/c1-7(11)14-8(5-9(12)13)6-10(2,3)4/h8H,5-6H2,1-4H3/p+1
        """

    Set<String> result = AnalyzerFactory.getInchiCodeAnalyzer().analyze(text)
    println result
           
    assertTrue(result.size() == 1)
    assertTrue(result.contains("InChI=1S/C9H17NO4/c1-7(11)14-8(5-9(12)13)6-10(2,3)4/h8H,5-6H2,1-4H3/p+1"))
  }

  /**
   * analyzer specialiced on inchi hash keys
   * @return
   */
  public void testGetInchiHashKeyAnalyzer() {
    String text = """

          this is a simple test and we should get only RDHQFKQIGNGIED-UHFFFAOYSA-O but
          not RDHQFKQIGNGIED-IKLDFBCSAV
        """

    Set<String> result = AnalyzerFactory.getInchiHashKeyAnalyzer().analyze(text)

    assertTrue(result.size() == 1)
    assertTrue(result.contains("RDHQFKQIGNGIED-UHFFFAOYSA-O"))
  }

  /**
   * analyzer specialiced on inchi hash keys
   * @return
   */
  public void testGetHMDBAnalyzer() {
    String text = """

          this is a simple test and we should get only HMDB0001 but nothing else
        """

    Set<String> result = AnalyzerFactory.getHMDBAnalyzer().analyze(text)

    assertTrue(result.size() == 1)
    assertTrue(result.contains("HMDB0001"))
  }


}

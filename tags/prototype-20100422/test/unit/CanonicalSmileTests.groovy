import grails.test.*

class CanonicalSmileTests extends GrailsUnitTestCase {

    CanonicalSmile smile

    protected void setUp() {
        super.setUp()
      smile = new CanonicalSmile()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSetCode(){
      smile.code = "das"

      assert(smile.code == "das")
    }
  
    void testToString() {
      smile.code = "yada"

      assert(smile.toString() == "Smiles: yada")

    }
}

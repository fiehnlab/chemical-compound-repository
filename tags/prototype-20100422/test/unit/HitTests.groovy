import types.Hit

/**
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 4:53:13 PM
 *
 */
class HitTests extends GroovyTestCase {

  public void testEquals() {
    Set hits = new HashSet()

    hits.add(new Hit(value: "glucose"))
    hits.add(new Hit(value: "Glucose"))
    hits.add(new Hit(value: "D-glucose"))
    hits.add(new Hit(value: "D-Glucose"))
    hits.add(new Hit(value: "d-glucose"))


    println hits.size()
    assertTrue(hits.size() == 2)
  }
}

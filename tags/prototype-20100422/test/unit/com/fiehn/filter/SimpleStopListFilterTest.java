package com.fiehn.filter;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import static org.junit.Assert.*;


public class SimpleStopListFilterTest{
	@Test
	public void testCaseInsentiveFilter() throws FileNotFoundException{
		
		Filter filter = FilterFactory.createSimpleStopListFilter();
		
		Set<String> content = new HashSet<String>();
		
		content.add("House");
		content.add("Benzene"); //should be in result
		
		Set<String> result = filter.filter(content);
		System.out.println(result);
		//some random tests
		assertTrue(result != null);
		assertTrue(result.size() == 1);
		assertTrue(result.contains("Benzene"));
	}

}

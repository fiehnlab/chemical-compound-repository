import grails.test.*
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory

class TransformControllerTests extends ControllerUnitTestCase {
    protected void setUp() {
    super.setUp()

    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }

  protected void tearDown() {
    super.tearDown()
    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }
  

   void testTransformInChi()
   {
      String from = "inchikey"
      def to = "hmdb"
      String id = "XFNJVJPLKCPIBV-UHFFFAOYSA-N"
      int offset = 0;
      int max =10;

      TransformController.metaClass.getParams = {-> ["from": from, "to": to, "idValue": id ] }

      //create a new controller
      TransformController controller = new TransformController();

      /// Initialize services : Start
      controller.convertService = new ConvertService()
      controller.convertService.lookupService = new LookupService()
      // Initialize services : End

       def result = controller.transform()

//     println  "result.size() : ${result.transform.size()}"
//     println  "result.transform : ${result.transform}"
     assertTrue(result.transform.size() == 1)
     def result1 = result.transform

    for (def res: result1) {
      assertTrue(res.compound instanceof Compound)
      def com = res.compound
      assertTrue(com.id == 895883)

    }
  }


  void testTransformKegg()
   {
      String from = "kegg"
      def to = "inchikey"
      String id = "C10001"
      
      TransformController.metaClass.getParams = {-> ["from": from, "to": to, "idValue": id ] }

      //create a new controller
      TransformController controller = new TransformController();

      /// Initialize services : Start
      controller.convertService = new ConvertService()
      controller.convertService.lookupService = new LookupService()
      // Initialize services : End

       def result = controller.transform()

//     println  "result.size() : ${result.transform.size()}"
//     println  "result.transform : ${result.transform}"
     assertTrue(result.transform.size() == 1)
     def result1 = result.transform

    for (def res: result1) {
      assertTrue(res.compound instanceof Compound)
      def com = res.compound
      assertTrue(com.id == 931421)
    }
  }

  void testTransformSid()
   {
      String from = "sid"
      def to = "inchikey"
      String id = "74380251"

      TransformController.metaClass.getParams = {-> ["from": from, "to": to, "idValue": id ] }

      //create a new controller
      TransformController controller = new TransformController();

      /// Initialize services : Start
      controller.convertService = new ConvertService()
      controller.convertService.lookupService = new LookupService()
      // Initialize services : End

       def result = controller.transform()

//     println  "result.size() : ${result.transform.size()}"
     println  "result.transform : ${result.transform}"
     assertTrue(result.transform.size() == 1)
     def result1 = result.transform

    for (def res: result1) {
      assertTrue(res.compound instanceof Compound)
      def com = res.compound
      assertTrue(com.id == 931396)
    }
  }

  void testTransformCid()
   {
      String from = "cid"
      def to = "inchikey"
      String id = "1"

      TransformController.metaClass.getParams = {-> ["from": from, "to": to, "idValue": id ] }

      //create a new controller
      TransformController controller = new TransformController();

      /// Initialize services : Start
      controller.convertService = new ConvertService()
      controller.convertService.lookupService = new LookupService()
      // Initialize services : End

       def result = controller.transform()

//     println  "result.size() : ${result.transform.size()}"
     println  "result.transform : ${result.transform}"
     assertTrue(result.transform.size() == 1)
     def result1 = result.transform

    for (def res: result1) {
      assertTrue(res.compound instanceof Compound)
      def com = res.compound
      assertTrue(com.id == 931715)
    }
  }

}

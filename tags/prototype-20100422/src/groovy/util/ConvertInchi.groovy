package util

import org.openscience.cdk.DefaultChemObjectBuilder
import org.openscience.cdk.interfaces.IMolecularFormula
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Mar 10, 2010
 * Time: 4:09:46 PM
 */
class ConvertInchi {

  /**
   * converts an inchi code to a molare mass
   * @param inchi
   * @return
   */
  public static double convertInchiToMolareMass(String inchi) {

    IMolecularFormula moleculeFormula = MolecularFormulaManipulator.getMolecularFormula(convertInchiToMolecularFormula(inchi),DefaultChemObjectBuilder.getInstance())

    return MolecularFormulaManipulator.getTotalExactMass(moleculeFormula)

  }

  /**
   *
   * converts an inchi to a molecular formula
   * @param
   inchi
   * @return
   */
  public static String convertInchiToMolecularFormula(String inchi) {
    return inchi.split("/")[1]

  }
}

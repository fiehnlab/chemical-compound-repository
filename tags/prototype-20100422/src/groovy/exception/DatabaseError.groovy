package exception

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 5, 2010
 * Time: 12:23:36 PM
 * To change this template use File | Settings | File Templates.
 */
class DatabaseError extends RuntimeException{

  def DatabaseError() {
    super();    //To change body of overridden methods use File | Settings | File Templates.
  }

  def DatabaseError(String message) {
    super(message);    //To change body of overridden methods use File | Settings | File Templates.
  }

  def DatabaseError(String message, Throwable cause) {
    super(message, cause);    //To change body of overridden methods use File | Settings | File Templates.
  }

  def DatabaseError(Throwable cause) {
    super(cause);    //To change body of overridden methods use File | Settings | File Templates.
  }
}

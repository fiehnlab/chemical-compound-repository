package resolver.interceptor

import com.fiehn.filter.FilterFactory
import org.apache.log4j.Logger

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Feb 24, 2010
 * Time: 4:07:03 PM
 * To change this template use File | Settings | File Templates.
 */
class StoplistInterceptor implements ResolveableInterceptor{

  Logger logger = Logger.getLogger("interceptor")

  /**
   * intercewpts the given queries and filtes them by the stop list
   */

  def Set<String> intercept(Set<String> queries) {
    queries = FilterFactory.createSimpleStopListFilter().filter(queries)


    assert queries != null , "somehow the parser returned null, this shouldnt be! Please check your implementation!"
    return queries
  }
}

package com.fiehn.util.dbunit;

import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;
import org.dbunit.dataset.datatype.DefaultDataTypeFactory;

import java.sql.Types;

/**
 * required because of a DBUnit bug with HSQLDB
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 3, 2010
 * Time: 4:20:03 PM
 */
public class HSQLDataTypeFactory extends DefaultDataTypeFactory{

    @Override
    public DataType createDataType(int sqlType, String sqlTypeName) throws DataTypeException {
        if(sqlType == Types.BOOLEAN){
            return DataType.BOOLEAN;
        }
        
        return super.createDataType(sqlType,sqlTypeName);
    }
}

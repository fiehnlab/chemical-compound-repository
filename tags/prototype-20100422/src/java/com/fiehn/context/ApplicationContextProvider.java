package com.fiehn.context;

import org.springframework.context.ApplicationContext;
import org.springframework.beans.BeansException; 
import org.springframework.context.ApplicationContextAware;


/**
 * User: pradeep
 * Date: Mar 4, 2010
 * Time: 3:46:24 PM
 * This class provides an application-wide access to the
 * Spring ApplicationContext! The ApplicationContext is
 * injected in a static method of the class "AppContext".
 * Use AppContext.getApplicationContext() to get access
 * to all Spring Beans.
 */

public class ApplicationContextProvider implements ApplicationContextAware {
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        // Wiring the ApplicationContext into a static method
        AppContext.setApplicationContext(ctx);
    }
} 
package com.fiehn.filter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.fiehn.filter.exception.FilterException;

/**
 * used to create filters for us
 * 
 * @author pradeep
 * 
 */
public class FilterFactory {

	private static SimpleStopListFilter simpleStopListFilterInstance = null;

	/**
	 * creates a simple case insentivie stoplist filter
	 * 
	 * @return
	 */
	public static Filter createSimpleStopListFilter() {
		if (simpleStopListFilterInstance == null) {
			simpleStopListFilterInstance = new SimpleStopListFilter();
			simpleStopListFilterInstance.configure(null, false);
		}
		return simpleStopListFilterInstance;
	}

	

	/**
	 * creates a stop list filter for me
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static Filter createStopListFilter(String file) {
		try {
			return createStopListFilter(new File(file));
		} catch (FileNotFoundException e) {
			throw new FilterException(e.getMessage(), e);
		}
	}

	/**
	 * creates a stop list filter for me
	 * 
	 * @param
	 * @return
	 */
	public static Filter createStopListFilter(InputStream input) {
		Filter filter = new StopListFilter();
		filter.configure(input, false);

		return filter;
	}

	/**
	 * creates a stop list filter for me
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static Filter createStopListFilter(File file)
			throws FileNotFoundException {
		return createStopListFilter(new FileInputStream(file));
	}

	/**
	 * creates a stop list filter for me
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static Filter createCaseSensitiveStopListFilter(String file) {
		try {
			return createCaseSensitiveStopListFilter(new File(file));
		} catch (FileNotFoundException e) {
			throw new FilterException(e.getMessage(), e);
		}
	}

	/**
	 * creates a stop list filter for me
	 * 
	 * @param 
	 * @return
	 */
	public static Filter createCaseSensitiveStopListFilter(InputStream input) {
		Filter filter = new StopListFilter();
		filter.configure(input, true);

		return filter;
	}

	/**
	 * creates a stop list filter for me
	 * 
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	public static Filter createCaseSensitiveStopListFilter(File file)
			throws FileNotFoundException {
		return createCaseSensitiveStopListFilter(new FileInputStream(file));
	}

}

class NCI extends DBLinks {

  static constraints = {
    experimentalLogP(nullable: true)
    kowLogP(nullable: true)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }


  static searchable = {

    nciId index: 'not_analyzed', name : "nci"
  }
  
  double experimentalLogP

  double kowLogP

  String nciId

  String toString(){
    return "NCI: ${nciId}"
  }
}

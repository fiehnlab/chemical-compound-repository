class PubchemSubstance extends PubChem {


  static searchable = {

    sid index: 'not_analyzed'
  }

  static constraints = {
    sid(nullable: false, unique: true)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  /**
   * the internal pubchem substance id
   */
  int sid


  String toString() {
    return "SID: ${sid}";
  }
}

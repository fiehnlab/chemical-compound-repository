import java.awt.Color

/**
 * displays a formula for us
 */

class FormulaTagLib {

  //our injected image service
  ImageService imageService

  /**
   * renders a simple formula
   */
  def structure = { attrs ->

    try {
      assert attrs.id != null, "please provide a compound id, its needed for the file name"

      assert attrs.inchi != null, "please provide a inchi so we can render it"
      assert attrs.size != null, "please a size"
      assert attrs.color != null, "please provide a background color"

      out << "<img src=\"${imageService.getStructure(attrs.id, attrs.inchi, attrs.size, Color.decode(attrs.color))}\" alt = \"sorry was not able to display structure\"></img>"

    } catch (Exception e) {
      out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
      log.error(e.getMessage(), e)

    }
  }

  /**
   * renders a simple formula
   */
  def structureThumb = { attrs ->

    try {

      assert attrs.id != null, "please provide a compound id, its needed for the file name"

      assert attrs.inchi != null, "please provide a inchi so we can render it"

      Color color = calculateColor(attrs)
      //log.debug "attrs.color :: "+attrs.color
      
      out << "<img src=\"${imageService.getThumbmail(attrs.id, attrs.inchi, color)}\" alt = \"sorry was not able to display structure\"></img>"

    } catch (Exception e) {
      out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
      log.error(e.getMessage(), e)

    }
  }

  /**
   * strips # of the color tag
   * @param attrs
   * @return
   */
  private Color calculateColor(attrs) {
    Color color = Color.WHITE
    if (attrs.color != null) {
      String code = attrs.color.toString()
      if(code.startsWith("#")){
        code = code.replaceFirst("#","")
      }
      if(code.startsWith("0x") == false){
        code = "0x" + code
      }
      
      color = Color.decode(attrs.color)
    }
    return color
  }

  /**
   * renders a simple formula
   */
  def structureSmall = { attrs ->

    try {
      assert attrs.id != null, "please provide a compound id, its needed for the file name"
      assert attrs.inchi != null, "please provide a inchi so we can render it"
      Color color = calculateColor(attrs)

      out << "<img src=\"${imageService.getSmall(attrs.id, attrs.inchi, color)}\" alt = \"sorry was not able to display structure\"></img>"

    } catch (Exception e) {
      out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
      log.error(e.getMessage(), e)

    }
  }
  /**
   * renders a simple formula
   */
  def structureMedium = { attrs ->

    try {
      assert attrs.id != null, "please provide a compound id, its needed for the file name"

      assert attrs.inchi != null, "please provide a inchi so we can render it"
      Color color =calculateColor(attrs)

      out << "<img src=\"${imageService.getMedium(attrs.id, attrs.inchi, color)}\" alt = \"sorry was not able to display structure\"></img>"

    } catch (Exception e) {
      out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
      log.error(e.getMessage(), e)

    }
  }
  /**
   * renders a simple formula
   */
  def structureLarge = { attrs ->

    try {
      //log.info "got: ${attrs}"
      assert attrs.id != null, "please provide a compound id, its needed for the file name"

      assert attrs.inchi != null, "please provide a inchi so we can render it"
      Color color = calculateColor(attrs)

      out << "<img src=\"${imageService.getLarge(attrs.id, attrs.inchi, color)}\" alt = \"sorry was not able to display structure\"></img>"

    } catch (Exception e) {
      out << "<h4>an error occured, please check the log! (${e.getMessage()})</h4>"
      log.error(e.getMessage(), e)

    }
  }
}

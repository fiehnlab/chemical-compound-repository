dataSource {
  pooled = true
  driverClassName = "org.hsqldb.jdbcDriver"
  username = "sa"
  password = ""
}
hibernate {
  cache.use_second_level_cache = true
  cache.use_query_cache = true
  cache.provider_class = 'com.opensymphony.oscache.hibernate.OSCacheProvider'
}
// environment specific settings
environments {

  //normal development datasource
  development {
    dataSource {
      pooled = true
      loggingSql = true
      dbCreate = "update"
      url = "jdbc:postgresql://bbtest.fiehnlab.ucdavis.edu:5432/compound-repository-devel"
//      driverClassName = "org.postgresql.Driver"
      driverClassName = "com.p6spy.engine.spy.P6SpyDriver" // use this driver to enable p6spy logging
      username = "compound"
      password = "asdf"
    }
  }

  //lucene testing database
  lucene {
    dataSource {
      dbCreate = "update"
      url = "jdbc:postgresql://bbtest.fiehnlab.ucdavis.edu:5432/compound-repository-lucene"
      driverClassName = "org.postgresql.Driver"
      username = "compound"
      password = "asdf"
    }
  }

  //test datasource
  test {

    dataSource {

      pooled = true
      loggingSql = false
      driverClassName = "org.hsqldb.jdbcDriver"
      username = "sa"
      password = ""
      dbCreate = "create-drop" // one of 'create', 'create-drop','update'
      url = "jdbc:hsqldb:mem:devDB"
    }
  }
  
  production {
    dataSource {
      pooled = true
      loggingSql = false
      dbCreate = "update"
      url = "jdbc:postgresql://uranus.fiehnlab.ucdavis.edu:5432/compound-repository"
      driverClassName = "org.postgresql.Driver"
      username = "compound"
      password = "asdf"
    }
  }
}
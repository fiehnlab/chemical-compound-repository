// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if(System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }


grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [ html: ['text/html','application/xhtml+xml'],
                      xml: ['text/xml', 'application/xml'],
                      text: 'text-plain',
                      js: 'text/javascript',
                      rss: 'application/rss+xml',
                      atom: 'application/atom+xml',
                      css: 'text/css',
                      csv: 'text/csv',
                      pdf: 'application/pdf',
                      rtf: 'application/rtf',
                      excel: 'application/vnd.ms-excel',
                      ods: 'application/vnd.oasis.opendocument.spreadsheet',
                      all: '*/*',
                      json: ['application/json','text/json'],
                      form: 'application/x-www-form-urlencoded',
                      multipartForm: 'multipart/form-data'
                    ]
// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"

// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true

// set per-environment serverURL stem for creating absolute links
environments {
  production {
    grails.serverURL = "http://uranus.fiehnlab.ucdavis.edu:8080/${appName}"


  }
  development {
    grails.serverURL = "http://localhost:8080/${appName}"


    log4j = {
      console name: 'stdout', layout: pattern(conversionPattern: '[%5p] [%t] : %m%n')

      appenders {
        console name: 'stdout', layout: pattern(conversionPattern: '[%5p] [%t] : %m%n')
        file name: 'statistic', file: new File("log/statistic.log"), layout: pattern(conversionPattern: 'date:%d{dd/MMM/yyyy} time:%d{HH-mm-ss} %m%n'), append: true
      }

      //save the statistic output during the import of the data
      info statistic: 'statistic.import', additivity: false

      debug 'ImportKegg',
              'ImportChebi',
              'ImportPubchem',
              'NativeArtefactLocator',
              'Import Lipid Maps',
              'ImportNCI',
              'ImportHMDB',
              'ImportGLAbbrev',
              'statistic.import',
              'edu'
    }
  }
  test {
    grails.serverURL = "http://localhost:8080/${appName}"

    log4j = {
      console name: 'stdout', layout: pattern(conversionPattern: '[%5p] [%t] : %m%n')

      appenders {
        console name: 'stdout', layout: pattern(conversionPattern: '[%5p] [%t] : %m%n')
      }

      debug 'ImportKegg',
              'ImportChebi',
              'ImportPubchem',
              'NativeArtefactLocator',
              'Import Lipid Maps',
              'ImportNCI',
              'ImportHMDB',
              'statistic.import',
              'edu',
              'grails.app',
              'resolver',
              'query',
              'DataSetInfo',
              'test'


    }
  }
  lucene {
    grails.serverURL = "http://localhost:8080/${appName}"
  }

}

// log4j configuration
log4j = {
  // Example of changing the log pattern for the default console
  // appender:
  //
  appenders {
    console name: 'stdout', layout: pattern(conversionPattern: '[%5p] [%t] : %m%n')
    file name: 'statistic', file: new File("log/statistic.log"), layout: pattern(conversionPattern: 'date:%d{dd/MMM/yyyy} time:%d{HH-mm-ss} %m%n'), append: true
  }

  error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
          'org.codehaus.groovy.grails.web.pages', //  GSP
          'org.codehaus.groovy.grails.web.sitemesh', //  layouts
          'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
          'org.codehaus.groovy.grails.web.mapping', // URL mapping
          'org.codehaus.groovy.grails.commons', // core / classloading
          'org.codehaus.groovy.grails.plugins', // plugins
          'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
          'org.springframework',
          'org.hibernate'


  warn 'org.mortbay.log'

  info 'edu',
          'ImportKegg',
          'ImportChebi',
          'ImportPubchem',
          'NativeArtefactLocator',
          'Import Lipid Maps',
          'ImportNCI',
          'ImportHMDB',
          'ImportGLAbbrev'

  //save the statistic output during the import of the data
  info statistic: 'statistic.import', additivity: false

  debug 'grails.app',
          'resolver',
          'query',
          'CompoundHelper'

  /*,
          'ImportKegg',
          'ImportChebi',
          'ImportPubchem',
          'NativeArtefactLocator',
          'Import Lipid Maps',
          'ImportNCI',
          'ImportHMDB',
          'edu.ucdavis.genomics.metabolomics.binbase.connector.references.sdf.SdfToInchi'
    */
}

/**
 * code coverage reports
 */
coverage {
  nopost = true
  xml = true
  enabledByDefault = true
}

/**
 * google analytics settings
 */
google.analytics.webPropertyID = "UA-2237528-17"


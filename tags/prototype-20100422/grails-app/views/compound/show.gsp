
<html>
    <head>
        <title>Compound Details</title>
    </head>

<body class="BodyBackground" >
<div id="contents">

    <div id="main">
<table  border='0' width="70%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td  class="AppsLabel" align="center"> Compound Details </td>
	</tr>

    <tr>
		<td  align='right'>
                  [ <a  href="javascript://nop" onclick="history.back(1)">Back to result</a> ]
				  [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
	    </td>
	</tr>

  <tr height='2'> <td> &nbsp; </td> </tr>

  <tr>
  <td>


<table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >
<%
  if(compoundInstance != null )
  { %>
    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" width='15%' >Id</td>
        <td valign="top" width='1%'>:</td>
        <td valign="top" class="Label3" width="auto" >${compoundInstance.id}</td>
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" class="Label3" rowspan="12" width="20%" align="right" valign="top">

          <g:structureMedium id="${compoundInstance.id}" inchi="${compoundInstance.inchi}" color="#F7F7F7"/>

        </td>
        <td valign="top" width='1%' >&nbsp;</td>

    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" ><g:message code="inchi" /></td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'inchi')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" >Formula</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'formula')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" >Exact Molar Mass</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'exactMolareMass')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Date Created</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'dateCreated')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top"><g:message code="links" /></td>
        <td valign="top" >:</td>
        <td  valign="top" style="text-align:left;" class="Label3">
            <ul>
            <g:each var="d" in="${compoundInstance.dbLinks}">
                <li>${d?.encodeAsHTML()}</li>
            </g:each>
            </ul>
        </td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top" ><g:message code="LIST.INCHI_HASH_KEY" /></td> 
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${compoundInstance?.inchiHashKey?.encodeAsHTML()}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top"><g:message code="iupac" /> Name</td>
        <td valign="top" >:</td>
        <td  valign="top" style="text-align:left;" class="Label3">
            <ul>
            <g:each var="s" in="${compoundInstance.iupac}">
                <li>${s.toString().split(":")[1]?s.toString().split(":")[1].encodeAsHTML():s.toString().split(":")[0].encodeAsHTML()}</li>
            </g:each>
            </ul>
        </td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top">Last Updated</td>
        <td valign="top" >:</td>
        <td valign="top" class="Label3">${fieldValue(bean:compoundInstance, field:'lastUpdated')}</td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top"><g:message code="smiles" /></td>
        <td valign="top" >:</td>
        <td  valign="top" style="text-align:left;" class="Label3">
            <ul>
            <g:each var="s" in="${compoundInstance.smiles}">
                <li>${s.toString().split(":")[1]?s.toString().split(":")[1].encodeAsHTML():s.toString().split(":")[0].encodeAsHTML() }</li>
            </g:each>
            </ul>
        </td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>

    <tr class="prop">
        <td valign="top" width='1%' >&nbsp;</td>
        <td valign="top"><g:message code="names" /></td>
        <td valign="top" >:</td>
        <td  valign="top" style="text-align:left;" class="Label3">
            <ul>
            <g:each var="s" in="${compoundInstance.synonyms}">
                <li>${s.toString().split(":")[1]?s.toString().split(":")[1].encodeAsHTML():s.toString().split(":")[0].encodeAsHTML()}</li>
            </g:each>
            </ul>
        </td>
        <td valign="top" width='1%' >&nbsp;</td>
    </tr>
<%}else{%>

<tr >
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" colspan="11">
       <table border=0 align ="center" width="98%" class="">
         <tr> <td height='20' > </td> </tr>

			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr>
			   <td align='center' height='70' valign="middle" >
              <g:if test="${flash.message}">
               <div class="AlertLabel">
                  ${flash.message}
               </div>
              </g:if> </td>
			</tr>
			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr> <td height='20' > </td> </tr>
        </table>
  </td>
  <td class="Label3" width="1%">&nbsp;</td>
</tr>

<% } %>  
</table>
  </td>
  </tr>
  </table>
</div>

  <div id="footer">
  <g:render template="/footer" />
</div>

</div>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 4:00:39 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><title><g:message code="TRANSFORM" /></title></head>

<body class="BodyBackground"  >

<div id="contents">

  <div id="main">

<table width="1000px"  border="0" align="center" height="100%">
  <tr>
    <td>
      <table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
       <tr align='center' valign='bottom'>
          <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-SIZE: 21px;FONT-FAMILY: 'Verdana';TEXT-TRANSFORM: capitalize">

              <g:message code="PROJECT_TITLE" />

          </td>
        </tr>
        <tr>

          <td class="AppsLabel" align="center" style="COLOR: #0B3861; FONT-FAMILY: 'Verdana';">
              <g:message code="PROTOTYPE_MSG" args='["${session.getAttribute("PrototypeSize")}"]' />
          </td>
        </tr>
     <!--   <tr>
            <td  align='right' width="100%">
                      [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
            </td>
        </tr>   -->
     </table>

    </td>
  </tr>

   <tr>
     <td>
       <div id="pagePath">
			<h1><g:link controller="homePage" action="index">Home</g:link> <span>&raquo;</span> <g:message code="TRANSFORM" /></h1>
		</div>
       <div class="line"></div>


<g:form name="transForm" controller="transform">
<table  border='0' width="100%" align='center' cellpadding="0" cellspacing="3">
	
  <tr height='2' >
    <td colspan="9">
    <g:if test="${flash.message}">
       <div class="AlertLabel">
          ${flash.message}
       </div>
     </g:if>
      &nbsp;
    </td>
  </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td width="35%"> Id : <g:textField tabindex='1' name="idValue" style="width:300px" value="${idValue?idValue:''}"/> </td>
    <td width="1%"> &nbsp; </td>
    <td width="30%">From : <g:select tabindex='2' name="from" from="${fromFields}" optionValue="VALUE" optionKey="ID" value="${from?from:''}"  noSelection="${['null': '--- Select ---']}"  /> </td>
    <td width="1%"> &nbsp; </td>
    <td width="20%"> To : <g:select tabindex='3' name="to" from="${toFields}" optionValue="VALUE" optionKey="ID" value="${to?to:''}" noSelection="${['null': '--- Select ---']}" /></td>
    <td width="1%"> &nbsp; </td>
    <td width="15%"> <g:actionSubmit tabindex='4' value="Submit" action="transform"/>  </td>
    <td width="1%"> &nbsp; </td>
  </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td colspan="7"  height="30px" align="center"> &nbsp;  </td>
    <td width="1%"> &nbsp; </td>
  </tr>

</table>

</g:form>
  
  </td>
</tr>
  </table>
</div>
<div id="footer">
  <g:render template="/footer" />
</div>
  </div>
  </body>

</html>
<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 24, 2010
  Time: 4:59:28 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>
<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'FooterLayout.css')}");</style>

<div id="copy" align="center" valign="middle">
  <div  class="LabelFooter" align="center" valign="middle" style="padding:5px;">
            <a class="Footer" href="http://binbase.fiehnlab.ucdavis.edu:8080/jira/secure/Dashboard.jspa">Report a problem</a>
          | <a class="Footer" href="http://binbase.fiehnlab.ucdavis.edu:8080/jira/secure/Dashboard.jspa">Request a feature</a>
          | <a class="Footer" href="http://binbase.fiehnlab.ucdavis.edu:8080/jira/secure/Administrators.jspa">Contact administrators</a>
  </div>
  <div class="LabelFooter" align="center" valign="middle">
     Powered by a free Atlassian <a class="Footer" href='http://www.atlassian.com/software/jira'>JIRA</a> open source license
  </div>

</div>


<!-- google anlytics, do not change -->
<ga:trackPageview/>
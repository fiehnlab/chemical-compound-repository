<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title><g:message code="DISCOVER.RESULTS" /></title>
<export:resource />
</head>


<body class="BodyBackground"  >
<div id="contents">

    <div id="main">
<g:form name="discoveryForm" controller="discovery" method="post">

<table  border='0' width="100%" align='center'>
<!--<tr>
<td>
   <table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9" class="AppsLabel" align="center"> <g:message code="DISCOVER.RESULTS" /> </td>
	</tr>

  <!--  <tr>
		<td colspan="9" align='right'>
                  [ <g:link controller="discovery" action="index" ><g:message code="DISCOVER.BACK" /></g:link> ]
				  [ <g:link controller="homePage" action="index"><g:message code="HOME.BACK" /></g:link> ]
	    </td>
	</tr>    
</table>
</td>
</tr>   -->

<tr>
  <td>
      <div id="pagePath">
        <h1><g:link controller="homePage" action="index">Home</g:link> <span>&raquo;</span> <g:link controller="discovery" action="index"><g:message code="DISCOVER" /></g:link> <span>&raquo;</span> <g:message code="DISCOVER.RESULTS" /></h1>
      </div>
      <div class="line"></div>
    <div >&nbsp;</div>
      <table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >

   <tr>
   <td >
  <table border="0" id="tblSample1" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >


  <tr align='left' valign='bottom' class="TableHeaderBG">
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="20%"> <g:message code="LIST.COMPOUND_NAME" /> </td>
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="20%"> <g:message code="LIST.INCHI_HASH_KEY" /> </td>
		<td class="TableHeader" width="1%">&nbsp;</td>
        <td class="TableHeader" width="15%">Discovered in Text</td>
        <td class="TableHeader" width="1%">&nbsp;</td>
        <td class="TableHeader" width="10%">Origin</td>
        <td class="TableHeader" width="1%">&nbsp;</td>
        <td class="TableHeader" width="20%"> Reference </td>
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="5%"> <g:message code="LIST.STRUCTURE" /> </td>
        <td class="TableHeader" width="1%">&nbsp;</td>
	</tr>


  <%

   if( result!=null && result.size() > 0 )
   {
    int liCnt=0;
    String lStrClass = "";
    String lColorFormulaBG = "";
    for(Map map : result)
    {
      Compound compound = map.compound

      if( compound==null)
         continue;

      if( liCnt++ % 2 == 0 )
      {
        lStrClass="TableRowBG2";
        lColorFormulaBG = "#FFFFFD";
      }
    else
      {
         lStrClass = "TableRowBG1";
         lColorFormulaBG = "#DBE8F6";
      }

      %>
  <tr class='<%=lStrClass%>'>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left">${compound.getDefaultName()}</td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left"><g:link action="show" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left">${map.hit}</td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left">${map.resolver}</td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left">${map.result}</td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left"><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}" /></td>
  <td class="Label3" width="1%">&nbsp;</td>

  </tr>
      <%
    }
        }else
    {
  %>
  <tr >
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" colspan="11">
       <table border=0 align ="center" width="98%" class="TableBG">
         <tr> <td height='20' > </td> </tr>

			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr>
			   <td align='center' height='70' valign="middle" > <font color='red' > No Compound Found... </font> </td>
			</tr>
			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr> <td height='20' > </td> </tr>
        </table>
  </td>
  <td class="Label3" width="1%">&nbsp;</td>
  </tr>

  <% } %>


</table>
  </td>
</tr>

  <%

   if( result!=null && result.size() > 0 )
   { %>
       <tr>
          <td class="Label3">
            <export:formats action="process" controller="discovery" formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']" />
          </td>
       </tr>
  <% } %>

</table>
  </td>
       </tr>
  </table>

</g:form>
</div>

  <div id="footer">
  <g:render template="/footer" />
</div>

</div>
</body>
</html>
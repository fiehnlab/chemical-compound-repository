<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Mar 22, 2010
  Time: 10:55:52 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head>
    <title>Chemical Similarty Map</title>
    <style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>
  </head>

  <body>
 <g:form name="similarityMap" controller="chemicalSimilarityMap" enctype="multipart/form-data">

<table id='tabTitle' border='0' width="800px" align='center'>
<tr align='center' >
	<td class="Titles"> Chemical Similarity Map </td>
</tr>

<tr>
  <td height="25" valign="bottom">
    <g:if test="${flash.message}">
     <div class="AlertLabel">
        ${flash.message}
     </div>
   </g:if>
  </td>
</tr>

<tr>
  <td>
<table id='tabForm' class="TableBG" border='0' width="100%" align='center'>
<tr height="30" >
    <td width="1%" ></td>
	<td width="15%" class="Label3" align='left' valign='top'> Enter Data </td>
	<td width="1%" class="Label3" align='center' valign='top'> : </td>
	<td width="80%" class="Label3" align='left' >
		<textarea class="Label3" name="txtCSVData"  rows="10" cols="85" title="Enter CSV file data."></textarea>
	</td>
	<td width="1%" ></td>
</tr>

<tr height="30" >
    <td width="1%" ></td>
	<td width="10%" class="Label3" align='left'>  </td>
	<td width="1%"  class="Label3" align='center'> or </td>
	<td width="80%" class="Label3" align='left' >
		<input type="file" id="filePath" name="filePath" title="Upload only .csv extension file" /> &nbsp; <font color="red"> Upload only .csv and .txt extension file</font>
   	</td>
	<td width="1%" ></td>
</tr>

<tr height="30" >
    <td width="1%" ></td>
	<td width="10%" class="Label3" align='left'> Probability Factor </td>
	<td width="1%"  class="Label3" align='center'> : </td>
	<td width="80%" class="Label3" align='left' >
		<input type="text" id="txtProbabilityFactor" name="txtProbabilityFactor" title="Range between 0.01 to 1.0" value="0.7"/> &nbsp; <font color="red">Enter Probability Factor between the range of 0.01 to 1.0</font>
	</td>
	<td width="1%" ></td>
</tr>

</table>

</td>
   </tr>
</table>   

<p align="center">
  <g:actionSubmit  action="index" value="Submit"  />
</p>  


<% if( Result!=null && Result.size() > 0 ){ %>
<table id='tabForm1' class="TableBG" border='0'  width="800px" align='center'>
 <tr height="10" >
    <td width="1%" ></td>
	<td width="15%" class="Label3" align='left' valign='top'> [<a href="${filePath}" > Download Result File</a>] </td> 
 </tr>


<tr height="30" >
    <td width="1%" ></td>
	<td width="15%" class="Label3" align='left' valign='top'> Result </td>
	<td width="1%" class="Label3" align='center' valign='top'> : </td>
	<td width="80%" class="Label3" align='left' >
		<textarea class="Label3" name="txtResult" readonly rows="12" cols="85"><% for(int index=0;index< Result.size();index++){String s = (String) Result.get(index);%>
<%=s%><%} %></textarea>
	</td>
	<td width="1%" ></td>
</tr>
</table>
<% } %>

   </g:form> 

  </body>
</html>
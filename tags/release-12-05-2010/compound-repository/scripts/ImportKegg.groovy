includeTargets << grailsScript("Init")
includeTargets << new File("scripts/RunScript.groovy")


target(main: "imports the kegg database") {
  args = "builder/ImportKegg.groovy"

  //execute the script
  depends(runScript)
}

setDefaultTarget(main)

import grails.test.*
import compound.repository.entries.BinBaseBin

class BinBaseBinTests extends GrailsUnitTestCase {

  BinBaseBin bin

  protected void setUp() {
    super.setUp()
     bin = new BinBaseBin()

  }

  protected void tearDown() {
    super.tearDown()
  }

  void testSetBinId() {

    bin.binId = 21

    assertTrue(bin.getBinId() == 21)
  }

  void testSetMassSpec(){
    bin.massSpec = "12:0 22:2 23:32"

    assertTrue(bin.massSpec == "12:0 22:2 23:32")
  }
  
  void testSetInternalName(){
    bin.internalName = "yada"

    assertTrue(bin.getInternalName() == "yada")
  }

  void testSetUniqueMass(){
    bin.unqiueMass = 334

    assertTrue(bin.getUnqiueMass() == 334)
  }

  void testSetQuantMass(){
    bin.quantMass = 343

    assertTrue(bin.getQuantMass() == 343)
  }

  void testSetRelatedDatabase(){
    bin.relatedDatabase = "test"
    
    assertTrue(bin.relatedDatabase == "test")
  }
  
}

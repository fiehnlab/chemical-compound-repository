import grails.test.*
import compound.repository.entries.Chebi

class ChebiTests extends GrailsUnitTestCase {

  Chebi chebi

  protected void setUp() {
    super.setUp()
    chebi = new Chebi()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testToString() {

    chebi.chebiId = "fsdfs"

    assertTrue(chebi.toString() == "Chebi: fsdfs")
  }

  void testSetChebiId() {
    chebi.chebiId = "Dasd"

    assertTrue(chebi.chebiId == "Dasd")
  }
}

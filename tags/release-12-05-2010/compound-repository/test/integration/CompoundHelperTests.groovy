import grails.test.GrailsUnitTestCase
import org.apache.log4j.Logger
import compound.repository.entries.Compound
import compound.repository.entries.PubchemSubstance
import compound.repository.entries.Synonym
import compound.repository.entries.Cas
import compound.repository.entries.Chebi
import compound.repository.entries.Kegg
import compound.repository.entries.LipidMap
/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Mar 19, 2010
 * Time: 11:38:21 AM
 *
 * required to test the compound helper
 */
class CompoundHelperTests extends GrailsUnitTestCase {
  Logger logger = Logger.getLogger(getClass())

  protected void setUp() {
    super.setUp()
  }

  protected void tearDown() {
    super.tearDown()
  }

  public void testGetCompound() {

    Compound compound = CompoundHelper.getCompound(895880)

    assertTrue(compound.id == 895880)
    assertTrue(compound.inchi == "InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1")

  }

  public void testUpdatesTheCasNumber() {
    Compound compound = CompoundHelper.getCompound(895880)

    //make sure it doesn't exist yet
    assertTrue(Cas.findAllByCasNumber("33-33-33").size() == 0)

    //store the cas number
    CompoundHelper.updatesTheCasNumber "33-33-33", compound, logger
    assertTrue(Cas.findAllByCasNumber("33-33-33").size() == 1)

    //make sure it doesn't generate duplicates
    CompoundHelper.updatesTheCasNumber "33-33-33", compound, logger
    assertTrue(Cas.findAllByCasNumber("33-33-33").size() == 1)

  }

  public void testStoreCas() {
    Compound compound = CompoundHelper.getCompound(895880)

    Cas.findAllByCompound(compound).each {Cas cas -> cas.delete()}
    //make sure it doesn't exist yet
    assertTrue(Cas.findAllByCasNumber("33-33-33").size() == 0)

    //make sure we don't have any cas numbers for this compound
    assertTrue(Cas.findAllByCompound(compound).size() == 0)

    //attemp to store a null cas number which should not cause an exception
    CompoundHelper.storeCas(null, compound, logger)

    //we should still have 0 cas numbers
    assertTrue(Cas.findAllByCompound(compound).size() == 0)

    //attemp to store a cas number with '"Not Available"' which should not cause an exception
    CompoundHelper.storeCas("Not Available", compound, logger)
    assertTrue(Cas.findAllByCompound(compound).size() == 0)

    //store a real number
    CompoundHelper.storeCas "33-33-33", compound, logger

    //we sould now have a cas number
    assertTrue(Cas.findAllByCasNumber("33-33-33").size() == 1)

  }

  public void testSaveCompound() {
    Compound compound = new Compound()
    compound.inchi = "InChI=1S/C17H19NO4S/c1-12-8-9-16(13(2)10-12)23(21,22)18-15(11-17(19)20)14-6-4-3-5-7-14/h3-10,15,18H,11H2,1-2H3,(H,19,20)"

    assertTrue(Compound.findAllByInchi("InChI=1S/C17H19NO4S/c1-12-8-9-16(13(2)10-12)23(21,22)18-15(11-17(19)20)14-6-4-3-5-7-14/h3-10,15,18H,11H2,1-2H3,(H,19,20)").size() == 0)

    CompoundHelper.saveCompound(compound, logger)


    assertTrue(Compound.findAllByInchi("InChI=1S/C17H19NO4S/c1-12-8-9-16(13(2)10-12)23(21,22)18-15(11-17(19)20)14-6-4-3-5-7-14/h3-10,15,18H,11H2,1-2H3,(H,19,20)").size() == 1)

    assertTrue(compound.id != null)
  }


  public void testAddSynonym() {
    Compound compound = CompoundHelper.getCompound(895880)

    //we know we have 1 synonym in the database
    assertTrue(Synonym.findAllByCompound(compound).size() == 1)

    //should work and checks if null is accepted and throws no exceptions

    CompoundHelper.addSynonym(null, logger, compound)

    //add a simple synonyms
    CompoundHelper.addSynonym(["test"], logger, compound)
    assertTrue(Synonym.findAllByCompound(compound).size() == 2)

    //should not add anything
    CompoundHelper.addSynonym([""], logger, compound)
    assertTrue(Synonym.findAllByCompound(compound).size() == 2)

    //should not anything
    CompoundHelper.addSynonym(["Not Available"], logger, compound)
    assertTrue(Synonym.findAllByCompound(compound).size() == 2)

    //test if we can't add duplictes
    CompoundHelper.addSynonym(["test"], logger, compound)
    assertTrue(Synonym.findAllByCompound(compound).size() == 2)

    //test for compound.repository.entries.HMDB splitter bug
    CompoundHelper.addSynonym(["test;yada"], logger, compound)
    assertTrue(Synonym.findAllByCompound(compound).size() == 3)


  }

  public void testUpdateSid() {
    Compound compound = CompoundHelper.getCompound(895880)

    assertTrue(PubchemSubstance.findAllByCompound(compound).size() == 0)

    CompoundHelper.updateSID(logger, compound, null)
    assertTrue(PubchemSubstance.findAllByCompound(compound).size() == 0)

    CompoundHelper.updateSID(logger, compound, 2)
    assertTrue(PubchemSubstance.findAllByCompound(compound).size() == 1)

    CompoundHelper.updateSID(logger, compound, 2)
    assertTrue(PubchemSubstance.findAllByCompound(compound).size() == 1)


  }

  public void testUpdateChebi(){
    Compound compound = CompoundHelper.getCompound(895880)

    logger.info "find by compound..."
    assertTrue(Chebi.findAllByCompound(compound).size() == 0)



    logger.info "add null..."
    CompoundHelper.updateChebiId(logger, compound, null)
    assertTrue(Chebi.findAllByCompound(compound).size() == 0)


    logger.info "add chebi..."
    CompoundHelper.updateChebiId(logger, compound, "2")
    assertTrue(Chebi.findAllByCompound(compound).size() == 1)


    logger.info "add identical chebi..."
    CompoundHelper.updateChebiId(logger, compound, "2")
    assertTrue(Chebi.findAllByCompound(compound).size() == 1)
  }
  public void testUpdateKegg(){
    Compound compound = CompoundHelper.getCompound(895880)

    logger.info "find by compound..."
    assertTrue(Kegg.findAllByCompound(compound).size() == 0)



    logger.info "add null..."
    CompoundHelper.updateKegg(logger, compound, null)
    assertTrue(Kegg.findAllByCompound(compound).size() == 0)


    logger.info "add kegg..."
    CompoundHelper.updateKegg(logger, compound, "2")
    assertTrue(Kegg.findAllByCompound(compound).size() == 1)


    logger.info "add identical krgg..."
    CompoundHelper.updateKegg(logger, compound, "2")
    assertTrue(Kegg.findAllByCompound(compound).size() == 1)
  }

  public void testLipidMaps(){
    Compound compound = CompoundHelper.getCompound(895880)

    assertTrue(LipidMap.findAllByCompound(compound).size() == 0)



    CompoundHelper.updateLipidMaps(logger, compound, null)
    assertTrue(LipidMap.findAllByCompound(compound).size() == 0)


    CompoundHelper.updateLipidMaps(logger, compound, "2")
    assertTrue(LipidMap.findAllByCompound(compound).size() == 1)


    CompoundHelper.updateLipidMaps(logger, compound, "2")
    assertTrue(LipidMap.findAllByCompound(compound).size() == 1)
  }
}

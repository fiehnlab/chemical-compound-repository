package compound.repository

import grails.test.ControllerUnitTestCase
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory
import compound.repository.LookupService
import compound.repository.ConvertService
import compound.repository.MassLookupController
/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 1, 2010
 * Time: 11:58:59 AM
 * To change this template use File | Settings | File Templates.
 */
class MassLookupControllerTests extends ControllerUnitTestCase {

    protected void setUp() {
    super.setUp()

    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }

  protected void tearDown() {
    super.tearDown()
    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }

  void testMassTransform()
    {
      String from = "kegg"
      def StrTo = ["cas","names","mass","inchikey"] // Multiple Inputs
      String ids = "C10000\nC10001\nC10003\nC10004\nC10005\nC10006"
      int offset = 0;
      int max =10;

      MassLookupController.metaClass.getParams = {-> ["from": from, "to": StrTo, "ids": ids ] }

      //create a new controller
      MassLookupController controller = new MassLookupController()

      // Initialize services : Start
      controller.convertService = new ConvertService()
      controller.convertService.lookupService = new LookupService()
      // Initialize services : End

      def result = controller.masstransform()

      //println result.transform
      println "result.transform.size() :: "+result.transform.size()
      //make sure we don't have a null result
      assertTrue result != null
      // 6- records expected 
      assertTrue result.transform.size() == 6
      
    }


  void testMassTransformOntToOne()
    {
      String from = "kegg"
      def StrTo = "cas"  // Single Input
      String ids = "C10000\nC10001\nC10003\nC10004\nC10005\nC10006"
      int offset = 0;
      int max =10;

      MassLookupController.metaClass.getParams = {-> ["from": from, "to": StrTo, "ids": ids, "offset":offset, "max":max ] }

      //create a new controller
      MassLookupController controller = new MassLookupController()

      // Initialize services : Start
      controller.convertService = new ConvertService()
      controller.convertService.lookupService = new LookupService()
      // Initialize services : End

      def result = controller.masstransform()

      //println result.transform
      println "result.transform.size() :: "+result.transform.size()
      //make sure we don't have a null result
      assertTrue result != null
      // 6- records expected
      assertTrue result.transform.size() == 6

    }
}


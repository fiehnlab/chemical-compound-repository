package com.fiehn.filter;


/**
 * a simple stop list filter which always uses the same internal dictionarey from the ressources
 * @author pradeep
 *
 */
public class SimpleStopListFilter extends StopListFilter{

	/**
	 * loads a dictionary from a local ressource
	 */
	@Override
	public void configure(Object config, boolean caseSensitive) {
		super.configure(getClass().getResourceAsStream("resources/SimpleStopListDictionary.txt"), false);
	}

}

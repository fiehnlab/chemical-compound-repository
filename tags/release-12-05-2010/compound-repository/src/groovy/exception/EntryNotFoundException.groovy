package exception

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 5, 2010
 * Time: 12:22:58 PM
 * To change this template use File | Settings | File Templates.
 */
class EntryNotFoundException extends DatabaseError{

  def EntryNotFoundException() {
    super();    //To change body of overridden methods use File | Settings | File Templates.
  }

  def EntryNotFoundException(String message) {
    super(message);    //To change body of overridden methods use File | Settings | File Templates.
  }

  def EntryNotFoundException(String message, Throwable cause) {
    super(message, cause);    //To change body of overridden methods use File | Settings | File Templates.
  }

  def EntryNotFoundException(Throwable cause) {
    super(cause);    //To change body of overridden methods use File | Settings | File Templates.
  }
}

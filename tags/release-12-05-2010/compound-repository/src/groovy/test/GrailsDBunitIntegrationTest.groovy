package test

import com.fiehn.util.dbunit.DBunitUtil
import grails.test.GrailsUnitTestCase
import javax.sql.DataSource
import org.apache.log4j.Logger
import org.dbunit.dataset.xml.XmlDataSet

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Mar 4, 2010
 * Time: 2:53:04 PM

 * support for dbuint initialization before the actual test
 */
abstract class GrailsDBunitIntegrationTest extends GrailsUnitTestCase {

  final static String DEFAULT_DATASET = "test/datasets/compounds/data.xml"

  Logger logger = Logger.getLogger("test")

  DataSource dataSource

  /**
   * returns our default data set
   * @return
   */
  protected String getDataSet() {
    return DEFAULT_DATASET
  }


  protected void setUp() {
    super.setUp()

    logger.info "calling internal setup..."
    preSetup()

  }

  /**
   * setups the datalayer for us
   */
  protected synchronized void preSetup() {
    logger.info "setting up database"
    assertTrue(dataSource != null)

    File file = new File(getDataSet())

    logger.info "using: ${file}"
    assertTrue(file.exists());

    DBunitUtil.intialize(dataSource, new XmlDataSet(new FileReader(file)))

  }

}

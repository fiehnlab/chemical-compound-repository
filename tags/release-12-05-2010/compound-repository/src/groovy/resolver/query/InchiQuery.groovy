package resolver.query

import compound.repository.entries.Compound
import pattern.PatternHelper

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: May 4, 2010
 * Time: 12:38:12 PM
 */
class InchiQuery extends AbstractQuery {
  protected String getClassOfInterrest() {
    return "compound.repository.entries.Compound";  //To change body of implemented methods use File | Settings | File Templates.
  }

  protected String getQuerySymbol() {
    return "inchi";
  }

  protected GString buildQueryString() {
    return "Select distinct a.id from ${getClassOfInterrest()} as a where LOWER(a.${getQuerySymbol()}) = ?"
  }

  protected GString buildBatchQuery() {
    return "Select distinct a.id,a.${getQuerySymbol()} from ${getClassOfInterrest()} as a where lower(a.${getQuerySymbol()}) in ("
  }

  boolean match(String value) {
    return value.matches(PatternHelper.STD_INCHI_PATTERN)
  }
}

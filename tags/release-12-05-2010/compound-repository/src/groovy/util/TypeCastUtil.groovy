package util

import org.apache.log4j.Logger

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 5, 2010
 * Time: 1:56:28 PM
 * This utility is used to cast from one data type to other datatype 
 */
class TypeCastUtil
{

  private static TypeCastUtil instance = null

  private Logger  logger = Logger.getLogger(getClass())

  public static TypeCastUtil getInstance(){
    if (instance == null){
      instance = new TypeCastUtil()
    }

    return instance
  }

  def  castIntoInteger(def value)
  {
      def v

      if (value instanceof Collection)
      {
          v = []
          value.each {  val->
            try{
              v.add Integer.parseInt(val.toString())
            }catch (NumberFormatException e){
              logger.error(e.getMessage(),e)
            }
           }
      } else {
        if (value instanceof Integer == false) {
          try{
            v = Integer.parseInt(value.toString())
            }catch (NumberFormatException e){
              logger.error(e.getMessage(),e)
            }
         }else{
            v = value
        }
      }
      return v
  }

  def  castIntoLong(def value)
  {
      def v

      if (value instanceof Collection)
      {
          v = []
          value.each {  val->
            try{
              v.add Long.parseLong(val.toString())
              }catch (NumberFormatException e){
              logger.error(e.getMessage(),e)
            }
           }
      } else {
        if (value instanceof Long == false) {
           try{
             v = Long.parseLong(value.toString())
           }catch (NumberFormatException e){
              logger.error(e.getMessage(),e)
            }
         }else{
            v = value
         }
      }
      return v
  }

  def  castIntoDouble(def value)
  {
      def v

      if (value instanceof Collection)
      {
          v = []
          value.each {  val->
            try{
              v.add Double.parseDouble(val.toString())
              }catch (NumberFormatException e){
              logger.error(e.getMessage(),e)
            }
           }
      } else {
        if (value instanceof Double == false) {
            try{
              v = Double.parseDouble(value.toString())
              }catch (NumberFormatException e){
              logger.error(e.getMessage(),e)
            }
         }else{
            v = value
         }
      }

      return v
  }

  def  toLowerCase(def value)
  {
      def v

      if (value instanceof Collection)
      {
          v = []
          value.each {  val->
              v.add val?val.toString().toLowerCase():""
           }
      } else {
            v = value?value.toString().toLowerCase():""
      }
      return v
  }


  def  toUpperCase(def value)
  {
      def v

      if (value instanceof Collection)
      {
          v = []
          value.each {  val->
              v.add val?val.toString().toUpperCase():""
           }
      } else {
            v = value?value.toString().toUpperCase():""
      }
      return v
  }

}

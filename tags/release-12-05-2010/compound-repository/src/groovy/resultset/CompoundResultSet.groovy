package resultset
/**
 *  Result Class for upload
 */
class CompoundResultSet {
  String SNO

  String DefaultName

  String id

  String inchiHashKey

  String cas

  String sid

  String cid

  String kegg

  String hmdb

  String links

  String chebi

  String inchi

  String iupac

  String smiles

  String mass

  String inchikey

  String names

  String lipidmap

  String formula

  String DiscoveredInText

  String Origin

  String Reference

  String Translation

  String compound

  String From
}
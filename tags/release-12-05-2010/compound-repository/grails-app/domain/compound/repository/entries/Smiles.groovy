package compound.repository.entries
/**
 * smile codes
 */

class Smiles {


  static searchable = {

    code index: 'not_analyzed'
  }
  
  static belongsTo = [compound: Compound]

  static constraints = {
    code(maxSize: 15000, nullable: false)

  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  Date dateCreated
  Date lastUpdated

  String code

  String toString() {
    return "Smiles: ${code}"
  }
}

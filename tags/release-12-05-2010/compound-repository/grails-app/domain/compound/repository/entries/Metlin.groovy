package compound.repository.entries
class Metlin extends DBLinks {

  static constraints = {
  }


  static searchable = {

    metlinId index: 'not_analyzed', name : "metlin"
  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String metlinId

  String toString() {
    return "Metlin: ${metlinId}"
  }


  def getDefaultName() {
    return metlinId;
  }
}

package compound.repository.entries
class LipidMap extends DBLinks {


  static searchable = {

    lipidMapId index: 'not_analyzed', name : "lipidmap"
  }

  static constraints = {
    lipidMapId(maxSiz: 2000, unique: true)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String lipidMapId


  String toString() {
    return "LipidMapId: ${lipidMapId}";
  }


  def getDefaultName() {
    return lipidMapId;
  }
}

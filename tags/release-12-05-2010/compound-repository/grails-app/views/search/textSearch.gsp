<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="org.springframework.util.ClassUtils" contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Search Result</title>
<export:resource />
</head>


<body class="BodyBackground"  >

<div id="contents">

  <div id="main">
    <!--
    <br>
    <table border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="9" class="AppsLabel" align="center"><g.message code="SEARCH.RESULTS" /></td>
      </tr>

      <tr>
        <td colspan="9" align='right'>
       <!--   [ <g:link controller="search" action="index" params="[query: params.query]"><g:message code="SEARCH.BACK" /></g:link> ]
          [ <g:link controller="homePage" action="index" params="[query: params.query]" ><g:message code="HOME.BACK" /></g:link> ]
        </td>
      </tr>

      <tr height='2'><td>&nbsp;</td></tr>
    </table>
             -->
  <table  border='0' width="100%" align='center'>
  <tr>
  <td>
      <div id="pagePath">
        <h1><g:link controller="homePage" action="index" params="[query: params.query]" >Home(Search)</g:link> <span>&raquo;</span> <g:message code="SEARCH.RESULTS" /></h1>
      </div>
      <div class="line"></div>
    <div >&nbsp;</div>
    <table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG">
     <tr>
      <td>
         <table border="0" id="tblSample1" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG">
            <tr align='left' valign='bottom'>
              <td class="Label3" width="1%">&nbsp;</td>
              <td width="3%" valign="top"> Query  </td>
              <td class="Label3" width="1%">:</td>
              <td class="Label3" width="90%" valign="top" colspan="3" align="left">${params.query}</td>
              <td class="Label3" width="1%">&nbsp;</td>
            </tr>
         </table>
      </td>
     </tr>   

    <tr>
      <td>
          <table border="0" id="tblSample2" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG">
                     <tr align='left' valign='bottom' class="TableHeaderBG">
                        <td class="TableHeader" width="1%">&nbsp;</td>
                        <td class="TableHeader" width="35%"><g:message code="LIST.COMPOUND_NAME" /></td>
                        <td class="TableHeader" width="1%">&nbsp;</td>
                        <td class="TableHeader" width="45%"><g:message code="LIST.INCHI_HASH_KEY" /></td>
                        <td class="TableHeader" width="1%">&nbsp;</td>
                        <td class="TableHeader" width="20%"><g:message code="LIST.STRUCTURE" /></td>
                        <td class="TableHeader" width="1%">&nbsp;</td>
                      </tr>
               <%
              if( searchResult!=null && searchResult.results != null && searchResult.results.size() > 0 )
              {
                %>
                  <g:each var="compound" in="${searchResult.results}" status="index">
                          <%
                            String lStrClass = "";
                            String lColorFormulaBG = "";

                            if (index % 2 == 0) {
                              lStrClass = "TableRowBG2"
                              lColorFormulaBG = "#FFFFFD"
                            }else {
                              lStrClass = "TableRowBG1"
                              lColorFormulaBG = "#DBE8F6"
                            }

                          %>

                          <tr class='<%=lStrClass%>'>
                            <td class="Label3" width="1%">&nbsp;</td>
                            <td class="Label3" valign="top" align="left" width="35%">${compound.getDefaultName()}</td>
                            <td class="Label3" width="1%">&nbsp;</td>
                            <td class="Label3" valign="top" align="left" width="45%" ><g:link action="show" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>
                            <td class="Label3" width="1%">&nbsp;</td>
                            <td class="Label3" valign="top" align="left"><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}" /></td>
                            <td class="Label3" width="1%">&nbsp;</td>
                          </tr>

                  </g:each>
            <%
 }else
   {
     %>
    <tr>
        <td class="Label3" colspan="7">

         <table border=0 align ="center" width="98%">
         <tr> <td height='20'  > </td> </tr>

			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr>
			   <td align='center' height='70' valign="middle" > <font color='red' > No Compound Found... </font> </td>
			</tr>
			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr> <td height='20' > </td> </tr>
        </table>

        </td>

     </tr>
 <%   }
  %>
          </table>
      </td>
    </tr>
 <%
    if( searchResult!=null && searchResult.results != null && searchResult.results.size() > 0 )
    {
      %>
    <tr>
      <td>
           <g:customPaginate controller="search" action="textSearch" params="[query: params.query]" total="${searchResult.total}" prev="&lt; previous" next="next &gt;"/>
       </td>
    </tr>


      <tr>
        <td height="30px">
          <export:formats controller="search" action="textSearch" formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']" params="[query: params.query,total:searchResult.total]" />
        </td>
      </tr>


 <% } %>     
    </table>
   </td>
    </tr>
    </table>
</div>



<div id="footer">
  <g:render template="/footer" />
</div>
  
</div>
</body>
</html>
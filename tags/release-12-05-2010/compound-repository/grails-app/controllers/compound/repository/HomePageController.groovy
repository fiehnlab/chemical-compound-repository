package compound.repository

import compound.repository.entries.Compound

class HomePageController {

    def index = {

      // Remove session attribute
      if( session.getAttribute("ids") != null )
      {
        session.removeAttribute "ids"
      }
      if( session.getAttribute("query") != null )
      {
        session.removeAttribute "query"
      }

      def strQuery = "";
      if (params.query != null) {
        strQuery = params.query
      }

      return [query: strQuery]
    }
}

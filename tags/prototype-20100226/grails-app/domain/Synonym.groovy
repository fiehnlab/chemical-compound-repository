/**
 * a simple synonym for a compound
 */

class Synonym {

  /**
   * makes this synonym searchable
   */
  static searchable = {
    spellCheck "include"
  }
  
  static belongsTo = [compound: Compound]

  static constraints = {
    name(maxSize: 15000)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }

  String name

  public String toString() {
    return "Synonym: ${name}"
  }

  Date dateCreated
  Date lastUpdated

}


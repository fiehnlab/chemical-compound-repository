<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 22, 2010
  Time: 12:22:19 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Result</title></head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="contents">


<div id="main">
<table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">

    <tr>
		<td colspan="9" class="AppsLabel" align="center"> Result </td>
	</tr>

    <tr>
		<td colspan="9" align='right'>
                  [ <g:link controller="massLookup" action="index">Back to Mass Lookup</g:link> ]
				  [ <g:link controller="homePage" action="index">Back to home</g:link> ]
	    </td>
	</tr>

  <tr height='2'> <td> &nbsp; </td> </tr>
</table>



<table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG" >

  <tr>
    <td colspan="9">
      <table class="TableBG" width="100%">
      <tr>
          <td width="1%"> &nbsp; </td>
          <td width="3%" valign="top"> Ids  </td>
          <td width="1%" valign="top"> : </td>
          <td width="50%" colspan="6" class="Label3" align="left" valign="top">

          <% if( ids!=null && ids.size() > 0 )
              {
                for(int liCnt=0; liCnt < 10 && liCnt < ids.size(); liCnt++)
                  {

                     if( liCnt < ids.size()-1 )
                     {
                %>
                        ${ids.get(liCnt)+",&nbsp;"}
                <%   } else {%>
                        ${ids.get(liCnt)}
              <%    }
                  }

                if( ids.size() > 10 )
                    { %>
                    ${"..."}
                  <%
                    }
                  }%>
            
          </td>
          <td width="1%"> &nbsp; </td>
      </tr>
        <tr>
          <td width="1%"> &nbsp; </td>
          <td width="3%" valign="top">From </td>
          <td width="1%" valign="top"> : </td>
          <td width="15%" class="Label3" valign="top">${from}</td>
          <td width="1%"> &nbsp; </td>
          <td width="3%" valign="top"> To</td>
          <td width="1%" valign="top"> :</td>
          <td width="70%" class="Label3" align="left" valign="top">
              <% if( to!=null && to.size() > 0 )
                  {
                    for(int liCnt=0; liCnt < to.size(); liCnt++)
                      {

                         if( liCnt < to.size()-1 )
                         {
                    %>
                            ${to.get(liCnt)+",&nbsp;"}
                    <%   } else {%>
                            ${to.get(liCnt)}
                  <%    }
                      }
                    }%>
          </td>
          <td width="15%"> &nbsp; </td>
      </tr>
      </table>
    </td>
  </tr>

  <tr>
  <td colspan="9">
  <table border="0" id="tblSample1" width="100%" align='center' cellpadding="3" cellspacing="1" class="TableBG" >
   <tr align='left' valign='bottom' class="TableHeaderBG">

        <td class="TableHeader" width="10%" rowspan="2"> Name </td>

        <td class="TableHeader" width="10%" rowspan="2"> Hash Key </td>

		<td class="TableHeader" width="70%" colspan="${to.size}" align="center"> Translation </td>

        <td class="TableHeader" width="5%" rowspan="2"> Formula </td>
	</tr>

    <tr align='left' valign='bottom' class="TableHeaderBG">
    <% if( to!=null && to.size() > 0 )
      {
        for(int liCnt=0; liCnt < to.size(); liCnt++)
        {
	%>
            <td class="TableHeader" width="10%"> <%= to.get(liCnt) %> </td>
	<%    }
       } %>
    </tr>

  <%
  if( transform!=null && transform.size() > 0 )
  {
    int liCnt=0;
    String lStrClass = "";
    String lColorFormulaBG = "";
    for(Map map : transform)
    {
      Compound compound = map.compound

      if( compound==null)
         continue;

      if( liCnt++ % 2 == 0 )
      {
        lStrClass="TableRowBG2";
        lColorFormulaBG = "#FFFFFD";
      }
    else
      {
         lStrClass = "TableRowBG1";
         lColorFormulaBG = "#DBE8F6";
      }

      %>

  <tr class='<%=lStrClass%>'>

    <td class="Label3" valign="top" align="left">${compound.getDefaultName()}</td>
    <td class="Label3" valign="top" align="left"><g:link action="show" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>

    <%
     if( to!=null && to.size() > 0 )
     {
        HashMap lMap = (map.result);
        for(int liCnt1=0; liCnt1 < to.size(); liCnt1++)
        {
            String keyname = to.get(liCnt1)
            def lResult =  lMap.get(keyname.toString().trim())
	%>
    <td class="Label3" valign="top" align="left">
      <%
          if( lResult instanceof Collection)
          {
      %>
            <ul>
              <g:each var="i" in="${lResult}">
                  <li>${i?.encodeAsHTML()}</li>
              </g:each>
            </ul>
      <%
          }else
          {
      %>
              <ul>  <li>${lResult}</li>    </ul>
      <%
          }
      %>


    </td>
	<%   }
       }
    %>

      <td class="Label3" valign="top" align="left"><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}" /></td>

     </tr>
      <%
    }
 }else
   {
     %>
    <tr>
        <td class="Label3" colspan="${3+to.size}">

         <table border=0 align ="center" width="98%" class="TableBG">
         <tr> <td height='20' > </td> </tr>

			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr>
			   <td align='center' height='70' valign="middle" > <font color='red' > No Compound Found... </font> </td>
			</tr>
			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr> <td height='20' > </td> </tr>
        </table>

        </td>
        
     </tr>
 <%   }
  %>
      </table>
    </td>
  </tr>

</table>
</div>
  
<g:render template="/footer" />

</div>  

</body>
</html>
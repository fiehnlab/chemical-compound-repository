<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 22, 2010
  Time: 11:06:39 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head><title>Transform</title></head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="contents">

    <div id="main">
<g:form name="masslookup" controller="massLookup">
<table  border='0' width="50%" align='center' cellpadding="3" cellspacing="0" >
	<tr>
		<td colspan="5" class="AppsLabel" align="center"> Mass transform </td>
	</tr>
    <tr>
          <td colspan="5" class="TableHeaderDescription" align="center"> Mass transform from one identifier to many other identifiers </td>
    </tr>

    <tr>
		<td colspan="5" align='right'>
				  [ <g:link controller="homePage" action="index">Back to home</g:link> ]
	    </td>
	</tr>

   <tr height='1'> <td colspan="5"> &nbsp;

   <g:if test="${flash.message}">
     <div class="AlertLabel">
        ${flash.message}
     </div>
   </g:if>

   </td> </tr>
  
  <tr>
      <td width="1%"> &nbsp; </td>
      <td width="5%" valign="top"> Ids </td>
      <td width="1%" valign="top"> : </td>
      <td width= "95%"> <g:textArea name="ids" value="" rows="10" cols="80"></g:textArea> </td>
      <td width="1%"> &nbsp; </td>
  </tr>
  <tr>
    <td width="1%"> &nbsp; </td>
    <td width="5%" valign="top"> From </td>
    <td width="1%" valign="top"> : </td>
    <td width="10%"><g:select name="from" from="${fromFields}"/> </td>
    <td width="1%"> &nbsp; </td>
  </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td width="5%" valign="top"> To </td>
    <td width="1%" valign="top"> : </td>
    <td width= "60%">
         <table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">              
          <%
        int liCnt = 0;

        for(int i=0 ; i < toFields.size() ; i++)
          {
       %>
          <tr>
       <%
            for(int j=0 ; j < 4 && i < toFields.size() ; j++)
            {
              String str = toFields.get(i);
      %>
          <td> <g:checkBox name="to" value="${str}" checked="false" />${str}   </td>
            <%
                i++;  
              }
              i--;
            %>
          </tr>
        <%
          }
        %>
       </table>

    </td>
    <td width="1%"> &nbsp; </td>
  </tr>
  
  <tr height='1'> <td> &nbsp; </td> </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td colspan="3" align="center"> &nbsp; <g:actionSubmit value="Submit" action="masstransform" controler="massLookup"/></td>
    <td width="1%"> &nbsp; </td>
  </tr>

  <tr>
    <td width="1%"> &nbsp; </td>
    <td colspan="3" align="left" >
      <table>
        <tr>
          <td width="5%">
               Note:
          </td>
          <td>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="LableTips" align="left">
               Enter Ids in vertical feshion like <br>C00001 <br>C00002 <br>C00003 <br>C00004 <br>C00005 <br>C00006
          </td>
        </tr>
      </table>
    </td>
    <td width="1%"> &nbsp; </td>
  </tr>
</table>

</g:form>
       </div>
<g:render template="/footer" />  
  </div>
  </body>

</html>
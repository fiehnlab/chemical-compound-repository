<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Result</title></head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="contents">

    <div id="main">

<table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9" class="AppsLabel" align="center"> Result </td>
	</tr>

    <tr>
		<td colspan="9" align='right'>
                  [ <g:link controller="transform" action="index">Back to transform</g:link> ]
				  [ <g:link controller="homePage" action="index">Back to home</g:link> ]
	    </td>
	</tr>

  <tr height='2'> <td> &nbsp; </td> </tr>
</table>



<table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >

  <tr>
    <td colspan="9">
      <table class="TableBG" width="100%">
      <tr>
          <td width="1%"> &nbsp; </td>
          <td width="3%"> Id : </td>
          <td width="30%" class="Label3" align="left">${id}</td>
          <td width="1%"> &nbsp; </td>
          <td width="5%">From : </td>
          <td width="5%" class="Label3">${from}</td>
          <td width="1%"> &nbsp; </td>
          <td width="5%"> To :</td>
          <td width="5%" class="Label3">${to}</td>
          <td width="40%"> &nbsp; </td>
      </tr>
      </table>
    </td>
  </tr>



  <tr>
    <td colspan="9">
  <table border="0" id="tblSample1" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >
  <tr align='left' valign='bottom' class="TableHeaderBG">
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="30%"> Name </td>
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="20%"> Hash Key </td>
		<td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="40%"> Translation </td>
        <td class="TableHeader" width="1%">&nbsp; </td>
		<td class="TableHeader" width="5%"> Formula </td>
		<td class="TableHeader" width="1%">&nbsp;</td>
	</tr>


  <%
  if( transform!=null && transform.size() > 0 )
  {
    int liCnt=0;
    String lStrClass = "";
    String lColorFormulaBG = "";
    for(Map map : transform)
    {
      Compound compound = map.compound

      if( compound==null)
         continue;

      if( liCnt++ % 2 == 0 )
      {
        lStrClass="TableRowBG2";
        lColorFormulaBG = "#FFFFFD";
      }
    else
      {
         lStrClass = "TableRowBG1";
         lColorFormulaBG = "#DBE8F6";
      }

      %>
  <tr class='<%=lStrClass%>'>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left">${compound.getDefaultName()}</td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left"><g:link action="show" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left">
    <%
          if( map.result instanceof Collection)
          {
      %>
            <ul>
              <g:each var="i" in="${map.result}">
                  <li>${i?.encodeAsHTML()}</li>
              </g:each>
            </ul>
      <%
          }else
          {
      %>
              ${map.result}
      <%
          }
      %>


  </td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" valign="top" align="left"><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}" /></td>
  <td class="Label3" width="1%">&nbsp;</td>
     </tr>
      <%
    }
 }else
   {
     %>
    <tr>
        <td class="Label3" width="1%">&nbsp;</td>
        <td class="Label3" colspan="7">

          <table border=0 align ="center" width="100%" BGCOLOR="WHITE" >
         <tr> <td height='20' > </td> </tr>

			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr>
			   <td align='center' height='70' valign="middle" > <font color='red' > No Compound Found... </font> </td>
			</tr>
			<tr bgcolor="Gray">
			   <td height='2' align='center' valign="middle" ></td>
			</tr>
			<tr> <td height='20' > </td> </tr>
        </table>

        </td>
        <td class="Label3" width="1%">&nbsp;</td>
     </tr>
 <%   }
  %>
      </table>
    </td>
  </tr>


</table>
     </div>
<g:render template="/footer" />
</div>
</body>
</html>
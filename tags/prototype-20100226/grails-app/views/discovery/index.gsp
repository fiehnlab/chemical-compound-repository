<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 2, 2010
  Time: 2:40:38 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Discovery</title>
</head>
<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="contents">

    <div id="main">
<g:form name="discoveryForm" controller="discovery">

  <table id='tabTitle' border='0' width="60%" align='center' cellpadding="0" cellspacing="0">
      <tr>
          <td colspan="6" class="AppsLabel" align="center"> Discovery </td>
      </tr>
      <tr>
          <td colspan="6" class="TableHeaderDescription" align="center"> here you can discover compound in your provided text</td>
      </tr>
      <tr>
          <td colspan="6" align='right'>
                    [ <g:link controller="homePage" action="index">Back to home</g:link> ]
          </td>
      </tr>

    <tr height='2'> <td> &nbsp; </td> </tr>

    <tr>
      <td width="1%"> &nbsp;</td>
      <td width="10%" valign="top"> Query </td>
      <td width="5%"  valign="top"> : </td>
      <td width="1%"> &nbsp;</td>
      <td width="60%">
        <g:textArea name="query" value="Hello benzene world, you are full of vitamin a" rows="10" cols="100"></g:textArea>
      </td>
      <td width="1%"> &nbsp;</td>
    </tr>

  <tr height='2'> <td> &nbsp; </td> </tr>

    <tr>
      <td width="1%"> &nbsp;</td>
      <td colspan="4" align="center">
        <g:actionSubmit value="Submit"  action="process"/>
      </td>
      <td width="1%"> &nbsp;</td>
    </tr>
   </table>
</g:form>
      </div>
<g:render template="/footer" />  
  </div>

</body>
</html>
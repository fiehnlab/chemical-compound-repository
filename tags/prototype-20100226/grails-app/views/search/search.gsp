<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Result</title></head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="contents">

    <div id="main">
<table  border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="9" class="AppsLabel" align="center"> Result </td>
	</tr>

    <tr>
		<td colspan="9" align='right'>
                  [ <g:link controller="search" action="index">Back to search</g:link> ]
				  [ <g:link controller="homePage" action="index">Back to home</g:link> ]
	    </td>
	</tr>

  <tr height='2'> <td> &nbsp; </td> </tr>
</table>



<table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0"class="TableBG" >

  <tr align='left' valign='bottom' class="TableHeaderBG">
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="5%"> S.No. </td>
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="5%"> Compound </td>
		<td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="15%"> Key </td>
        <td class="TableHeader" width="1%">&nbsp; </td>
	</tr>


  <%

    int liCnt=0;
    String lStrClass = "";
    for(Compound compound : result)
    {
      if( liCnt++ % 2 == 0 )
         lStrClass="TableRowBG2";
    else
         lStrClass = "TableRowBG1";


      %>
      <tr class='<%=lStrClass%>'>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3" width="1%">${liCnt}</td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3"><g:link action="show" controller="compound" params="[id:compound.id]"  >${compound.id}</g:link> </td>
  <td class="Label3" width="1%">&nbsp;</td>
  <td class="Label3">${compound.inchiHashKey.completeKey}</td>
  <td class="Label3" width="1%">&nbsp;</td>
     </tr>
      <%
    }
  %>
</table>
</div>
<g:render template="/footer" />
</div>
</body>
</html>
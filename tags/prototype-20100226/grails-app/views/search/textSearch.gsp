<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="org.springframework.util.ClassUtils" contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Result</title></head>

<style type="text/css">@import url("${createLinkTo(dir: 'css',file : 'WebGUIStandards.css')}");</style>

<body topMargin=0 background="${createLinkTo(dir: 'images',file : 'FormBG.gif')}"  >

<div id="contents">

  <div id="main">
    <br>
    <table border='0' width="100%" align='center' cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="9" class="AppsLabel" align="center">Result</td>
      </tr>

      <tr>
        <td colspan="9" align='right'>
          [ <g:link controller="search" action="index">Back to search</g:link> ]
          [ <g:link controller="homePage" action="index">Back to home</g:link> ]
        </td>
      </tr>

      <tr height='2'><td>&nbsp;</td></tr>
    </table>

    <table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG">
     <tr>
      <td>
         <table border="0" id="tblSample1" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG">
            <tr align='left' valign='bottom'>
              <td class="Label3" width="1%">&nbsp;</td>
              <td width="3%" valign="top"> Query  </td>
              <td class="Label3" width="1%">:</td>
              <td class="Label3" width="90%" valign="top" colspan="3" align="left">${params.query}</td>
              <td class="Label3" width="1%">&nbsp;</td>
            </tr>
         </table>
      </td>
     </tr>   

    <tr>
      <td>
          <table border="0" id="tblSample2" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG">
                     <tr align='left' valign='bottom' class="TableHeaderBG">
                        <td class="TableHeader" width="1%">&nbsp;</td>
                        <td class="TableHeader" width="35%">Name</td>
                        <td class="TableHeader" width="1%">&nbsp;</td>
                        <td class="TableHeader" width="45%">Hash Key</td>
                        <td class="TableHeader" width="1%">&nbsp;</td>
                        <td class="TableHeader" width="20%">Formula</td>
                        <td class="TableHeader" width="1%">&nbsp;</td>
                      </tr>

                  <g:each var="compound" in="${searchResult.results}" status="index">
                     <div class="result">
                          <%
                            String lStrClass = "";
                            String lColorFormulaBG = "";

                            if (index % 2 == 0) {
                              lStrClass = "TableRowBG2"
                              lColorFormulaBG = "#FFFFFD"
                            }else {
                              lStrClass = "TableRowBG1"
                              lColorFormulaBG = "#DBE8F6"
                            }

                          %>

                          <tr class='<%=lStrClass%>'>
                            <td class="Label3" width="1%">&nbsp;</td>
                            <td class="Label3" valign="top" align="left" width="35%">${compound.getDefaultName()}</td>
                            <td class="Label3" width="1%">&nbsp;</td>
                            <td class="Label3" valign="top" align="left" width="45%" ><g:link action="show" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>
                            <td class="Label3" width="1%">&nbsp;</td>
                            <td class="Label3" valign="top" align="left"><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}" /></td>
                            <td class="Label3" width="1%">&nbsp;</td>
                          </tr>
                        </div>
                  </g:each>
          </table>
      </td>
    </tr>

    <tr>
      <td>
          <div>
            <div class="paging">
              Page:
              <g:set var="totalPages" value="${Math.ceil(searchResult.total / searchResult.max)}"/>
              <g:if test="${totalPages == 1}"><span class="currentStep">1</span></g:if>
              <g:else><g:paginate controller="search" action="textSearch" params="[query: params.query]" total="${searchResult.total}" prev="&lt; previous" next="next &gt;"/></g:else>
            </div>
          </div>
      </td>
    </tr>
    </table>
  </div>
  <g:render template="/footer"/>
</div>
</body>
</html>
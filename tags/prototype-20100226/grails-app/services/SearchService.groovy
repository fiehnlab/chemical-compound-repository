import pattern.PatternHelper

class SearchService {

  //association to the lookup service
  LookupService lookupService

  /**
   * reference to the searchable lucene
   * service
   */
  SearchableService searchableService = null


  boolean transactional = true

  /**
   * executes a generic search against the datbase dependend of regular expressions
   */
  def searchGeneric(String value, Map params = [:]) {

    if (value.matches(PatternHelper.STD_INCHI_KEY_PATTERN))
      return lookupService.lookupByInchiKey(value,params)
    else if (value.matches(PatternHelper.STD_INCHI_PATTERN))
      return lookupService.lookupByInchi(value,params)
    else if (value.matches(PatternHelper.CAS_PATTERN))
        return lookupService.lookupByCas(value,params)
      else if (value.matches(PatternHelper.KEGG_PATTERN))
          return lookupService.lookupByKegg(value,params)
        else if (value.matches(PatternHelper.LIPID_MAPS_PATTERN))
            return lookupService.lookupByLipidMapId(value,params)
          else if (value.matches(PatternHelper.HMDB_PATTERN))
              return lookupService.lookupByHMDB(value,params)
            else if (value.matches(PatternHelper.SID_PATTERN))
                return lookupService.lookupBySID(Integer.parseInt(value.split(":")[1]),params)
              else if (value.matches(PatternHelper.CID_PATTERN))
                  return lookupService.lookupByCID(Integer.parseInt(value.split(":")[1]),params)
                else
                  return lookupService.lookupByName(value,params)
  }

  /**
   * executes a lucene/compass search
   */
  def textSearch(String query, Map params = [:]) {
    def result = Compound.search(params.query, params)

    return result
  }
}

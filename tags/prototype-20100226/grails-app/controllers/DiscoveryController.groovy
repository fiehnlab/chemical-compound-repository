import resolver.DatabaseContentResolver
import resolver.Resolveable
import resolver.OscarResolver
import resolver.SingleLineRegExpressionResolver
import types.Hit
import com.fiehn.util.StopListParser

/**
 * this controller is used to discover chemical compounds in the given parameter
 */
class DiscoveryController {

  DiscoveryService discoveryService

  def index = { }
  /**
   * process a query and returns a map containning all the found chemical names
   */
  def process = {

    //logger is added automatically by grails
    log.debug "executing a query..."

    //params are added automatically by grails
    assert (params.query != null)

    def limitConfidence = 0.5

    //if we don't have a confidence limit set we use the default
    //otherwise we assign on
    if (params.limitConfidence != null) {
      limitConfidence = params.limitConfidence
    }

    log.debug "params.query ::: "+params.query
 
    //contains the final result
    Collection<Map<Compound, ?>> result = discoveryService.process(params.query ,limitConfidence)

    //forward to the controller
    return [result: result]
  }

  
}

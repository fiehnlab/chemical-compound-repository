import org.compass.core.engine.SearchEngineQueryParseException

class SearchController {

  /**
   * reference to the search services
   */
  SearchService searchService = null

  def index = { }

  /**
   * search against the controller
   */
  def search = {
    assert params.query != null, "you need to provide some a query in the params object"


    def result = searchService.searchGeneric(params.query,params)

    //forward to the controller
    return [result: result]
  }

  /**
   * advanced search against the controller
   */
  def advancedSearch = {}

  /**
   * executes a text based search using lucene and compass
   */
  def textSearch = {
    assert params.query != null, "you need to provide some a query in the params object"

    if (!params.query?.trim()) {
      return [:]
    }
    try {
      return [searchResult: searchService.textSearch(params.query, params)]
    } catch (SearchEngineQueryParseException ex) {
      return [parseException: true]
    }
  }
}

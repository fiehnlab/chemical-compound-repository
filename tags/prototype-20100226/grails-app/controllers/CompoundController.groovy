

/**
 * simpl compoun controllr
 */
class CompoundController {
                  
    def show = {
        def compoundInstance = Compound.get( params.id )

        if(!compoundInstance) {
            flash.message = "Compound not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ compoundInstance : compoundInstance ] }
    }
}

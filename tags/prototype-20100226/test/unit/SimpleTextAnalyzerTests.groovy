import resolver.analyzer.Analyzer
import resolver.analyzer.SimpleTextAnalyzer

/**
 * tests the text analyzer
 */
class SimpleTextAnalyzerTests extends GroovyTestCase {

  String document = """

  Glucose (Glc), a monosaccharide (or simple sugar) also known as grape sugar, blood sugar, or corn sugar, is a very important carbohydrate in biology. The living cell uses it as a source of energy and metabolic intermediate. Glucose is one of the main products of photosynthesis and starts cellular respiration in both prokaryotes (bacteria and archaea) and eukaryotes (animals, plants, fungi, and protists).
The name "glucose" comes from the Greek word glukus (??????), meaning "sweet", and the suffix "-ose," which denotes a sugar.
Two stereoisomers of the aldohexose sugars are known as glucose, only one of which (D-glucose) is biologically active. This form (D-glucose) is often referred to as dextrose monohydrate, or, especially in the food industry, simply dextrose (from dextrorotatory glucose[2]). This article deals with the D-form of glucose. The mirror-image of the molecule, L-glucose, cannot be metabolized by cells in the biochemica
  """

  protected void setUp() {
    super.setUp()
  }

  protected void tearDown() {
    super.tearDown()
  }

  /*
  void testAnalyzeGenericTest() {

    Analyzer ana = new SimpleTextAnalyzer()

    Set<String> result = ana.analyze(document)

    assertTrue(result.contains("glucose"))
    assertTrue(result.contains("aldohexose"))
    assertTrue(result.contains("D-glucose"))
    assertTrue(result.contains("Glc"))

    assertTrue(result.contains("dextrorotatory"))
    assertTrue(result.contains("animals"))
    assertTrue(result.contains("bacteria"))
    assertTrue(result.contains("fungi"))


  }

*/



  void testAnalyzeSynonymsTest() {

    //a couple of synonyms to test
    String synonomys = """

1-Methylhistidine
1,3-Diaminopropane
2-Ketobutyric acid
2-Hydroxybutyric acid
2-Methoxyestrone
(R)-3-Hydroxybutyric acid
Deoxyuridine
Deoxycytidine
Cortexolone
Deoxycorticosterone
2-methoxy-12-methyloctadec-17-en-5-ynoyl anhydride
N-(3S-hydroxydecanoyl)-L-serine
N-(3-(hexadecanoyloxy)-heptadecanoyl)-L-ornithine
N-(9Z,12Z,15Z-octadecatrienoyl)-glutamine
N-(3-(15-methyl-hexadecanoyloxy)-13-methyl-tetradecanoyl)-L-serine
2-((2S)-6-amino-2-(3-hydroxy-14-methylpentadecanamido)hexanoyloxy)ethyl-2-hydroxy-13-methyltetradecanoate
N-hydroxydecanamide
(9S,10S)-10-hydroxy-9-(phosphonooxy)octadecanoic acid
6-(6-aminohexanamido)hexanoic acid
hexadecanoic acid
ethanoic acid
propanoic acid
butanoic acid
pentanoic acid
hexanoic acid
heptanoic acid
octanoic acid
nonanoic acid
decanoic acid
undecanoic acid
dodecanoic acid
tridecanoic acid
tetradecanoic acid
pentadecanoic acid
heptadecanoic acid
octadecanoic acid
nonadecanoic acid
eicosanoic acid
heneicosanoic acid
docosanoic acid
tricosanoic acid
tetracosanoic acid
pentacosanoic acid
hexacosanoic acid
heptacosanoic acid
octacosanoic acid
nonacosanoic acid
triacontanoic acid
hentriacontanoic acid
dotriacontanoic acid
tritriacontanoic acid
tetratriacontanoic acid
pentatriacontanoic acid
hexatriacontanoic acid
heptatriacontanoic acid
octatriacontanoic acid
hexatetracontanoic acid
methanoic acid
2-hydroxy-2-methyl-propanoic acid
17-methyl-6Z-octadecenoic acid
6-methyl-octanoic acid
7-methyl-octanoic acid
10-methyl-undecanoic acid
10-methyl-dodecanoic acid
11-methyl-dodecanoic acid
12-methyl-tridecanoic acid
12-methyl-tetradecanoic acid
13-methyl-tetradecanoic acid
14-methy-pentadecanoic acid
14-methyl-hexadecanoic acid
15-methyl-hexadecanoic acid
10-methyl-heptadecanoic acid
16-methyl-heptadecanoic acid
10-methyl-octadecanoic acid
16-methyl-octadecanoic acid
18-methyl-nonadecanoic acid
(+)-18-methyl-eicosanoic acid
20-methyl-heneicosanoic acid
(+)-20-methyl-docosanoic acid
22-methyl-tricosanoic acid
3,13,19-trimethyl-tricosanoic acid
23-methyl-tetracosanoic acid
24-methyl-pentacosanoic acid
(+)-24-methyl-hexacosanoic acid
26-methyl-heptacosanoic acid
2,4,6-trimethyl-octacosanoic acid
(+)-28-methyl-triacontanoic acid
2-methyl-2Z-butenoic acid
2-methyl-2E-butenoic acid
4-methyl-3-pentenoic acid
2,4,6-trimethyl-2Z-tetracosenoic acid
2,6-dimethyl-nonadecanoic acid
2,6-dimethyl-undecanoic acid
2,6-dimethyl-dodecanoic acid
4,8-dimethyl-dodecanoic acid
4,12-dimethyl-tridecanoic acid
2,6-dimethyl-tetradecanoic acid
2,8-dimethyl-tetradecanoic acid
2,6-dimethyl-pentadecanoic acid
4,8-dimethyl-pentadecanoic acid
vitamin a
vitamine a
(1R,4aS,7S,7aR)-4,7-dimethyl-1,4a,5,6,7,7a-hexahydrocyclopenta[c]pyran-1-ol

(+)-3-Carene(+)-Delta(3)-carene(1S)-(+)-3-carene(1S)-3,7,7-trimethylbicyclo[4.1.0]hept-3-ene(1S,6R)-(+)-3-carene(S)-(+)-3-carene
(1S,6R)-3,7,7-trimethylbicyclo[4.1.0]hept-3-ene1alpha,6alpha-car-3-ene
(1R,2S,5R)-2-methyl-5-[(1R)-1-methyl-2-oxoethyl]cyclopentanecarbaldehyde


  """

    Analyzer ana = new SimpleTextAnalyzer()

    Set<String> result = ana.analyze(synonomys)

    println "false negative"

    synonomys.split("\n").each {
      if (!result.contains(it.trim())) {
        if (it.trim().size() > 0) {
          println "negative: ${it}"
        }
      }
    }

    println "-----------"

    println "false positive"
    result.each {String s ->

      if (!synonomys.split("\n").toList().contains(s)) {
        println "positive: ${s}"
      }

    }
    println "-----------"

    //our checks
    assertTrue(result.contains("(R)-3-Hydroxybutyric acid"))
    assertTrue(result.contains("1-Methylhistidine"))
    assertTrue(result.contains("1,3-Diaminopropane"))
    assertTrue(result.contains("2-Ketobutyric acid"))
    assertTrue(result.contains("2-Hydroxybutyric acid"))
    assertTrue(result.contains("2-Methoxyestrone"))
    assertTrue(result.contains("Deoxyuridine"))
    assertTrue(result.contains("Cortexolone"))
    assertTrue(result.contains("Deoxycorticosterone"))
    assertTrue(result.contains("6-(6-aminohexanamido)hexanoic acid"))
    assertTrue(result.contains("N-(3S-hydroxydecanoyl)-L-serine"))
    assertTrue(result.contains("N-(3-(hexadecanoyloxy)-heptadecanoyl)-L-ornithine"))
    assertTrue(result.contains("N-(3-(15-methyl-hexadecanoyloxy)-13-methyl-tetradecanoyl)-L-serine"))
    assertTrue(result.contains("N-hydroxydecanamide"))
    assertTrue(result.contains("2-((2S)-6-amino-2-(3-hydroxy-14-methylpentadecanamido)hexanoyloxy)ethyl-2-hydroxy-13-methyltetradecanoate"))
    assertTrue(result.contains("6-(6-aminohexanamido)hexanoic acid"))
    assertTrue(result.contains("2-methoxy-12-methyloctadec-17-en-5-ynoyl anhydride"))


  }

    void testAnalyzeGenericTest() {

      Analyzer ana = new SimpleTextAnalyzer()

      Set<String> result = ana.analyze(document)

      assertTrue(result.contains("glucose"))
      assertTrue(result.contains("aldohexose"))
      assertTrue(result.contains("D-glucose"))
      assertTrue(result.contains("Glc"))

      assertTrue(result.contains("dextrorotatory"))
      assertTrue(result.contains("animals"))
      assertTrue(result.contains("bacteria"))
      assertTrue(result.contains("fungi"))


    }



  void testAnalyzeManySynonymsTest() {

    //a couple of synonyms to test
    String synonomys = """
Fast Yellow S(Biological stain)
6-Amino-3,4'-azodibenzenesulfonic acid
Cilefa Yellow R
Disodium 4-aminoazobenzene-3,4'-disulfonate
Food Yellow 2
Yellow acid
Aniline Yellow (Biological stain) (VAN)
Amacid Yellow RG
C.I. Food Yellow 2
5-(acetylamino)-8-amino-2,3-naphthalenedisulfonic acid
5-amino-6-ethoxy-2-naphthalenesulfonic acid
Ethoxy Cleve's Acid
C.I. 38480
2-Naphthalenesulfonic acid, 5-amino-6-ethoxy-
5-Amino-6-ethoxy-2-naphthalenesulfonic acid
Kyselina 1-amino-2-ethoxynaftalen-6-sulfonova (CZECH)
Kyselina ethoxy-cleve-1,6 (CZECH)
9-acridinylamine
Acridine, 9-amino-, monohydrochloride
Acridine, 9-amino-, hydrochloride
Aminoacridine hydrochloride
Acramine Yellow
9-Acridinamine monohydrochloride
9-Aminoacridine monohydrochloride
Monacrin
9-Aminoacridine hydrochloride
Aminacrine hydrochloride(USAN)
Mycosert
Aminacrine Hydrochloride (USAN)
9-Acridinamine, monohydrochloride
9-Aminoacridinium chloride
9-acridinamine
9-acridinamine 9-acridinylamine
Benzenesulfonic acid, {5-amino-2-[(4-methylphenyl)amino]-}
Benzenesulfonic acid, 5-amino-2-p-toluidino-
5-Amino-2-p-toluidinobenzenesulfonic acid
5-amino-2-(4-toluidino)benzenesulfonic acid
1-Anthracenesulfonic acid, 8-amino-5,7-dibromo-9,10-dihydro-9, 10-dioxo-
8-amino-5,7-dibromo-9,10-dioxo-9,10-dihydro-1-anthracenesulfonic acid
2-Anthracenesulfonic acid, 1-amino-4-bromo-9, 10-dihydro-9,10-dioxo-
1-Amino-4-bromo-9,10-dihydro-9,10-dioxo-2-anthracenesulfonic acid
Alizarine Cyanol Grey G (VAN)
Bromamine acid
Bromaminic acid
1-amino-4-bromo-9,10-dioxo-9,10-dihydro-2-anthracenesulfonic acid
9,10-dioxo-9,10-dihydro-1-anthracenesulfonic acid
Sodium anthraquinone-.alpha.-sulfonate
Sodium anthraquinone-1-sulfonate
Golden Salt
Gold Salt
Alizarin Blue S
5,6-dihydroxy-7,12-dioxo-4a,5,6,7,12,12b-hexahydronaphtho[2,3-f]quinoline-5,6-disulfonic acid
Fumaric acid dibenzyl ester
Benzyl fumarate
2-Butenedioic acid (E)-, bis(phenylmethyl) ester
dibenzyl 2-butenedioate
Fumaric acid, dibenzyl ester
Dibenzyl fumarate
Red Violet 2RN Acid Anthraquinone
1-Anilino-9,10-dihydro-9, 10-dioxo-2-anthroic acid
1-anilino-9,10-dioxo-9,10-dihydro-2-anthracenecarboxylic acid
2-Anthracenecarboxylic acid, 9, 10-dihydro-9,10-dioxo-1-(phenylamino)-
2-Anthroic acid, 1-anilino-9,10-dihydro-9,10-dioxo-
Blue anthraquinone dye B/M
Sky Blue base
9, 10-Anthracenedione, {1-amino-2-bromo-4-[(4-methylphenyl)amino]-}
Solvent Blue 12
Waxoline Blue BA
Alizarine Blue GRL Base
Anthraquinone Blue SKY Base
Anthraquinone, 1-amino-2-bromo-4-p-toluidino-
1-amino-2-bromo-4-(4-toluidino)anthra-9,10-quinone
C.I. Solvent Blue 12
Ahcoquinone Sky Blue B Base
Toyo Oriental Oil Black K
1-Amino-2-bromo-4-p-toluidinoanthraquinone
C.I. 62100
Oil Soluble Anthraquinone Pure Blue K
1-amino-4-anilino-2-methoxyanthra-9,10-quinone
1-((4'-((2-carboxy-9,10-dioxo-9,10-dihydro-1-anthracenyl)amino)[1,1'-biphenyl]-4-yl)amino)-9,10-dioxo-9,10-dihydro-2-anthracenecarboxylic acid
{3-Oxiranyl-7-oxabicyclo[4.1.0]heptene}
7-Oxabicyclo(4.1.0)heptane, 3-(epoxyethyl)-
1,2-Epoxy-4-(epoxyethyl)cyclohexane
4-Vinyl-1,2-cyclohexene diepoxide
1-(Epoxyethyl)-3,4-epoxycyclohexane
Unox 4206
4-Vinyl-1-cyclohexene diepoxide
EP-206
Vinylcyclohexene dioxide
1-Vinyl-3-cyclohexene dioxide
Vinyl cyclohexene dioxide
4-Vinlycyclohexene dioxide
RD4
4-Vinyl-1-cyclohexene dioxide
Erla-2271
Chissonox 206
3-(2-oxiranyl)-7-oxabicyclo[4.1.0]heptane
4-Vinylcyclohexene diepoxide
Vinyl cyclohexene diepoxide
{7-Oxabicyclo[4.1.0]heptane,} 3-oxiranyl-
{7-Oxabicyclo[4.1.0]heptane,} 3-(epoxyethyl)-
Erla-2270
1-Ethyleneoxy-3,4-epoxycyclohexane
4-(1, {2-Epoxyethyl)-7-oxabicyclo[4.1.0]heptane}
3-(1, {2-Epoxyethyl)-7-oxabicyclo[4.1.0]heptane}
Unox Epoxide 206
{4-(Epoxyethyl)-7-oxabicyclo[4.1.0]heptane}
Unox epoxide 206
{3-(Epoxyethyl)-7-oxabicyclo[4.1.0]heptane}
1,2-Epoxy-2,4,4-trimethylpentane
Pentane, 1,2-epoxy-2,4,4-trimethyl-
Oxirane, 2-(2,2-dimethylpropyl)-2-methyl-
2-methyl-2-neopentyloxirane
2,4,4-Trimethyl-1,2-epoxypentane
Oxirane, 2-methyl-2-neopentyl-
Acrolein, cyclic diacetal with pentaerythritol
{2,4,8,10-Tetraoxaspiro[5.5]undecane,} 3,9-diethenyl-
Acrolein, cyclic neopentanetetrayl acetal
3,9-Divinyl-2,4,8, {10-tetraoxaspiro[5.5]undecane}
Acrolein pentaerythritol bisacetal
3,9-Divinylspirobi(m-dioxane)
Diallylidenepentaerythritol
Diallylidene pentaerythritol
Acrolein-pentaerythritol dicyclic acetal
{2,4,8,10-Tetraoxaspiro[5.5]undecane,} 3, 9-divinyl-
3,9-divinyl-2,4,8,10-tetraoxaspiro[5.5]undecane
2-phenoxy-1-phenylethanone
Epoxide 201
Chissonox 201
Cyclohexanecarboxylic acid, 3,4-epoxy-6-methyl-, 3, 4-epoxy-6-methylcyclohexylmethyl ester
Unox Epoxide 201
3, 4-Epoxy-6-methylcyclohexylmethyl 3', 4'-epoxy-6'-methylcyclohexanecarboxylate
Unox 201
6-Methyl-3, 4-epoxycyclohexylmethyl 6-methyl-3,4-epoxycyclohexanecarboxylate
(4-methyl-7-oxabicyclo[4.1.0]hept-3-yl)methyl 4-methyl-7-oxabicyclo[4.1.0]heptane-3-carboxylate
3, 4-Epoxy-6-methylcyclohexylmethyl 3,4-epoxy-6-methylcyclohexane carboxylate
{7-Oxabicyclo[4.1.0]heptane-3-carboxylic} acid, 4-methyl-, {(4-methyl-7-oxabicyclo[4.1.0]hept-3-yl)methyl} ester
3, 4-Epoxy-6-methylcyclohexenecarboxylic acid (3, 4-epoxy-6-methylcyclohexylmethyl) ester
4, 5-Epoxy-2-methylcyclohexylmethyl 4, 5-epoxy-2-methylcyclohexanecarboxylate
3,4-Epoxy-6-methylcyclohexylmethyl 3, 4-epoxy-6-methylcyclohexanecarboxylate
EP 201
1-phenyl-2-(4-propoxyphenoxy)ethanone
allyl mesityl ether
2-(allyloxy)-1,3,5-trimethylbenzene
2-(allyloxy)-1,3,5-trimethylbenzene allyl mesityl ether
1-(3-methyl-3-butenyl)isoquinoline
2-(1-isoquinolinyl)-1,1-diphenylethanol
Methyl cyanide
Acetonitrile
Cyanure de methyl (FRENCH)
Methanecarbonitrile
Acetonitril (GERMAN, DUTCH)
Methane, cyano-
Cyanomethane
Usaf ek-488
Ethanenitrile
acetonitrile
Ethyl nitrile
acetaldehyde
Acetic aldehyde
Acetaldeyde
ACETALDEHYDE
Aldeide acetica (ITALIAN)
Acetaldehyde ( )
Ethanal
Ethyl aldehyde
Acetaldehyd (GERMAN)
NCI-C56326
Aldehyde acetique (FRENCH)
Octowy aldehyd (POLISH)
Acetamidine hydrochloride
ethanimidamide
Ethanimidamide, monohydrochloride
Acetamidinium chloride
Acetamidine monohydrochloride
Acetamidine, monohydrochloride
.alpha.-Amino-.alpha.-iminoethane hydrochloride
Ethanamidine hydrochloride
SN 4455
Ethenylamidine hydrochloride
Acediamine hydrochloride
2-Propenyl bromide
3-bromo-1-propene
Bromallylene
Propene, 3-bromo-
3-Bromo-1-propene
1-Bromo-2-propene
3-Bromopropene
1-Propene, 3-bromo-
Allyl bromide
3-Bromopropylene
N-Acetylthiourea
1-Acetyl-2-thiourea
Acetamide, N-(aminothioxomethyl)-
Acetothiourea
N-acetylthiourea
1-Acetylthiourea
Urea, 1-acetyl-2-thio-
N-Acetylthiocarbamide
Usaf ek-4890
N-Acetyl-2-thiourea
Acetylthiourea
Sodium bisulfite-acetone adduct
Acetone sodium bisulfite
Acetone-sodium bisulfite compound (1:1)
2-hydroxy-2-propanesulfonic acid
Sodium 2-hydroxy-2-propane-sulfonate
Acetone, compound with sodium bisulfite (1:1)
2-Propanone, compd. with monosodium sulfite (1:1)
Sodium 1-hydroxy-1-methylethanesulfonate
Sodium acetone bisulfite
Acetone-monosodium sulfite adduct
Acetone-sodium bisulfite adduct
allylarsonic acid
3-Aminopropylene
Monoallylamine
Allylamine
allylamine
2-Propen-1-amine
2-propen-1-amine
3-Aminopropene
3-Amino-1-propene
2-Propenamine
2-Propenylamine
2-propen-1-amine allylamine
2-Propanone, oxime
Acetoxime
acetone oxime
Acetone, oxime
.beta.-Isonitrosopropane
Acetonoxime
Acetone oxime
2-Propanone oxime
ALANINE, ALPHA
DL-Alanine
D, L-Alanine
Alanine, DL-
dl-.alpha.-Aminopropionic acid
DL-.alpha.-Alanine
dl-2-Aminopropanoic acid
alanine
(.+-.)-Alanine
3-Aminopropionic acid
beta-alanine
.beta.-Aminopropionic acid
Propanoic acid, 3-amino-
ALANINE, BETA
.beta.-Alanine
3-Aminopropanoic acid
Abufene
Triisopropoxyaluminum
Aliso
Aluminium isopropoxide
Triisopropyloxyaluminum
Aluminum isopropylate
Aluminum isopropoxide
Aluminum triisopropylate
Aluminum(II) isopropylate
2-propanol
Aluminum tris(sec-propoxide)
Aluminum triisopropoxide
Tris(isopropoxy)aluminum
Aluminum isopropanolate
Aluminum sec-propanolate
N-Acetylglycine
Glycine, N-acetyl-
Acetylaminoacetic acid
Acetamidoacetic acid
Ethanoylaminoethanoic acid
Acetylglycocoll
Aceturic acid
(acetylamino)acetic acid
5-Ureidohydantoin
AVC/Dienestrolcream
Hydantoin, 5-ureido-
ALLANTOIN
Urea, (2, 5-dioxo-4-imidazolidinyl)-
Alantan
Allantoin
Allantol
Cordianine
Glyoxyldiureide
Sebical
Glyoxyldiureid
N-(2,5-dioxo-4-imidazolidinyl)urea
Allylcarbamide
Monoallylurea
Allylurea
Urea, allyl-
N-allylurea
N-Allylurea
1-Allylurea
Urea, 2-propenyl-
N-2-Propenylurea
N'-Methyl-N-acetylurea
N-Methyl-N'-acetylurea
N-acetyl-N'-methylurea
1-Acetyl-3-methylurea
Urea, 1-acetyl-3-methyl-
Acetamide, {N-[(methylamino)carbonyl]-}
1-Hydroxyethyl methyl ketone
Acetoin
2-Hydroxy-3-butanone
2-Butanone, 3-hydroxy-
Methanol, acetylmethyl-
Dimethylketol
.gamma.-Hydroxy-.beta.-oxobutane
Acetyl methyl carbinol
3-Hydroxy-2-butanone
2,3-Butanolone
ACETOIN
3-hydroxy-2-butanone
3-Hydroxybutyraldehyde
3-Butanolal
Butanal, 3-hydroxy-
Aldol
Oxybutanal
Acetaldol
Butyraldehyde, 3-hydroxy-
3-hydroxybutanal
3-Hydroxybutanal
.beta.-Hydroxybutyraldehyde
Oxybutyric aldehyde
Phenylglycine nitrile
Anilinoacetonitrile
Acetonitrile, (phenylamino)-
N-(Cyanomethyl)aniline
Glycinonitrile, N-phenyl-
anilinoacetonitrile
2-Amino-2-phenylacetonitrile
Acetonitrile, anilino-
1-Anilinoacetonitrile
(Phenylamino)acetonitrile
3-Acetoxypropene
2-Propenyl ethanoate
1-Propen-2-ol acetate
Acetic acid, allyl ester
Allyl acetate
Acetic acid, 2-propenyl ester
2-Propenyl acetate
allyl acetate
2,3-pentanedione
Acetylpropionyl (VAN)
2,3-Pentadione
2,3-Pentanedione
N-(2-methoxyethyl)acetamide
Equisetic acid
Pyrocitric acid
Glutaconic acid, 3-carboxy-
Achilleaic acid
1-propene-1,2,3-tricarboxylic acid
1-Propene-1,2, 3-tricarboxylic acid
2-Pentenedioic acid, 3-carboxy-
Aconitic acid
Citridic acid
Achilleic acid
Citridinic acid
ACONITIC ACID
Adipic acid nitrile
1, 4-Dicyanobutane
Nitrile adipico(ITALIAN)
Adipyldinitrile
Adipic acid dinitrile
Adipodinitrile
hexanedinitrile
Tetramethylene dicyanide
Adiponitrile
Tetramethylene cyanide
Hexanedioic acid, dinitrile
Hexanedinitrile
Sulfonamide P
Sulphonamide
Benzenesulfonamide, p-amino-
Sulfana
Exoseptoplix
Strepamide
Sulfocidine
p-Anilinesulfonamide
p-Aminobenzenesulfonylamide
Streptrocide
Pronzin Album
Erysipan
Ambeside
PABS
Streptagol
Septanilam
Sulfanilamide
A-349
Infepan
Astrocid
p-Aminobenzenesulfonamide
4-Sulfamoylaniline
Albexan
White streptocide
Rubiazol A
Septamide Album
Streptozone
Prontosil White
Stramid
Streptamid
Lusil
4-aminobenzenesulfonamide
Sulfamine
Dipron
Proseptal
Gombardol
Bactesid
Copticide
Streptopan
Stopton Album
1162 F
Streptocom
Therapol
Prontalbin
Septolix
Streptocid album
Sulfonylamide
Proseptine
Antistrept
Astreptine
p-Sulfamidoaniline
Ergaseptine
Strepsan
Lysococcine
p-Aminobenzenesulfamide
Benzenesulfonamide, 4-amino-
F 1162
p-Sulfamoylaniline
Septoplex
Streptol
Streptosil
Sulfanalone
Sulphanilamide
p-Aminophenylsulfonamide
Sulfonamide
Septoplix
Deseptyl
Streptozol
Aniline-p-sulfonic amide
Albosal
Streptocide (VAN)
Streptocid
Gerison
Strepton
Orgaseptine
Streptocide White
Bacteramid
Sanamid
Streptasol
4-Aminophenylsulfonamide
Pysococcine
Collomide
Streptoclase
Sulfanidyl
Prontosil Album
Sulfanil
Sulfanilimidic acid
4-Aminobenzenesulfonamide
Prontylin
Estreptocida
Streptamin
Tolder
Fourneau 1162
Prontosil I
Septinal
Sulfamidyl
Neococcyl
Proseptol
Colsulanyde
Ether, diallyl
3,3'-Oxybis(1-propene)
Allyl ether
1-Propene, 3,3'-oxybis-
diallyl ether
3-(allyloxy)-1-propene
Diallyl ether
3-(allyloxy)-1-propene diallyl ether
Propionic acid, allyl ester
Propanoic acid, 2-propenyl ester
Allyl propionate
allyl propionate
2,5-Hexanedione
2,5-Hexadione
HDO
1,2-Diacetylethane
2, 5-Diketohexane
Acetonyl acetone
.alpha.,.beta.-Diacetylethane
2,5-hexanedione
Diacetonyl
Acetone, acetonyl-
hexanedioic acid
1,6-Hexanedioic acid
Molten adipic acid
Kyselina adipova(CZECH)
Adilactetten
1, 4-Butanedicarboxylic acid
Acifloctin
ADIPIC ACID
Adipinic acid
Acinetten
1, 4-Butanedicarboxamide
Adipamide
Hexanediamide
Adipic acid amide
NCI-C02095
Adipic acid diamide
Adipic diamide
hexanediamide
Acetal (VAN)
1,1-Dietossietano(ITALIAN)
Acetal diethylique(FRENCH)
Ethane, 1,1-diethoxy-
Ethylidene diethyl ether
Acetaldehyde diethyl acetal
1, 1-Diaethoxy-aethan(GERMAN)
Diaethylacetal(GERMAN)
1,1-Diethoxy-ethaan(DUTCH)
Acetaldehyde, diethyl acetal
1,1-diethoxyethane
Acetaal(DUTCH)
1, 1-Diethoxyethane
Diethyl acetal
p-Cyanoaniline
4-Aminobenzonitrile
Benzonitrile, 4-amino-
Benzonitrile, p-amino-
4-Cyanoaniline
1-Amino-4-cyanobenzene
4-aminobenzonitrile
Aniline, p-cyano-
p-Aminobenzonitrile
m-Cyanoaniline
Benzonitrile, 3-amino-
3-aminobenzonitrile
Benzonitrile, m-amino-
3-Aminobenzonitrile
3-Cyanoaniline
m-Aminobenzonitrile
Super Shade by Coppertone
Vitamin H'
PABA
PAB
Hachemina
1-Amino-4-carboxybenzene
p-Carboxyaniline
4-Aminobenzoic acid
Benzoic acid, p-amino-
Anticanitic vitamin
Pabafilm
Vitamin BX
Anti-Chromotrichia factor
Trichochromogenic factor
Benzoic acid, 4-amino-
4-aminobenzoic acid
Paranate
Bacterial vitamin h1
Aminobenzoic acid
Pabamine
Pabanol
Paraminol
Ophthalmic acid
Norophthalmic acid
Homoanserine
Kyotorphin
Balenine
Endomorphin-1
Endomorphin-2
Tetragastrin
Kentsin
Morphiceptin
PE(O-18:1(1Z)/20:4(5Z,8Z,11Z,14Z))
PE(O-16:1(1Z)/22:6(4Z,7Z,10Z,13Z,16Z,19Z))
Glycitein
Hesperetin
Gingerol
Hydroxytyrosol
Indole-3-carbinol
Theaflavin-3-gallate
Theaflavin
Tetrahydrocurcumin
Tannin
Sulforaphane
Quercetin
Punicalagin
Picrocrocin
Peonidin
Malabaricone C
Scolymoside
Luteolin
Kaempferol
Isoeugenol
Gamma-terpinene
Gallic acid
Formononetin
Eugenol
Eriodictyol
Eriocitrin
Geraniol
Testosterone enanthate
2-Acetamido-2-deoxy-6-O-a-L-fucopyranosyl-D-glucose
Beta-Cortol
Difucosyllacto-N-hexaose I
Galactinol
5a-Androstan-3b-ol
Sorbitol-6-phosphate
6b-Hydroxymethandienone
Ethyl isobutyl ketone
2-Oxohexane
8-isoprostaglandin E2
Ethyl isopropyl ketone
4-Androstenediol
2-Methylguanosine
Dimethyldisulfide
19-Noretiocholanolone
Dihydrogenistein
N4-Acetylcytidine
Androstenol
5b-Pregnanediol
Lithium
D-Pipecolic acid
Dehydroandrosterone
Diketogulonic acid
Tetrahydrodeoxycortisol
Dimethyltryptamine
20-Hydroxyeicosatetraenoic acid
3'-Hydroxystanozolol
Ethyltestosterone
Epimetendiol
7a,17-dimethyl-5b-Androstane-3a,17b-diol
3'-O-Methyladenosine
Mevalonolactone
Norbolethone
Oxymesterone
N-Acetylglutamine
11-Ketoetiocholanolone
Imidazoleacetic acid ribotide
4-Dihydroboldenone
Mesterolone
8-Hydroxy-7-methylguanine
3'-O-Methylguanosine
8-Chloroxanthine
N2-Methylguanine
Dityrosine
5a-Androst-3-en-17-one
Bolasterone
O-Phosphotyrosine
o-Tyrosine
1-docosanoyl-2-eicosanoyl-sn-glycero-3-phosphocholine
1-docosanoyl-2-(11Z-eicosenoyl)-sn-glycero-3-phosphocholine
1-docosanoyl-2-heneicosanoyl-sn-glycero-3-phosphocholine
1,2-didocosanoyl-sn-glycero-3-phosphocholine
2,3-didocosanoyl-sn-glycero-1-phosphocholine
1-docosanoyl-2-docosanoyl-sn-glycero-3-phosphocholine
1-docosanoyl-2-(13Z-docosenoyl)-sn-glycero-3-phosphocholine
1-docosanoyl-2-tricosanoyl-sn-glycero-3-phosphocholine
1-docosanoyl-2-tetracosanoyl-sn-glycero-3-phosphocholine
1-docosanoyl-2-(15Z-tetracosenoyl)-sn-glycero-3-phosphocholine
1-docosanoyl-2-pentacosanoyl-sn-glycero-3-phosphocholine
1-docosanoyl-2-hexacosanoyl-sn-glycero-3-phosphocholine
1,2-di-(13E-docosenoyl)-sn-glycero-3-phosphocholine
1-(13E-docosenoyl)-2-(13E-docosenoyl)-sn-glycero-3-phosphocholine
1,2-di-(13Z-docosenoyl)-sn-glycero-3-phosphocholine
1-(13Z-docosenoyl)-2-(13Z-docosenoyl)-sn-glycero-3-phosphocholine
1-(9Z-docosenoyl)-2-(9Z-docosenoyl)-sn-glycero-3-phosphocholine
1-(5Z,9Z-docosadienoyl)-2-(5Z,9Z-docosadienoyl)-sn-glycero-3-phosphocholine
1-(7Z,10Z,13Z,16Z-docosatetraenoyl)-2-octadecanoyl-sn-glycero-3-phosphocholine
1-(4E,7E,10E,13E,16E,19E-docosahexaenoyl)-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-(4Z,7Z,10Z,13Z,16Z,19Z-docosahexaenoyl)-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-(4Z,7Z,10Z,13Z,16Z,19Z-docosahexaenoyl)-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-(4Z,7Z,10Z,13Z,16Z,19Z-docosahexaenoyl)-2-(9Z,12Z-octadecadienoyl)-sn-glycero-3-phosphocholine
1,2-di-(4Z,7Z,10Z,13Z,16Z,19Z-docosahexaenoyl)-sn-glycero-3-phosphocholine
2,3-di-(4Z,7Z,10Z,13Z,16Z,19Z-docosahexaenoyl)-sn-glycero-1-phosphocholine
1-(4Z,7Z,10Z,13Z,16Z,19Z-docosahexaenoyl)-2-(4Z,7Z,10Z,13Z,16Z,19Z-docosahexaenoyl)-sn-glycero-3-phosphocholine
1-tricosanoyl-2-decanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-undecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-dodecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-tridecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-tetradecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-pentadecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-heptadecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-octadecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-nonadecanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-eicosanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-heneicosanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-docosanoyl-sn-glycero-3-phosphocholine
1,2-ditricosanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-tricosanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-tetracosanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-pentacosanoyl-sn-glycero-3-phosphocholine
1-tricosanoyl-2-hexacosanoyl-sn-glycero-3-phosphocholine
1-(14Z-tricosenoyl)-2-(14Z-tricosenoyl)-sn-glycero-3-phosphocholine
1,2-di-(9Z-tricosenoyl)-sn-glycero-3-phosphocholine
1-(9Z-tricosenoyl)-2-(9Z-tricosenoyl)-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-undecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-dodecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-tridecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-tetradecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-pentadecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-heptadecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-octadecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-(6Z-octadecenoyl)-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-(9Z-octadecenoyl)-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-nonadecanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-eicosanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-(11Z-eicosenoyl)-sn-glycero-3-phosphocholine
.gamma.-Aminobenzoic acid
4-Carboxyaniline
p-Carboxyphenylamine
Amben
Chromotrichia factor
AMINOBENZOIC ACID, PARA
Pabacyd
Vitamin Bx
2-Iminobenzimidazoline
2-Aminobenzimidazole
1,3-dihydro-2H-benzimidazol-2-imine
1H-Benzimidazol-2-amine
Usaf ek-4037
Benzimidazole, 2-amino-
Devol Red K
5-Chloro-2-aminotoluene hydrochloride
Fast Red Salt TR
Sanyo Fast Red Salt TR
Kromon Green B
Red TRS Salt
Brentamine Fast Red TR Salt
4-Chloro-2-methylbenzenamine hydrochloride
C.I. 37085
Chlorhydrate de 4-chloroorthotoluidine (FRENCH)
4-Chloro-o-toluidine (NH2=1) hydrochloride
p-Chloro-o-toluidine hydrochloride
Azoene Fast Red TR Salt
Natasol Fast Red TR Salt
o-Toluidine, 4-chloro-, hydrochloride
Fast Red TR Salt
Red Salt Irga IX
Fast Red Salt Tra
Red Salt Ciba IX
2-Methyl-4-chloroaniline hydrochloride
Azoic diazo component 11 base
Neutrosel Red TRVA
Hindasol Red TR Salt
Devol Red TA Salt
4-Chloro-2-methylaniline hydrochloride
4-Chloro-2-toluidine hydrochloride
Fast Red Salt TRN
4-Chloro-o-toluidine (NH2=1)
NCI-C02368
Daito Red Salt TR
Benzenamine, 4-chloro-2-methyl-, hydrochloride
Amarthol Fast Red TR Salt
2-Amino-5-chlorotoluene hydrochloride
Devol Red TR
C.I. Azoic Diazo Component 11
4-Chloro-6-methylaniline hydrochloride
Fast Red 5CT Salt
Diazo Fast Red TR
Azanil Red Salt TRD
4-Chloro-o-toluidine hydrochloride
2-Amino-p-cresol
6-Hydroxy-m-toluidine
2-Hydroxy-5-methylaniline
5-Methyl-2-hydroxyaniline
4-Methyl-2-aminophenol
Phenol, 2-amino-4-methyl-
p-Cresol, 2-amino-
3-Amino-4-hydroxytoluene
2-Amino-4-methylphenol
2-amino-4-methylphenol
3-Aminoanisole
m-Methoxyaniline
3-Methoxyaniline
1-Amino-3-methoxybenzene
m-Aminoanisole
3-methoxyaniline
3-methoxyphenylamine
Benzenamine, 3-methoxy-
m-Anisidine
m-Anisylamine
3-methoxyaniline 3-methoxyphenylamine
Shell 345
Allylidene acetate
Shell SD 345
2-Propene-1,1-diol, diacetate
1-(acetyloxy)-2-propenyl acetate
Diacetoxypropene
1, 1-Diacetoxypropene-2
Acrolein, diacetate
SD-345
Acrolein, monohydrate, diacetate
SD 345 (ester)
3, 3-Diacetoxypropene
Allylidene diacetate
N-Acetyl-DL-methionine
DL-Methionine, N-acetyl-
Acetyl-DL-methionine
dl-Acetylmethionine
N-acetyl-S-methylhomocysteine
Methionine, N-acetyl-, DL-
Alloxantin
ALLOXANTIN, DIHYDRATE
Uroxin
5,5'-Bibarbituric acid, 5,5'-dihydroxy-
Uroxine
{[5,5'-Bipyrimidine]-2,2',4,4',6,6'(1H,1'H,3H,3'H,5H,5'H)-hexone,} 5,5'-dihydroxy-
Ethanone, 1-phenyl-
Acetophenon
Usaf ek-496
1-Phenylethanone
Methyl phenyl ketone
1-Phenyl-1-ethanone
1-phenylethanone
Hypnon
Acetophenone
Ketone, methyl phenyl-
Hypnone
ACETOPHENONE
Benzoyl methide
Phenyl methyl ketone
Acetylaniline
Phenalgin
N-phenylacetamide
Acetanil
Benzenamine, N-acetyl-
Acetoanilide
Acetic acid anilide
N-Acetylaniline
Acetylaminobenzene
Antifebrin
Aniline, N-acetyl-
Acetamidobenzene
Acetamide, N-phenyl-
N-Phenylacetamide
Phenalgene
Acetanilid
Acetanilide
Usaf ek-3
Acetophenone, 3'-amino-
1-(3-aminophenyl)ethanone
3-Aminoacetophenone
m-Aminoacetophenone
m-Aminoacetylbenzene
Ethanone, 1-(3-aminophenyl)-
m-Acetylaniline
.beta.-Aminoacetophenone
3-Acetylaniline
1-ethyl-1H-pyrrole-2,5-dione
N-Ethylmaleimide
Ethylmaleimide
Usaf B-121
1H-Pyrrole-2,5-dione, 1-ethyl-
NEM
Maleic acid N-ethylimide
Maleimide, N-ethyl-
2,5-dimethylphenylamine
1-Amino-2,5-dimethylbenzene
p-Xylidine
Benzenamine, 2,5-dimethyl-
6-Methyl-m-toluidine
2, 5-Xylidine
Aniline, 2,5-dimethyl-
2,5-dimethylaniline
3-Amino-1,4-dimethylbenzene
2,5-Dimethylbenzenamine
2-Amino-1, 4-xylene
2,5-Dimethylaniline
5-Amino-1, 4-dimethylbenzene
5-Methyl-o-toluidine
2,5-dimethylaniline 2,5-dimethylphenylamine
1-Amino-2, 4-dimethylbenzene
Benzenamine, 2,4-dimethyl-
4-Amino-1,3-xylene
4-Methyl-o-toluidine
2, 4-Dimethylbenzenamine
m-Xylidine (VAN)
Aniline, 2,4-dimethyl-
2,4-dimethylaniline
2,4-Dimethylaniline
2-Methyl-p-toluidine
4-Amino-3-methyltoluene
2,4-dimethylphenylamine
2,4-Xylidine
4-Amino-1, 3-dimethylbenzene
m-4-Xylidine
2,4-dimethylaniline 2,4-dimethylphenylamine
2,3-dimethylphenylamine
2,3-dimethylaniline
2,3-dimethylaniline 2,3-dimethylphenylamine
2,3-octanedione
2,3-Octanedione
Methylphenylglyoxal
Benzoylacetyl
Benzoyl methyl ketone
3-Phenyl-2,3-propanedione
1-Phenyl-1,2-propanedione
Phenylmethyldiketone
1,2-Propanedione, 1-phenyl-
1-phenyl-1,2-propanedione
Pyruvophenone
Acetylbenzoyl
p-Acetotoluidide
4-Acetotoluide
N-(4-methylphenyl)acetamide
Acetyl-p-toluidine
4-Methylacetanilide
p-Acetamidotoluene
1-Acetamido-4-methylbenzene
N-Acetyl-p-toluidide
p-Methylacetanilide
4-(Acetylamino)toluene
N-(4-Methylphenyl)acetamide
p-Acetotoluide
N-Acetyl-p-toluidine
Acetamide, N-(4-methylphenyl)-
Diethyl acetamidomalonate
Propanedioic acid, (acetylamino)-, diethyl ester
Malonic acid, acetamido-, diethyl ester
Diethyl 2-acetamidomalonate
Diethylester kyseliny acetylaminomalonove (CZECH)
Ethyl acetamidomalonate
Diethyl acetaminomalonate
diethyl 2-(acetylamino)malonate
(Acetylamino)propanedioic acid diethyl ester
Acetamidomalonic acid diethyl ester
Diethyl acetylaminomalonate
2', 5'-Acetoxylidide
N-(2,5-dimethylphenyl)acetamide
2,5-Dimethylacetanilide
Acetanilide, 2', 5'-dimethyl-
Acetamide, N-(2,5-dimethylphenyl)-
p-Acetotoluidide, N-methyl-
Acetamide, N-methyl-N-(4-methylphenyl)-
N-Methyl-N-p-tolylacetamide
N-methyl-N-(4-methylphenyl)acetamide
o-Acetotoluidide, N-methyl-
N-methyl-N-(2-methylphenyl)acetamide
Acetamide, N-(2-ethoxyphenyl)-
2'-Ethoxyacetanilide
N-(2-ethoxyphenyl)acetamide
o-Acetophenetidide
2-Ethoxyacetanilide
Acetanilide, 2'-ethoxy-
N-(3-ethoxyphenyl)acetamide
3'-Ethoxyacetanilide
3-Ethoxyacetanilide
m-Acetophenetidide
Acetamide, N-(3-ethoxyphenyl)-
Acetanilide, 3'-ethoxy-
Phenedina
Phenacitin
Acetophenetidine
component of Percodan
Acet-p-phenalide
Kalmin
component of Phensal
p-Phenetidine, N-acetyl-
4'-Ethoxyacetanilide
p-Acetphenetidin
Acet-p-phenetidin
p-Acetophenetidide
Acetophenetidin
Phenacet
Acetophenetin
Phenacetin
component of Ansemco 2
Pertonal
N-(4-ethoxyphenyl)acetamide
Aceto-4-phenetidine
component of P-A-C compound
Pyraphen
component of Butigetic
Phenazetin
p-Acetophenetidine
Fenina
Phenin
Fenidina
Phenidin
component of A.S.A. compound
4-Ethoxyacetanilide
N-(4-Ethoxyphenyl)acetamide
p-Ethoxyacetanilide
N-Acetyl-p-phenetidine
Acetanilide, 4'-ethoxy-
Phenacetine
Acetamide, N-(4-ethoxyphenyl)-
p-Acetophenetide
component of A.S.A. and Codeine compound
ADENOSINE
6-Amino-9.beta.-D-ribofuranosyl-9H-purine
.beta.-Adenosine
.beta.-D-Adenosine
Usaf cb-10
9H-Purin-6-amine, 9.beta.-D-ribofuranosyl-
Boniton
Sandesin
Adenine nucleoside
Adenosin (GERMAN)
Adenosine
Myocol
.beta.-D-Ribofuranose, 1-(6-amino-9H-purin-9-yl)-1-deoxy-
Adenine riboside
Nucleocardyl
.beta.-D-Ribofuranoside, adenine-9
9.beta.-D-Ribofuranosyladenine
Siran N, N-diethyl-p-fenylendiaminu (CZECH)
sulfuric acid compound with N~1~,N~1~-diethyl-1,4-benzenediamine (1:1)
N, N-Diethyl-p-phenylenediamine sulfate (VAN)
Diethyl-p-phenylenediamine sulfate
p-Phenylenediamine, N,N-diethyl-, sulfate (1:1)
1, 4-Benzenediamine, N,N-diethyl-, sulfate (1:1)
N-(3-quinolinyl)acetamide
2-Acetoacetylaminotoluene
Acetoacet-o-toluidine
Acetoacetyl-2-methylanilide
Butanamide, N-(2-methylphenyl)-3-oxo-
o-Acetoacetotoluidide
2'-Methylacetoacetanilide
N-(2-methylphenyl)-3-oxobutanamide
Acetoacet-o-toluidide
Acenaphthenedione
1, 2-Acenaphthenedione
1, 2-Acenaphthylenedione
1,2-Acenaphthenequinone
Acenaphthoquinone
acenaphthoquinone
Acenaphthenequinone
peri-Ethylenenaphthalene
Acenaphthene
Naphthyleneethylene
Acenaphthylene
1, 2-Dihydroacenaphthylene
1,8-Ethylenenaphthalene
1,2-dihydroacenaphthylene
Acenaphthylene, 1,2-dihydro-
2-Acetonaphthone
.beta.-Acetylnaphthalene
Methyl 2-naphthyl ketone
.beta.-Naphthyl methyl ketone
2-Acetylnaphthalene
Acetonaphthone
.beta.-Acetonaphthone
1-(2-Naphthalenyl)ethanone
Ethanone, 1-(2-naphthalenyl)-
.beta.-Methyl naphthyl ketone
.beta.-Acetonaphthalene
1-(2-naphthyl)ethanone
2'-Acetonaphthone
Methyl .beta.-naphthyl ketone
Oranger cyrstals
Ketone, methyl 2-naphthyl
2-Naphthyl methyl ketone
Ethanone, 1-(1-naphthalenyl)-
1-Naphthyl methyl ketone
.alpha.-Acetylnaphthalene
1-Acetonaphthone
.alpha.-Acetonaphthone
1-Acetylnaphthalene
.alpha.-Naphthyl methyl ketone
Methyl 1-naphthyl ketone
1-(1-Naphthalenyl)ethanone
Methyl .alpha.-naphthyl ketone
1'-Acetonaphthone
1-Acetonaphthalene
1-(1-naphthyl)ethanone
p-Phenylaniline
Xenylamine
p-Aminobiphenyl
p-Xenylamine
Biphenylamine
4-Aminodiphenyl
[1,1'-biphenyl]-4-ylamine
4-Biphenylylamine
4-Aminobiphenyl
p-Aminodiphenyl
{[1,1'-Biphenyl]-4-amine}
4-Phenylaniline
4-Biphenylamine
p-Biphenylamine
Xenylamin (CZECH)
Aniline, p-phenyl-
4-Aminodifenil (spanish)
[1,1'-biphenyl]-4-amine
[1,1'-biphenyl]-4-amine [1,1'-biphenyl]-4-ylamine
o-Aminobiphenyl
2-Aminodiphenyl
{[1,1'-Biphenyl]-2-amine}
o-Aminodiphenyl
[1,1'-biphenyl]-2-amine
2-Aminobiphenyl
NCI-C50282
2-Phenylaniline
[1,1'-biphenyl]-2-ylamine
o-Biphenylamine
2-Biphenylamine
o-Phenylaniline
[1,1'-biphenyl]-2-amine [1,1'-biphenyl]-2-ylamine
Butylacetanilide
Acetamide, N-butyl-N-phenyl-
N-Butylacetanilide
BAA
N-butyl-N-phenylacetamide
Acetanilide, N-butyl-
1-(2-isopropyl-5-methylcyclohexyl)ethanone
9-Acridanone
9(10H)-Acridone
Acridanone
Acridone
9(10H)-Acridinone
9-Acridone
Acridine, 9,10-dihydro-9-oxo-
9(10H)-acridinone
(4-aminophenyl)(phenyl)methanone
Methanone, (4-aminophenyl)phenyl-
p-Benzoylaniline
Benzophenone, 4-amino-
4-Aminobenzophenone
Usaf a-233
p-Aminobenzophenone
N-(4-(diethylamino)-2-methylphenyl)acetamide
diallyl phthalate
Phthalic acid, diallyl ester
NCI-C50657
o-Phthalic acid, diallyl ester
1,2-Benzenedicarboxylic acid, di-2-propenyl ester
Diallyl phthalate
Allyl phthalate
Dapon 35
Dapon R
Aleuritic acid
9,10,16-trihydroxyhexadecanoic acid
ALEURITIC ACID
9,10,16-Trihydroxypalmitic acid
Hexadecanoic acid, 9,10,16-trihydroxy-, (R*,S*)-(.+-.)-
(.+-.)-Aleuritic acid
STROPHANTHIN-K, BETA
STROPHANTHIN
3-(dibutylamino)-2-hydroxypropanal
2-(1-pyrrolidinyl)ethyl 2-cyclopentylpentanoate
S-(1-(1-pyrrolidinyl)ethyl) 2-cyclopentylpentanethioate
1-methyl-2-(1-pyrrolidinyl)ethyl 2-cyclopentylpentanoate
3-(1-pyrrolidinyl)propyl 2-cyclopentylpentanoate
3-(1-pyrrolidinyl)propyl 2-(2-cyclopenten-1-yl)hexanoate 2-hydroxy-1,2,3-propanetricarboxylate
1-methyl-2-(1-pyrrolidinyl)propyl 2-cyclopentylpentanoate
1-methyl-3-(1-pyrrolidinyl)propyl 2-cyclopentylpentanoate
2-(3,4-dimethyl-1-pyrrolidinyl)ethyl 2-cyclopentylpentanoate
2-(3,3-dimethyl-1-pyrrolidinyl)ethyl 2-cyclopentylpentanoate
4-(1-pyrrolidinyl)butyl 2-cyclopentylpentanoate 2-hydroxy-1,2,3-propanetricarboxylate
2-(2,2,4-trimethyl-1-pyrrolidinyl)ethyl 2-cyclopentylpentanoate
2-(2,2,5-trimethyl-1-pyrrolidinyl)ethyl 2-cyclopentylpentanoate
2-(2,5-dimethyl-1-pyrrolidinyl)-1-methylethyl 2-cyclopentylpentanoate
2-(1-pyrrolidinyl)propyl 2-cyclohexen-1-yl(2-cyclopenten-1-yl)acetate 2-hydroxy-1,2,3-propanetricarboxylate
2-(1-pyrrolidinyl)propyl diphenylacetate
1-methyl-2-(1-pyrrolidinyl)ethyl diphenylacetate
2-(2-methyl-1-pyrrolidinyl)ethyl 2-cyclohexen-1-yl(phenyl)acetate
2-(2,4-dimethyl-1-pyrrolidinyl)ethyl 2-cyclopenten-1-yl(phenyl)acetate
2-(3,4-dimethyl-1-pyrrolidinyl)ethyl 2-cyclopenten-1-yl(phenyl)acetate
2-(2,2-dimethyl-1-pyrrolidinyl)ethyl cyclopentyl(phenyl)acetate
4-(1-pyrrolidinyl)butyl 2-cyclohexen-1-yl(phenyl)acetate
2-(3,4-dimethyl-1-pyrrolidinyl)ethyl 2-cyclohexen-1-yl(phenyl)acetate
2-(2,2,4-trimethyl-1-pyrrolidinyl)ethyl 2-cyclopenten-1-yl(phenyl)acetate
2-(2,5-dimethyl-1-pyrrolidinyl)-1-methylethyl 2-cyclopenten-1-yl(phenyl)acetate
2-(2,5-dimethyl-1-pyrrolidinyl)-1-methylethyl 2-cyclohexen-1-yl(phenyl)acetate
2,2-dimethyl-3-(1-pyrrolidinyl)propyl 2-cyclohexen-1-yl(phenyl)acetate
2-(2,2,4-trimethyl-1-pyrrolidinyl)ethyl 2-cyclohexen-1-yl(phenyl)acetate
succinic acid
butyric acid
Copper, bis(N-hydroxy-N-nitrosobenzenaminato-O,O')-
Bis(cupferronato)copper
Bis(N-nitroso-N-phenylhydroxylaminato)copper(II)
Copper, bis(N-nitroso-N-phenylhydroxylaminato)-
Nickel citrate
2-hydroxy-1,2,3-propanetricarboxylic acid
2(3H)-Benzothiazolone
2-Hydroxybenzothiazole
2-Benzothiazolinone
2-Benzothiazolol
Carbamothioic acid, (2-mercatophenyl)-, .gamma.-lactone
Benzothiazolone
1,3-benzothiazol-2(3H)-one
2-Benzothiazolone
Nitrodracylic acid
Kyselina p-nitrobenzoova (CZECH)
4-Nitrodracylic acid
4-(hydroxy(oxido)amino)benzoic acid
Benzoic acid, 4-nitro-
4-Nitrobenzoic acid
Benzoic acid, p-nitro-
1-tetracosanoyl-2-heneicosanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-docosanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-tricosanoyl-sn-glycero-3-phosphocholine
1,2-ditetracosanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-tetracosanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-pentacosanoyl-sn-glycero-3-phosphocholine
1-tetracosanoyl-2-hexacosanoyl-sn-glycero-3-phosphocholine
1,2-di-(15Z-tetracosenoyl)-sn-glycero-3-phosphocholine
1-(15Z-tetracosenoyl)-2-(15Z-tetracosenoyl)-sn-glycero-3-phosphocholine
1-(5Z-tetracosenoyl)-2-(5Z-tetracosenoyl)-sn-glycero-3-phosphocholine
1-(9Z-tetracosenoyl)-2-(9Z-tetracosenoyl)-sn-glycero-3-phosphocholine
1-(5Z,9Z-tetracosadienoyl)-2-(5Z,9Z-tetracosadienoyl)-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-undecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-dodecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-tridecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-tetradecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-pentadecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-heptadecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-octadecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-nonadecanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-eicosanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-heneicosanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-docosanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-tricosanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-tetracosanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-pentacosanoyl-sn-glycero-3-phosphocholine
1-pentacosanoyl-2-hexacosanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-undecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-dodecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-tridecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-tetradecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-pentadecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-heptadecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-octadecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-nonadecanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-eicosanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-heneicosanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-docosanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-tricosanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-tetracosanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-pentacosanoyl-sn-glycero-3-phosphocholine
1-hexacosanoyl-2-hexacosanoyl-sn-glycero-3-phosphocholine
1-(5E,9E-hexacosadienoyl)-sn-glycero-3-phosphocholine
1,2-di-(5E,9Z-hexacosadienoyl)-sn-glycero-3-phosphocholine
1,2-di-(5Z,9E-hexacosadienoyl)-sn-glycero-3-phosphocholine
1-(5Z,9Z-hexacosadienoyl)-2-hexadecanoyl-sn-glycero-3-phosphocholine
1,2-di-(5Z,9Z-hexacosadienoyl)-sn-glycero-3-phosphocholine
1-(5Z,9Z-hexacosadienoyl)-2-(5Z,9Z-hexacosadienoyl)-sn-glycero-3-phosphocholine
1,2-di-(6Z,9Z-hexacosadienoyl)-sn-glycero-3-phosphocholine
1-propionyl-2-acetyl-sn-glycero-3-phosphocholine
1,2-dipropionyl-sn-glycero-3-phosphocholine
1-propionyl-2-propionyl-sn-glycero-3-phosphocholine
1-dotriacontanoyl-2-(5Z,8Z,11Z,14Z-eicosatetraenoyl)-sn-glycero-3-phosphocholine
1-tetratriacontanoyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-hexatriacontanoyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-butyryl-2-octadecanoyl-sn-glycero-3-phosphocholine
1-butyryl-2-(9Z-octadecenoyl)-sn-glycero-3-phosphocholine
1,2-dibutyryl-sn-glycero-3-phosphocholine
1-butyryl-2-butyryl-sn-glycero-3-phosphocholine
1-Carboxy-4-nitrobenzene
p-Nitrobenzoic acid
dimethyl 2-cyanosuccinate
3-(5-oxotetrahydro-2-furanyl)propanoic acid
bis(1,1,2,2,3,3,4,4,4-nonafluorobutyl) ether
1,1,1,2,2,3,3,4,4-nonafluoro-4-(1,1,2,2,3,3,4,4,4-nonafluorobutoxy)butane
Ether, bis(nonafluorobutyl)
Diperfluorobutyl ether
Dinonafluorobutyl ether
Butane, {1,1'-oxybis[1,1,2,2,3,3,4,4,4-nonafluoro-}
Perfluoro-n-dibutyl ether
Perfluorodibutyl ether
17-beta-estradiol-3-glucuronide
bis(1,1,2,2,3,3,4,4,4-nonafluorobutyl) ether 1,1,1,2,2,3,3,4,4-nonafluoro-4-(1,1,2,2,3,3,4,4,4-nonafluorobutoxy)butane
2,5-Furandione, 3, 4-bis(acetyloxy)dihydro-, (3R-trans)-
4-(acetyloxy)-2,5-dioxotetrahydro-3-furanyl acetate
Tartaric anhydride, diacetate
Cyclohexanone, 2-acetyl-
.alpha.-Acetylcyclohexanone
2-acetylcyclohexanone
2-Acetylcyclohexanone
2-Acetyl-1-cyclohexanone
ethyl 2-(acetyloxy)-2-methylpropanoate
isopropyl 2-(acetyloxy)propanoate
1-(acetyloxy)-2-methylpropyl acetate
methyl 2-((propoxycarbonyl)oxy)propanoate
Nickel octanoate (VAN)
Nickel octoate (VAN)
Nickel dioctanoate
Nickel(2+) caprylate
Nickel(II) octanoate
methyl 3-(2-ethoxyethoxy)propanoate
Timtex AQS
Bis(8-quinolinato)copper
Quinolate
8-Hydroxyquinoline copper complex
Bis(8-quinolinolato)copper
Copper, bis(8-quinolinolato)- (VAN)
Quinolate 15
Quinondo
Copper 8-quinolinolate
Copper 8-hydroxyquinolinate
Cuproquin
Milmer 1
Copper oxyquinolate
Copper oxyquinoline
Copper 8-quinolinol
Fennotox 45
Copper dioxinate
Copper 8-hydroxyquinoline
Copper 8-hydroxyquinolate
Oxime copper
Copper-oxine
Cupric 8-hydroxyquinolate
Copper, bis(8-quinolinolato-N(sup 1),O(sup 8))-
Copper hydroxyquinolinate
Copper 8-quinolinolate(1:2)
Cellu-Quin
Cupric 8-quinolinolate
Copper, bis(8-quinolinolato-n(sup1),O(sup8))-
Quinolate 20 (VAN)
Bioquin
Oxine-copper
Bis(8-oxyquinoline)copper
Cunilate
8-Quinolinol, copper (II) chelate
Copper(2+) oxinate
Bioquin 1
Copper 8-quinolate
Copper-8
Copper oxine
Copper oxinate
Cunilate 2174
Cunilate 2419
Cuprinol Dispersion AQ
Copper quinolinolate
Copper hydroxyquinolate
Copper, bis(8-quinolinolato-N(1),O(8))-
Copper bis(8-hydroxyquinolinate)
4-Hydroxy-L-proline
20-Carboxyleukotriene B4
4-Hydroxyphenylacetylglutamine
Troxerutin
Scyllitol
3-Methylsulfinylpropyl isothiocyanate
Enterolactone
12-HETE
Malondialdehyde
Benzaldehyde
3-Hydroxyhippuric acid
APGPR Enterostatin
7b-Hydroxycholesterol
2,4-Dihydroxy-nitrophenol
4,8 dimethylnonanoyl carnitine
5-alpha-Dihydrotestosterone glucuronide
Heptadecanoyl carnitine
2-(Formamido)-N1-(5-phospho-D-ribosyl)acetamidine
(S)-Propane-1,2-diol
Naphthalene epoxide
11-cis-Retinol
9-cis-Retinol
9-cis-Retinal
13-cis-Retinoic acid
13-cis-Retinal
13-cis Retinol
Ercalcitriol
24R,25-Dihydroxyvitamin D3
1-a,24R,25-Trihydroxyvitamin D2
24-Hydroxycalcitriol
5-Diphosphoinositol pentakisphosphate
Bisdiphosphoinositol tetrakisphosphate
1D-Myo-inositol 1,3-bisphosphate
1D-Myo-inositol 3,4-bisphosphate
Phenylacetaldehyde
S-aminomethyldihydrolipoamide
Demethylated antipyrine
3,4-Dihydroxymandelaldehyde
Tetracosahexaenoyl CoA
18-Hydroxyarachidonic acid
Tetracosatetraenoic acid n-6
25-Hydroxycholesterol
5-L-Glutamyl-L-alanine
3-Decaprenyl-4-hydroxybenzoic acid
4-Hydroxyretinoic acid
6beta-Hydroxytestosterone
N-Acetylneuraminate 9-phosphate
Linoelaidic acid
Zymosterol intermediate 2
5-Amino-2-oxopentanoic acid
5-amino-1-(5-phospho-D-ribosyl)imidazole-4-carboxylate
Dopamine 3-O-sulfate
5a-Dihydrotestosterone sulfate
7-a,25-Dihydroxycholesterol
7-a,27-dihydroxycholesterol
20alpha-Hydroxycholesterol
L-2,4-diaminobutyric acid
4-oxo-Retinoic acid
All trans decaprenyl diphosphate
Alpha-Linolenoyl-CoA
Chenodeoxycholoyl-CoA
L-Erythrulose
16-hydroxy hexadecanoic acid
Cob(II)alamin
trans-Hexadec-2-enoyl carnitine
Gamma-linolenyl carnitine
Alpha-linolenyl carnitine
2,6 dimethylheptanoyl carnitine
Docosa-4,7,10,13,16-pentaenoyl carnitine
Alpha-Tocotrienol
D-Tagatose 1-phosphate
D-Mannose 1-phosphate
cis,cis-Muconic acid
3-Dehydro-L-gulonate
Beta-tocopherol
ADP-ribose 2'-phosphate
Selenocystathionine
Alpha-N-Phenylacetyl-L-glutamine
L-Citramalyl-CoA
Hexacosanoyl carnitine
Pseudoecgonine
Milmer
Timtex 17
Bis(8-hydroxyquinolinato)copper
Copper, bis(8-quinolinolato-N1, O8)-
Copper quinolate
VERATRIC ACID
Veratric acid
Dimethylprotocatechuic acid
Benzoic acid, 3,4-dimethoxy-
3,4-dimethoxybenzoic acid
3,4-Dimethoxybenzoic acid
2-(allyloxy)-1-methyl-2-oxoethyl 2-hydroxypropanoate
ethyl 3-oxo-3-(3-pyridinyl)propanoate
diethyl 1,2-dibromo-1,2-cyclobutanedicarboxylate
Pinene
2,6, {6-Trimethylbicyclo[3.1.1]hept-2-ene}
.alpha.-Pinene
2-Pinene
PINENE, ALPHA
Pinene isomer
2,6,6-trimethylbicyclo[3.1.1]hept-2-ene
2,6, {6-Trimethylbicyclo[3.1.1]-2-hept-2-ene}
{Bicyclo[3.1.1]hept-2-ene,} 2,6,6-trimethyl-
2,6, {6-Trimethylbicyclo[3.1.1]-2-heptene}
1,6,9,13-tetraoxadispiro[4.2.4.2]tetradecane
Nickel(II) 4-cyclohexylbutyrate
Nickel cyclohexylbutyrate
Nickel cyclohexanebutyrate
Nickel 4-cyclohexylbutyrate
ethyl 2,2,4-trimethyl-3-oxopentanoate
4-Methyl-3-nitroaniline
3-Nitro-p-toluidine
3-Nitro-4-toluidin(CZECH)
4-Amino-2-nitrotoluene
p-Toluidine, 3-nitro-
m-Nitro-p-toluidine
3-Nitro-4-methylaniline
(5-amino-2-methylphenyl)(hydroxy)azane oxide
2-Nitro-4-aminotoluene
5-Nitro-4-toluidine
Benzenamine, 4-methyl-3-nitro-
(acetyloxy)(phenyl)methyl acetate
2-phenoxyethyl 2-hydroxypropanoate
{Bicyclo[2.2.1]heptane-1-acetic} acid, 2-chloro-7,7-dimethyl-
1-Apobornaneacetic acid, 2-chloro-
(2-chloro-7,7-dimethylbicyclo[2.2.1]hept-1-yl)acetic acid
diethyl 2,6-dibromoheptanedioate
ethyl 3-methyl-2-octenoate
2-ethylhexyl 2-hydroxypropanoate
2-(hexyloxy)ethyl 2-hydroxypropanoate
2-Cyclohexyl-4,6-dinitrophenol
6-Cicloesil-2,4-dinitr-fenolo(ITALIAN)
Dowspray 17
DN 1
ENT 157
SN 46
Dn dust no. 12
2-Cyclohexyl-4, 6-dinitrofenol(DUTCH)
Dry Mix No. 1
4,6-Dinitro-o-cyclohexylphenol
2-cyclohexyl-4,6-bis(hydroxy(oxido)amino)phenol
6-Cyclohexyl-2, 4-dinitrophenol
Dinex
Phenol, 2-cyclohexyl-4,6-dinitro-
Phenol, 6-cyclohexyl-2, 4-dinitro-
Dinitro-o-cyclohexylphenol
DNOCHP
Dn dry mix no. 1
Pedinex(FRENCH)
DN Dry Mix No. 1
2, 4-Dinitro-6-cyclohexylphenol
Dinitrocyclohexylphenol
Dnochp
Glutamic acid, N-(m-nitrobenzoyl)-, L-
L-Glutamic acid, N-(m-nitrobenzoyl)-
N-(3-(hydroxy(oxido)amino)benzoyl)glutamic acid
phenyl 2-(acetyloxy)-2-methylpropanoate
2-(benzyloxy)ethyl 2-hydroxypropanoate
2-phenoxyethyl 2-(acetyloxy)propanoate
diethyl 2,6-dimethyl-4-oxo-4H-pyran-3,5-dicarboxylate
1-methyl-2-((2-methyl-2-propenyl)oxy)-2-oxoethyl 2-(acetyloxy)-2-methylpropanoate
diethyl 2,8-dibromononanedioate
octyl 2-(acetyloxy)propanoate
methyl 2-(((octyloxy)carbonyl)oxy)propanoate
pentyl 2-(((pentyloxy)carbonyl)oxy)propanoate
2-butoxyethyl 2-(((pentyloxy)carbonyl)oxy)propanoate
Lactic acid, dodecyl ester
Ceraphyl 31
Eutanol G
Lauryl lactate
Propanoic acid, 2-hydroxy-, dodecyl ester
dodecyl 2-hydroxypropanoate
Cyclochem LVL
butyl 2-(((octyloxy)carbonyl)oxy)propanoate
dodecyl 2-(acetyloxy)propanoate
2-(hexyloxy)ethyl 2-(((hexyloxy)carbonyl)oxy)propanoate
Cinnamic acid, 2-bornyl ester, endo-
1,7,7-trimethylbicyclo[2.2.1]hept-2-yl 3-phenylacrylate
2-Propenoic acid, 3-phenyl-, 1,7, {7-trimethylbicyclo[2.2.1]hept-2-yl} ester, endo-
Bornyl cinnamate
1-(2-butoxy-1-methyl-2-oxoethyl) 2-butyl phthalate
1,4-Butanedione, 1,2,4-triphenyl-
1,2,4-triphenyl-1,4-butanedione
2-Pyridinealdoxime methiodide
Pralidoxime methiodide
Pam (CZECH)
2-Formyl-N-methylpyridinium oxime iodide
1-methyl-1lambda~5~-pyridine-2-carbaldehyde oxime
2-Hydroxyiminomethyl-1-methylpyridinium iodide
Pyridinium, 2-formyl-1-methyl-, iodide, oxime
Protopam iodide
2-Formyl-1-methylpyridinium iodide oxime
1-Methyl-2-hydroxyiminomethylpyridinium iodide
2-Pyridinaldoxim methojodid (GERMAN)
Pyridinium-2-aldoxime N-methyliodide
Pyridinium, 2-formyl-1-methyl--iodide, oxime
2-Pyridinaldoxime methiodide
Pyridinium, {2-[(hydroxyimino)methyl]-1-methyl-,} iodide
2-PAM
PAM (pharmaceutical)
Pralidoxime Iodide (USAN)
Pyridine aldoxime methiodide
1-Methyl-2-aldoximinopyridinium iodide
PAM (VAN)
2-Pam
2-Pyridine aldoxymethiodide
2-Pam iodide
Pralidoxime iodide(USAN)
2-(Hydroximinomethyl)-1-methylpyridinium iodide
2-Pyridine aldoxime methyl iodide
2-Pyridinecarboxaldehyde aldoxime methiodide
2-PAM iodide
Pyridine-2-aldoxime methiodide
Pyridine-2-aldoxime methyl iodide
N-Methylpyridinium-2-aldoxime iodide
2-Pyridine aldoxime iodomethylate
Pyridin-2-aldoxin (CZECH)
GS 1043
2-Pyridylaldoxime methiodide
N-Methylpyridine-2-aldoxime iodide
p-2-Am
Dithiobiuret
Imidodicarbonodithioic diamide
Biuret, dithio-
Imidodicarbonimidothioic diamide
2,4-Dithiobiuret
Urea, 2-thio-1-(thiocarbamoyl)-
DTB
dicarbonodithioimidic diamide
Thioimidodicarbonic diamide {([(H2N)C(S)]2NH)}
Biuret, 2,4-dithio-
phosphoric acid compound with amino((amino(imino)methyl)amino)oxomethane (1:1)
PAN (VAN)
Barex 210 Resin
PAN (polymer)
Acrylonitrile, polymers
Dralon T
Acrylonitrile homopolymer
Acrylonitrile polymer
2-Propenenitrile, homopolymer
Bulana
Polyacrylonitrile
Acrylonitrile polymers
Poly(acrylonitrile)
Poly(acrylonitrile), fibers
2-Hydroxypropanenitrile
Acetocyanohydrin
Acetaldehyde, cyanohydrin
Propanenitrile, 2-hydroxy-
Propionitrile, 2-hydroxy-
Lactonitrile
.alpha.-Hydroxypropionitrile
2-hydroxypropanenitrile
2-Hydroxypropionitrile
N-Cyano-N-methylmethanamine
Cyanamide, dimethyl-
dimethylcyanamide
Dimethylcyanamide
3-amino-1-propanol
3-Aminopropyl alcohol
Propanolamine
1-Amino-3-hydroxypropane
3-Hydroxypropylamine
.gamma.-Aminopropanol
.beta.-Alaninol
3-Propanolamine
1-Propanol, 3-amino-
3-Amino-1-propanol
3-Aminopropanol
1-Amino-3-propanol
Propanenitrile, 3-ethoxy-
.beta.-Ethoxypropionitrile
Propionitrile, 3-ethoxy-
3-Ethoxypropionitrile
3-ethoxypropanenitrile
diethylcyanamide
Diethylcyanamide
N, N-Diethylcyanamide
Usaf ek-6326
Cyanamide, diethyl-
.beta.,.beta.'-Bis(cyanoethyl) ether
Bis(2-cyanoethyl) ether
Propanenitrile, 3,3'-oxybis-
.beta., .beta.'-Dicyanodiethyl ether
Propionitrile, 3,3'-oxydi-
3,3'-Oxydipropionitrile
Ether, bis(2-cyanoethyl)
.beta.,.beta.'-Oxydipropionitrile
BBCE
Propanenitrile, 3,3'-iminobis-
N,N-Bis(2-cyanoethyl)amine
Bis(cyanoethyl)amine
Di(2-cyanoethyl)amine
Imino-.beta.,.beta.'-dipropionitrile
{3,3'-Iminobis[propanenitrile]}
Bis(2-cyanoethyl)amine
Usaf a-8564
Propionitrile, 3, 3'-iminodi-
3, 3'-Iminodipropanenitrile
Ethanamine, 2-cyano-N-(2-cyanoethyl)-
IDPN
.beta.,.beta.'-Iminodipropionitrile
Iminodipropanenitrile
Diethylamine, 2,2'-dicyano-
3, 3'-Iminobispropionitrile
3,3'-Iminodipropionitrile
Bis(.beta.-cyanoethyl)amine
Propanenitrile, {3-[(1-methylethyl)amino]-}
Propionitrile, 3-(isopropylamino)-
3-(Isopropylamino)propionitrile
3-(isopropylamino)propanenitrile
O,O-diisopropyl hydrogen dithiophosphate
3, {3'-Iminobis[propylamine]}
Dipropylenetriamine (VAN)
Initiating explosive iminobispropylamine (DOT)
1-valeryl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1,2-divaleryl-sn-glycero-3-phosphocholine
1-valeryl-2-valeryl-sn-glycero-3-phosphocholine
1-hexanoyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-hexanoyl-2-octadecanoyl-sn-glycero-3-phosphocholine
1,2-dihexanoyl-sn-glycero-3-phosphocholine
2,3-dihexanoyl-sn-glycero-1-phosphocholine
1-hexanoyl-2-hexanoyl-sn-glycero-3-phosphocholine
1-hexanoyl-2-heptanoyl-sn-glycero-3-phosphocholine
1-hexanoyl-2-octanoyl-sn-glycero-3-phosphocholine
1-hexanoyl-2-octanoyl-sn-glycero-3-phosphocholine
1,2-di-(2E,4E-hexadienoyl)-sn-glycero-3-phosphocholine
1-(3E,5E-hexadienoyl)-2-(11E,13E-tetradecadienoyl)-sn-glycero-3-phosphocholine
1-heptanoyl-2-heneicosanoyl-sn-glycero-3-phosphocholine
1,2-diheptanoyl-sn-glycero-3-phosphocholine
2,3-diheptanoyl-sn-glycero-1-phosphocholine
1-heptanoyl-2-heptanoyl-sn-glycero-3-phosphocholine
1-heptanoyl-2-octanoyl-sn-glycero-3-phosphocholine
1-octanoyl-2-heptadecanoyl-sn-glycero-3-phosphocholine
1-octanoyl-2-octadecanoyl-sn-glycero-3-phosphocholine
1-octanoyl-2-nonadecanoyl-sn-glycero-3-phosphocholine
1-octanoyl-2-eicosanoyl-sn-glycero-3-phosphocholine
1-octanoyl-2-heneicosanoyl-sn-glycero-3-phosphocholine
1-octanoyl-2-valeryl-sn-glycero-3-phosphocholine
1-octanoyl-2-hexanoyl-sn-glycero-3-phosphocholine
1-octanoyl-2-hexanoyl-sn-glycero-3-phosphocholine
1-octanoyl-2-heptanoyl-sn-glycero-3-phosphocholine
1,2-dioctanoyl-sn-glycero-3-phosphocholine
2,3-dioctanoyl-sn-glycero-1-phosphocholine
1-octanoyl-2-octanoyl-sn-glycero-3-phosphocholine
1,2-di-(2E,4E-octadienoyl)-sn-glycero-3-phosphocholine
1-nonanoyl-2-decanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-undecanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-dodecanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-tridecanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-tetradecanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-pentadecanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-heptadecanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-octadecanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-nonadecanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-eicosanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-heneicosanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-docosanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-tricosanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-tetracosanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-valeryl-sn-glycero-3-phosphocholine
1,2-dinonanoyl-sn-glycero-3-phosphocholine
1-nonanoyl-2-nonanoyl-sn-glycero-3-phosphocholine
1-tetradecanoyl-2-eicosanoyl-sn-glycero-3-phosphocholine
1-eicosanoyl-2-(9Z,12Z-octadecadienoyl)-sn-glycero-3-phosphocholine
1-(7Z-hexadecenoyl)-2-(5Z,8Z,11Z,14Z-eicosatetraenoyl)-sn-glycero-3-phosphocholine
1-(7Z-hexadecenoyl)-2-(4Z,7Z,10Z,13Z,16Z,19Z-docosahexaenoyl)-sn-glycero-3-phosphocholine
1-hexadecyl-2-(9Z-octadecenoyl)-sn-glycero-3-phosphocholine
1-methyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-methyl-2-acetyl-sn-glycero-3-phosphocholine
1-methyl-2-acetyl-sn-glycero-3-phosphocholine
1-decyl-2-acetyl-sn-glycero-3-phosphocholine
1-dodecyl-2-decanoyl-sn-glycero-3-phosphocholine
1-dodecyl-2-acetyl-sn-glycero-3-phosphocholine
1-dodecyl-2-acetyl-sn-glycero-3-phosphocholine
1-tetradecyl-2-pentadecanoyl-sn-glycero-3-phosphocholine
1-tetradecyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-tetradecyl-2-hexadecanoyl-sn-glycero-3-phosphocholine
1-tetradecyl-2-(9Z-hexadecenoyl)-sn-glycero-3-phosphocholine
Bis(3-aminopropyl)amine
1,7-Diamino-4-azaheptane
P 2 (hardener)
Iminobis(propylamine)
4-Azaheptane-1,7-diamine
N-(3-Aminopropyl)-1, 3-propanediamine
N-3-Aminopropyl-1,3-diaminopropane
1-Propanamine, 3,3'-iminobis-
Aminobis(propylamine)
Propylamine, 3,3'-iminobis-
Dipropylamine, 3,3'-diamino-
3, 3'-Diaminodipropylamine
N~1~-(3-aminopropyl)-1,3-propanediamine
3,3'-Iminobis(propylamine)
Imino-bis(3-propylamine)
1,3-Propanediamine, N-(3-aminopropyl)-
Caldine
N-((acryloylamino)methyl)acrylamide
Methylenebisacrylamide
N,N'-Methylenebis(acrylamide)
Methylenediacrylamide
N, N'-Methylenebisacrylamide
N, N'-Methylidenebisacrylamide
N,N'-Methylenediacrylamide
Acrylamide, N,N'-methylenebis-
2-Propenamide, N,N'-methylenebis-
Cyanamide, bis(1-methylethyl)-
Diisopropylcyanimide
Diisopropylcyanamide
diisopropylcyanamide
Cyanamide, diisopropyl-
3-(N, N-Diethylamino)-1-propylamine
N, N-Diethyl-1,3-propanediamine
N,N-Diethyl-1,3-diaminopropane
1-Amino-3-(diethylamino)propane
N,N-Diethylpropylenediamine
N-Diethyltrimethylenediamine
1, 3-Propanediamine, N,N-diethyl-
3-(Diethylamino)propylamine
N~1~,N~1~-diethyl-1,3-propanediamine
.gamma.-(Diethylamino)propylamine
Diethylaminotrimethylenamine
N-(3-Diethylaminopropyl)amine
N, N-Diethyltrimethylenediamine
1-(Diethylamino)propylamine-3
N, N-(Diethylamino)propylamine
3-(Diethylamino)-1-propylamine
UF 3
Chimassorb 90
Methanone, (2-hydroxy-4-methoxyphenyl)phenyl-
MOD
Oxybenzone(USAN)
2-Hydroxy-4-methoxybenzophenone
4-Methoxy-2-hydroxybenzophenone
Uvistat 24
Usaf cy-9
Uvinul 9
Uvinul M 40
Syntase 62
Cyasorb UV 9 Light Absorber
(2-hydroxy-4-methoxyphenyl)(phenyl)methanone
Benzophenone-3
UV 9
Benzophenone, 2-hydroxy-4-methoxy-
Advastab 45
Cyasorb UV 9
Sunscreen UV-15
MOB
Anuvex
Uvinul M-40
Spectra-Sorb UV 9
Oxybenzon
Solbaleite
Diotilan
Monawet MO-70 RP
Pelex OT
Dioctyl sodium sulphosuccinate
Monawet MO-70
Dioctyl sodiosulfosuccinate
Elfanol 883
Dioctyl sulfosuccinate sodium
Sodium bis(octyl)sulfosuccinate
Butyl-cerumen
Monawet MO-84 R2W
Dioctyl-Medo forte
Manoxol
Monoxol OT
Bu-cerumen
1,4-bis(octyloxy)-1,4-dioxo-2-butanesulfonic acid
Vatsol OT
Sodium O,O-dioctylsulfosuccinic acid
Sodium di-n-octyl sulfosuccinate
Triton GR-7
Dioctyl sodium sulfosuccinate
Sodium sulfosuccinic acid dioctyl ester
Rapisol
Aerosol OT-B
Sodium dioctyl sulfosuccinate
6-heptadecyl-1,3,5-triazine-2,4-diamine
Antioxidant 1
Nocrack NS 6
Antioxidant 2246
AO 1 (VAN)
Di(2-hydroxy-5-methyl-3-tert-butylphenyl)methane
2, {2'-Methylenebis[6-tert-butyl-p-cresol]}
NG 2246
2246
Plastanox 2246
AO 2246
Antioxidant NG-2246
Alterungsschutzmittel BKF
Plastanox 2246 Antioxidant
Advastab 405
Anti Ox
Antioxidant BKF
2, {2'-Methylenebis[6-(1,1-dimethylethyl)-p-cresol]}
Bisalkofen BP
AO 1 (antioxidant)
Catolin 14
A-22-46
Chemanox 21
Methane, 2,2'-bis(6-tert-butyl-p-cresyl)-
CAO 14
Calco 2246
p-Cresol, {2,2'-methylenebis[6-tert-butyl-}
Lederle 2246
2, 2'-Bis-6-terc.butyl-p-kresylmethan(CZECH)
CAO-14
CAO 5
S 67
2, {2'-Methylenebis[4-methyl-6-tert-butylphenol]}
Phenol, {2,2'-methylenebis[6-(1,} 1-dimethylethyl)-4-methyl-
BKF
Nocrac NS 6
Phenol, {2,2'-methylenebis[6-tert-butyl-4-ethyl-}
2,2'-Methylenebis(4-ethyl-6-tert-butylphenol)
AO 425
Bis(2-hydroxy-3-tert-butyl-5-ethylphenyl)methane
2, 2'-Methylenebis(6-tert-butyl-4-ethylphenol)
Plastanox 425 Antioxidant
Usaf cy-6
Phenol, {2,2'-methylenebis[6-(1,1-dimethylethyl)-4-ethyl-}
Plastanox 425
Chemanox 22
Antioxidant 425
1,4-dioxo-1,4-bis(tridecyloxy)-2-butanesulfonic acid
Bis(tridecyl) sodium sulfosuccinate
Sodium ditridecyl sulfosuccinate
Aerosol TR-70
Sodium 1,4-ditridecyl sulfosuccinate
Aerosol TR
Monawet MT
Vaccenyl carnitine
Dolichol phosphate
Deoxythymidine diphosphate-l-rhamnose
D-Glucurono-6,3-lactone
cis-2-Methylaconitate
Gamma-linolenoyl-CoA
ADP-Mannose
L-Glyceric acid
3-Oxohexadecanoyl-CoA
Pseudoecgonyl-CoA
Decanoyl-CoA (n-C10:0CoA)
Ecgonine methyl ester
Selenophosphate
4-Hydroxy tolbutamide
Tyramine-O-sulfate
L-2-Amino-3-oxobutanoic acid
Arachidonyl carnitine
N-Acetyl-L-glutamyl 5-phosphate
D-Lactaldehyde
Hexacosanoyl-CoA
Arachidyl carnitine
Linoelaidyl carnitine
Homocysteinesulfinic acid
Stearidonyl carnitine
Elaidic carnitine
Leukotriene F4
4-hydroxybenzoyl-CoA
4-Hydroxydebrisoquine
Linoleyl carnitine
Methylisocitric acid
Calcitroic acid
Iso-Valeraldehyde
N-Acetyl-D-galactosamine 1-phosphate
LysoSM(d18:1)
D-Aspartic acid
2-Decaprenyl-3-methyl-6-methoxy-1,4-benzoquinone
10-formyldihydrofolate
5-Methyldihydrofolic acid
Pentaglutamyl folate
N-Acetyl-L-glutamate 5-semialdehyde
4-Nitrophenyl sulfate
Clupanodonyl carnitine
Heptadecanoyl CoA
3-Oxooctadecanoyl-CoA
Previtamin D3
P1,P4-Bis(5'-adenosyl) tetraphosphate
Phenylacetyl-CoA
Lumisterol 3
Retinoyl CoA
Nervonyl carnitine
Cervonyl carnitine
2,3-Diketo-L-gulonate
3-Mercaptolactate-cysteine disulfide
Docosa-4,7,10,13,16-pentaenoyl CoA
Timnodonyl CoA
Tetracosatetraenoyl CoA
Stearidonoyl CoA
cis-Vaccenoyl CoA
trans-3-Hexenoyl-CoA
Arachidonyl-CoA
3-Indoleacetonitrile
(+)-a-Pinene
Tetracosanoyl-CoA
Clupanodonic acid
trans-Octadec-2-enoyl-CoA
trans-2-Methyl-5-isopropylhexa-2,5-dienoyl-CoA
Palmitoleyl CoA
D-Xylulose 1-phosphate
Beta-1,4-mannose-N-acetylglucosamine
(a-D-mannosyl)2-b-D-mannosyl-N-acetylglucosamine
cis-2-Methyl-5-isopropylhexa-2,5-dienoyl-CoA
Debrisoquine
Stearidonic acid
Ecgonine
Aflatoxin B1
dIMP
L-4-Hydroxyglutamate semialdehyde
ADP-glucose
2,3-Epoxyaflatoxin B1
Heptaglutamyl folic acid
Tachysterol 3
8-iso-13,14-dihydro-15-keto-PGF2a
Hexaglutamyl folate
Sialyl-Lewis X
Ditridecyl sodium sulfosuccinate
Sodium bis(tridecyl) sulfosuccinate
Imidazole-4-carboxamide, 5-amino-
AICA
5-amino-1H-imidazole-4-carboxamide
IMIDAZOLE-4(OR 5)-CARBOXAMIDE, 5(OR 4)-AMINO-
AIC
4-Aminoimidazole-5-carboxamide
5-Aminoimidazol-4-carboxamide
1H-Imidazole-4-carboxamide, 5-amino-
4-Carbamoyl-5-aminoimidazole
4-Carboxamido-5-aminoimidazole
4-Amino-5-imidazolecarboxamide
Ba 2756
Colahepat
Ethylenecarboxamide
Akrylamid (CZECH)
acrylamide
2-Propenamide
Propenamide
Acrylic amide
L-.gamma.-Glutamylhydrazide
5-hydrazino-5-oxonorvaline
Glutamic acid, 5-hydrazide, L-
.gamma.-Glutamic acid hydrazide
L-Glutamate-.gamma.-hydrazide
L-Glutamic acid, 5-hydrazide
.gamma.-Glutamyl hydrazide
L-Glutamic acid .gamma.-hydrazide
GAH
2-hydrazino-3-methylpentanoic acid
Bis(.beta.-chloroethyl) vinylphosphonate
Vinifos
Bis-(2-chlorethyl)vinylfosfonat(CZECH)
Phosphonic acid, ethenyl-, bis(2-chloroethyl) ester
Phosphonic acid, vinyl-, bis(2-chloroethyl) ester
Bis(2-chloroethyl) vinylphosphonate
bis(2-chloroethyl) vinylphosphonate
Vinylphosphonic acid bis(.beta.-chloroethyl) ester
Fyrol BB
4-Nitroanthranilic acid
2-amino-4-(hydroxy(oxido)amino)benzoic acid
Benzoic acid, 2-amino-4-nitro-
Anthranilic acid, 4-nitro-
2-Amino-4-nitrobenzoic acid
NCI-C01945
4-amino-1H-pyrazolo[3,4-d]pyrimidin-6-yl hydrosulfide
4-amino-1H-pyrazolo[3,4-d]pyrimidine-6-thiol
4-amino-1H-pyrazolo[3,4-d]pyrimidine-6-thiol 4-amino-1H-pyrazolo[3,4-d]pyrimidin-6-yl hydrosulfide
2-Chloro-5-nitroaniline
6-Chloro-3-nitroaniline
Aniline, 2-chloro-5-nitro-
Benzenamine, 2-chloro-5-nitro-
(3-amino-4-chlorophenyl)(hydroxy)azane oxide

  """

    Analyzer ana = new SimpleTextAnalyzer()

    Set<String> result = ana.analyze(synonomys)

    println "false negative"

    int neg = 0
    int negTotal = 0
    synonomys.split("\n").each {
      negTotal++
      if (!result.contains(it.trim())) {
        if (it.trim().size() > 0) {
          neg++
          println "negative: ${it}"
        }
      }
    }

    println "-----------"

    int pos = 0;
    int total = 0;
    println "false positive"

    boolean first = true
    println "select distinct lower(name) from synonym where lower(name) in ("
    result.each {String s ->

      total++
      if (!synonomys.split("\n").toList().contains(s)) {
        if(!first){
          print ","
        }
        else{
          first = false
        }
        println "'${s.toLowerCase()}'"


        pos++
      }

    }
    println ")"
    println "-----------"

    println "false pos: ${(double) pos / (double) total * 100}"
    println "false neg: ${(double) neg / (double) negTotal * 100}"

    assertTrue((double) pos / (double) total * 100 < 40)


  }
}

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory
import grails.test.ControllerUnitTestCase

class DiscoveryControllerTests extends ControllerUnitTestCase {

  //our test document
  String document = """"

    Using these reactions we can now follow Fischer's train of logic in assigning the configuration of D-glucose.

1.   Ribose and arabinose (two well known pentoses) both gave erythrose on Ruff degradation.
    As expected, Kiliani-Fischer synthesis applied to erythrose gave a mixture of ribose and arabinose.
2.   Oxidation of erythrose gave an achiral (optically inactive) aldaric acid. This defines the configuration of erythrose.
3.   Oxidation of ribose gave an achiral (optically inactive) aldaric acid. This defines the configuration of both ribose and arabinose.
4.   Ruff shortening of glucose gave arabinose, and Kiliani-Fischer synthesis applied to arabinose gave a mixture of glucose and mannose.
5.   Glucose and mannose are therefore epimers at C-2, a fact confirmed by the common product from their osazone reactions.
6.   A pair of structures for these epimers can be written, but which is glucose and which is mannose?


    """

  protected void setUp() {
    super.setUp()

    SimpleCacheFactory.newInstance().createCache().getCache().clear()
    
  }

  protected void tearDown() {
    super.tearDown()
    SimpleCacheFactory.newInstance().createCache().getCache().clear()

  }

  void testProcessing() {

    //overwrite the getPArams methode, basically a simple mocking
    DiscoveryController.metaClass.getParams = {-> [query: document] }

    //create a new controller
    DiscoveryController controller = new DiscoveryController()

    def result = controller.process()

    println result
    //make sure we don't have a null result
    assertTrue result != null

    //we exspect 6 results

    println result.hits
    println "hits: ${result.hits}"
    println result.hits.size()
    assertTrue result.hits.size() == 9

    println "success"
  }

  void testProcessingWithSpecifiedConfidenceLevel() {

    //overwrite the getPArams methode, basically a simple mocking
    DiscoveryController.metaClass.getParams = {-> [query: document, limitConfidence: 0.9] }

    //create a new controller
    DiscoveryController controller = new DiscoveryController()

    def result = controller.process()

    println result
    //make sure we don't have a null result
    assertTrue result != null
    assertTrue result.hits != null

    println "hits: ${result.hits}"
    println result.hits.size()
    assertTrue result.hits.size() == 5

    println "success"
  }

  void testProcessingWithSpecifiedConfidenceLevelWhichShallHaveNoResults() {

    //overwrite the getParams methode, basically a simple mocking
    DiscoveryController.metaClass.getParams = {-> [query: document, limitConfidence: 1.0] }

    //create a new controller
    DiscoveryController controller = new DiscoveryController()

    def result = controller.process()

    println result
    //make sure we don't have a null result
    assertTrue result != null
    println "hits: ${result.hits}"
    println result.hits.size()

    assertTrue result.hits.size() == 5

    println "success"
  }

  /**
   * tests the resolving of inchi code
   */
  void testInchiResolving() {


    String toTest = """
    gdgfdgdfg
    gfdgfdgdfg
    gfdgfgdvcv
                          fgdgndflgdsfgfds
        InChI=1S/C6H12O6/c7-1-3(9)5(11)6(12)4(10)2-8/h1,3-6,8-12H,2H2/t3-,4+,5+,6+/m0/s1

        gfgnxk.cnml/xdmcbxcl
        InChI=1S/C6H12O6/c7-1-3(9)5(11)6(12)4(10)2-8/h3,5-9,11-12H,1-2H2/t3-,5-,6-/m1/s1
        vsdfdlsngm.fmgvxzv

    """

    //overwrite the getParams methode, basically a simple mocking
    DiscoveryController.metaClass.getParams = {-> [query: toTest] }

    //create a new controller
    DiscoveryController controller = new DiscoveryController()

    def result = controller.process()

    println result

    //make sure we don't have a null result
    assertTrue result != null

    println result.hits
    println result.hits.size()

    assertTrue result.hits.size() == 2

    println "success"
  }

}

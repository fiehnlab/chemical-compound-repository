import grails.test.GrailsUnitTestCase
/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Feb 24, 2010
 * Time: 11:54:56 AM
 * To change this template use File | Settings | File Templates.
 */
class DiscoveryServiceTests extends GrailsUnitTestCase {
    DiscoveryService service = null

    protected void setUp() {
      service = new DiscoveryService()
      super.setUp()
    }

    protected void tearDown() {
      super.tearDown()
      service = null
    }

    void testProcess()
    {
      String query = "Hello benzene world, you are full of vitamin a"
      def result = service.process(query,0.5)

      assertTrue(result.size() == 1)

    }
}

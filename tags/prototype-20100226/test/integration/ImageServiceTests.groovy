/**
 * User: wohlgemuth
 * Date: Feb 22, 2010
 * Time: 11:55:20 PM
 */

import grails.test.GrailsUnitTestCase

class ImageServiceTests extends GrailsUnitTestCase {
  protected void setUp() {
    super.setUp()


  }

  protected void tearDown() {
    super.tearDown()

  }

  void testImageCreation() {

    Compound compound = Compound.get(281705)

    ImageService service = new ImageService()
    RenderService render = new RenderService()
    service.renderService = render

    String path = service.getStructure(compound, 320)

    log.info("path: ${path}")
  }
}

import grails.test.GrailsUnitTestCase

class ConvertServiceTests extends GrailsUnitTestCase {
  ConvertService service = new ConvertService()

  protected void setUp() {
    super.setUp()
    service = new ConvertService()

    //assign the lookup service
    service.lookupService = new LookupService()

  }

  protected void tearDown() {
    super.tearDown()
  }

  void testConvert() {
    service.convert("kegg", "cas", ["C00001","C00002"])
  }


  Compound getCompound(long id) {
    return service.lookupService.lookupByCompoundId(id);
  }

  void testConvertToCOMPOUND() {
    assertTrue(service.convertToCOMPOUND(getCompound(271574)).inchiHashKey.completeKey == "ZKHQWZAMYRWXGA-KQYNXXCUSA-N")
  }

  void testConvertToINCHI() {
    assertTrue(service.convertToINCHI(getCompound(271574)) == "InChI=1S/C10H16N5O13P3/c11-8-5-9(13-2-12-8)15(3-14-5)10-7(17)6(16)4(26-10)1-25-30(21,22)28-31(23,24)27-29(18,19)20/h2-4,6-7,10,16-17H,1H2,(H,21,22)(H,23,24)(H2,11,12,13)(H2,18,19,20)/t4-,6-,7-,10-/m1/s1")

  }

  void testConvertToINCHIKEY() {
    assertTrue(service.convertToINCHIKEY(getCompound(271574)).completeKey == "ZKHQWZAMYRWXGA-KQYNXXCUSA-N")
  }

  void testConvertToLINKS() {
    assertTrue(service.convertToLINKS(getCompound(271574)).size() == 6)
  }

  void testConvertToSMILES() {
    assertTrue(service.convertToSMILES(getCompound(339635)).size() == 2)

  }

  void testConvertToIUPAC() {
    assertTrue(service.convertToIUPAC(getCompound(271571)).size() == 2)
  }

  void testConvertToNAMES() {
    assertTrue(service.convertToNAMES(getCompound(271583)).size() == 16)
  }

  void testConvertToMASS() {
    assertTrue(service.convertToMASS(getCompound(271608)) == 626.491)
  }

  void testConvertToFORMULA() {
    assertTrue(service.convertToFORMULA(getCompound(271608)) == "C40H66O5")
  }

  void testConvertToCas() {
    assertTrue(service.convertToCas(getCompound(271580))[0].casNumber == "109-76-2")
  }

  void testConvertToKegg() {
    assertTrue(service.convertToKegg(getCompound(271571))[0].keggId == "C00001")

  }

  void testConvertToLipidMap() {
    assertTrue(service.convertToLipidMap(getCompound(271583))[0].lipidMapId == "LMFA01060002")

  }

  void testConvertToCID() {
    assertTrue(service.convertToCID(getCompound(271571))[0].cid == 962)

  }

  void testConvertToSID() {
    assertTrue(service.convertToSID(getCompound(271583))[0].sid == 7850243)

  }

  void testConvertToHMDB() {
    assertTrue(service.convertToHMDB(getCompound(271571))[0].hmdbId == "HMDB02111")

  }

  void testconvertToChebi() {
    assertTrue(service.convertToChebi(getCompound(271574))[0].chebiId== "15422")

  }
}

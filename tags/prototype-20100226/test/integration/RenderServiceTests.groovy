import grails.test.GrailsUnitTestCase
import java.io.*;
import org.openscience.cdk.io.*;
import org.openscience.cdk.*;
import org.openscience.cdk.io.iterator.*;
import org.openscience.cdk.layout.*;
import org.openscience.cdk.nonotify.*;
import org.openscience.cdk.inchi.*;
import org.openscience.cdk.interfaces.IAtomContainer
import org.openscience.cdk.*
import org.openscience.cdk.renderer.*

import edu.ucdavis.genomics.metabolomics.binbase.connector.references.sdf.SdfToInchi;


class RenderServiceTests extends GrailsUnitTestCase {

  RenderService service = null

  protected void setUp() {
    super.setUp()
    service = new RenderService()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testRenderToMolString() {

    String from = "InChI=1S/C6H12O6/c7-1-3(9)5(11)6(12)4(10)2-8/h1,3-6,8-12H,2H2"
    String mol = service.renderToMol(from)

    String to = SdfToInchi.sdfToInchi(new ByteArrayInputStream(mol.getBytes()))


    assert(from == to)

  }

  void testRenderToMolCompound() {
    String from = "InChI=1S/C6H12O6/c7-1-3(9)5(11)6(12)4(10)2-8/h1,3-6,8-12H,2H2"

    fail("todo")
  }

}

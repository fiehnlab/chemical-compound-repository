import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory
import resolver.DatabaseContentResolver
import resolver.Resolveable
import types.Hit

/**
 * User: wohlgemuth
 * Date: Feb 8, 2010
 * Time: 2:14:01 PM
 *
 */
class DatabaseContentResolverTests extends GroovyTestCase {

  public void testResolve()

  {
    SimpleCacheFactory.newInstance().createCache().getCache().clear()

    Resolveable resolve = DatabaseContentResolver.getInstance()

    resolve.activateResolver()
    def result = resolve.resolve("ethanoic acid")

    println result

    resolve.disactivateResolver()

    assertTrue(result.size() == 1)

    Hit hit = result.asList().get(0)

    assertTrue(hit.type == Hit.COMPOUND_ID)
    assertTrue(hit.value.equals("ethanoic acid"))


  }


  public void testResolveBatchMode() {

    SimpleCacheFactory.newInstance().createCache().getCache().clear()

    Resolveable resolve = DatabaseContentResolver.getInstance()

    resolve.activateResolver()
    resolve.addBatch "ethanoic acid"
    def result = resolve.resolve()

    println result

    resolve.disactivateResolver()

    assertTrue(result.size() == 1)

    Hit hit = result.asList().get(0)

    assertTrue(hit.type == Hit.COMPOUND_ID)
    assertTrue(hit.value.equals("ethanoic acid"))


  }


}

package resource

import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader
import org.springframework.core.io.Resource
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.FileSystemResource

/**
 * User: wohlgemuth
 * Date: Feb 2, 2010
 * Time: 4:05:39 PM
 *
 */
class LocalResourceLoader implements ResourceLoaderAware, ResourceLoader {
  void setResourceLoader(ResourceLoader resourceLoader) {

  }

  Resource getResource(String s) {
    File file = new File(s)

    println file.getAbsolutePath()
    if (file.exists()) {
      return new FileSystemResource(s)
    }
    else {
      return new ClassPathResource(s)
    }
  }

  ClassLoader getClassLoader() {
    return getClass().getClassLoader();
  }

}

package annotations

import java.lang.reflect.Method

/**
 * User: wohlgemuth
 * Date: Feb 10, 2010
 * Time: 7:55:21 PM
 *
 */
class AnnotationHelper {

  /**
   * generates the list of annotations in this class
   */
  static Collection<QueryObject> getQueryableAnnotations(Class c) {

    //contains the result
    Set<QueryObject> set = new HashSet<QueryObject>()

    for (Method method: c.getMethods()) {
      if (method.isAnnotationPresent(Queryable.class)) {
        Queryable q = method.getAnnotation(Queryable.class)
        QueryObject object = new QueryObject()
        object.annotation = q
        object.method = method

        set.add(object)
      }
    }

    return set
  }
}

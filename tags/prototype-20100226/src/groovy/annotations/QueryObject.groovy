package annotations

import java.lang.reflect.Method

/**
 * simple wrapper object
 * User: wohlgemuth
 * Date: Feb 10, 2010
 * Time: 8:01:33 PM
 * 
 */
class QueryObject implements Comparable{

  Queryable annotation

  Method method

  int compareTo(Object o) {
    return method.getName().compareTo(o.method.getName());
  }
}

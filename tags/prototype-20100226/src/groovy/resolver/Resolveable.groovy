package resolver

import types.Hit
import resolver.interceptor.ResolveableInterceptor

/**
 * User: wohlgemuth
 * Date: Feb 1, 2010
 * Time: 10:48:51 PM
 */
public interface Resolveable {

  /**
   * resolves a couple of hits in this input stream
   */
  public Set<Hit> resolve(String input)

  /**
   * sets the minimum required confidence level 
   */
  public void setConfidenceLevel(double value)

  /**
   * activates something in a resolver
   */
  public void activateResolver()

  /**
   * disactivates something in a resolver
   */
  public void disactivateResolver()



   /**
  *   adds a new interceptor
    *  */
  public Resolveable addInterceptor(ResolveableInterceptor interceptor)

  /**
   * remove an interceptor
   */
  public Resolveable removeInterveptor(ResolveableInterceptor interceptor)

  /**
   * returns all interceptors
   */
  public Collection<ResolveableInterceptor> getInterceptors()
}
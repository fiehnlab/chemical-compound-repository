package resolver.analyzer

/**
 * a simple analyzer to analyze some text input
 */
public interface Analyzer {

  /**
   * analyzes the given string and trys to generate a list of possible terms for it
   * @param String
   * @return
   */
  Set<String> analyze(String text)

}
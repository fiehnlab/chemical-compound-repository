package resolver.interceptor

import org.apache.log4j.Logger
import com.fiehn.util.StopListParser

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Feb 24, 2010
 * Time: 4:07:03 PM
 * To change this template use File | Settings | File Templates.
 */
class StoplistInterceptor implements ResolveableInterceptor{

  Logger logger = Logger.getLogger("interceptor")

  /**
   * intercewpts the given queries and filtes them by the stop list
   */

  def Set<String> intercept(Set<String> queries) {

    //filter
    logger.error "need implementation!"
    queries = StopListParser.getParsedData(queries)
    return queries
  }
}

package pattern

/**
 * simple helper class to give us patterns
 *
 * User: wohlgemuth
 * Date: Feb 1, 2010
 * Time: 11:43:39 PM
 */
class PatternHelper {

  /**
   * defines a pattern for a standard inchi
   */

  public final static String STD_INCHI_PATTERN = "InChI=1S/([^/]+)(?:/[^/]+)*\\S"

  /**
   * defines the pattern for a standard inchi key
   */
  public final static String STD_INCHI_KEY_PATTERN = "[A-Z]{14}-[A-Z]{10}-[A-Z,0-9]"

  /**
   *
   * identifies a cas entry
   */
  public final static String CAS_PATTERN = "\\d{1,7}-\\d\\d-\\d"

  /**
   * identifies a kegg entry
   */
  public final static String KEGG_PATTERN = "C[0-9]{5}"

  /**
   * pattern for lipid maps
   */
  public final static String LIPID_MAPS_PATTERN = "LMFA[0-9]{8}"

  /**
   * pattern for hmdb
   */
  public final static String HMDB_PATTERN = "HMDB[0-9]*"

  /**
   * pattern for cid
   */
  public final static String CID_PATTERN = "(cid:[0-9]*)|(CID:[0-9]*)"

  /**
   * pattern for sid
   */
  public final static String SID_PATTERN = "(sid:[0-9]*)|(SID:[0-9]*)"

}

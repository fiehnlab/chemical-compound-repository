package com.fiehn.util;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Feb 23, 2010
 * Time: 11:25:14 PM
 * To change this template use File | Settings | File Templates.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class StopListParser {
	private static ArrayList<String> gDataList = null;

	private static void prepareStopList()
	{
        gDataList = new ArrayList<String>();
		File file = new File(getStopListDictionaryPath());

        FileInputStream fis = null;
	    Scanner scanner = null;

	    try {
	      fis = new FileInputStream(file);
	      scanner = new Scanner(fis);
	      //first use a Scanner to get each line
	      while ( scanner.hasNextLine() )
	      {
	          // this statement reads the line from the file and add it to gDataList
	    	  gDataList.add( scanner.nextLine().trim().toLowerCase() );
	      }

	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    } finally
	    {
	        //ensure the underlying stream is always closed
	        try
	        {
				scanner.close();
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	      }
	}


    private static String getStopListDictionaryPath()
    {
        return System.getProperty("user.dir")+"/src/java/com/fiehn/util/StopListDictionary.txt";
    }


	public static String getParsedData(String inputStr)
	{
		String lStrResult = "" ;
		try
		{
			// set stop data list
			if( gDataList == null )
			{
				prepareStopList();
			}

			String[] strList = inputStr.split(" ");
			int licnt = 0;
			for (String string : strList)
			{
				if( !gDataList.contains(string.trim().toLowerCase() ) )
				{
					if( licnt == 0)
						lStrResult += string;
					else
						lStrResult += " "+string;
					licnt++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lStrResult;
	}

    public static Set<String> getParsedData(Set<String> queries)
	{
		Set<String> lResultSet = new HashSet<String>();

		try
		{
			// set stop data list
			if( gDataList == null )
			{
				prepareStopList();
			}

			int licnt = 0;
			for (String string : queries)
			{
				if( !gDataList.contains(string.trim().toLowerCase() ) && lResultSet.add(string) );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lResultSet;
	}

}
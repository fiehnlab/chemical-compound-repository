import csvreader.CsvReader;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 22, 2010
 * Time: 12:16:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChemSimilarityUtil
{

        double num[][] = null;
        int rowCount = 0;
        int colCount = 0;
        double probabilityFactor = 0.7;

        public ArrayList<String> getMappingFromText(String data)
        {
            ArrayList<String> lArrList = null;
            try
            {
                char delimiter = getDelimiter(data);
                StringReader reader1 = new StringReader(data);
                CsvReader reader = new CsvReader(reader1,delimiter);
                lArrList = getResult(reader);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return lArrList;
        }

        public boolean createOutputFile(ArrayList data,String fileName)
        {
            FileOutputStream out; // declare a file output object
            PrintStream p; // declare a print stream object
            try
            {
                // Create a new file output stream
                out = new FileOutputStream(fileName);

                // Connect print stream to the output stream
                p = new PrintStream( out );

                for (int index=0 ; index < data.size() ; index++)
                {
                   p.println (data.get(index).toString().trim());
                }

                p.close();
            }
            catch (Exception e)
            {
                System.err.println ("Error writing to file");
                e.printStackTrace();
                return false;
            }
            return true;
        }




        private char getDelimiter(String s)
        {
            char delimiter='-';
            if( s.contains(",") )
            {
                delimiter = ',';
            }else
            if( s.contains("\t") )
            {
                delimiter = '\t';
            }
            return delimiter;
        }


        public ArrayList<String> getMappingFromFile(String fileName)
        {
            ArrayList<String> lArrList = null;
            try {
                CsvReader reader = new CsvReader(fileName);
                lArrList = getResult(reader);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return lArrList;
        }

        private ArrayList<String>  getResult(CsvReader reader) {
            ArrayList<String> lArrList = new ArrayList<String>();
            try
            {
                reader.readRecord();
                rowCount = reader.getColumnCount();
                colCount = rowCount;
                System.out.println("rowCount :: "+rowCount);
                System.out.println("colCount :: "+colCount);
                num = new double[rowCount][colCount+1];

                int rowId = 0 ;

                do{
                    for(int colId=0; colId<colCount ; colId++ )
                    {
                        num[rowId][colId] = Double.parseDouble("0"+reader.get(colId));
                    }
                    rowId++;
                }while(reader.readRecord());
                reader.close();

                System.out.println("================ Input =============");
                showTable();

                System.out.println("\n================ Step-1: Create Lower Triangular Matrix =============");
                getLowerTriangularMatrix();
                showTable();

                System.out.println("\n================ Step-2: Create Max Valued Matrix =============");
                getMaxValueMatrix();
                showTable();

                System.out.println("\n================ Step-3: Create Probability Matrix =============");
                getProbabilityMatrix();
                showTable();

                System.out.println("\n================ Step-4: Get Result =============");
                lArrList = getResultMatrix();
                //showTable();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return lArrList;
        }

        void showTable()
        {
            for(int rowId=0; rowId<rowCount ; rowId++ )
            {
                for(int colId=0; colId<colCount+1 ; colId++ )
                {
                    System.out.print( num[rowId][colId] );
                    System.out.print("\t");
                }
                System.out.println("");
            }
        }

        void getLowerTriangularMatrix()
        {
            for(int rowId=1; rowId<rowCount ; rowId++ )
            {
                for(int colId=1; colId<colCount ; colId++ )
                {
                    if( rowId <= colId)
                        num[rowId][colId] = num[rowId][colId] - num[colId][rowId];
                }
            }
        }

        void getMaxValueMatrix()
        {
            for(int rowId=1; rowId<rowCount ; rowId++ )
            {
                int id = getMaximumListValue(num[rowId],rowId);
                if(id!=-1)
                {
                    num[rowId][colCount] = num[0][id];
                }
            }
        }

        void getProbabilityMatrix()
        {
            for(int rowId=1; rowId<rowCount ; rowId++ )
            {
                for(int colId=1; colId<rowId ; colId++ )
                {
                    if( num[rowId][colId] < probabilityFactor )
                    {
                        num[rowId][colId]=0;
                    }
                }
            }
        }

        ArrayList<String> getResultMatrix()
        {
            ArrayList<String> lArrList = new ArrayList<String>();
            for(int rowId=2; rowId<rowCount ; rowId++ )
            {
                lArrList.add( Math.round( num[rowId][0] )+ "\t" + "tm\t"+ Math.round( num[rowId][colCount] ) );
            }
            System.out.println("----->>");
            for(int rowId=1; rowId<rowCount ; rowId++ )
            {
                for(int colId=1; colId<rowId ; colId++ )
                {
                    if( num[rowId][colId] != 0.0 )
                    {
                        lArrList.add( Math.round( num[rowId][0] ) + "\t" + "tm\t"+ Math.round( num[0][colId] ) );
                    }
                }
            }
            return lArrList;
        }


        int getMaximumListValue(double[] val,int rowId)
        {
            double res=-1;
            int resIndex=-1;
            try
            {
                for(int index=1; index<rowId ; index++ )
                {
                    if( res < val[index]  ){
                        resIndex = index;
                        res = val[index];
                    }

                }
            }catch(Exception e)
            {
                e.printStackTrace();
            }

            return resIndex;
        }


}

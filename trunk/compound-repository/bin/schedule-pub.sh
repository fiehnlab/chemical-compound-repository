 #!/bin/sh

#schedules all jobs for the pubchem sdf files in the specified directory


#determine the java home directory for this server
JAVA_HOME=`which java`

 PATH=$JAVA_HOME:$PATH

JAVA_HOME=`dirname $JAVA_HOME`
JAVA_HOME=`dirname $JAVA_HOME`

export PATH JAVA_HOME


#find all files in the given argumetns
for file in $@
do

	#generate the absolute file name for the file
	DIR=`dirname $file`
	DIR=`cd $DIR; pwd`
	FILE=`basename $file`
	FILE="$DIR/$FILE"
	echo scheduling $FILE

	#schedule the calculation
	qsub -S /bin/bash -m beas -N import-pubchem-compound -V -o ../log -e ../log -cwd pub.sh $FILE

	#sleep for 1 second so the qmaster has a chance to catchup
	sleep 1
done


exit 0

/**
 * simple cts example to transform data into eachother, written by wohlgemuth@ucdavis.edu - Gert Wohlgemuth
 */

/**
 * where can we find our dependencies
 */

@GrabResolver(name = 'binbase', root = 'http://binbase-maven.googlecode.com/svn/trunk/repo')
@GrabResolver(name = 'jboss', root = 'http://repository.jboss.com/maven2')
@GrabResolver(name = 'javanet', root = 'http://download.java.net/maven/2')
@GrabResolver(name = 'codehaus', root = 'http://repository.codehaus.org')
@GrabResolver(name = 'codehaus-snapshot', root = 'http://snapshots.repository.codehaus.org')

/**
 * we don't need these
 */
@GrabExclude('javax.jms:jms')
@GrabExclude('javax.transaction:jta')
@GrabExclude('org.springframework:spring:2.5')

/**
 * required dependencies, it can take a while to download them so be patiant
 */
@Grab(group = 'org.codehaus.groovy.modules', module = 'groovyws', version = '0.5.2')
@Grab(group = 'edu.ucdavis.genomics.metabolomics.binbase.connector', module = 'binbase-connector', version = '3.4.8')
@Grab(group = 'wsdl4j', module = 'wsdl4j', version = '1.6.2')

/**
 * required imports
 */
import groovyx.net.ws.WSClient
/**
 * check to command line arguments
 */
if (args.length != 4) {
    println "Usage:"
    println "args[0] = server:port"
    println "args[1] = from"
    println "args[2] = to"
    println "args[3] = value"

    println ""
    println "please provide the required arguments..."
    println ""
    return
}

/**
 * command line arguments
 */
def server = args[0]
def from = args[1]
def to = args[2]
def value = args[3]

/**
 * access the webserver
 */
proxy = new WSClient("http://${server}/cts/services/transformWeb?wsdl", this.class.classLoader)

//set a long timeout
proxy.setConnectionTimeout(90000)
proxy.initialize()

println "\n"

println "result:\n"
proxy.convertFromTo(from, to, value).each {String s ->
    println s
}
println ""

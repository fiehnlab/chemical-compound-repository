package compound.repository

import test.GrailsDBunitIntegrationTest
import types.Hit
import compound.repository.entries.Compound
import compound.repository.LookupService

//import com.redpointtech.dbunit.*


class LookupServiceTests extends GrailsDBunitIntegrationTest {
  LookupService lookupService = null

  public LookupServiceTests() {

  }

  protected void setUp() {
    super.setUp()

  }

  protected void tearDown() {
    super.tearDown()
  }


  void testLookupByPartialInchiKey() {

    logger.debug "lookup by partial"
    String key = "LNRYWMBIOOXPID"
    def result = lookupService.lookupByPartialInchiKey(key)
    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookupByInchiKey() {

    logger.debug "lookup by inchi key"
    String key = "ATQXXRJEZULCTG-DLCFQVRISA-N"
    def result = lookupService.lookupByInchiKey(key)

    logger.debug "result : ${result}"
    logger.debug "result.inchiHashKey.completeKey : ${result.inchiHashKey.completeKey}"

    println result
    
    assertTrue(result.get(0) instanceof Compound)

    assertTrue(result.get(0).inchiHashKey.completeKey == key)

  }


  void testLookupByInchi() {

    logger.debug "lookup by inchi"
    String key = "InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1"
    def result = lookupService.lookupByInchi(key)

    assertTrue(result instanceof Compound)

    assertTrue(result.inchi == "InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1")

  }




  void testLookupByKegg() {

    logger.debug "lookup by kegg"
    String key = "C10001"
    def result = lookupService.lookupByKegg(key)

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }

  void testLookupByKeggList() {

    logger.debug "lookup by kegg list"
    def key = ["C10001", "C10002"]
    def result = lookupService.lookupByKegg(key)

    assertTrue(result.size() == 2)

  }


  void testLookupByCas() {

    logger.debug "lookup by cas"
    String key = "109345-26-8"
    def result = lookupService.lookupByCas(key)

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }

  }

  void testLookupByCID() {

    logger.debug "lookup by cid"
    int key = 1
    def result = lookupService.lookupByCID(key)


    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }

  }


  void testLookupBySID() {

    logger.debug "lookup by sid"
    int key = 74380251
    def result = lookupService.lookupBySID(key)


    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookupBySmile() {

    logger.debug "lookup by smile"
    String key = "CC(CN)O"
    def result = lookupService.lookupBySmile(key)


    assertTrue(result.size() == 2)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }

  void testLookupByExactMass() {

    logger.debug "lookup by exact mass"
    double key = 203.115758
    def result = lookupService.lookupByExactMass(key)


    logger.debug("Mass : Result : size : ${result.size()}")

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }



  void testLookupByFormula() {

    logger.debug "lookup by formula"
    String key = "C9H17NO4"
    def result = lookupService.lookupByFormula(key)

    logger.debug("Formula : Result : size : ${result.size()}")

    assertTrue(result.size() == 1)

    for (def res: result) {
      assertTrue(res instanceof Compound)
    }
  }


  void testLookUpByName() {

    logger.debug "lookup by name"
    def result = lookupService.lookupByName("Deoxycytidine")

    assertTrue(result.size() == 1)

  }

  void testLookUpByNameWildCard() {

    logger.debug "lookup by name"
    def result = lookupService.lookupByName("Deoxycytidi?e")

    assertTrue(result.size() == 1)

  }


  void testLookUpByMultipleName() {

    logger.debug "lookup by multi name"
    def result = lookupService.lookupByName(["Deoxycytidine", "Cortexolone", "Deoxycorticosterone"])
    assertTrue(result.size() == 3)

  }

  void testLookUpByHMDB() {

    logger.debug "lookup by hmdb"
    def result = lookupService.lookupByHMDB("HMDB00011")

    assertTrue(result.size() == 1)

  }



  void testLookUpByLipidMap() {

    logger.debug "lookup by lipid map"
    def result = lookupService.lookupByLipidMapId("LMFA00000001")

    assertTrue(result.size() == 1)

  }



  void testLookUpByChebi() {

    logger.debug "lookup by chebi"
    def result = lookupService.lookupByChebiId("7")

    println result
    assertTrue(result.size() == 1)

  }

  void testLookUpByCas() {

    logger.debug "lookup by cas"
    def result = lookupService.lookupByCas("109345-26-8")

    assertTrue(result.size() == 1)

  }


  void testLookUpBySid() {

    logger.debug "lookup by sid"
    def result = lookupService.lookupBySID("74380251")

    assertTrue(result.size() == 1)

  }

  void testLookUpByCid() {

    logger.debug "lookup by cid"
    def result = lookupService.lookupByCID(1)

    assertTrue(result.size() == 1)
  }

  void testLookUpByHitCas() {

    logger.debug "lookup by hit cas"
    Hit hit = new Hit()
    hit.type = Hit.KEGG
    hit.value = "C10094"

    def result = lookupService.lookupByHit(hit)

    logger.debug "result for test lookup by hit: ${result}"
    assertTrue(result.size() == 1)

  }



  void testCountLookupByPartialInchiKey() {

    logger.debug "lookup by count partial"
    String key = "LNRYWMBIOOXPID"
    Long result = lookupService.countlookupByPartialInchiKey(key)
    assertTrue(result == 1)
  }

  void testCountLookupByInchiKey() {

    logger.debug "lookup by count inchi key"
    String key = "ATQXXRJEZULCTG-DLCFQVRISA-N"
    Long result = lookupService.countlookupByInchiKey(key)
    assertTrue(result == 1)
  }

  void testCountLookupByInchi() {

    logger.debug "lookup by count inchi"
    String key = "InChI=1S/C7H11N3O2/c1-10-3-5(9-4-10)2-6(8)7(11)12/h3-4,6H,2,8H2,1H3,(H,11,12)/t6-/m0/s1"
    Long result = lookupService.countlookupByInchi(key)

    assertTrue(result == 1)
  }

  void testCountLookupByKegg() {
    logger.debug "lookup by count kegg"

    def key = ["C10001", "C10002"]
    Long result = lookupService.countlookupByKegg(key)
    assertTrue(result == 2)
  }

  void testCountLookupByCas() {
    logger.debug "lookup by count cas"

    String key = "109345-26-8"
    Long result = lookupService.countlookupByCas(key)
    assertTrue(result == 1)
  }

  void testCountLookupByCID() {
    logger.debug "lookup by count cid"

    int key = 1
    Long result = lookupService.countlookupByCID(key)
    assertTrue(result == 1)
  }

  void testCountLookupBySID() {
    logger.debug "lookup by count sid"

    int key = 74380251
    Long result = lookupService.countlookupBySID(key)
    assertTrue(result == 1)
  }

  void testCountLookupBySmile() {
    logger.debug "lookup by count smile"

    String key = "CC(CN)O"
    Long result = lookupService.countlookupBySmile(key)
    assertTrue(result == 1)
  }

  void testCountLookupByExactMass() {
    logger.debug "lookup by count exact mass"

    double key = 203.115758
    Long result = lookupService.countlookupByExactMass(key)
    logger.debug result
    assertTrue(result == 1)
  }

  void testCountLookupByFormula() {
    logger.debug "lookup by count formula"

    String key = "C9H17NO4"
    Long result = lookupService.countlookupByFormula(key)
    assertTrue(result == 1)
  }

  void testCountLookUpByHMDB() {
    logger.debug "lookup by count hmdb"

    Long result = lookupService.countlookupByHMDB("HMDB00011")
    assertTrue(result == 1)
  }

  void testCountLookUpByLipidMap() {
    Long result = lookupService.countlookupByLipidMapId("LMFA00000001")
    assertTrue(result == 1)
  }

  void testCountLookUpByChebi() {
    Long result = lookupService.countlookupByChebiId("7")
    assertTrue(result == 1)
  }

  void testCountLookUpByMultipleName() {

    Long result = lookupService.countlookupByName(["Deoxycytidine", "Cortexolone", "Deoxycorticosterone"])

    assertTrue(result == 3)

  }

  void testCountLookUpByName() {

    Long result = lookupService.countlookupByName("Deoxycytidine")

    assertTrue(result == 1)

  }

}

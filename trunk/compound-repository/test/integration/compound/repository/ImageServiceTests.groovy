package compound.repository
/**
 * User: wohlgemuth
 * Date: Feb 22, 2010
 * Time: 11:55:20 PM
 */

import test.GrailsDBunitIntegrationTest
import compound.repository.entries.Compound
import compound.repository.ImageService
import compound.repository.RenderService
import org.springframework.context.ApplicationContext

class ImageServiceTests extends GrailsDBunitIntegrationTest {

  ApplicationContext  applicationContext

  protected void setUp() {
    super.setUp()
  }

  protected void tearDown() {
    super.tearDown()

  }

  void testImageCreation() {

    Compound compound = Compound.get(895880)
    logger.info("Compound ::  ${compound} ")
    logger.info("Compound ::  ${compound.getClass()} ")

    ImageService service = new ImageService()
    RenderService render = new RenderService()

    service.applicationContext = applicationContext
    service.g = new org.codehaus.groovy.grails.plugins.web.taglib.ApplicationTagLib()
    service.renderService = render

    String path = service.getStructure(compound.id,compound.inchi, 320)

    log.info("path: ${path}")
  }
}

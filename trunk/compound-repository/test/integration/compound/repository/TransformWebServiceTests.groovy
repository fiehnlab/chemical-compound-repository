package compound.repository

import grails.test.*

class TransformWebServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {

    }

    /**
     * transforms a molfile to an inchi key
     * @param molFile
     * @return
     */
    void testTransformMolToInchi() {

        String molFile = """14700003
  -OEChem-12260900122D

 33 35  0     1  0  0  0  0  0999 V2000
    0.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 N   0  0  3  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    0.0000    0.0000    0.0000 H   0  0  0  0  0  0  0  0  0  0  0  0
  1 12  2  0  0  0  0
  2 14  2  0  0  0  0
  3 15  2  0  0  0  0
  4 17  2  0  0  0  0
  5  6  1  0  0  0  0
  5 12  1  0  0  0  0
  5 18  1  0  0  0  0
  6 13  1  0  0  0  0
  6 22  1  0  0  0  0
  7 14  1  0  0  0  0
  7 17  1  0  0  0  0
  7 19  1  0  0  0  0
  8 15  1  0  0  0  0
  8 17  1  0  0  0  0
  8 20  1  0  0  0  0
  9 10  1  0  0  0  0
  9 16  1  0  0  0  0
 10 12  1  0  0  0  0
 10 13  2  0  0  0  0
 11 14  1  0  0  0  0
 11 15  1  0  0  0  0
 11 16  2  0  0  0  0
 13 21  1  0  0  0  0
 16 33  1  0  0  0  0
 18 23  2  0  0  0  0
 18 24  1  0  0  0  0
 19 27  1  0  0  0  0
 20 28  1  0  0  0  0
 23 25  1  0  0  0  0
 24 26  2  0  0  0  0
 25 29  2  0  0  0  0
 26 29  1  0  0  0  0
 27 30  1  0  0  0  0
 28 31  2  0  0  0  0
 30 32  1  0  0  0  0
M  END
> <PUBCHEM_COMPOUND_ID_TYPE>
0

> <PUBCHEM_TOTAL_CHARGE>
0

> <PUBCHEM_SUBSTANCE_ID>
14700003

> <PUBCHEM_SUBSTANCE_VERSION>
2

> <PUBCHEM_EXT_DATASOURCE_NAME>
ZINC

> <PUBCHEM_EXT_DATASOURCE_REGID>
ZINC08431301

> <PUBCHEM_SUBSTANCE_COMMENT>
<a target=_blank href="http://www.specs.net/enter.php?specsid=AJ-030/12105198">Specs AJ-030/12105198</A>

> <PUBCHEM_SUBSTANCE_SYNONYM>
ZINC08431301

> <PUBCHEM_XREF_EXT_ID>
ZINC08431301

> <PUBCHEM_EXT_DATASOURCE_URL>
http://zinc.docking.org/

> <PUBCHEM_EXT_SUBSTANCE_URL>
http://zinc.docking.org/srchdb.pl?zinc=8431301

> <PUBCHEM_CID_ASSOCIATIONS>
6877131  1

\$\$\$\$"""

        println molFile

        TransformWebService service = new TransformWebService()
        String inchi = service.transformMolToInchi(molFile)

        println "result: ${inchi}"
        assertTrue(inchi != null)

    }

    /**
     * trnasforms an inchi code to an inchi key
     * @param inchiCode
     * @return
     */
    void testTransformInchiCodeToKey() {
        String inchi = "InChI=1S/C23H27N5O4/c1-5-7-14-27-21(30)18(20(29)26(13-6-2)23(27)32)15-24-19-16(3)25(4)28(22(19)31)17-11-9-8-10-12-17/h6,8-12,15,24H,2,5,7,13-14H2,1,3-4H3"
        TransformWebService service = new TransformWebService()

        String key = service.transformInchiCodeToKey(inchi)

        println "result ${key}"

        assertTrue(key != null)
    }
}

import grails.test.*
import compound.repository.entries.ChEBI

class ChebiTests extends GrailsUnitTestCase {

  ChEBI chebi

  protected void setUp() {
    super.setUp()
    chebi = new ChEBI()
  }

  protected void tearDown() {
    super.tearDown()
  }

  void testToString() {

    chebi.chebiId = "fsdfs"

    assertTrue(chebi.toString() == "ChEBI: fsdfs")
  }

  void testSetChebiId() {
    chebi.chebiId = "Dasd"

    assertTrue(chebi.chebiId == "Dasd")
  }
}

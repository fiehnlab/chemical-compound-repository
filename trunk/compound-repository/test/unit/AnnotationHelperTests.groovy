import compound.repository.LookupService
import edu.ucdavis.genomics.metabolomics.binbase.annotations.AnnotationHelper
import edu.ucdavis.genomics.metabolomics.binbase.annotations.QueryObject

/**
 * User: wohlgemuth
 * Date: Feb 10, 2010
 * Time: 7:55:51 PM
 *
 */
class AnnotationHelperTests extends GroovyTestCase {

  public void testQueryableAnnotation() {
    Collection<QueryObject> set = AnnotationHelper.getQueryableAnnotations(LookupService.class)

    assertTrue(set.size() > 0)

    for (QueryObject o: set) {
      assertTrue(o.getAnnotation() != null)
      assertTrue(o.getMethod() != null)
    }
  }
}

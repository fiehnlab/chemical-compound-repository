package com.fiehn.filter;

import java.util.Set;

/**
 * used to filter data
 * 
 * @author pradeep
 * 
 */
public interface Filter {

	/**
	 * contains our configuration
	 */
	void configure(Object configuration,boolean caseSensitive);

	/**
	 * filters the data in this set and returns the filtered content
	 * 
	 * @param content
	 * @return
	 */
	Set<String> filter(Set<String> content);

	/**
	 * are we going to be case sensitive
	 * 
	 * @param value
	 */
	public boolean isCaseSensitive();
}

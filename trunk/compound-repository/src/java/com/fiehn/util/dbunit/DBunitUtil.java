package com.fiehn.util.dbunit;

import org.apache.log4j.Logger;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;

import javax.sql.DataSource;

/**
 * Created by IntelliJ IDEA.
 * User: pradeep
 * Date: Mar 3, 2010
 * Time: 4:22:48 PM
 */
public class DBunitUtil {


    /**
     * initialize a dbunit dataset for testing, works only with HSQLDB!
     * @param source
     * @param data
     * @return
     * @throws Exception
     */
    public static synchronized void intialize(DataSource source, IDataSet data) throws Exception{
        Logger logger = Logger.getLogger(DBunitUtil.class);
        logger.info("create connection...");
        DatabaseConnection dbunit = new DatabaseConnection(source.getConnection());

        logger.info("create configuration");
        DatabaseConfig config = dbunit.getConfig();

        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,new HSQLDataTypeFactory());

        logger.info("execute clean insert");
        DatabaseOperation.CLEAN_INSERT.execute(dbunit,data);
        logger.info("done and clean connection");
        dbunit.close();
        config = null;

    }
}

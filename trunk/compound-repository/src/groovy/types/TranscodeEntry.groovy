package types

import compound.repository.entries.Compound

/**
 * User: wohlgemuth
 * Date: Aug 23, 2010
 * Time: 3:48:16 PM
 * 
 */
class TranscodeEntry {

  /**
   * identifier for this entry
   */

  def identifier

  /**
   * associated compounds
   */
  Collection<Compound> compounds = new ArrayList()

  /**
   * generated result
   */
  Map<String,Collection> result = new HashMap()
  
}

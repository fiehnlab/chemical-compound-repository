package types

import compound.repository.entries.DBLinks

/**
 * User: wohlgemuth
 * Date: Aug 18, 2010
 * Time: 2:14:35 PM
 *
 */
class DBLinkCollection {

  /**
   * name of this collection
   */

  String name

  /**
   * contains our links
   */
  ArrayList<DBLinks> links = new ArrayList<DBLinks>()


  def getSize() {
    return links.size()
  }

  /**
   * string representation
   * @return
   */
  String toString() {
    return "${name}(${getSize()})"
  }

  DBLinks getTopElement(){
    return links.get(0)
  }

}

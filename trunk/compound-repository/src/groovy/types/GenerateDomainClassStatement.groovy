package types

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Jul 15, 2010
 * Time: 2:20:09 PM
 * To change this template use File | Settings | File Templates.
 */

class GenerateDomainClassStatement {

  public static void main(String[] args) {
    File file = new File("classes.txt")
    Scanner scanner = new Scanner(file)

    while (scanner.hasNextLine()) {
      String name = cleanClassName(scanner.nextLine())
      println "grails create-domain-class compound.repository.entries.${name}"
    }
  }


  static String cleanClassName(String name){
    return name.replaceAll("-","").replaceAll("\\s","").replaceAll("\\(","").replaceAll("\\)","").replaceAll(",","").replaceAll("\\.","")
  }
}

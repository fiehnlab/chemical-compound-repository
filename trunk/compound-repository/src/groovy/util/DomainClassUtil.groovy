package util

import org.codehaus.groovy.grails.commons.ApplicationHolder
import compound.repository.entries.DBLinks
import org.apache.log4j.Logger
import types.GenerateDomainClassStatement

/**
 * utility to find domain classes
 * To change this template use File | Settings | File Templates.
 */
class DomainClassUtil {
    private static DomainClassUtil instance = null
    Logger log = Logger.getLogger(getClass())

    /**
     * valid keys
     */
    def validKeys = new HashSet()

    /**
     * associated classes
     */
    def validClasses = [:]

    private DomainClassUtil() {}

    static DomainClassUtil getInstance() {
        if (instance == null) {
            instance = new DomainClassUtil()
            instance.build()
        }
        return instance
    }

    private void build() {
        ApplicationHolder.application.domainClasses.each {

            if (DBLinks.isAssignableFrom(it.clazz)) {
                def value = GenerateDomainClassStatement.cleanClassName(it.clazz.simpleName)
                validKeys.add(value)
                validClasses.put(value, it.clazz)
                log.info "added: ${value}"
            }
        }
    }
}

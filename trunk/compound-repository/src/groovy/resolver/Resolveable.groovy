package resolver

import resolver.analyzer.Analyzer
import resolver.interceptor.ResolveableInterceptor
import types.Hit

/**
 * User: wohlgemuth
 * Date: Feb 1, 2010
 * Time: 10:48:51 PM
 */
public interface Resolveable {

  /**
   * resolves a couple of hits in this input stream
   */
  public Set<Hit> resolve(String input)

  /**
   * sets the minimum required confidence level 
   */
  public void setConfidenceLevel(double value)

  /**
   * activates something in a resolver
   */
  public void activateResolver()

  /**
   * disactivates something in a resolver
   */
  public void disactivateResolver()

  /**
   *   adds a new interceptor
   * */
  public Resolveable addInterceptor(ResolveableInterceptor interceptor)

  /**
   * remove an interceptor
   */
  public Resolveable removeInterveptor(ResolveableInterceptor interceptor)

  /**
   * returns all interceptors
   */
  public Collection<ResolveableInterceptor> getInterceptors()

  /**
   * returns the used analyzer for this text
   * @return
   */
  public Analyzer getAnalyzer()

  /**
   * sets the analyzer to use
   * @param analyzer
   */
  public void setAnalyzer(Analyzer analyzer)
}
package resolver.query

import compound.repository.entries.HMDB
import pattern.PatternHelper

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Jun 9, 2010
 * Time: 6:38:07 PM
 * To change this template use File | Settings | File Templates.
 */
class HMDBQuery extends AbstractQuery {
  protected String getClassOfInterrest() {
    return HMDB.class.getName();
  }

  protected String getQuerySymbol() {
    return "hmdbId";
  }

  boolean match(String value) {
    return value.matches(PatternHelper.HMDB_PATTERN)
  }
}
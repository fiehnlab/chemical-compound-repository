package resolver.query

import pattern.PatternHelper
import compound.repository.entries.KEGG

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Jun 9, 2010
 * Time: 5:49:22 PM
 * To change this template use File | Settings | File Templates.
 */
class KeggQuery extends AbstractQuery {
  protected String getClassOfInterrest() {
    return KEGG.class.getName();
  }

  protected String getQuerySymbol() {
    return "keggId";
  }

  boolean match(String value) {
    return value.matches(PatternHelper.KEGG_PATTERN)
  }
}

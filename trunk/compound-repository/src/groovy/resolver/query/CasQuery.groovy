package resolver.query

import pattern.PatternHelper
import compound.repository.entries.Cas

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Jun 9, 2010
 * Time: 5:49:39 PM
 * To change this template use File | Settings | File Templates.
 */
class CasQuery extends AbstractQuery {
  protected String getClassOfInterrest() {
    return Cas.class.getName();
  }

  protected String getQuerySymbol() {
    return "casNumber";
  }

  boolean match(String value) {
    return value.matches(PatternHelper.CAS_PATTERN)
  }
}

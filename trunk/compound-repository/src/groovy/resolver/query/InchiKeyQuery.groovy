package resolver.query

import pattern.PatternHelper

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: May 4, 2010
 * Time: 12:45:31 PM
 */
class InchiKeyQuery extends AbstractQuery {

  protected String getClassOfInterrest() {
    return "compound.repository.entries.InchiHashKey";  //To change body of implemented methods use File | Settings | File Templates.
  }

  protected String getQuerySymbol() {
    return "completeKey";
  }

  boolean match(String value) {
    return value.matches(PatternHelper.STD_INCHI_KEY_PATTERN)
  }
}

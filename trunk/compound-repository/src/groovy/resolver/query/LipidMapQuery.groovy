package resolver.query

import compound.repository.entries.LipidMAPS
import pattern.PatternHelper

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: Jun 9, 2010
 * Time: 5:49:51 PM
 * To change this template use File | Settings | File Templates.
 */
class LipidMapQuery  extends AbstractQuery {
  protected String getClassOfInterrest() {
    return LipidMAPS.class.getName();
  }

  protected String getQuerySymbol() {
    return "lipidMapId";
  }

  boolean match(String value) {
    return value.matches(PatternHelper.LIPID_MAPS_PATTERN)
  }
}

includeTargets << grailsScript("Init")

includeTargets << new File("scripts/RunScript.groovy")

target(main: "Imports all the pubchem files into the datbase") {

  //define which script we want tp exececute
  args = "builder/ImportPubchemSubstance.groovy ${args}"

  //execute the script
  depends(runScript)
}

setDefaultTarget(main)

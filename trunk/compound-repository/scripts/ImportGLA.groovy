includeTargets << grailsScript("Init")
includeTargets << new File("scripts/RunScript.groovy")

target(main: "Imports all the pubchem files into the datbase") {

  //define which script we want tp exececute
  //args = "builder/importPubchem.groovy"
  args = "builder/importGLAbbrev.groovy"

  //execute the script
  depends(runScript)
}

setDefaultTarget(main)

package compound.repository

import types.Hit
import compound.repository.entries.Compound

class LookupController {

  def lookupService
  
  /**
   * displays a lookup result
   */
  def show = {
    def model = chainModel["compound"]

    return [model: model]
  }

  /**
   * looksup by a list of hits
   */
  def lookup = {
    assert params.hits != null, "please provide the hits in the params object as instance of a collection"

    def hits = params.hits

    assert hits instanceof Collection, "hit's need to be instance of a collection!"

    Set<Compound> result = new HashSet<Compound>()
    for (Hit hit: hits) {
      for (Compound c: lookupService.lookupByHit(hit,params)) {
        result.add(c)
      }
    }
    return [lookup: result]
  }

  /**
   * looks up the results by a single given hit
   */
  def lookupByHit = {
    assert params.hit != null, "please provide a 'hit' in the params object!"

    Hit hit = null
    if (flash[params.hit] != null) {
      log.debug("using hit from flash scope!")
      hit = flash[params.hit]
    }
    else {
      Object temp = params.hit
      assert params.hit instanceof Hit, "please make sure that the object hit is of the right instance, it is of instance: ${temp.class.name}"
      hit = params.hit

    }

    //calculate the result
    Collection<Compound> result = lookupService.lookupByHit(hit,params)

    //return the model to the view
    return [lookup: result]
  }

  /**
   * can return 0 - n
   */
  def lookupBySmile = {
    assert params.smile != null, "please provide a 'smile' in the params object!"
    assert params.smile instanceof String, "please make sure that the object smile is of the right instance"

    String smile = params.smile
    return [lookup: lookupService.lookupBySmile(smile,params)]
  }

  /**
   * can return 0 - n
   */
  def lookupByKegg = {
    assert params.kegg != null, "please provide a 'kegg' in the params object!"
    assert params.kegg instanceof String, "please make sure that the object kegg is of the right instance"

    String kegg = params.kegg
    return [lookup: lookupService.lookupByKegg(kegg,params)]
  }

  /**
   * can return 0 - n
   */
  def lookupByCID = {
    assert params.cid != null, "please provide a 'cid' in the params object!"
    assert params.cid instanceof String, "please make sure that the object cid is of the right instance"

    String cid = params.cid
    return [lookup: lookupService.lookupByCID(Integer.parseInt(cid),params)]
  }

  /**
   * can return 0-n
   */
  def lookupBySID = {
    assert params.sid != null, "please provide a 'sid' in the params object!"
    assert params.sid instanceof String, "please make sure that the object sid is of the right instance"

    String sid = params.sid
    return [lookup: lookupService.lookupBySID(Integer.parseInt(sid))]
  }

  /**
   * can return 0-n
   */
  def lookupByCas = {
    assert params.cas != null, "please provide a 'cas' in the params object!"
    assert params.cas instanceof String, "please make sure that the object cas is of the right instance"

    String cas = params.cas
    return [lookup: lookupService.lookupByCas(cas,params)]
  }

  /**
   * can return 0 - 1
   */
  def lookupByCompoundId = {
    assert params.compoundId != null, "please provide a 'compoundId' in the params object!"
    assert params.compoundId instanceof String, "please make sure that the object compoundId is of the right instance"

    String compoundId = params.compoundId
    return [lookup: lookupService.lookupByCompoundId(Long.parseLong(compoundId),params)]
  }

  /**
   * can return 0 - 1
   */
  def lookupByInchi = {

    assert params.inchi != null, "please provide a 'inchi' in the params object!"
    assert params.inchi instanceof String, "please make sure that the object inchi is of the right instance"

    String inchi = params.inchi
    return [lookup: lookupService.lookupByInchi(inchi,params)]
  }

  /**
   * can return 0 - 1
   */
  def lookupByInchiKey = {
    assert params.inchiKey != null, "please provide a 'inchiKey' in the params object!"
    assert params.inchiKey instanceof String, "please make sure that the object inchiKey is of the right instance"

    String inchiKey = params.inchiKey
    return [lookup: lookupService.lookupByInchiKey(inchiKey,params)]
  }

  /**
   * can return 0 - n
   */
  def lookupByPartialInchiKey = {
    assert params.inchiKey != null, "please provide a 'inchiKey' in the params object!"
    assert params.inchiKey instanceof String, "please make sure that the object inchiKey is of the right instance"

    String inchiKey = params.inchiKey
    return [lookup: lookupService.lookupByPartialInchiKey(inchiKey,params)]
  }


}

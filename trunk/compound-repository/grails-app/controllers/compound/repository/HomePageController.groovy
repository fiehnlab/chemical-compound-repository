package compound.repository

import compound.repository.entries.Compound

class HomePageController {

  def index = {

    // Remove session attribute
    if (session.getAttribute("ids") != null) {
      session.removeAttribute "ids"
    }
    if (session.getAttribute("query") != null) {
      session.removeAttribute "query"
    }

    def strQuery = "";
    if (params.query != null) {
      strQuery = params.query
    }

    return [query: strQuery]
  }

  def show_help_remote = {
    render """<div class="message">"Enter Query Like<br>chebi:*&nbsp;,glucose&nbsp;,benzene&nbsp;,C00001&nbsp;,XLYOFNOQVPJJNP-UHFFFAOYSA-N etc.</div>"""
  }

  def show_help_mol_remote = {
    render """<div class="message">"Please paste a standard mol file into this box and we will search the database against it</div>"""
  }
}

package compound.repository

import compound.repository.entries.Compound

class ExportMolController {

  RenderService renderService

  def index = { }

  /**
   * exports a mol file for a compound
   */
  def exportMol = {
    assert params.id,"please provide us with a valid compound id!"

    log.info "requested compound for id: ${params.id}"
    Compound compound = Compound.get(params.id)

    assert compound != null, "sorry no compound found with id: ${params.id}"

    def mol =  renderService.renderToMol(compound).trim()

    return [mol:mol]
  }

}

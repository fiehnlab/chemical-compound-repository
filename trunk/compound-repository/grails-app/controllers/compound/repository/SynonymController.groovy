package compound.repository

import compound.repository.entries.Synonym
import compound.repository.entries.Compound

class SynonymController {

  /**
   * promotes an entry
   */
  def promote = {
    assert params.id != null, "you need to provide an id in the params"

    Synonym s = Synonym.get(params.id)
    s.rating = s.rating + 1

    s.save(flush:true )

    Compound compound = s.compound


    render(template:'/compound/render_synonyms', model:[compoundInstance:compound,promoted:s])
  }


   /**
    * demotes an entry
    */
  def demote = {
    assert params.id != null, "you need to provide an id in the params"

    Synonym s = Synonym.get(params.id)
    s.rating = s.rating - 1

    s.save(flush:true )

    Compound compound = s.compound

    render(template:'/compound/render_synonyms', model:[compoundInstance:compound,demoted:s])
  }


   /**
    * flags an entry as wrong 
    */
  def flag = {
    assert params.id != null, "you need to provide an id in the params"

    Synonym s = Synonym.get(params.id)
    s.flagged = true

    s.save(flush:true )

    Compound compound = s.compound

    render(template:'/compound/render_synonyms', model:[compoundInstance:compound,flagged:s])

  }
}

package compound.repository

import compound.repository.entries.Compound
import types.TranscodeEntry
import util.ConvertHelper

class TranscodeController {

  /**
   * takes care of the export
   */
  DataExportService dataExportService = new DataExportService();

  def index = { }

  /**
   * transcodes the result view
   */
  def transcode = {
    assert params.sessionId != null, "sorry we need a parameter with the name 'sessionId' or this wont work"
    assert params.from != null, "sorry we need a parameter with the name 'from' or this wont work"
    assert params.to != null, "sorry we need a parameter with the name 'to' or this wont work"

    def data = session[params.sessionId]

    assert data != null, "the session id was not found, are you sure you specified the right one?"
    assert data instanceof Collection, "the session id was of the wrong type, are you sure you specified the right one?"

    //contains the transcoded result
    Map<String, TranscodeEntry> result = new HashMap<String, TranscodeEntry>()

    data.each {Map<Compound, ?> content ->
      String from = content["fromValue"]

      if (result[from] == null) {
        result[from] = new TranscodeEntry()
        result[from].identifier = from
      }

      TranscodeEntry entry = result[from]

      entry.compounds.add(content.compound)

      //convert to a collection
      if (params.to instanceof Collection == false) {
        println ConvertHelper.getInstance().getToConversions()
        
        if (ConvertHelper.getInstance().getToIdentifiers().contains(params.to.toString().toLowerCase())) {
          def list = [params.to]
          params.to = list
        }
        else {
          params.to = params.to.toList()
        }
      }

      params.to.each {String value ->

        if (entry.result[value] == null) {
          entry.result[value] = new HashSet()
        }

        if (content.result[value] != null) {
          if (content.result[value] instanceof Collection) {
            if (!content.result[value].isEmpty()) {

              content.result[value].each {def cval ->
                entry.result[value].add(ConvertHelper.getInstance().toString(cval))
              }
            }
          }
          else {
            def cval = content.result[value]
            entry.result[value].add(ConvertHelper.getInstance().toString(cval))
          }
        }
      }
    }

    session["transcode_code"] = result

    return [transcode: result, from: params.from, to: params.to, sessionId: "transcode_code"]
  }

  def show_help_transcode = {
    render """<div class="message">The transcode function combines your data based on the from field. This means that all the properties with an identical from field are shown as one entity.</div>"""
  }

  /**
   * transcode export function
   */
  def transcodeExport = {
    assert params.sessionId != null, "sorry we need a parameter with the name 'sessionId' or this wont work"
    assert params.from != null, "sorry we need a parameter with the name 'from' or this wont work"
    assert params.to != null, "sorry we need a parameter with the name 'to' or this wont work"

    def data = session[params.sessionId]

    assert data != null, "no data found in the session with the specified session id: ${params.sessionId}"

    dataExportService.exportTranscode(data, response, params)
  }
}
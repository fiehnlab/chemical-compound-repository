package compound.repository

import compound.repository.entries.Compound

/**
 * simpl compoun controllr
 */
class CompoundController {

    def show = {

        def compoundInstance = Compound.get( params.id )
        if(!compoundInstance) {
            flash.message = "Compound not found with id ${params.id}"
            return [  :  ]
        }
        else { return [ compoundInstance : compoundInstance ] }
    }
}

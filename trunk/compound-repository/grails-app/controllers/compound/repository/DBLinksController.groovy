package compound.repository

import compound.repository.entries.DBLinks
import grails.converters.JSON

class DBLinksController {

  /**
   * returns the url properties in the json format
   */

  def getHasURLasJSON = {
    assert params.id, "sorry we need an id object in the params"
    DBLinks link = DBLinks.get(Long.parseLong(params.id.toString()))

    Map res = new HashMap()

    res["url"] = link.calculateURL()
    res["has_url"] = link.hasCalculateableURL()

    res["linkID"] = link.getLinkID()
    res["linkSource"] = link.getSourceName()

    render res as JSON
  }

  /**
   * returns all ids for the given class and compound id
   * to reduce the server load
   */
  def getHASURLasJSONForClassAndCompound = {
    assert params.id, "sorry we need an id object in the params"

    DBLinks l = DBLinks.get(Long.parseLong(params.id.toString()))

    def className = l.class.getName()
    def id = l.compound.id

    def result = DBLinks.executeQuery("Select a from DBLinks as a where a.class = ? and a.compound.id = ?", [className, id])

    Collection results = new HashSet()

    result.each { DBLinks link ->

      Map res = new HashMap()

      res["url"] = link.calculateURL()
      res["has_url"] = link.hasCalculateableURL()
      res["linkID"] = link.getLinkID()
      res["linkSource"] = link.getSourceName()
      res["id"] = link.id

      results.add res
    }

    render results as JSON

  }

}

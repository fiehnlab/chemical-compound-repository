package compound.repository

import grails.converters.JSON
import grails.converters.XML
import util.ConvertHelper
import util.ConvertInchi

/**
 * provides a webservice to transform compounds from any given name
 */
class TransformWebService {

    static expose = ['cxf']

    boolean transactional = true

    ConvertService convertService = new ConvertService()

    String transformToXmlString(String from, String to, String value) {
        from = from?.toLowerCase()
        to = to?.toLowerCase()

        assert (ConvertHelper.getInstance().getFromIdentifiers().contains(from))
        assert (ConvertHelper.getInstance().getToIdentifiers().contains(to))
        assert (value != null)
        return convertService.convert(from, to, value) as XML

    }

    String transformToDeepXmlString(String from, String to, String value) {
        from = from?.toLowerCase()
        to = to?.toLowerCase()

        assert (ConvertHelper.getInstance().getFromIdentifiers().contains(from))
        assert (ConvertHelper.getInstance().getToIdentifiers().contains(to))
        assert (value != null)

        return convertService.convert(from, to, value) as grails.converters.deep.XML

    }

    String transformToJSONString(String from, String to, String value) {
        from = from?.toLowerCase()
        to = to?.toLowerCase()

        assert (ConvertHelper.getInstance().getFromIdentifiers().contains(from))
        assert (ConvertHelper.getInstance().getToIdentifiers().contains(to))
        assert (value != null)

        return convertService.convert(from, to, value) as JSON

    }

    String transformToDeepJSONString(String from, String to, String value) {
        from = from?.toLowerCase()
        to = to?.toLowerCase()

        assert (ConvertHelper.getInstance().getFromIdentifiers().contains(from))
        assert (ConvertHelper.getInstance().getToIdentifiers().contains(to))
        assert (value != null)

        return convertService.convert(from, to, value) as grails.converters.deep.JSON

    }

    String getFromIdentifiersAsXML() {
        return ConvertHelper.getInstance().getFromIdentifiers() as XML

    }

    /**
     * converts from to method and returns 1:n values
     * @param from
     * @param to
     * @param value
     * @return
     */
    String[] convertFromTo(String from, String to, String value) {
        def data = convertService.convert(from, to, value)
        String[] result = new String[data.size()]

        data.eachWithIndex { Map o, int i ->
            result[i] = o.result.toString()
        }

        return result
    }

    /**
     * list all to identifiers
     * @return
     */
    String[] listToIdentifiers() {
        def value = ConvertHelper.getInstance().getToIdentifiers()

        String[] result = new String[value.size()]

        value.eachWithIndex {String v, int i ->
            result[i] = v
        }

        return result
    }

    /**
     * list all from identifiers
     * @return
     */
    String[] listFromIdentifiers() {
        def value = ConvertHelper.getInstance().getFromIdentifiers()

        String[] result = new String[value.size()]

        value.eachWithIndex {String v, int i ->
            result[i] = v
        }

        return result

    }


    String[] getToIdentifiersAsXML() {
        return ConvertHelper.getInstance().getToIdentifiers() as XML
    }


    String getFromIdentifiersAsJSON() {
        return ConvertHelper.getInstance().getFromIdentifiers() as JSON
    }

    String getToIdentifiersAsJSON() {
        return ConvertHelper.getInstance().getToIdentifiers() as JSON

    }

    /**
     * transforms a molfile to an inchi key
     * @param molFile
     * @return
     */
    String transformMolToInchi(String molFile) {
        log.debug "received: \n ${molFile}"
        if (molFile == null | molFile.length() == 0) {
            log.warn "file should never be null/empty! something is wrong with it"
            return ""
        }

        return ConvertInchi.convertMolToInchi(molFile)
    }

    /**
     * trnasforms an inchi code to an inchi key
     * @param inchiCode
     * @return
     */
    String transformInchiCodeToKey(String inchiCode) {
        return ConvertInchi.convertInchiToKey(inchiCode)
    }
}

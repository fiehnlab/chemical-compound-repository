package compound.repository

import compound.repository.entries.*
import util.ConvertHelper
import edu.ucdavis.genomics.metabolomics.binbase.annotations.Queryable

/**
 * service to convert from an entry to an entry
 */

class ConvertService {

  boolean transactional = true

  void afterPropertiesSet() {}

  LookupService lookupService

  ConvertHelper helper = ConvertHelper.getInstance()

  /**
   * constructor which initializes the mapping of compounds
   */
  public ConvertService() {

  }

  /**
   * does the actual converting
   */
  Long getCount(String from, def to, def value, Map params = [:]) {

    //convert the names to lowercase
    from = from.toLowerCase()
    //to = to.toLowerCase()  commented by pradeep

    //get the lookup name
    String fromMethod = helper.getFromMethodName(from)
    //String toMethod = methodToNames[to]     commented by pradeep

    //make sure we actually know the mapping
    Long count = 0

    if (fromMethod == null) {
      log.debug "fromMethod is null, so count will be 'null'"
    } else {
      fromMethod = "count" + fromMethod;
      count = lookupService."$fromMethod"(value, params, false)

    }

    //fetches the compounds

    log.debug("Count is : ${count}")
    return count

  }

  /**
   * does the actual converting
   */
  Collection<Map<Compound, ?>> convert(String from, def to, def value, Map params = [:]) {

    //convert the names to lowercase
    from = from.toLowerCase()
    //to = to.toLowerCase()  commented by pradeep

    //get the lookup name
    String fromMethod = helper.getFromMethodName(from)

    //make sure we actually know the mapping
    if (fromMethod == null) {
      return ["error, unknown from mapping, ${from}"]
    }

    //fetches the compounds
    log.debug("looking for: ${value}")

    //contains our results
    Collection<Map<Compound, ?>> result = new ArrayList<Map<Compound, ?>>()

    if (value instanceof Collection) {
      value.each {def v ->

        log.info "lookup: ${v}"
        def res = lookupService."$fromMethod"(v, params, false)

        log.info "result is: ${res.size()}"
        if (res.size() == 0) {
          filterResult(null, result, to,v)
        }
        else {
          if (res instanceof Collection) {
            res.each {Compound compound ->
              filterResult(compound, result, to,v)
            }
          }
          else {
            filterResult(res, result, to,v)
          }
        }
      }
    }
    else {
      def res = lookupService."$fromMethod"(value, params, false)
      if (res instanceof Compound) {
        filterResult(res, result, to,value)
      }
      else {
        if (res instanceof Collection) {
          res.each {Compound compound ->
            filterResult(compound, result, to,value)
          }
        }
        else {
          filterResult(res, result, to,value)
        }
      }
    }

    //return our result
    return result
  }

  /**
   * filter the result and omit empty collections
   */
  private def filterResult(Compound compound, Collection<Map<Compound, ?>> result, def toMethod,def fromValue) {
    //log.debug "toMethod ::: ${toMethod.getClass()}"
    if (toMethod instanceof Collection) {
      HashMap temp1 = new HashMap()
      for (String s: toMethod) {
        String toMethod1 = helper.getToMethodName(s)
        if (toMethod1 == null) {
          return ["error, unknown to mapping, ${s}"]
        }

        def temp = null

        if (compound != null) {
          temp = this."${toMethod1}"(compound)
        }
        else {
          temp = "None"
        }

        temp1.put(s.trim(), temp);
      }

      if (temp1 instanceof Collection) {
        if (!temp1.isEmpty()) {
          result.add([compound: compound, result: temp1,fromValue:fromValue])
        }
      }
      else {
        result.add([compound: compound, result: temp1,fromValue:fromValue])
      }
    } else {
      if (toMethod instanceof String) {
        String toMethod1 = helper.getToMethodName(toMethod)
        if (toMethod1 == null) {
          return ["error, unknown to mapping, ${toMethod}"]
        }
        def temp = null

        if (compound != null) {
          temp = this."${toMethod1}"(compound)
        }
        else {
          temp = "None"
        }

        if (temp instanceof Collection) {
          if (!temp.isEmpty()) {
            result.add([compound: compound, result: temp,fromValue:fromValue])
          }
        }
        else {
          result.add([compound: compound, result: temp,fromValue:fromValue])
        }
      }
    }
  }

  /**
   * returns the compound or list of compounds
   */
//  @Queryable(name = "compound")
  def convertToCOMPOUND(Compound compound) {
    return compound
  }

  /**
   * converts to inchi key
   */
  @Queryable(name = "inchi")
  def convertToINCHI(Compound compound) {
    return compound.inchi
  }

  /**
   * converts to inchi hash key
   */
  @Queryable(name = "inchikey")
  def convertToINCHIKEY(Compound compound) {
    return compound.inchiHashKey
  }

  /**
   * converts to db links hash key
   */
//  @Queryable(name = "links")
  def convertToLINKS(Compound compound) {
    return compound.dbLinks
  }

  /**
   * converts to smile
   */
  @Queryable(name = "smiles")
  def convertToSMILES(Compound compound) {
    return compound.smiles
  }

  /**
   * converts to iupac name
   */
  @Queryable(name = "iupac")
  def convertToIUPAC(Compound compound) {
    return compound.iupac
  }

  /**
   * converts to synonyms
   */
  @Queryable(name = "names")
  def convertToNAMES(Compound compound) {
    return compound.synonyms
  }

  /**
   * converts to molare mass
   */
  @Queryable(name = "mass")
  def convertToMASS(Compound compound) {
    return compound.exactMolareMass
  }

  /**
   * converts to molare mass
   */
  @Queryable(name = "formula")
  def convertToFORMULA(Compound compound) {
    if (compound.formula != null)
      return compound.formula
    else {
      return "unknown"
    }
  }

  /**
   * converts to cas
   */
  @Queryable(name = "cas")
  def convertToCas(Compound compound) {
    return Cas.findAllByCompound(compound)
  }

  /**
   * converts to kegg
   */
  @Queryable(name = "kegg")
  def convertToKegg(Compound compound) {
    return KEGG.findAllByCompound(compound)
  }

  /**
   * converts to lipidmap
   */
  @Queryable(name = "lipidmap")
  def convertToLipidMap(Compound compound) {
    return LipidMAPS.findAllByCompound(compound)
  }

  /**
   * converts to cid
   */
  @Queryable(name = "cid")
  def convertToCID(Compound compound) {
    return PubchemCompound.findAllByCompound(compound)
  }

  /**
   * converts to sid
   */
  @Queryable(name = "sid")
  def convertToSID(Compound compound) {
    return PubchemSubstance.findAllByCompound(compound)
  }

  /**
   * converts to hmdb
   */
  @Queryable(name = "hmdb")
  def convertToHMDB(Compound compound) {
    return HMDB.findAllByCompound(compound)
  }

  /**
   * converts to chebi
   */
  @Queryable(name = "chebi")
  def convertToChebi(Compound compound) {
    return ChEBI.findAllByCompound(compound)
  }

}

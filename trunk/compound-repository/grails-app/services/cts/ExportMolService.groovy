package cts

import compound.repository.RenderService
import compound.repository.entries.Compound

class ExportMolService {

    static expose = ['cxf']


    RenderService renderService

    boolean transactional = true


    String generateMolFileFromCompound(long compoundId) {
        Compound compound = Compound.get(compoundId)
        return renderService.renderToMol(compound)

    }

    String generateMolFileFromInChI(String inchiKey) {

        Compound compound = Compound.findByInchi(inchiKey)
        return renderService.renderToMol(compound)
    }


}

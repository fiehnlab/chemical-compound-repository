package compound.repository

/**
 * used to update the index in certain intervals
 * to keep the queries in sync
 */
class IndexerJob {

  static triggers = {

    //fire every hour at the 20th minute
    cron name: 'indexer', cronExpression: "0 20 * * * ? *"
  }

  def group = "index"

  def searchableService

  /**
   * does the actual execution
   * @return
   */
  def execute() {
    log.info "indexing the searchable service..."
    //searchableService.index()
    log.info "done with indexing..."

  }
}

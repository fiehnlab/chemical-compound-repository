<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Aug 23, 2010
  Time: 4:05:39 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="types.TranscodeEntry" contentType="text/html;charset=UTF-8" %>
<html>
<head><title><g:message code="TRANSCODE.RESULTS"/></title>
  <meta name="layout" content="main"/>

  <export:resource/>
</head>

<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>

  <div class="box">

    <div id="pagePath">
      <h3><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <g:message code="MASS_LOOKUP"/> <span>&raquo;</span><a class="result" href="javascript://nop" onclick="history.back(1)"><g:message code="MASS_LOOKUP.RESULTS"/></a></h3>
    </div>
    <div class="line"></div>

    <div class="box">
      <div class="left-field"><h4>Results</h4></div>
      <div class="line"></div>



      <table id="result">
        <thead>

        <tr>
          <th>From</th>

          <g:each var="header" in="${to}">
            <th><g:message code="${header}"/></th>
          </g:each>
        </tr>
        </thead>
        <tbody>

        <%
          def index = 0

          transcode.each { String key, TranscodeEntry value ->

            String lStrClass = "";
            String lColorFormulaBG = "";
            if (index % 2 == 0) {
              lStrClass = "even"
              lColorFormulaBG = "#ffffff"
            }
            else {
              lStrClass = "odd"
              lColorFormulaBG = "#f7f7f7"
            }

            index++
        %>

        <tr class='<%=lStrClass%>' onMouseOver="this.bgColor = 'yellow';">
          <!-- from -->
          <td class="inchi-key-no-break">
            ${key}
          </td>

        <!-- to -->

          <g:each status="i" var="toValue" in="${to}">
            <td class="inchi-key-no-break">
              <ul>
                <g:each in="${value.result[toValue]}" var="val" status="x">

                  <li>${val}</li>
                </g:each>
              </ul>

            </td>
          </g:each>
        </tr>

        <%

          }
        %>
        </tbody>
      </table>


      <%
        if (transcode != null && transcode.size() > 0) {
      %>

      <div class='pagination'>
        <table><tr><td class='paginationLabel'>Total Records : ${transcode.size()}
        </td></tr></table>
      </div>

      <export:formats controller="transcode" action="transcodeExport" formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']" params="[from:params.from,to:params.to,sessionId:sessionId]"/>


      <% } %>
    </div>
  </div>
</div>
</body>
</html>
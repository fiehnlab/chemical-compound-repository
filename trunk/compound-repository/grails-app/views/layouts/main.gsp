<html>
<head>
  <title><g:layoutTitle default="CTS: Chemical Translation Service"/></title>

  <link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}"/>

  <style type="text/css">@import url("${resource(dir: 'css',file : 'pagination.css')}");</style>
  <style type="text/css">@import url("${resource(dir: 'css',file : 'font.css')}");</style>


  <!--
  <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon"/>
  -->

  <g:layoutHead/>
  <g:javascript library="application"/>
  <g:javascript library="prototype"/>

  <meta name="layout" content="main"/>

</head>
<body class="BodyBackground">
<div id="spinner" class="spinner" style="display:none;">
  <img src="${resource(dir: 'images', file: 'spinner.gif')}" alt="Spinner"/>
</div>

<!-- testing for the browser and print an error message in case they use the internet explorer -->
<g:javascript>
  if (navigator.appName == 'Microsoft Internet Explorer') {
    document.write('<div class=\"box\"><div class=\"errors-center\" >we recommend that you use Firefox, Safari or Google Chrome for the best expirience! Right now you are using the Microsoft Internet Eplorer and we do not actively support or recommend it!</div></div>');
  }
</g:javascript>

<g:layoutBody/>

<div id="footer">
  <g:render template="/footer"/>
</div>

</body>
</html>
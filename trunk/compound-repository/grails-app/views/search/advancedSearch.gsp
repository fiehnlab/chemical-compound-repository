<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 11, 2010
  Time: 11:17:31 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html; charset=ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    <meta name="layout" content="main"/>

<title>Chemical Compound Repository: Advance Search</title>
</head>




<script type="text/javascript">

function addRowToTable()
{
  var tbl = document.getElementById('tblSample');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;

  if( parseInt(iteration) > 15)
	  return;

  var row = tbl.insertRow(lastRow);

  // left cell
  var textNode = document.createTextNode(iteration);

  // right cell
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);
  var cell5 = row.insertCell(4);
  var cell6 = row.insertCell(5);

  cell1.innerHTML = "&nbsp;";
  cell2.appendChild(textNode);
  cell3.innerHTML = "<select id='cmbOption'"+iteration+" > <option> AND </option> <option> OR </option>  </select>";
  cell4.innerHTML = "<select id='cmbCriteria'"+iteration+" style='width:200px'> <option> --Select-- </option> <option> c1 </option> <option> c2 </option>  </select>";
  cell5.innerHTML = "<select id='cmbSubCriteria'"+iteration+" > <option> --Select-- </option> <option> Equals </option> <option> Not Equals </option> <option> Less Than </option> <option> Greater Than </option> </select>";
  cell6.innerHTML = "<input id='txtSearchData'"+iteration+" value='' style='width:150px;'/>";
}

function removeRowFromTable()
{
  var tbl = document.getElementById('tblSample');
  var lastRow = tbl.rows.length;
  if (lastRow > 2) tbl.deleteRow(lastRow-1);
}
</script>

<body class="BodyBackground" onload="addRowToTable();addRowToTable();addRowToTable();addRowToTable();">
<form action="post" id="AdSearch" name="AdSearch" >

<table id='tabTitle' border='0' width="60%" align='center' cellpadding="0" cellspacing="0">
	<tr>
		<td class="AppsLabel" align="center"> Advanced Search </td>
	</tr>

    <tr>
		<td colspan="5" align='right'>
                  [ <g:link controller="search" action="index">Back to search</g:link> ]
				  [ <g:link controller="homePage" action="index">Back to home</g:link> ]
	    </td>
	</tr>

  <tr height='5'> <td> &nbsp; </td> </tr>

    <tr align='right' valign='bottom' height="20px" valign='middle'>
        <td width="10%" colspan="5" align="right" valign='middle'>
	        <input type="button" class="EditableField" value="Add Criteria" onclick="addRowToTable();" />
			<input type="button" class="EditableField" value="Remove Criteria" onclick="removeRowFromTable();" />
			<input type="button" class="EditableField" value="Submit" onclick="alert('Under Construction');" />
		</td>
	</tr>
  <tr height='2'> <td> &nbsp; </td> </tr>
<tr align='left' valign='bottom'>
		<td align="center" >

<table border="0" id="tblSample" width="100%" align='center' cellpadding="2" cellspacing="0"class="TableBG" >

    <tr align='left' valign='bottom' class="TableHeaderBG">
        <td class="TableHeader" width="1%">&nbsp; </td>
        <td class="TableHeader" width="10%"> S.No. </td>
		<td class="TableHeader" width="10%"> Options </td>
		<td class="TableHeader" width="30%"> Criteria </td>
		<td class="TableHeader" width="20%"> Sub Criteria</td>
		<td class="TableHeader" width="30%"> Search Content </td>
	</tr>
  <tr>
    <td>&nbsp;</td>
    <td>1</td>
    <td >  </td>
	<td > <select id='cmbOption' style="width:200px" > <option> --Select-- </option> <option> c1 </option> <option> c2 </option>  </select> </td>
	<td > <select id='cmbCriteria' > <option> --Select-- </option> <option> Equals </option> <option> Not Equals </option> <option> Less Than </option> <option> Greater Than </option> </select>  </td>
	<td > <input id="txtSearchData" value="" class="Label3" style="width:150px"/> </td>
  </tr>
</table>
</td>
</tr>

<tr height='2'> <td> &nbsp; </td> </tr>  

<tr align='right' valign='bottom' height="20px" valign='middle'>
        <td class="Titles" width="10%" colspan="5" align="right" valign='middle'>
	        <input type="button" class="EditableField" value="Add Criteria" onclick="addRowToTable();" />
			<input type="button" class="EditableField" value="Remove Criteria" onclick="removeRowFromTable();" />
			<input type="button" class="EditableField" value="Submit" onclick="alert('Under Construction');" />
		</td>
</tr>

</table>

</form>
 
</body>
</html>
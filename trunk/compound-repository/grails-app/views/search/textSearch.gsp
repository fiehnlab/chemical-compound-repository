<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="compound.repository.entries.Compound" contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Result</title>
  <meta name="layout" content="main"/>
  <export:resource/>
</head>


<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>

  <div class="box">

    <div id="pagePath">
      <h3><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <g:link controller="search" params="[query:params.query]" action="index"><g:message code="SEARCH"/></g:link> <span>&raquo;</span> <g:message code="SEARCH.RESULTS"/></h3>
    </div>
    <div class="line"></div>


    <div class="box">
      <h4>Search Description</h4>
      <div class="line"></div>
      <div>Query:${params.query}</div>

    </div>

    <div class="box">
      <h4>Results</h4>
      <div class="line"></div>
      <table>
        <tr>
          <th class="name-with-break"><g:message code="LIST.COMPOUND_NAME"/></th>
          <th class="inchi-key-no-break"><g:message code="LIST.INCHI_HASH_KEY"/></th>
          <th class="formula-no-break"><g:message code="LIST.STRUCTURE"/></th>
        </tr>
        <%
          if (searchResult != null && searchResult.results != null && searchResult.results.size() > 0) {
        %>
        <g:each var="compound" in="${searchResult.results}" status="index">
          <%
              String lStrClass = "";
              String lColorFormulaBG = "";

              if (index % 2 == 0) {
                lStrClass = "even"
                lColorFormulaBG = "#ffffff"
              } else {
                lStrClass = "odd"
                lColorFormulaBG = "#f7f7f7"
              }

          %>

          <tr class='<%=lStrClass%>'>
            <td class="name-with-break">${compound.getDefaultName()}</td>
            <td class="inchi-key-no-break"><g:link action="show" controller="compound" params="[id:compound.id]" class="detail">${compound.inchiHashKey.completeKey}</g:link></td>
            <td><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}"/></td>
          </tr>

        </g:each>
        <%
          } else {
        %>
        <tr>
          <td colspan="3">
            <div class="box">

              No Compound Found...

            </div>
          </td>
        </tr>
        <% }
        %>
      </table>

      <%
        if (searchResult != null && searchResult.results != null && searchResult.results.size() > 0) {
      %>
      <div class="pagination">
        <g:customPaginate controller="search" action="textSearch" params="[query: params.query]" total="${searchResult.total}" prev="&lt; previous" next="next &gt;"/>
      </div>

      <export:formats controller="search" action="textSearch" formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']" params="[query: params.query,total:searchResult.total]"/>


      <% } %>

    </div>
  </div>
</div>

</body>
</html>
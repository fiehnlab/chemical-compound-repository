<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="compound.repository.entries.Compound" contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Result</title>
  <meta name="layout" content="main"/>
</head>


<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>

  <div class="box">

    <div id="pagePath">
      <h3><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <g:link controller="search" params="[query:query]" action="index"><g:message code="SEARCH"/></g:link> <span>&raquo;</span> <g:message code="SEARCH.RESULTS"/></h3>
    </div>
    <div class="line"></div>



    <table border="0" id="tblSample" width="100%" align='center' cellpadding="3" cellspacing="0" class="TableBG">

      <tr align='left' valign='bottom' class="TableHeaderBG">
        <td class="TableHeader" width="1%">&nbsp;</td>
        <td class="TableHeader" width="5%">S.No.</td>
        <td class="TableHeader" width="1%">&nbsp;</td>
        <td class="TableHeader" width="5%">Compound</td>
        <td class="TableHeader" width="1%">&nbsp;</td>
        <td class="TableHeader" width="15%">Key</td>
        <td class="TableHeader" width="1%">&nbsp;</td>
      </tr>


      <%

        
        int liCnt = 0;
        String lStrClass = "";
        for (Compound compound: result) {
          if (liCnt++ % 2 == 0)
            lStrClass = "TableRowBG2";
          else
            lStrClass = "TableRowBG1";


      %>
      <tr class='<%=lStrClass%>'>
        <td class="Label3" width="1%">&nbsp;</td>
        <td class="Label3" width="1%">${liCnt}</td>
        <td class="Label3" width="1%">&nbsp;</td>
        <td class="Label3"><g:link action="show" controller="compound" params="[id:compound.id]">${compound.id}</g:link></td>
        <td class="Label3" width="1%">&nbsp;</td>
        <td class="Label3">${compound.inchiHashKey.completeKey}</td>
        <td class="Label3" width="1%">&nbsp;</td>
      </tr>
      <%
        }
      %>
    </table>
  </div>
</div>
</body>
</html>
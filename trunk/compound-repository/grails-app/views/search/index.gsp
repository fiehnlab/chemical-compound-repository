<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 4:00:39 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Search</title>  <meta name="layout" content="main"/>
</head>

<body>

<div id="main">

    <div class="page_header">
        <g:message code="PROJECT_TITLE"/>
    </div>

    <div class="box">

        <h3><g:link controller="homePage" action="index" class="home">Home</g:link> &raquo;Search</h3>
        <div class="line"></div>

        <g:render template="/error"/>

        <div class="box">

            <h4>Search</h4>
            <div class="line"></div>
            <g:render template="/searchForm"/>


            <h4>Search by Molfile</h4>
            <div class="line"></div>
            <g:render template="/searchMolForm"/>
        </div>
    </div>
</div>
</body>
</html>
<html>
<head>
  <meta name="layout" content="main"/>

</head>

<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>


  <div class="box">
    <h3><g:link controller="homePage" action="index" class="home">Home</g:link></h3>
    <div class="line"></div>

  </div>

  <div class="box">

    <h3>Ooops, what did just happen?</h3>

    <div class="line"></div>

    <div class="message">
      oh dear this is rather embarissing, but it looks like you discovered a bug. To help us avoiding this in the future, would you mind to send a bug report to <a href="mailto:wohlgemuth@ucdavis.edu">Gert Wohlgemuth</a> with as many informations as possible, so we can reproduce this and fix it!

    </div>

    <h3>Error Details</h3>

    <div class="line"></div>

    <div class="errors">
      <strong>Error ${request.'javax.servlet.error.status_code'}:</strong> ${request.'javax.servlet.error.message'.encodeAsHTML()}<br/>
      <strong>Servlet:</strong> ${request.'javax.servlet.error.servlet_name'}<br/>
      <strong>URI:</strong> ${request.'javax.servlet.error.request_uri'}<br/>

      <g:if test="${exception}">
        <strong>Exception Message:</strong> ${exception.message?.encodeAsHTML()} <br/>
        <strong>Caused by:</strong> ${exception.cause?.message?.encodeAsHTML()} <br/>
        <strong>Class:</strong> ${exception.className} <br/>
        <strong>At Line:</strong> [${exception.lineNumber}] <br/>
        <strong>Code Snippet:</strong><br/>
        <div class="snippet">
          <g:each var="cs" in="${exception.codeSnippet}">
            ${cs?.encodeAsHTML()}<br/>
          </g:each>
        </div>
      </g:if>

    </div>

    <g:if test="${exception}">
      <h3>Stack Trace</h3>

      <div class="line"></div>

      <div class="errors">
        <pre><g:each in="${exception.stackTraceLines}">${it.encodeAsHTML()}<br/></g:each></pre>
      </div>
    </g:if>

  </div>

</div>
</body>
</html>
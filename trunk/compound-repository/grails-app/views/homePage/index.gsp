<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 12:01:05 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>


<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>

<div id="main">

    <div class="page_header">
        <g:message code="PROJECT_TITLE"/>
    </div>

    <div class="box">

        <div id="pagePath">
            <h3>
                <g:link controller="homePage" action="index" class="home">Home</g:link>
            </h3>
        </div>

        <div class="line"></div>

        <div id="Left">

            <div class="box">
                <h3>Welcome</h3>
                <div class="line"></div>
                <p class="Label3">
                    Publicly available chemical information including structures, chemical names, chemical synonyms, database identifiers, molecular masses, XlogP and proton-donor/acceptor data were downloaded from different databases and combined into a single internal repository for compound-specific, structure-based cross references.
                    Molfile (SDF) to InChI code converters (vs. 1.0.2) and InChI code to InChI key converters were integrated into the tool set to allow any type of query access, e.g. by chemical names, structures, database identifiers.
                    For data storage and retrieval we are using a PostgresSQL database.
                </p>
                <p class="Label3">
                    This database allows executing queries at far higher speed compared to web-based applications that query external repositories in real time.
                    For accessibility we use a web GUI and a soap-based application-programming interface was implemented for automated access.
                </p>

            </div>

            <div class="box">

                <g:render template="/error"/>

                <h3>Search</h3>
                <div class="line"></div>
                <g:render template="/searchForm"/>

                <h3>Search by Molfile</h3>
                <div class="line"></div>
                <g:render template="/searchMolForm"/>
            </div>

        </div>

        <div id="Right">

            <div class="box">
                <h3>General Information</h3>
                <div class="line"></div>
                <p class="Label3">
                    Currently we are still in the data import phase of the project, which will take a couple of months. For this reason not all data are available yet, specially we are missing large parts of the pubchem database right now.

                </p>
            </div>

            <div class="box">
                <h3>Services</h3>
                <div class="line"></div>
                <td title="Discover">

                    <ul class="services">

                        <li class="discovery">
                            <g:link controller="discovery">
                                <h4><g:message code="DISCOVER"/></h4>
                                <span><h5><g:message code="DISCOVER.DESCRIPTION"/></h5></span>
                            </g:link>
                        </li>

                        <li class="transform">
                            <g:link controller="transform">
                                <h4><g:message code="TRANSFORM"/></h4>
                                <span><h5><g:message code="TRANSFORM.DESCRIPTION"/></h5></span>
                            </g:link>
                        </li>

                        <li class="mass_transform">
                            <g:link controller="massLookup">
                                <h4><g:message code="MASS_LOOKUP"/></h4>
                                <span><h5><g:message code="MASS_LOOKUP.DESCRIPTION"/></h5></span>
                            </g:link>
                        </li>


                        <li class="search">
                            <g:link controller="search">
                                <h4><g:message code="SEARCH"/></h4>
                                <span><h5><g:message code="SEARCH.DESCRIPTION"/></h5></span>
                            </g:link>
                        </li>


                        <li class="other">
                            <a href="/cts/static/moreServices.gsp">
                                <h4><g:message code="MORE"/></h4>
                                <span><h5><g:message code="MORE.DESCRIPTION"/></h5></span>
                            </a>
                        </li>

                    </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
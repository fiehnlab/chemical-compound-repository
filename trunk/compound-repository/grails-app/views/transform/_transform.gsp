<%@ page import="compound.repository.DataExportService; compound.repository.entries.Compound" contentType="text/html;charset=UTF-8" %>



<h4>Result</h4>

<div class="line"></div>


<table id="result">
  <tr>
    <th class="name-with-break"><g:message code="LIST.COMPOUND_NAME"/></th>
    <th class="inchi-key-no-break"><g:message code="LIST.INCHI_HASH_KEY"/></th>
    <th><g:message code="${to}"/></th>
    <th class="formula-no-break"><g:message code="LIST.STRUCTURE"/></th>
  </tr>


  <%
    if (transform != null && transform.size() > 0) {
      int liCnt = 0;
      String lStrClass = "";
      String lColorFormulaBG = "";
      DataExportService dataExportService = new DataExportService();
      for (Map map: transform) {
        compound.repository.entries.Compound compound = map.compound

        if (compound == null)
          continue;

        if (liCnt++ % 2 == 0) {
          lStrClass = "even";
          lColorFormulaBG = "#ffffff";
        }
        else {
          lStrClass = "odd";
          lColorFormulaBG = "#f7f7f7";
        }

        def lResult = map.result
        String str = dataExportService.splitPreeFix(to, lResult)

  %>
  <tr class='<%=lStrClass%>'>
    <td class="name-with-break">${compound.getDefaultName()}</td>
    <td class="inchi-key-no-break"><g:link class="detail" action="show" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>
  <g:render template="/massLookup/rendertransform" model="['lResult':lResult,'str':str]"/>
    <td class="formula-no-break"><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}"/></td>
  </tr>
  <%
      }
    } else {
  %>
  <tr>
    <td colspan="4">
      <div class="box">

        No Match or Compound Found...

      </div>
    </td>
  </tr>
  <% }
  %>
</table>


<%
  if (transform != null && transform.size() > 0) {
%>
<div class='pagination'>
  <table><tr><td class='paginationLabel'>Total Records : ${transform.size()}
  </td></tr></table>
</div>

<export:formats params="['idValue': idValue,'from': from, 'to': to]" action="transform" controller="transform" formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']"/>

<% } %>


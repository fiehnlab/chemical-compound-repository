<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
  <title>
    <g.message code="TRANSFORM.RESULTS"></g.message>
  </title>
  <meta name="layout" content="main"/>

  <export:resource/>
</head>


<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>


  <div class="box">

    <div id="pagePath">
      <h3><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <g:link controller="transform" params="[idValue:idValue,from:from,to:to]" action="index"><g:message code="TRANSFORM"/></g:link> <span>&raquo;</span> <g:message code="TRANSFORM.RESULTS"/></h3>
    </div>
    <div class="line"></div>


    <div class="box">
      <h4>Convert Descriptions</h4>
      <div class="line"></div>

      <div>From:<g:message code="${from}"/></div>
      <div>To:<g:message code="${to}"/></div>

    </div>

    <div class="box">

      <g:render template="transform"/>

    </div>
  </div>
</div>
</body>
</html>
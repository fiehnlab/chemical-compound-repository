<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 9, 2010
  Time: 4:00:39 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title><g:message code="TRANSFORM"/></title>
  <meta name="layout" content="main"/>
  <export:resource/>
  
</head>

<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>

  <div class="box">

    <h3>
      <g:link controller="homePage" action="index" class="home">Home</g:link> &raquo;<g:message code="TRANSFORM"/>
    </h3>

    <div class="line"></div>

    <div class="box">
      <g:form name="transForm" controller="transform" method="post">

        <H4>Convert</H4>
        <div class="line"></div>

        <span class="label">ID/Name</span><g:textField tabindex='1' name="idValue" value="${idValue?idValue:''}"/>
        <span class="label">From</span> <g:select tabindex='2' name="from" from="${fromFields}" optionValue="VALUE" optionKey="ID" value="${from?from:''}" noSelection="${['null': '--- Select ---']}"/>
        <span class="label">To</span><g:select tabindex='3' name="to" from="${toFields}" optionValue="VALUE" optionKey="ID" value="${to?to:''}" noSelection="${['null': '--- Select ---']}"/>

        <span class="buttons">
          <g:submitToRemote tabindex='4' value="Submit" action="transform_remote" update="[success:'transform_result',failure:'error']" class="search"/>
        </span>

        <span class="buttons">
          <g:remoteLink action="show_help_remote" update="transform_help" class="info"/>
        </span>
      </g:form>

      <g:render template="/error"/>

      <div id="transform_help"></div>
      <div class="spacer"></div>
      <div id="transform_result"></div>

    </div>
  </div>
</div>
</body>

</html>
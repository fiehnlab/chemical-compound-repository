<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Aug 9, 2010
  Time: 6:58:25 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head><title>Display Mol File</title></head>
<body>
<pre>
  <code>
    <div style="overflow:auto; height:500px;">
      ${mol.replaceAll("<","&lt;").replaceAll(">","&gt;")}
    </div>
  </code>
</pre>
</body>
</html>
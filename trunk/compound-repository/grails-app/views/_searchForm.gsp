<!-- provides a simple search field -->


  <g:form name="searchForm" controller="search" method="post" class="searchForm">
    <g:textField name="query" value="${params.query?params.query:''}" tabindex='1'/>
    <span class="buttons">
      <g:actionSubmit value="Search..." action="textSearch" class="search"/>
    </span>
    <span class="buttons">
      <g:remoteLink controller="homePage" action="show_help_remote" update="search_help" class="info"/>
    </span>

    <div id="search_help"></div>

  </g:form>


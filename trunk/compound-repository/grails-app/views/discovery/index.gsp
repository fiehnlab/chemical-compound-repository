<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 2, 2010
  Time: 2:40:38 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="main"/>
</head>
<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>

  <div class="box">

    <h3>
      <g:link controller="homePage" action="index" class="home">Home</g:link>
      <span>&raquo;</span>
      <g:message code="DISCOVER"/>
    </h3>

    <div class="line"></div>

    <div class="box">
      <g:form name="discoveryForm" controller="discovery">

        <h4>Query</h4>
        <div class="line"></div>

        <div>
          <g:textArea tabindex='1' id="query_field" name="query" class="full-width"/>
        </div>
        <div class="line"></div>

        <span class="buttons">
          <g:actionSubmit tabindex='2' value="Discover" action="process" class="search"/>

        </span>
        <span class="buttons">
          <input tabindex='3' type=Submit value="Clear" onclick="document.getElementById('query_field').value='';" class="clear"/>

        </span>
        <span class="buttons">
          <g:remoteLink action="show_help_remote" update="discovery_help" class="info"/>
        </span>

      </g:form>

      <g:render template="/error"/>

      <div id="discovery_help"></div>
    </div>

  </div>
</div>
</body>
</html>
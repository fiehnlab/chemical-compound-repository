<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 10, 2010
  Time: 8:49:40 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="compound.repository.entries.Compound" contentType="text/html;charset=UTF-8" %>
<html>
<head><title><g:message code="DISCOVER.RESULTS"/></title>
  <meta name="layout" content="main"/>

  <export:resource/>
</head>


<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>

  <div class="box">

    <g:form name="discoveryForm" controller="discovery" method="post">

      <div id="pagePath">
        <h3><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <g:link controller="discovery" action="index"><g:message code="DISCOVER"/> <span>&raquo;</span><g:message code="DISCOVER.RESULTS"/></g:link></h3>
      </div>
      <div class="line"></div>

      <div class="box">

      <h4>Result</h4>

      <div class="line"></div>

      <table>

        <tr>
          <th ><g:message code="LIST.COMPOUND_NAME"/></th>
      <th class="inchi-key-no-break"><g:message code="LIST.INCHI_HASH_KEY"/></th>
      <th>Discovered in Text</th>
      <th>Origin</th>
      <th class="formula-no-break"><g:message code="LIST.STRUCTURE"/></th>
      </tr>


      <%

        if (result != null && result.size() > 0) {
          int liCnt = 0;
          String lStrClass = "";
          String lColorFormulaBG = "";
          for (Map map: result) {
            Compound compound = map.compound

            if (compound == null)
              continue;

            if (liCnt++ % 2 == 0) {
              lStrClass = "even";
              lColorFormulaBG = "#ffffff";
            }
            else {
              lStrClass = "odd";
              lColorFormulaBG = "#f7f7f7";
            }

      %>
      <tr class='<%=lStrClass%>'>
        <td class="name-with-break">${compound.getDefaultName()}</td>
        <td><g:link action="show" class="detail" controller="compound" params="[id:compound.id]">${compound.inchiHashKey.completeKey}</g:link></td>
        <td>${map.hit}</td>
        <td>${map.resolver}</td>
        <td><g:structureThumb inchi="${compound.inchi}" id="${compound.id}" color="${lColorFormulaBG}"/></td>

      </tr>
      <%
          }
        } else {
      %>
      <tr>
        <td colspan="6">
          <div class="box">

            No Compound Found...

          </div>
        </td>
      </tr>

      <% } %>

      </table>

      <%

        if (result != null && result.size() > 0) { %>

      <div class='pagination'>
        <table><tr><td class='paginationLabel'>Total Records : ${result.size()}
        </td></tr></table>
      </div>

      <export:formats action="process" controller="discovery" formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']"/>

      <% } %>

    </g:form>
  </div>

  </div>
</div>

</body>
</html>
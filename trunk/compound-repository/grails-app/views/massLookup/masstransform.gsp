<%--
  Created by IntelliJ IDEA.
  User: pradeep
  Date: Feb 22, 2010
  Time: 12:22:19 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="compound.repository.entries.DBLinks; compound.repository.DataExportService; compound.repository.entries.Compound; org.springframework.util.ClassUtils" contentType="text/html;charset=UTF-8" %>
<html>
<head><title><g:message code="MASS_LOOKUP.RESULTS"/></title>
    <meta name="layout" content="main"/>

    <export:resource/>
</head>


<body>

<div id="main">

    <div class="page_header">
        <g:message code="PROJECT_TITLE"/>
    </div>

    <div class="box">

        <div id="pagePath">
            <h3><g:link controller="homePage" action="index" class="home">Home</g:link> <span>&raquo;</span> <g:link
                    controller="massLookup" params="[from: from, to: to]" action="index"><g:message
                        code="MASS_LOOKUP"/></g:link> <span>&raquo;</span> <g:message code="MASS_LOOKUP.RESULTS"/></h3>
        </div>

        <div class="line"></div>

        <div class="box">
            <h4>Convert Descriptions</h4>

            <div class="line"></div>

            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

            <div>From:<g:message code="${from}"/></div>

            <div>To:
            <% if (to != null && to.size() > 0) {
                for (int liCnt = 0; liCnt < to.size(); liCnt++) {

                    if (liCnt < to.size() - 1) {
            %>
            <g:message code="${to.get(liCnt)}"/> ${",&nbsp;"}
            <% } else { %>
            <g:message code="${to.get(liCnt)}"/>
            <% }
            }
            } %>

            </div>

        </div>

        <div class="box">

            <div class="left-field"><h4>Results</h4></div>

            <div class="right-field">
                <h4>
                    <g:link controller="transcode" action="transcode"
                            params="[sessionId: session_id, from: from, to: to]">transcode</g:link>      <g:remoteLink
                            controller="transcode" action="show_help_transcode" update="convert_help" class="info"/>
                </h4>
            </div>

            <div class="line"></div>

            <div id="convert_help"></div>

            <table id="result">
                <tr>

                    <th><g:message code="LIST.CONVERT_COMPOUND_ORIGIN"/></th>

                    <th><g:message code="LIST.COMPOUND_NAME"/></th>

                    <th class="inchi-key-no-break"><g:message code="LIST.INCHI_HASH_KEY"/></th>



                    <% if (to != null && to.size() > 0) {
                        for (int liCnt = 0; liCnt < to.size(); liCnt++) {
                    %>
                    <th><g:message code="${to.get(liCnt)}"/></th>
                    <% }
                    } %>
                    <th class="formula-no-break"><g:message code="LIST.STRUCTURE"/></th>

                </tr>


                <%
                    if (transform != null && transform.size() > 0) {
                        DataExportService dataExportService = new DataExportService();
                %>
                <g:each var="map" in="${transform}" status="index">

                    <%
                            Compound compound = map.compound

                            String compoundFrom = map.fromValue
                            String lStrClass = "";
                            String lColorFormulaBG = "";

                            if (index % 2 == 0) {
                                lStrClass = "even"
                                lColorFormulaBG = "#ffffff"
                            } else {
                                lStrClass = "odd"
                                lColorFormulaBG = "#f7f7f7"
                            }

                            def compoundName = "None"
                            def inchi = "None"
                            def id = "None"

                            if (compound != null) {
                                compoundName = compound.getDefaultName()
                                inchi = compound.inchi
                                id = compound.id
                            }

                    %>

                    <tr class='<%=lStrClass%>' onMouseOver="this.bgColor = 'yellow';">

                        <td class="name-with-break">${compoundFrom}</td>

                        <td class="name-with-break">${compoundName}</td>
                        <td class="inchi-key-no-break"><g:if test="${compound != null}"><g:link action="show"
                                                                                                controller="compound"
                                                                                                class="detail"
                                                                                                params="[id: compound.id]">${compound.inchiHashKey.completeKey}</g:link></g:if></td>

                        <%
                                if (to != null && to.size() > 0) {
                                    HashMap lMap = (map.result);

                                    for (int liCnt1 = 0; liCnt1 < to.size(); liCnt1++) {
                                        String keyname = to.get(liCnt1)
                                        def lResult = lMap.get(keyname.toString().trim())

                        %>

                        <g:render template="rendertransform" model="['lResult': lResult]"/>
                        <%

                                    }
                                }

                        %>

                        <td>
                            <g:if test="${compound != null}">
                                <g:structureThumb inchi="${compound.inchi}" id="${compound.id}"
                                                  color="${lColorFormulaBG}"/>
                            </g:if>
                        </td>

                    </tr>

                </g:each>

                <%
                    } else {
                %>
                <tr>
                    <td colspan="${3 + to.size}">
                        <div class="box">

                            No Compound Found...

                        </div>
                    </td>

                </tr>
                <% }
                %>
            </table>

            <%
                if (transform != null && transform.size() > 0) {
            %>

            <div class='pagination'>
                <table><tr><td class='paginationLabel'>Total Records : ${transform.size()}
                </td></tr></table>
            </div>

            <export:formats controller="massLookup" action="masstransform"
                            formats="['csv', 'excel', 'ods', 'pdf', 'rtf', 'xml']"
                            params="[from: params.from, to: params.to]"/>


            <% } %>
        </div>

    </div>

</div>

</body>
</html>
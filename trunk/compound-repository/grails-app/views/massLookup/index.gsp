<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="main"/>
</head>


<body>


<g:javascript>

  function checkAll(field)
  {
    for (i = 0; i < field.length; i++)
      field[i].checked = true;
  }

  function uncheckAll(field)
  {
    for (i = 0; i < field.length; i++)
      field[i].checked = false;
  }

</g:javascript>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>

<div class="box">

  <h3><g:link controller="homePage" action="index" class="home">Home</g:link> &raquo;<g:message code="MASS_LOOKUP"/></h3>

  <div class="line"></div>

<div class="box">
  <h4>Batch Convert</h4>

  <div class="line"></div>
  <g:form name="masslookup" controller="massLookup" method="post">

    <div>
      <g:textArea id="ids" name="ids" rows="10" cols="80" tabindex='1' class="full-width"/>
    </div>

    <div class="small-spacer"></div>

    <div>
      <span class="label">
        From:
      </span>

      <span>
        <g:select name="from" from="${fromFields}" optionValue="VALUE" optionKey="ID" value="${from}" noSelection="${['null': '--- Select ---']}" tabindex='2'/>
      </span>

    </div>

    <div>
      <span class="label">
        To:
      </span>


      <div class="buttons">

        <%
          int liCnt = 0;

          for (int i = 0; i < toFields.size(); i++) {
        %>
        <span class="wrap">

          <%
              if(to instanceof Collection == false){
                Collection list = new ArrayList()
                list.add to
                to = list
              }
              
              ArrayList list = to
              String id = ((Map) toFields.get(i)).get("ID");
              String value = ((Map) toFields.get(i)).get("VALUE");

              if (list != null && list.contains(id)) {
          %>
          <g:checkBox name="to" value="${id}" checked="true" tabindex='${2+i+1}'/> ${value}
          <% } else {
          %>
          <g:checkBox name="to" value="${id}" checked="false" tabindex='${2+i+1}'/> ${value}
          <% }
          %>
        </span>
        <%
          }
        %>
      </div>

      <div class="buttons">
        <input class="check" type="button" name="CheckAll" value="Check All"
                onClick="checkAll(document.masslookup.to)">
        <input class="uncheck" type="button" name="UnCheckAll" value="Uncheck All"
                onClick="uncheckAll(document.masslookup.to)">
      </div>

    </div>
    <div class="small-spacer"></div>
    <span class="buttons">
      <g:actionSubmit value="Submit" action="masstransform" controler="massLookup" tabindex='${2 + toFields.size() + 1}' class="search"/>
    </span>
    <span class="buttons">
      <input tabindex='3' type=Submit value="Clear" onclick="document.getElementById('ids').value = '';" class="clear"/>

    </span>
    <span class="buttons">
      <g:remoteLink action="show_help_remote" update="masslookup_help" class="info"/>
    </span>

    <div id="masslookup_help"></div>

    <g:render template="/error"/></div>

    </div>

  </g:form>

</div>

</body>

</html>
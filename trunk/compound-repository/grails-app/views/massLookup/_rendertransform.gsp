<%@ page import="util.ConvertHelper; compound.repository.entries.DBLinks" %>
<%

  if (lResult instanceof Collection) {

    if (lResult.size() > 0) {
%>
<td class="name-with-break">
  <ul>
    <%

        for (Object f in lResult) {

    %>
    <li>
      <%
          if (f instanceof DBLinks) {
            if (f.hasCalculateableURL()) {
      %>
      <a href="${f.calculateURL()}" target="_blank" class="external">${ConvertHelper.getInstance().toString(f)}</a>
      <%
        }
        else {
      %>
      ${ConvertHelper.getInstance().toString(f)}
      <%
          }

        }
        else {
      %>
      ${ConvertHelper.getInstance().toString(f)}
      <%
          }
      %>
    </li>
    <%
        }
    %>
  </ul>
</td>
<%
  }
  else {
%>
<td class="name-with-break">${ConvertHelper.getInstance().toString(lResult)}</td>

<%

    }
  }
  else {
%>
<td class="name-with-break">${ConvertHelper.getInstance().toString(lResult)}</td>

<%
  }
%>


<g:form name="searchFormMol" controller="search" method="post" class="searchForm">
  <g:textArea cols="60" rows="50" name="query" value="${params.query?params.query:''}" tabindex='1'/>
  <span class="buttons">
    <g:actionSubmit value="Search..." action="molSearch" class="search"/>
  </span>
  <span class="buttons">
    <g:remoteLink controller="homePage" action="show_help_mol_remote" update="search_help_mol" class="info"/>
  </span>

    <div id="search_help_mol"></div>

</g:form>


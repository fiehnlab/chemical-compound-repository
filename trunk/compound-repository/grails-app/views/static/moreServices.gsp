<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 2, 2010
  Time: 2:40:38 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
</head>
<body>

<div id="main">

    <div class="page_header">
        <g:message code="PROJECT_TITLE"/>
    </div>

    <div class="box" id="bottom">

        <h3>
            <g:link controller="homePage" action="index" class="home">Home</g:link>
            <span>&raquo;</span>
            <g:message code="MORE"/>
        </h3>

        <div class="line">

        </div>

        <div class="box">
            <h3>Webservices</h3>
            You can directly access the system by the use of our simple soap based webservice. Right now we only support the convertion of one
            identifier to another, but will add other services at a later point in time.

            The documentation can be found <a href="/cts/services" class="external">here</a>
            <div class="no-icon-style">

                <ul>
                    <li>
                        <a href="${resource(dir: 'services', file: 'transformWeb?wsdl')}" class="external">WSDL File - transform</a>
                    </li>
                    <li>
                        <a href="${resource(dir: 'services', file: 'exportMol?wsdl')}" class="external">WSDL File - download</a>
                    </li>
                    <li>
                        <a href="${resource(dir: 'services', file: 'upload?wsdl')}" class="external">WSDL File - upload</a>
                    </li>
                </ul>
            </div>
        </div>


        <div class="box">
            <h3>Examples</h3>

            we provide a couple of examples how to access the system using the internal webservices. This is very useful if you like to upload your own compounds.

            <div class="no-icon-style">

                <ul>
                    <li>
                        <a href="${resource(dir: 'static/examples', file: 'groovyWebservices.gsp')}" class="external">Groovy Examples</a>
                    </li>
                </ul>
            </div>
        </div>



        <div class="box">
            <h3>Publications and Posters</h3>

            here you can find all realted publications and posters for this software.

            <div class="no-icon-style">

                <ul>
                    <li><a href="${resource(dir: 'download/publications', file: 'NCMTP2011.pdf')}" class="external">2011 National Conference On Metabolomics,Transcriptomics and Proteomics (NCMTP) Poster</a></li>
                    <li><a href="${resource(dir: 'download/publications', file: 'ASMS2010.pdf')}" class="external">2010 ASMS Poster</a></li>
                    <li><a href="${resource(dir: 'download/publications', file: 'MS2010.pdf')}" class="external">2010 Metabolomic Society Poster</a></li>
                </ul>
            </div>
        </div>


        <div class="box">
            <h3>Data Upload</h3>

            if you like to contribute chemical data please send us the SDF files, or use the upload webservices to upload the date yourself. The data have the be in the standard mol file format. We provide an example for the upload functionality <a href="${resource(dir: 'static/examples', file: 'groovyWebservices.gsp')}" class="external">here</a>
        </div>

    </div>
</div>
</body>
</html>
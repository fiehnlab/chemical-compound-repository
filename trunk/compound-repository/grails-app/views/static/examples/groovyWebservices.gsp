<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 2, 2010
  Time: 2:40:38 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
</head>
<body>

<div id="main">

    <div class="page_header">
        <g:message code="PROJECT_TITLE"/>
    </div>

    <div class="box" id="bottom">

        <h3>
            <g:link controller="homePage" action="index" class="home">Home</g:link>
            <span>&raquo;</span>
            <g:message code="MORE"/>
        </h3>

        <div class="line">

        </div>

        <div class="box">
            <h3>Upload data using groovy</h3>

            This is a simple groovy script to show how to upload data with groovy and the cts webservice. It exspects from you the server and the sdf file location.
            <br>
            <br>
            Arguments:

            <div class="box">
                <ul class="no-icon-style">
                    <li>arg[0] = server</li>
                    <li>arg[1] = sdfFile</li>
                </ul>
            </div>
            For example:

            <div class="box">
                groovy UploadSDFClient.groovy uranus.fiehnlab.ucdavis.edu:8080 mySdfFile.sdf
            </div>


            To download the test file, please click <a href="${resource(dir: 'examples/groovy', file: 'UploadSDFClient.groovy')}" class="external">here</a>
        </div>

        <div class="box">
            <h3>List all metadata keys using groovy</h3>

            This is a simple groovy script to show how to access all metadata keys with groovy and the cts webservice. It exspects from you the server as argument.
            <br>
            <br>
            Arguments:

            <div class="box">
                <ul class="no-icon-style">
                    <li>arg[0] = server</li>
                </ul>
            </div>
            For example:

            <div class="box">
                groovy FetchMetaDataKeys.groovy uranus.fiehnlab.ucdavis.edu:8080
            </div>


            To download the test file, please click <a href="${resource(dir: 'examples/groovy', file: 'FetchMetaDataKeys.groovy')}" class="external">here</a>
        </div>


        <div class="box">
            <h3>Update metadata using groovy</h3>

            This is a simple groovy script to show how to update the metadata of a specific compound using the cts webservice. It exspects from you the server,compoundId,key and value as arguments.

            <br>
            <br>
            Arguments:

            <div class="box">
                <ul class="no-icon-style">
                    <li>arg[0] = server</li>
                    <li>arg[1] = compoundId</li>
                    <li>arg[2] = Metadata Key</li>
                    <li>arg[3] = value</li>

                </ul>
            </div>
            For example:

            <div class="box">
                groovy UploadMetaData.groovy uranus.fiehnlab.ucdavis.edu:8080 2100298 PubchemSubstance 56404331
            </div>


            To download the test file, please click <a href="${resource(dir: 'examples/groovy', file: 'UploadMetaData.groovy')}" class="external">here</a>
        </div>


        <div class="box">
            <h3>Fetch Transform Keys using groovy</h3>

            This is a simple groovy script to show how to list all the available To/From transform keys using the cts webservice. It exspects from you the server,compoundId,key and value as arguments.

            <br>
            <br>
            Arguments:

            <div class="box">
                <ul class="no-icon-style">
                    <li>arg[0] = server</li>
                </ul>
            </div>
            For example:

            <div class="box">
                groovy FetchTransformKeys.groovy uranus.fiehnlab.ucdavis.edu:8080
            </div>


            To download the test file, please click <a href="${resource(dir: 'examples/groovy', file: 'FetchTransformKeys.groovy')}" class="external">here</a>
        </div>


        <div class="box">
            <h3>Transform data using groovy</h3>

            This is a simple groovy script to show how to transform a value using the cts webservice. It exspects from you the server,compoundId,key and value as arguments.

            <br>
            <br>
            Arguments:

            <div class="box">
                <ul class="no-icon-style">
                    <li>arg[0] = server</li>
                    <li>arg[1] = from</li>
                    <li>arg[2] = to</li>
                    <li>arg[3] = value</li>

                </ul>
            </div>
            For example:

            <div class="box">
                groovy TransformValue.groovy uranus.fiehnlab.ucdavis.edu:8080 inchikey formula QNAYBMKLOCPYGJ-UHFFFAOYSA-N
            </div>

            Be warned that some queries can take a very long time. For example if you want to transform a molecular formula to all possible inchi keys. It can take hours. So please make sure that you specify a long enough timeout

            To download the test file, please click <a href="${resource(dir: 'examples/groovy', file: 'TransformValue.groovy')}" class="external">here</a>
        </div>

    </div>
</div>
</body>
</html>
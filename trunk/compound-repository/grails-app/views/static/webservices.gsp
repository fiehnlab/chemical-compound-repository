<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Feb 2, 2010
  Time: 2:40:38 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="main"/>
</head>
<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>

  <div class="box">

    <h3>
      <g:link controller="homePage" action="index" class="home">Home</g:link>
      <span>&raquo;</span>
      <a href="/cts/static/moreServices.gsp"><g:message code="MORE"/></a>
      <span>&raquo;</span>
      <g:message code="WEBSERVICE_DOCU"/>
    </h3>

    <div class="line"></div>
    <div class="box">

      <p>
        this document is a short overview over the webservices and what they do for you, you can find an extended automatically generated documentation <a href='http://www.w3.org/2000/06/webdata/xslt?xslfile=http://tomi.vanek.sk/xml/wsdl-viewer.xsl&amp;xmlfile=http://uranus.fiehnlab.ucdavis.edu:8080/cts/services/transformWeb?wsdl&amp;transform=Submit' class="external" target="_blank">here</a> with all the source
      </p>
      
      <br>
      
      <h5>method:getFromIdentifiersAsJSON</h5>
      <p>

        result: a document containing all the from keys for queries in the json format

      </p>
      <br>

      <h5>method:getFromIdentifiersAsXML</h5>
      <p>

        result: a document containing all the from keys for queries in the xml format

      </p>
      <br>

      <h5>method:getToIdentifiersAsJSON</h5>
      <p>

        result: a document containing all the to keys for queries in the json format

      </p>
      <br>

      <h5>method:getToIdentifiersAsXML</h5>
      <p>

        result: a document containing all the to keys for queries in the xml format

      </p>
      <br>
      <h5>method:transformToDeepJSONString</h5>
      <p>
        p1:String from => a given database name like KEGG<br>
        p2:String to => a given database name like HMDB<br>
        p3:String value => a valid identifier<br>


        result: a document containing all the data in the json format including all relations

      </p>
      <br>

      <h5>method:transformToDeepXmlString</h5>
      <p>
        p1:String from => a given database name like KEGG<br>
        p2:String to => a given database name like HMDB<br>
        p3:String value => a valid identifier<br>


        result: a document containing all the data in xml including all relations

      </p>
      <br>
      <h5>method:transformToJSONString</h5>
      <p>
        p1:String from => a given database name like KEGG<br>
        p2:String to => a given database name like HMDB<br>
        p3:String value => a valid identifier<br>


        result: a document containing all the data in the json format

      </p>
      <br>
      <h5>method:transformToXmlString</h5>

      <p>
        p1:String from => a given database name like KEGG<br>
        p2:String to => a given database name like HMDB<br>
        p3:String value => a valid identifier<br>


        result: a document containing all the data in xml

      </p>
      <br>


    </div>

  </div>
</div>
</body>
</html>
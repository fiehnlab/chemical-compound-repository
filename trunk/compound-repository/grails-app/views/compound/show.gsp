<html>
<head>
  <meta name="layout" content="main"/>

  <style type="text/css">@import url("${resource(dir: 'css',file : 'showLayout.css')}");</style>
  <style type="text/css">@import url("${resource(dir: 'css',file : 'tabView.css')}");</style>


  <resource:tabView/>
  <modalbox:modalIncludes/>
  <resource:treeView/>

</head>


<body>

<div id="main">

  <div class="page_header">
    <g:message code="PROJECT_TITLE"/>
  </div>


  <div class="box">

    <div id="pagePath">

      <h3>
        <g:link controller="homePage" action="index" class="home">Home</g:link>
        <a class="result" href="javascript://nop" onclick="history.back(1)">Back to result</a>
      </h3>
    </div>

    <div class="line"></div>

    <div class="box">
      <h4>Compound Details</h4>
      <div class="line"></div>
    </div>

    <div class="box">

      <div id="top_part">
        <div id="general-properties">
          <h4>Properties</h4>
          <div class="line"></div>
          <g:render template="render_properties"/>

        </div>

      </div>

      <div id="bottom_part">

        <div id="synonyms">
          <h4>Known Synonyms</h4>
          <div class="line"></div>
          <g:render template="render_synonyms"/>
        </div>


        <div id="iupac-names">
          <h4>Known IUPAC Names</h4>
          <div class="line"></div>

          <div class="no-icon-style">

            <ul>
              <g:each var="s" in="${compoundInstance.iupac}">
                <li>${s.name.encodeAsHTML()}</li>
              </g:each>
            </ul>
          </div>

        </div>

        <div id="smile-codes">
          <h4>Known Smiles</h4>
          <div class="line"></div>

          <div class="no-icon-style">

            <ul>
              <g:each var="s" in="${compoundInstance.smiles}">
                <li>${s.code.encodeAsHTML()}</li>
              </g:each>
            </ul>
          </div>

        </div>

        <div id="database-identifier">
          <h4>Known Database Identifiers</h4>
          <div class="line"></div>
          <g:render template="render_dblinks"/>
        </div>

      </div>

    </div>
  </div>
</div>

</body>
</html>
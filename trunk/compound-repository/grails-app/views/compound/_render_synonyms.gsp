<%@ page import="compound.repository.entries.Synonym" %>
<%
  assert compoundInstance != null, "you need to provide a field with the name compound instance!"


  def list = compoundInstance.getFilteredSynonyms()
%>

<div id="synonym_content">

  <div class="box">
    <richui:tabView id="synonymTabView">
      <richui:tabLabels>
        <g:if test="${promoted == null && demoted == null && flagged == null }">
          <richui:tabLabel selected="true" title="Top 10"/>
          <richui:tabLabel title="All Synonyms"/>
          <richui:tabLabel title="Cloud Visualization"/>
          <richui:tabLabel title="Rating and Modification"/>
        </g:if>
        <g:else>
          <richui:tabLabel title="Top 10"/>
          <richui:tabLabel title="All Synonyms"/>
          <richui:tabLabel title="Cloud Visualization"/>
          <richui:tabLabel selected="true" title="Rating and Modification"/>
        </g:else>

      </richui:tabLabels>

      <richui:tabContents>

        <richui:tabContent>
          <div class="no-icon-style">

            <ul>
              <g:each status="i" var="s" in="${list}">
                <g:if test="${i < 10}">
                  <li>
                    ${s.name.encodeAsHTML()}
                  </li>
                </g:if>
              </g:each>
            </ul>

          </div>
        </richui:tabContent>

        <richui:tabContent>
          <div class="no-icon-style">

            <ul>
              <g:each var="s" in="${list}">
                <li>
                  ${s.name.encodeAsHTML()}
                </li>
              </g:each>
            </ul>

          </div>
        </richui:tabContent>

        <richui:tabContent>

          <div class="box">
            <%

              Map tagCloud = new HashMap()
              list.each {Synonym syn ->
                tagCloud.put(syn.getName(), syn.getRating())


              }
            %>
            <richui:tagCloud sortOrder="desc" maxSize="5" controller="search" action="doTagSearch" class="tag" values="${tagCloud}"/>
          </div>
        </richui:tabContent>

        <richui:tabContent>

          <g:if test="${promoted != null}">
            <div class="box">
              <div class="message">promoted synonym: ${promoted.name} to rank: ${promoted.rating}</div>
            </div>

          </g:if>

          <g:if test="${demoted != null}">
            <div class="box">
              <div class="message">demoted synonym: ${demoted.name} to rank: ${demoted.rating}</div>
            </div>

          </g:if>

          <g:if test="${flagged != null}">
            <div class="box">
              <div class="message">flagged synonym: ${flagged.name} as wrong</div>
            </div>
          </g:if>

          <div>
            <table>
              <thead>
              <th>
                remove
              </th>
              <th>
                modify ranking
              </th>

              <th>
                name
              </th>

              </thead>
              <tbody>
              <g:each var="s" in="${list}" status="index">
                <%
                  String lStrClass = "";
                  String lColorFormulaBG = "";

                  if (index % 2 == 0) {
                    lStrClass = "even"
                    lColorFormulaBG = "#ffffff"
                  } else {
                    lStrClass = "odd"
                    lColorFormulaBG = "#f7f7f7"
                  }

                %>


                <tr class='<%=lStrClass%>'>
                  <td style="text-align:center">
                    <span>
                      <g:remoteLink class="flag" controller="synonym" action="flag" id="${s.id}" update="synonym_content"/>
                    </span>
                  </td>
                  <td style="text-align:center">
                    <span>
                      <g:remoteLink class="promote" controller="synonym" action="promote" id="${s.id}" update="synonym_content"/>
                    </span>
                    <span>
                      <g:remoteLink class="demote" controller="synonym" action="demote" id="${s.id}" update="synonym_content"/>
                    </span>
                    <span style="text-align:right">
                      ${s.rating.encodeAsHTML()}
                    </span>
                  </td>
                  <td class="name-with-break">
                    ${s.name.encodeAsHTML()}
                  </td>
                </tr>
              </g:each>
              </tbody>
            </table>
          </div>
        </richui:tabContent>
      </richui:tabContents>

    </richui:tabView>

  </div>

</div>
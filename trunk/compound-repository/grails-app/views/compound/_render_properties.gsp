<%@ page import="compound.repository.entries.Synonym" %>
<%
  assert compoundInstance != null, "you need to provide a field with the name compound instance!"


%>

<div id="property_content">

  <div class="box">
    <div id="general-properties-tab">

      <richui:tabView id="propertyTabView">
        <richui:tabLabels>
          <richui:tabLabel selected="true" title="General"/>
          <richui:tabLabel title="Internal"/>

        </richui:tabLabels>

        <richui:tabContents>

          <richui:tabContent>

            <p><div class="label">Formula</div><div class="value">${compoundInstance.formula}</div></p>
            <p><div class="label">Exact Mass</div><div class="value">${compoundInstance.exactMolareMass}</div></p>
            <p><div class="label">Preffered Name</div><div class="value">${compoundInstance.getDefaultName()}</div></p>
            <p><div class="label">Std Inchi Code</div><div class="value">${compoundInstance.inchi}</div></p>
            <p><div class="label">Std Inchi Key</div><div class="value">${compoundInstance.inchiHashKey.completeKey}</div></p>

          </richui:tabContent>

          <richui:tabContent>
            <p><div class="label">Id</div><div class="value">${compoundInstance.id}</div></p>
            <p><div class="label">Initially Entered</div><div class="value">${compoundInstance.dateCreated}</div></p>
            <p><div class="label">Last Updated</div><div class="value">${compoundInstance.lastUpdated}</div></p>

          </richui:tabContent>

        </richui:tabContents>

      </richui:tabView>

    </div>
    <div id="structure">
                 
      <richui:tabView id="sructureViewer">
        <richui:tabLabels>
          <richui:tabLabel selected="true" title="2D"/>
          <richui:tabLabel title="3D"/>
        </richui:tabLabels>

        <richui:tabContents>
          <richui:tabContent>
            <g:structureMedium id="${compoundInstance.id}" inchi="${compoundInstance.inchi}" color="#edf5ff"/>

          </richui:tabContent>

          <richui:tabContent>
            <g:structureMedium3D id="${compoundInstance.id}" inchi="${compoundInstance.inchi}" color="#edf5ff"/>
          </richui:tabContent>

        </richui:tabContents>
      </richui:tabView>

      <p>
        <a class="molecule" href='/cts/exportMol/exportMol/${compoundInstance.id}' title='Display Molfile' onclick='Modalbox.show(this.href, {title: this.title, width: 1024});
        return false;'>display mol file</a>
      </p>

    </div>
  </div>

</div>
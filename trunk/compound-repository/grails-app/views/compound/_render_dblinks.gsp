<%@ page import="compound.repository.entries.DBLinks" %>
<%
  assert compoundInstance != null, "you need to provide a field with the name compound instance!"
%>

<richui:treeView xml="${compoundInstance.getDBLinksCollectionsXMLData()}" showRoot="false" onLabelClick="doNothing()"/>


<!-- modifies the tree into our required format -->
<script type="text/javascript">


  //does nothing to fix a bug in the treeview api from richui
  function doNothing() {

  }

  //modifyes the node and sets our attributes
  function modifyNode(node, json) {

    if (json != null) {
      if (json.has_url) {
        node.href = json.url;
        node.target = "_blank";
        node.labelStyle = "external-link";
      }
    }
  }

  //checks if we got an url for this object
  function fetchProperty(id) {

    //check if the id was properly defined
    if (id == undefined) {
      return null;
    }
    else if (id != '') {

      //create the url to execute
      var url = "${createLink(controller:'DBLinks',action:'getHasURLasJSON')}" + "/" + id;

      //contains the result of the ajax response
      var result = null;

      //fire a new request to fetch the content
      new Ajax.Request(url, {
        asynchronous:false,

        onSuccess: function(transport) {

          //assign the content to the result
          result = transport.responseText.evalJSON();

        }
      });

      //return the result
      return result;
    }

    //no id found, so just return null
    return null;
  }

  /**
   * fetches all the properties from the database in one call
   * @param compoundId
   * @param className
   */
  function fetchProperties(id) {

    //check if the id was properly defined
    if (id == undefined) {
      return null;
    }
    else if (id != '') {

      //create the url to execute
      var url = "${createLink(controller:'DBLinks',action:'getHASURLasJSONForClassAndCompound')}" + "/" + id;

      //contains the result of the ajax response
      var result = null;

      //fire a new request to fetch the content
      new Ajax.Request(url, {
        asynchronous:false,

        onSuccess: function(transport) {

          //assign the content to the result
          result = transport.responseText.evalJSON();

        }
      });

      //return the result
      return result;
    }

    //no id found, so just return null
    return null;
  }

  //dynamically loads data from the database when they are needed
  function loadNodeData(node, fnLoadComplete) {

    var children = node.children;

    if (children.length > 0) {

      var json = fetchProperties(children[0].additionalId)

      for (var i = 0; i < children.length; i++) {
        for (var x = 0; x < json.length; x++) {
          if (children[i].additionalId == json[x].id) {
            modifyNode(children[i], json[x]);
            children[i].isLeaf = true;
          }
        }
      }
    }
    node.loadComplete();
  }

  //allow dynamic content loading
  tree.setDynamicLoad(loadNodeData);

  //assign the properties for all the nodes
  var nodes = tree.root.children;
                        
  //discover leaf nodes
  for (var i = 0; i < nodes.length; i++) {

    var node = nodes[i];

    //is a leaf node so provide it with url properties
    if (node.children.length == 0) {

      var json = fetchProperty(node.additionalId);

      modifyNode(node, json);
      node.isLeaf = true;
    }
  }

  //redraw the tree
  tree.render();

</script>
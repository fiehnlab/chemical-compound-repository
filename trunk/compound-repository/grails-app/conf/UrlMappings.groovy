import compound.repository.TransformController

class UrlMappings {
  static mappings = {
    "/$controller/$action?/$id?" {
      constraints {
        // apply constraints here
      }
    }
    "/"(view: "/index")
    "500"(view: '/error')


    /*
    //handles all our transformations
    "/transform/$from/$to/$idValue/$format?" {
      controller = "transform"
      action = "transform"
      constraints {
           format(inList: TransformController.getFormats())
      }
    }
    */
  }
}

grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.war.file = "target/${appName}.war"
grails.project.dependency.resolution = {
  // inherit Grails' default dependencies
  inherits("global") {
    // uncomment to disable ehcache
    // excludes 'ehcache'
  }
  log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
  repositories {
    grailsPlugins()
    grailsHome()

    // uncomment the below to enable remote dependency resolution
    // from public Maven repositories
    mavenLocal()
    mavenCentral()

    mavenRepo "http://snapshots.repository.codehaus.org"
    mavenRepo "http://repository.codehaus.org"
    mavenRepo "http://download.java.net/maven/2/"
    mavenRepo "http://repository.jboss.com/maven2/"
    mavenRepo "http://binbase-maven.googlecode.com/svn/trunk/repo"
  }

  dependencies {
    // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

    // runtime 'mysql:mysql-connector-java:5.1.5'
    /*
    runtime('org.hibernate:hibernate-validator:3.1.0.GA') {
      excludes 'sl4j-api', 'hibernate.core', 'hibernate-commons-annotations', 'hibernate-entitymanager'
    }
    */

    runtime "postgresql:postgresql:8.2-504.jdbc3"

runtime("edu.ucdavis.genomics.metabolomics.binbase.binbase-core.binbase:binbase-annotations:3.4.8") {
 excludes 'sl4j-api',
         'hibernate.core',
         'hibernate-commons-annotations',
         'hibernate-entitymanager',
         'log4j',
         'logkit',
         'avalon-framework',
         'servlet-api',
         'ant:ant',
         'antlr:antlr',
         'junit:junit',
         'commons-logging',
         'commons-collections',
         'commons-net',
         'jms',
         'activation',
         'poi',
         'Jama',
         'hibernate',
         'asm',
         'cglib',
         'cglib-nodep',
         'c3p0',
         'spring-core',
         'spring-beans',
         'spring-context',
         'spring-webmvc',
         'spring',
         'cglib-nodep',
         'activation',
         'xmlParserAPIs',
         'xercesImpl',
         'xdoclet',
         'xdoclet-xdoclet-module',
         'xdoclet-hibernate-module',
         'uctask-xdoclet',
         "xercesImpl",
         "xmlParserAPIs",
         "xml-apis",
         'p6spy',
         'postgresql',
         'groovy-all',
         'ant',
         'antlr',
         'dbunit',
         'junit',
         'jep',
         'org.hibernate',
         'ehcache',
         'commons-beanutils',
         'ejb3-persistence',
         'geronimo-spec',
         'axis',
         'axis-jaxrpc',
         'jcommon',
         'commons-discovery',
         'xom'


}


    runtime("edu.ucdavis.genomics.metabolomics.binbase.connector:binbase-connector:3.4.8") {
      excludes 'sl4j-api',
              'hibernate.core',
              'hibernate-commons-annotations',
              'hibernate-entitymanager',
              'log4j',
              'logkit',
              'avalon-framework',
              'servlet-api',
              'ant:ant',
              'antlr:antlr',
              'junit:junit',
              'commons-logging',
              'commons-collections',
              'commons-net',
              'jms',
              'activation',
              'poi',
              'Jama',
              'hibernate',
              'asm',
              'cglib',
              'cglib-nodep',
              'c3p0',
              'spring-core',
              'spring-beans',
              'spring-context',
              'spring-webmvc',
              'spring',
              'cglib-nodep',
              'activation',
              'xmlParserAPIs',
              'xercesImpl',
              'xdoclet',
              'xdoclet-xdoclet-module',
              'xdoclet-hibernate-module',
              'uctask-xdoclet',
              "xercesImpl",
              "xmlParserAPIs",
              "xml-apis",
              'p6spy',
              'postgresql',
              'groovy-all',
              'ant',
              'antlr',
              'dbunit',
              'junit',
              'jep',
              'org.hibernate',
              'ehcache',
              'commons-beanutils',
              'ejb3-persistence',
              'geronimo-spec',
              'axis',
              'axis-jaxrpc',
              'jcommon',
              'commons-discovery',
              'xom'


    }


    runtime("edu.ucdavis.genomics.metabolomics.binbase.binbase-core.binbase:binbase-spring:3.4.8") {
      excludes 'sl4j-api',
              'hibernate.core',
              'hibernate-commons-annotations',
              'hibernate-entitymanager',
              'log4j',
              'logkit',
              'avalon-framework',
              'servlet-api',
              'ant:ant',
              'antlr:antlr',
              'junit:junit',
              'commons-logging',
              'commons-collections',
              'commons-net',
              'jms',
              'activation',
              'poi',
              'Jama',
              'hibernate',
              'asm',
              'cglib',
              'cglib-nodep',
              'c3p0',
              'spring-core',
              'spring-beans',
              'spring-context',
              'spring-webmvc',
              'spring',
              'cglib-nodep',
              'activation',
              'xmlParserAPIs',
              'xercesImpl',
              'xdoclet',
              'xdoclet-xdoclet-module',
              'xdoclet-hibernate-module',
              'uctask-xdoclet',
              "xercesImpl",
              "xmlParserAPIs",
              "xml-apis",
              'p6spy',
              'postgresql',
              'groovy-all',
              'ant',
              'antlr',
              'dbunit',
              'junit',
              'jep',
              'org.hibernate',
              'ehcache',
              'commons-beanutils',
              'ejb3-persistence',
              'geronimo-spec',
              'axis',
              'axis-jaxrpc',
              'jcommon',
              'commons-discovery'


    }
  }

}

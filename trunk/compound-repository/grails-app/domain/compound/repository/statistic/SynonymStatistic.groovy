package compound.repository.statistic

import compound.repository.entries.Synonym

class SynonymStatistic extends Statistic{

    static constraints = {
    }

  long calculateAmmount() {
    return Synonym.count();
  }
}

package compound.repository.entries

/**
 * contains the cas informations
 */

class Cas extends DBLinks {


  static searchable = {

    casNumber index: 'not_analyzed', name : "cas"
    root:false
  }


    def beforeInsert = {
      if (linkID == null) {
        linkID = casNumber.toString()
      }

      if(sourceName == null){
        sourceName = getClass().name
      }
    }

  
  static constraints = {
    casNumber(unique: false, nullable: false)
  }

  String casNumber

  String toString() {
    return "CAS: ${casNumber}";
  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }


  def getDefaultName() {
    return casNumber;
  }

  String getSourceName(){
    return "Cas"
  }
}

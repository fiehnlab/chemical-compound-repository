package compound.repository.entries
class PubchemSubstance extends PubChem {


    static searchable = {

        sid index: 'not_analyzed'
    }

    static constraints = {
        sid(nullable: false, unique: true)
    }
    static mapping = {
        version false // Required to avoid stale object exceptions when hibernate attempts a lock
    }
    /**
     * the internal pubchem substance id
     */
    int sid


    def beforeInsert = {

        if (linkID != null) {
            sid = Integer.parseInt(linkID)
        }

        if (linkID == null) {
            linkID = sid.toString()
        }


        if (sourceName == null) {
            sourceName = getClass().name
        }
    }


    String toString() {
        return "SID: ${sid}";
    }


    def getDefaultName() {
        return sid;
    }

    String calculateURL() {
        return "http://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid=${sid}"
    }


    String getSourceName() {
        return "Pubchem SID"
    }
}

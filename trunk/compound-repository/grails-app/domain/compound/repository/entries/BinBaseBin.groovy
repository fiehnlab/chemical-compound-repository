package compound.repository.entries

/**
 * a fiehnlab bin
 */

class BinBaseBin extends DBLinks {

  static constraints = {
  }

  int binId

  String massSpec

  int retentionIndex

  String internalName

  int unqiueMass

  int quantMass

  //from which databaase comes this bin
  String relatedDatabase

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }


    def beforeInsert = {
      if (linkID == null) {
        linkID = binId.toString()
      }

      if(sourceName == null){
        sourceName = getClass().name
      }
    }

  def getDefaultName() {
    return internalName;
  }
}

package compound.repository.entries
/**
 * inchi hash key for a compound
 */

class InchiHashKey {


  static searchable = {
    completeKey name : "inchiKey", index: 'not_analyzed'
    firstBlock index: 'not_analyzed', name : "structure"
    secondBlock index: 'not_analyzed'

  }

  static constraints = {
    completeKey(maxSize: 27, minSize: 27, nullable: false, unique: true)
    firstBlock(maxSize: 14, minSize: 14, nullable: false, unique: false)
    secondBlock(maxSize: 8, minSize: 8, nullable: false, unique: false)
    checkChar(maxSize: 1, nullable: false)
    flagChar(maxSize: 1, nullable: false)
    version(nullable: false)
    flagFixedH(nullable: false)
    flagIsotopic(nullable: false)
    flagStereo(nullable: false)
  }

  static indexes = {
    completeKeyIndex 'completeKey'
    firstBlockIndex 'firstBlock'
    secondBlockIndex 'secondBlock'
  }

  Compound compound

  String completeKey

  String firstBlock

  String secondBlock

  String checkChar

  String flagChar

  int version

  boolean flagFixedH

  boolean flagIsotopic

  boolean flagStereo

  Date dateCreated
  Date lastUpdated

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }


  String toString(){
    return completeKey
  }
}

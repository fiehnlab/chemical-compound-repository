package compound.repository.entries
class LipidMAPS extends DBLinks {


  static searchable = {

    lipidMapId index: 'not_analyzed', name: "lipidmap"
  }

  static constraints = {
    lipidMapId(maxSiz: 2000, unique: true)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }


  def beforeInsert = {
    if (linkID == null) {
      linkID = lipidMapId
    }

    if (sourceName == null) {
      sourceName = getClass().name
    }
  }


  void populate() {
    if (lipidMapId == null) {
      this.lipidMapId = this.linkID
    }
  }


  String lipidMapId


  String toString() {
    return "LipidMapId: ${lipidMapId}";
  }


  def getDefaultName() {
    return lipidMapId;
  }

  String calculateURL() {
    return "http://www.lipidmaps.org/data/LMSDRecord.php?&LMID=${lipidMapId}"
  }

}

package compound.repository.entries
class PubchemCompound extends PubChem {


  static searchable = {

    cid index: 'not_analyzed'
  }

  static constraints = {
    charge(nullable: true)
    hbondAcceptor(nullable: true)
    hbondDonor(nullable: true)
    rotableBond(nullable: true)
    experimentalLogP(nullable: true)
    subKeys(maxSize: 15000, nullable: true, unique: true)
    cid(unique: true, nullable: false)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }


    def beforeInsert = {
      if (linkID == null) {
        linkID = cid.toString()
      }

      if(sourceName == null){
        sourceName = getClass().name
      }
    }

  /**
   * the internal pubchem compound id
   */
  int cid

  int charge

  int hbondAcceptor

  int hbondDonor

  int rotableBond

  double experimentalLogP

  String subKeys

  String toString() {
    return "CID: ${cid}";
  }


  def getDefaultName() {
    return cid;
  }

  String calculateURL(){
    return "http://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?cid=${cid}"
  }


  String getSourceName(){
    return "Pubchem CID"
  }
}

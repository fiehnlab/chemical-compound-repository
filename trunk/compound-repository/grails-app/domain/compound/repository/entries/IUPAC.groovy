package compound.repository.entries
class IUPAC implements Comparable{

  static belongsTo = [compound: Compound]


  static searchable = {
    spellCheck "include"
  }
  static constraints = {
    name(maxSize: 15000, nullable: false)

  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String name


  String toString() {
    return "Name: ${name}";
  }

  Date dateCreated
  Date lastUpdated


  int compareTo(Object o) {
    return name.compareTo(o.name);
  }
}

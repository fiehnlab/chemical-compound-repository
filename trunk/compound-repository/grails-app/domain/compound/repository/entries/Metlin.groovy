package compound.repository.entries
class Metlin extends DBLinks {

  static constraints = {
  }


  static searchable = {

    metlinId index: 'not_analyzed', name : "metlin"
  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String metlinId

  String toString() {
    return "Metlin: ${metlinId}"
  }



    def beforeInsert = {
      if (linkID == null) {
        linkID = metlinId
      }

      if(sourceName == null){
        sourceName = getClass().name
      }
    }

  def getDefaultName() {
    return metlinId;
  }
}

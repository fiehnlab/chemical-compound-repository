package compound.repository.entries

/**
 * main class for database links
 */

class DBLinks implements Comparable {
  static belongsTo = [compound: Compound]

  static searchable = {
    linkID index: 'not_analyzed', name: "link"
    sourceName index: 'not_analyzed', name: "source"

  }

  String linkID

  String sourceName

  static constraints = {
    linkID(nullable: true)
    sourceName(nullable: true)

  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock

  }
  Date dateCreated
  Date lastUpdated

  def getDefaultName() {
    return "$sourceName : $linkID"
  }

  String calculateURL() {
    return null
  }


  boolean hasCalculateableURL() {
    if (calculateURL() != null) {
      return true
    }
    return false
  }

  void populate() {

  }

  String toString() {
    return getDefaultName()
  }

  int compareTo(Object o) {
    if (o != null && linkID != null) {
      return linkID.compareTo(o.linkID);
    }
    //should not happen, but does from time to time
    return -1
  }
}

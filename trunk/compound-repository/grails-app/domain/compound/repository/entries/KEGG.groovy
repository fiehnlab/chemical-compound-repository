package compound.repository.entries
/**
 * kegg informations
 */

class KEGG extends DBLinks {


  static searchable = {

    keggId index: 'not_analyzed', name: "kegg"
  }


  def beforeInsert = {
    if (linkID == null) {
      linkID = keggId
    }

    if (sourceName == null) {
      sourceName = getClass().name
    }
  }



  void populate() {
    if (keggId == null) {
      this.keggId = this.linkID
    }
  }

  static constraints = {
    keggId(maxSize: 6)
  }
  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }
  String keggId


  String toString() {
    return "KEGG: ${keggId}";
  }


  def getDefaultName() {
    return keggId;
  }

  String calculateURL() {
    return "http://www.genome.jp/dbget-bin/www_bget?${keggId}"
  }
}

package compound.repository.entries

/**
 * defines a chebi identifier
 */

class ChEBI extends DBLinks {


  static searchable = {

    chebiId index: 'not_analyzed', name: "chebi"
  }

  static constraints = {
    chebiId(unique: false, nullable: false)

  }

  String chebiId


  def beforeInsert = {
    if (linkID == null) {
      linkID = chebiId
    }

    if(sourceName == null){
      sourceName = getClass().name
    }
  }


  void populate(){
    if(chebiId == null){
      this.chebiId = this.linkID
    }
  }

  String toString() {
    return "ChEBI: ${chebiId}"
  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }


  def getDefaultName() {
    return chebiId;
  }

  String calculateURL() {
    return "http://www.ebi.ac.uk/chebi/searchId.do?chebiId=${chebiId}"
  }


  String getSourceName(){
    return "ChEBI"
  }
}

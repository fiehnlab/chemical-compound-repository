package compound.repository.entries
class HMDB extends DBLinks {

  static constraints = {
    description(maxSize: 15000, nullable: true)
  }

  static searchable = {

    hmdbId index: 'not_analyzed', name : "hmdb"
    description index: 'not_analyzed', name : "hmdb"

  }


    def beforeInsert = {
      if (linkID == null) {
        linkID = hmdbId
      }

      if(sourceName == null){
        sourceName = getClass().name
      }
    }

  String hmdbId

  String description

  String toString() {
    return "HMDB: ${hmdbId}";
  }

  static mapping = {
    version false // Required to avoid stale object exceptions when hibernate attempts a lock
  }


  def getDefaultName() {
    return hmdbId;
  }

  String calculateURL(){
    return "http://www.hmdb.ca/metabolites/${hmdbId}"
  }
}

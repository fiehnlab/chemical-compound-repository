package compound.repository.entries

class ChemSpider extends compound.repository.entries.DBLinks {

  static constraints = {
  }


  String calculateURL() {
    return "http://www.chemspider.com/Chemical-Structure.${linkID}.html"
  }
}
